#+TITLE: Manpages - zmq.7
#+DESCRIPTION: Linux manpage for zmq.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq - 0MQ lightweight messaging kernel

* SYNOPSIS
*#include <zmq.h>*

*cc* [/flags/] /files/ *-lzmq* [/libraries/]

* DESCRIPTION
The 0MQ lightweight messaging kernel is a library which extends the
standard socket interfaces with features traditionally provided by
specialised /messaging middleware/ products. 0MQ sockets provide an
abstraction of asynchronous /message queues/, multiple /messaging
patterns/, message filtering (/subscriptions/), seamless access to
multiple /transport protocols/ and more.

This documentation presents an overview of 0MQ concepts, describes how
0MQ abstracts standard sockets and provides a reference manual for the
functions provided by the 0MQ library.

** Context
The 0MQ /context/ keeps the list of sockets and manages the async I/O
thread and internal queries.

Before using any 0MQ library functions you must create a 0MQ /context/.
When you exit your application you must destroy the /context/. These
functions let you work with /contexts/:

Create a new 0MQ context

#+begin_quote
  *zmq_ctx_new*(3)
#+end_quote

Work with context properties

#+begin_quote
  *zmq_ctx_set*(3) *zmq_ctx_get*(3)
#+end_quote

Destroy a 0MQ context

#+begin_quote
  *zmq_ctx_shutdown*(3) *zmq_ctx_term*(3)
#+end_quote

\\

*Thread safety*

#+begin_quote
  A 0MQ /context/ is thread safe and may be shared among as many
  application threads as necessary, without any additional locking
  required on the part of the caller.

  Individual 0MQ /sockets/ are /not/ thread safe except in the case
  where full memory barriers are issued when migrating a socket from one
  thread to another. In practice this means applications can create a
  socket in one thread with /zmq_socket()/ and then pass it to a /newly
  created/ thread as part of thread initialisation, for example via a
  structure passed as an argument to /pthread_create()/.
#+end_quote

\\

*Multiple contexts*

#+begin_quote
  Multiple /contexts/ may coexist within a single application. Thus, an
  application can use 0MQ directly and at the same time make use of any
  number of additional libraries or components which themselves make use
  of 0MQ as long as the above guidelines regarding thread safety are
  adhered to.
#+end_quote

** Messages
A 0MQ message is a discrete unit of data passed between applications or
components of the same application. 0MQ messages have no internal
structure and from the point of view of 0MQ itself they are considered
to be opaque binary data.

The following functions are provided to work with messages:

Initialise a message

#+begin_quote
  *zmq_msg_init*(3) *zmq_msg_init_size*(3) *zmq_msg_init_buffer*(3)
  *zmq_msg_init_data*(3)
#+end_quote

Sending and receiving a message

#+begin_quote
  *zmq_msg_send*(3) *zmq_msg_recv*(3)
#+end_quote

Release a message

#+begin_quote
  *zmq_msg_close*(3)
#+end_quote

Access message content

#+begin_quote
  *zmq_msg_data*(3) *zmq_msg_size*(3) *zmq_msg_more*(3)
#+end_quote

Work with message properties

#+begin_quote
  *zmq_msg_gets*(3) *zmq_msg_get*(3) *zmq_msg_set*(3)
#+end_quote

Message manipulation

#+begin_quote
  *zmq_msg_copy*(3) *zmq_msg_move*(3)
#+end_quote

** Sockets
0MQ sockets present an abstraction of an asynchronous /message queue/,
with the exact queueing semantics depending on the socket type in use.
See *zmq_socket*(3) for the socket types provided.

The following functions are provided to work with sockets:

Creating a socket

#+begin_quote
  *zmq_socket*(3)
#+end_quote

Closing a socket

#+begin_quote
  *zmq_close*(3)
#+end_quote

Manipulating socket options

#+begin_quote
  *zmq_getsockopt*(3) *zmq_setsockopt*(3)
#+end_quote

Establishing a message flow

#+begin_quote
  *zmq_bind*(3) *zmq_connect*(3)
#+end_quote

Sending and receiving messages

#+begin_quote
  *zmq_msg_send*(3) *zmq_msg_recv*(3) *zmq_send*(3) *zmq_recv*(3)
  *zmq_send_const*(3)
#+end_quote

Monitoring socket events

#+begin_quote
  *zmq_socket_monitor*(3)
#+end_quote

*Input/output multiplexing*. 0MQ provides a mechanism for applications
to multiplex input/output events over a set containing both 0MQ sockets
and standard sockets. This mechanism mirrors the standard /poll()/
system call, and is described in detail in *zmq_poll*(3). This API is
deprecated, however.

There is a new DRAFT API with multiple zmq_poller_* function, which is
described in *zmq_poller*(3).

** Transports
A 0MQ socket can use multiple different underlying transport mechanisms.
Each transport mechanism is suited to a particular purpose and has its
own advantages and drawbacks.

The following transport mechanisms are provided:

Unicast transport using TCP

#+begin_quote
  *zmq_tcp*(7)
#+end_quote

Reliable multicast transport using PGM

#+begin_quote
  *zmq_pgm*(7)
#+end_quote

Local inter-process communication transport

#+begin_quote
  *zmq_ipc*(7)
#+end_quote

Local in-process (inter-thread) communication transport

#+begin_quote
  *zmq_inproc*(7)
#+end_quote

Virtual Machine Communications Interface (VMC) transport

#+begin_quote
  *zmq_vmci*(7)
#+end_quote

Unreliable unicast and multicast using UDP

#+begin_quote
  *zmq_udp*(7)
#+end_quote

** Proxies
0MQ provides /proxies/ to create fanout and fan-in topologies. A proxy
connects a /frontend/ socket to a /backend/ socket and switches all
messages between the two sockets, opaquely. A proxy may optionally
capture all traffic to a third socket. To start a proxy in an
application thread, use *zmq_proxy*(3).

** Security
A 0MQ socket can select a security mechanism. Both peers must use the
same security mechanism.

The following security mechanisms are provided for IPC and TCP
connections:

Null security

#+begin_quote
  *zmq_null*(7)
#+end_quote

Plain-text authentication using username and password

#+begin_quote
  *zmq_plain*(7)
#+end_quote

Elliptic curve authentication and encryption

#+begin_quote
  *zmq_curve*(7)
#+end_quote

Generate a CURVE keypair in armored text format

#+begin_quote
  *zmq_curve_keypair*(3)
#+end_quote

Derive a CURVE public key from a secret key: *zmq_curve_public*(3)

Converting keys to/from armoured text strings

#+begin_quote
  *zmq_z85_decode*(3) *zmq_z85_encode*(3)
#+end_quote

* ERROR HANDLING
The 0MQ library functions handle errors using the standard conventions
found on POSIX systems. Generally, this means that upon failure a 0MQ
library function shall return either a NULL value (if returning a
pointer) or a negative value (if returning an integer), and the actual
error code shall be stored in the /errno/ variable.

On non-POSIX systems some users may experience issues with retrieving
the correct value of the /errno/ variable. The /zmq_errno()/ function is
provided to assist in these cases; for details refer to *zmq_errno*(3).

The /zmq_strerror()/ function is provided to translate 0MQ-specific
error codes into error message strings; for details refer to
*zmq_strerror*(3).

* UTILITY
The following utility functions are provided:

Working with atomic counters

#+begin_quote
  *zmq_atomic_counter_new*(3) *zmq_atomic_counter_set*(3)
  *zmq_atomic_counter_inc*(3) *zmq_atomic_counter_dec*(3)
  *zmq_atomic_counter_value*(3) *zmq_atomic_counter_destroy*(3)
#+end_quote

* MISCELLANEOUS
The following miscellaneous functions are provided:

Report 0MQ library version

#+begin_quote
  *zmq_version*(3)
#+end_quote

* LANGUAGE BINDINGS
The 0MQ library provides interfaces suitable for calling from programs
in any language; this documentation documents those interfaces as they
would be used by C programmers. The intent is that programmers using 0MQ
from other languages shall refer to this documentation alongside any
documentation provided by the vendor of their language binding.

Language bindings (C++, Python, PHP, Ruby, Java and more) are provided
by members of the 0MQ community and pointers can be found on the 0MQ
website.

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.

* RESOURCES
Main web site: *http://www.zeromq.org/*

Report bugs to the 0MQ development mailing list:
<*zeromq-dev@lists.zeromq.org*[1]>

* COPYING
Free use of this software is granted under the terms of the GNU Lesser
General Public License (LGPL). For details see the files COPYING and
COPYING.LESSER included with the 0MQ distribution.

* NOTES
-  1. :: zeromq-dev@lists.zeromq.org

  mailto:zeromq-dev@lists.zeromq.org
