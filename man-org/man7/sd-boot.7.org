#+TITLE: Manpages - sd-boot.7
#+DESCRIPTION: Linux manpage for sd-boot.7
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about sd-boot.7 is found in manpage for: [[../systemd-boot.7][systemd-boot.7]]