#+TITLE: Manpages - th_print_long_ls.3
#+DESCRIPTION: Linux manpage for th_print_long_ls.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
th_print, th_print_long_ls - print out information about a tar file
header

* SYNOPSIS
*#include <libtar.h>*

*void th_print_long_ls(TAR **/t/*);*

*void th_print(TAR **/t/*);*

* VERSION
This man page documents version 1.2 of *libtar*.

* DESCRIPTION
The *th_print_long_ls*() function prints a line to /stdout/ which
describes the file pointed to by the current file header associated with
the /TAR/ handle /t/. The output is similar to that of "ls -l".

The *th_print*() function prints the value of each field of the current
file header associated with the /TAR/ handle /t/ to /stdout/. This is
mainly used for debugging purposes.

* SEE ALSO
*tar_open*(3), *th_read*(3)
