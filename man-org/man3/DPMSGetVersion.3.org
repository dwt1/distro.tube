#+TITLE: Manpages - DPMSGetVersion.3
#+DESCRIPTION: Linux manpage for DPMSGetVersion.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DPMSGetVersion - returns the version of the DPMS extension implemented
by the X server

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/dpms.h>
  Status DPMSGetVersion
  (


        Display *display ,


        int *major_version ,


        int *minor_version 
  );
#+end_example

* ARGUMENTS
- /display/ :: Specifies the connection to the X server

- /major_version/ :: Specifies the return location for the extension
  major version

- /minor_version/ :: Specifies the return location for the extension
  minor version

* DESCRIPTION
The /DPMSGetVersion/ function returns the version of the Display Power
Management Signaling (DPMS) extension implemented by the X server. It
returns a major and minor version number. The major version number will
be incremented for protocol incompatible changes, and the minor version
number will be incremented for small, upward compatible changes.

* RETURN VALUES
- TRUE :: The /DPMSGetVersion/ function returns TRUE when the extension
  is supported and values are returned.

- FALSE :: The /DPMSGetVersion/ function returns FALSE when the
  extension is not supported.

* SEE ALSO
*DPMSCapable*(3), *DPMSQueryExtension*(3)
