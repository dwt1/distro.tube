#+TITLE: Manpages - zmq_close.3
#+DESCRIPTION: Linux manpage for zmq_close.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_close - close 0MQ socket

* SYNOPSIS
*int zmq_close (void */*socket/*);*

* DESCRIPTION
The /zmq_close()/ function shall destroy the socket referenced by the
/socket/ argument. Any outstanding messages physically received from the
network but not yet received by the application with /zmq_recv()/ shall
be discarded. The behaviour for discarding messages sent by the
application with /zmq_send()/ but not yet physically transferred to the
network depends on the value of the /ZMQ_LINGER/ socket option for the
specified /socket/.

/zmq_close()/ must be called exactly once for each socket. If it is
never called, /zmq_ctx_term()/ will block forever. If it is called
multiple times for the same socket or if /socket/ does not point to a
socket, the behaviour is undefined.

#+begin_quote
  \\

  *Note*

  \\

  The default setting of /ZMQ_LINGER/ does not discard unsent messages;
  this behaviour may cause the application to block when calling
  /zmq_ctx_term()/. For details refer to *zmq_setsockopt*(3) and
  *zmq_ctx_term*(3).
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  This API will complete asynchronously, so not everything will be
  deallocated after it returns. See above for details about linger.
#+end_quote

* RETURN VALUE
The /zmq_close()/ function shall return zero if successful. Otherwise it
shall return -1 and set /errno/ to one of the values defined below.

* ERRORS
*ENOTSOCK*

#+begin_quote
  The provided /socket/ was NULL.
#+end_quote

* SEE ALSO
*zmq_socket*(3) *zmq_ctx_term*(3) *zmq_setsockopt*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
