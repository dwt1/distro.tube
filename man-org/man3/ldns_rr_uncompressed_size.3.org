#+TITLE: Manpages - ldns_rr_uncompressed_size.3
#+DESCRIPTION: Linux manpage for ldns_rr_uncompressed_size.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_uncompressed_size - calculates the uncompressed size of an RR

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

size_t ldns_rr_uncompressed_size(const ldns_rr *r);

* DESCRIPTION
/ldns_rr_uncompressed_size/() calculates the uncompressed size of an RR.
.br *r*: the rr to operate on .br Returns size of the rr

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/, /ldns_rr_list/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
