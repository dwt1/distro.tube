#+TITLE: Manpages - libssh2_session_set_timeout.3
#+DESCRIPTION: Linux manpage for libssh2_session_set_timeout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_session_set_timeout - set timeout for blocking functions

* SYNOPSIS
#include <libssh2.h>

#+begin_example
  void libssh2_session_set_timeout(LIBSSH2_SESSION *session, long timeout);
#+end_example

* DESCRIPTION
Set the *timeout* in milliseconds for how long a blocking the libssh2
function calls may wait until they consider the situation an error and
return LIBSSH2_ERROR_TIMEOUT.

By default or if you set the timeout to zero, libssh2 has no timeout for
blocking functions.

* RETURN VALUE
Nothing

* AVAILABILITY
Added in 1.2.9

* SEE ALSO
*libssh2_session_get_timeout(3)*
