#+TITLE: Manpages - elf_clone.3
#+DESCRIPTION: Linux manpage for elf_clone.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
elf_clone - Create a clone of an existing ELF descriptor.

#+begin_example
#+end_example

* SYNOPSIS
*#include <libelf.h>*

*Elf *elf_clone (int */filedes/*, Elf_Cmd */cmd/*);*

* DESCRIPTION
The *elf_clone*()
