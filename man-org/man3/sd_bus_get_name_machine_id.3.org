#+TITLE: Manpages - sd_bus_get_name_machine_id.3
#+DESCRIPTION: Linux manpage for sd_bus_get_name_machine_id.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_get_name_machine_id - Retrieve a bus clients machine identity

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_get_name_machine_id(sd_bus **/bus/*, const char **/name/*,
sd_id128_t **/machine/*);*

* DESCRIPTION
*sd_bus_get_name_machine_id()* retrieves the D-Bus machine identity of
the machine that the bus client identified by /name/ is running on.
Internally, it calls the *GetMachineId* method of the
*org.freedesktop.DBus.Peer* interface. The D-Bus machine identity is a
128-bit UUID. On Linux systems running systemd, this corresponds to the
contents of /etc/machine-id. On success, the machine identity is stored
in /machine/.

* RETURN VALUE
On success, this function returns a non-negative integer. On failure, it
returns a negative errno-style error code.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  An argument is invalid.
#+end_quote

*-ENOPKG*

#+begin_quote
  The bus cannot be resolved.
#+end_quote

*-ECHILD*

#+begin_quote
  The bus was created in a different process.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3)
