#+TITLE: Manpages - RAND_DRBG_reseed.3ssl
#+DESCRIPTION: Linux manpage for RAND_DRBG_reseed.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RAND_DRBG_reseed, RAND_DRBG_set_reseed_interval,
RAND_DRBG_set_reseed_time_interval, RAND_DRBG_set_reseed_defaults -
reseed a RAND_DRBG instance

* SYNOPSIS
#include <openssl/rand_drbg.h> int RAND_DRBG_reseed(RAND_DRBG *drbg,
const unsigned char *adin, size_t adinlen, int prediction_resistance);
int RAND_DRBG_set_reseed_interval(RAND_DRBG *drbg, unsigned int
interval); int RAND_DRBG_set_reseed_time_interval(RAND_DRBG *drbg,
time_t interval); int RAND_DRBG_set_reseed_defaults( unsigned int
master_reseed_interval, unsigned int slave_reseed_interval, time_t
master_reseed_time_interval, time_t slave_reseed_time_interval );

* DESCRIPTION
*RAND_DRBG_reseed()* reseeds the given *drbg*, obtaining entropy input
from its entropy source and mixing in the specified additional data
provided in the buffer *adin* of length *adinlen*. The additional data
can be omitted by setting *adin* to NULL and *adinlen* to 0. An
immediate reseeding from a live entropy source can be requested by
setting the *prediction_resistance* flag to 1. This feature is not
implemented yet, so reseeding with prediction resistance requested will
always fail.

*RAND_DRBG_set_reseed_interval()* sets the reseed interval of the
*drbg*, which is the maximum allowed number of generate requests between
consecutive reseedings. If *interval* > 0, then the *drbg* will reseed
automatically whenever the number of generate requests since its last
seeding exceeds the given reseed interval. If *interval* == 0, then this
feature is disabled.

*RAND_DRBG_set_reseed_time_interval()* sets the reseed time interval of
the *drbg*, which is the maximum allowed number of seconds between
consecutive reseedings. If *interval* > 0, then the *drbg* will reseed
automatically whenever the elapsed time since its last reseeding exceeds
the given reseed time interval. If *interval* == 0, then this feature is
disabled.

*RAND_DRBG_set_reseed_defaults()* sets the default values for the reseed
interval (*master_reseed_interval* and *slave_reseed_interval*) and the
reseed time interval (*master_reseed_time_interval* and
*slave_reseed_tme_interval*) of DRBG instances. The default values are
set independently for master DRBG instances (which don't have a parent)
and slave DRBG instances (which are chained to a parent DRBG).

* RETURN VALUES
*RAND_DRBG_reseed()*, *RAND_DRBG_set_reseed_interval()*, and
*RAND_DRBG_set_reseed_time_interval()*, return 1 on success, 0 on
failure.

* NOTES
The default OpenSSL random generator is already set up for automatic
reseeding, so in general it is not necessary to reseed it explicitly, or
to modify its reseeding thresholds.

Normally, the entropy input for seeding a DRBG is either obtained from a
trusted os entropy source or from a parent DRBG instance, which was
seeded (directly or indirectly) from a trusted os entropy source. In
exceptional cases it is possible to replace the reseeding mechanism
entirely by providing application defined callbacks using
*RAND_DRBG_set_callbacks()*.

The reseeding default values are applied only during creation of a DRBG
instance. To ensure that they are applied to the global and thread-local
DRBG instances (<master>, resp. <public> and <private>), it is necessary
to call *RAND_DRBG_set_reseed_defaults()* before creating any thread and
before calling any cryptographic routines that obtain random data
directly or indirectly.

* SEE ALSO
*RAND_DRBG_generate* (3), *RAND_DRBG_bytes* (3),
*RAND_DRBG_set_callbacks* (3). *RAND_DRBG* (7)

* HISTORY
The RAND_DRBG functions were added in OpenSSL 1.1.1.

* COPYRIGHT
Copyright 2017-2019 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
