#+TITLE: Manpages - rindex.3
#+DESCRIPTION: Linux manpage for rindex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about rindex.3 is found in manpage for: [[../man3/index.3][man3/index.3]]