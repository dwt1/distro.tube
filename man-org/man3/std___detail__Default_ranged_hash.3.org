#+TITLE: Manpages - std___detail__Default_ranged_hash.3
#+DESCRIPTION: Linux manpage for std___detail__Default_ranged_hash.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__detail::_Default_ranged_hash - Default ranged hash function H. In
principle it should be a function object composed from objects of type
H1 and H2 such that h(k, N) = h2(h1(k), N), but that would mean making
extra copies of h1 and h2. So instead we'll just use a tag to tell class
template hashtable to do that composition.

* SYNOPSIS
\\

=#include <hashtable_policy.h>=

* Detailed Description
Default ranged hash function H. In principle it should be a function
object composed from objects of type H1 and H2 such that h(k, N) =
h2(h1(k), N), but that would mean making extra copies of h1 and h2. So
instead we'll just use a tag to tell class template hashtable to do that
composition.

Definition at line *437* of file *hashtable_policy.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
