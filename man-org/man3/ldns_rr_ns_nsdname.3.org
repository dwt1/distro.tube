#+TITLE: Manpages - ldns_rr_ns_nsdname.3
#+DESCRIPTION: Linux manpage for ldns_rr_ns_nsdname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_ns_nsdname - get dname rdata field from NS RR

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_rdf* ldns_rr_ns_nsdname(const ldns_rr *r);

* DESCRIPTION
/ldns_rr_ns_nsdname/() returns the name of a LDNS_RR_TYPE_NS rr .br *r*:
the resource record .br Returns a ldns_rdf* with the name or NULL on
failure

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
