#+TITLE: Manpages - readproc.3
#+DESCRIPTION: Linux manpage for readproc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
readproc, freeproc - read information from next /proc/## entry

* SYNOPSIS
*#include <proc/readproc.h>*

*proc_t* readproc(PROCTAB **/PT/*, proc_t **/return_buf/*);*\\
*void freeproc(proc_t **/p/*);*

* DESCRIPTION
*readproc* reads the information for the next process matching the
criteria specified in /PT/ and fills them into a /proc_t/ structure. If
/return_buf/ is not NULL, it will use the struct pointed at by
/return_buf/. Otherwise it will allocate a new /proc_t/ structure and
return a pointer to it. Note that (if so specified in /PT/) readproc
always allocates memory if it fills in the /environ/ or /cmdline/ parts
of /proc_t/.

*freeproc* frees all memory allocated for the /proc_t/ struct /*p/.

The /proc_t/ structure is defined in /<proc/readproc.h>/, please look
there for a definition of all fields.

* RETURN VALUE
*readproc* returns a pointer to the next /proc_t/ or NULL if there are
no more processes left.

* SEE ALSO
*openproc*(3), *readproctab*(3), */usr/include/proc/readproc.h*,
*/proc/#pid/*,

* REPORTING BUGS
Please send bug reports to [[file:procps@freelists.org][]]
