#+TITLE: Manpages - CURLOPT_XOAUTH2_BEARER.3
#+DESCRIPTION: Linux manpage for CURLOPT_XOAUTH2_BEARER.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_XOAUTH2_BEARER - OAuth 2.0 access token

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_XOAUTH2_BEARER, char
*token);

* DESCRIPTION
Pass a char * as parameter, which should point to the null-terminated
OAuth 2.0 Bearer Access Token for use with HTTP, IMAP, POP3 and SMTP
servers that support the OAuth 2.0 Authorization Framework.

Note: For IMAP, POP3 and SMTP, the user name used to generate the Bearer
Token should be supplied via the /CURLOPT_USERNAME(3)/ option.

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
IMAP, POP3 and SMTP

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "pop3://example.com/");
    curl_easy_setopt(curl, CURLOPT_XOAUTH2_BEARER, "1ab9cb22ba269a7");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.33.0

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_MAIL_AUTH*(3), *CURLOPT_USERNAME*(3),
