#+TITLE: Manpages - archive_write_disk.3
#+DESCRIPTION: Linux manpage for archive_write_disk.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions provide a complete API for creating objects on disk from

descriptions. They are most naturally used when extracting objects from
an archive using the

interface. The general process is to read

objects from an archive, then write those objects to a

object created using the

family functions. This interface is deliberately very similar to the

interface used to write objects to a streaming archive.

Allocates and initializes a

object suitable for writing objects to disk.

Records the device and inode numbers of a file that should not be
overwritten. This is typically used to ensure that an extraction process
does not overwrite the archive from which objects are being read. This
capability is technically unnecessary but can be a significant
performance optimization in practice.

The options field consists of a bitwise OR of one or more of the
following values:

Attempt to restore Access Control Lists. By default, extended ACLs are
ignored.

Before removing a file system object prior to replacing it, clear
platform-specific file flags which might prevent its removal.

Attempt to restore file attributes (file flags). By default, file
attributes are ignored. See

or

for more information on file attributes.

Mac OS X specific. Restore metadata using

By default,

metadata is ignored.

Existing files on disk will not be overwritten. By default, existing
regular files are truncated and overwritten; existing directories will
have their permissions updated; other pre-existing objects are unlinked
and recreated from scratch.

The user and group IDs should be set on the restored file. By default,
the user and group IDs are not restored.

Full permissions (including SGID, SUID, and sticky bits) should be
restored exactly as specified, without obeying the current umask. Note
that SUID and SGID bits can only be restored if the user and group ID of
the object on disk are correct. If

is not specified, then SUID and SGID bits will only be restored if the
default user and group IDs of newly-created objects on disk happen to
match those specified in the archive entry. By default, only basic
permissions are restored, and umask is obeyed.

Extract files atomically, by first creating a unique temporary file and
then renaming it to its required destination name. This avoids a race
where an application might see a partial file (or no file) during
extraction.

Refuse to extract an absolute path. The default is to not refuse such
paths.

Refuse to extract a path that contains a

element anywhere within it. The default is to not refuse such paths.
Note that paths ending in

always cause an error, regardless of this flag.

Refuse to extract any object whose final location would be altered by a
symlink on disk. This is intended to help guard against a variety of
mischief caused by archives that (deliberately or otherwise) extract
files outside of the current directory. The default is not to perform
this check. If

Scan data for blocks of NUL bytes and try to recreate them with holes.
This results in sparse files, independent of whether the archive format
supports or uses them.

is specified together with this option, the library will remove any
intermediate symlinks it finds and return an error only if such symlink
could not be removed.

The timestamps (mtime, ctime, and atime) should be restored. By default,
they are ignored. Note that restoring of atime is not currently
supported.

Existing files on disk will be unlinked before any attempt to create
them. In some cases, this can prove to be a significant performance
improvement. By default, existing files are truncated and rewritten, but
the file is not recreated. In particular, the default behavior does not
break existing hard links.

Attempt to restore extended file attributes. By default, they are
ignored. See

or

for more information on extended file attributes.

The

objects contain both names and ids that can be used to identify users
and groups. These names and ids describe the ownership of the file
itself and also appear in ACL lists. By default, the library uses the
ids and ignores the names, but this can be overridden by registering
user and group lookup functions. To register, you must provide a lookup
function which accepts both a name and id and returns a suitable id. You
may also provide a

pointer to a private data structure and a cleanup function for that
data. The cleanup function will be invoked when the

object is destroyed.

This convenience function installs a standard set of user and group
lookup functions. These functions use

and

to convert names to ids, defaulting to the ids if the names cannot be
looked up. These functions also implement a simple memory cache to
reduce the number of calls to

and

More information about the

object and the overall design of the library can be found in the

overview. Many of these functions are also documented under

Most functions return

(zero) on success, or one of several non-zero error codes for errors.
Specific error codes include:

for operations that might succeed if retried,

for unusual conditions that do not prevent further operations, and

for serious errors that make remaining operations impossible.

returns a pointer to a newly-allocated

object.

returns a count of the number of bytes actually written, or

on error.

Detailed error codes and textual descriptions are available from the

and

functions.

The

library first appeared in

The

interface was added to

and first appeared in

The

library was written by

Directories are actually extracted in two distinct phases. Directories
are created during

but final permissions are not set until

This separation is necessary to correctly handle borderline cases such
as a non-writable directory containing files, but can cause unexpected
results. In particular, directory permissions are not fully restored
until the archive is closed. If you use

to change the current directory between calls to

or before calling

you may confuse the permission-setting logic with the result that
directory permissions are restored incorrectly.

The library attempts to create objects with filenames longer than

by creating prefixes of the full path and changing the current
directory. Currently, this logic is limited in scope; the fixup pass
does not work correctly for such objects and the symlink security check
option disables the support for very long pathnames.

Restoring the path

does create each intermediate directory. In particular, the directory

is created as well as the final object

In theory, this can be exploited to create an entire directory hierarchy
with a single request. Of course, this does not work if the

option is specified.

Implicit directories are always created obeying the current umask.
Explicit objects are created obeying the current umask unless

is specified, in which case they current umask is ignored.

SGID and SUID bits are restored only if the correct user and group could
be set. If

is not specified, then no attempt is made to set the ownership. In this
case, SGID and SUID bits are restored only if the user and group of the
final object happen to match those specified in the entry.

The

user-id and group-id lookup functions are not the defaults because

and

are sometimes too large for particular applications. The current design
allows the application author to use a more compact implementation when
appropriate.

There should be a corresponding

interface that walks a directory hierarchy and returns archive entry
objects.
