#+TITLE: Manpages - DPMSQueryExtension.3
#+DESCRIPTION: Linux manpage for DPMSQueryExtension.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DPMSQueryExtension - queries the X server to determine the availability
of the DPMS Extension

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/dpms.h>
  Bool DPMSQueryExtension
  (


        Display *display ,


        int *event_base ,


        int *error_base 
  );
#+end_example

* ARGUMENTS
- /display/ :: Specifies the connection to the X server

- /event_base/ :: Specifies the return location for the assigned base
  event

- /error_base/ :: Specifies the return location for the assigned base
  error

* DESCRIPTION
The /DPMSQueryExtension/ function queries the X server to determine the
availability of the Display Power Management Signaling (DPMS) Extension.
If the extension is available, it returns TRUE, and the /event_base/ and
/error_base/ are set to the base event and error numbers, respectively.
Otherwise, the return value is FALSE, and the values of /event_base/ and
/error_base/ are undefined.

* RETURN VALUES
- TRUE :: The /DPMSQueryExtension/ function returns TRUE if the
  extension is available, and /event_base/ and /error_base/ are set to
  the base event number and base error number for the extension,
  respectively.

- FALSE :: The /DPMSQueryExtension/ function returns FALSE if extension
  is not available, and the values of /event_base/ and /error_base/ are
  undefined.

* SEE ALSO
*DPMSGetVersion*(3)
