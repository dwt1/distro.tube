#+TITLE: Manpages - XSetWMClientMachine.3
#+DESCRIPTION: Linux manpage for XSetWMClientMachine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XSetWMClientMachine, XGetWMClientMachine - set or read a window's
WM_CLIENT_MACHINE property

* SYNTAX
void XSetWMClientMachine ( Display */display/ , Window /w/ ,
XTextProperty */text_prop/ );

Status XGetWMClientMachine ( Display */display/ , Window /w/ ,
XTextProperty */text_prop_return/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

- text_prop :: Specifies the *XTextProperty* structure to be used.

- text_prop_return :: Returns the *XTextProperty* structure.

23. Specifies the window.

* DESCRIPTION
The *XSetWMClientMachine* convenience function calls *XSetTextProperty*
to set the WM_CLIENT_MACHINE property.

The *XGetWMClientMachine* convenience function performs an
*XGetTextProperty* on the WM_CLIENT_MACHINE property. It returns a
nonzero status on success; otherwise, it returns a zero status.

* PROPERTIES
- WM_CLIENT_MACHINE :: The string name of the machine on which the
  client application is running.

* SEE ALSO
XAllocClassHint(3), XAllocIconSize(3), XAllocSizeHints(3),
XAllocWMHints(3), XSetCommand(3), XSetTransientForHint(3),
XSetTextProperty(3), XSetWMColormapWindows(3), XSetWMIconName(3),
XSetWMName(3), XSetWMProperties(3), XSetWMProtocols(3),
XStringListToTextProperty(3)\\
/Xlib - C Language X Interface/
