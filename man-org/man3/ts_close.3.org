#+TITLE: Manpages - ts_close.3
#+DESCRIPTION: Linux manpage for ts_close.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ts_close - close a touch screen input device

* SYNOPSIS
#+begin_example
  #include <tslib.h>

  int ts_close(struct tsdev *dev);
#+end_example

* DESCRIPTION
*ts_close*() closes a touch screen input device opened by *ts_open*() ,
see ts_open (3)

* RETURN VALUE
Zero is returned on success. A negative value is returned in case of an
error.

* SEE ALSO
*ts_open*(3), *ts_read*(3), *ts_config*(3), *ts.conf*(5)
