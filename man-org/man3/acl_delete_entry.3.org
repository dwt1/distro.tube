#+TITLE: Manpages - acl_delete_entry.3
#+DESCRIPTION: Linux manpage for acl_delete_entry.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function removes the ACL entry indicated by the

descriptor from the ACL pointed to by

Any existing ACL entry descriptors that refer to entries in

other than that referred to by

continue to refer to the same entries. The argument

and any other ACL entry descriptors that refer to the same ACL entry are
undefined after this function completes. Any existing ACL pointers that
refer to the ACL referred to by

continue to refer to the ACL.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

The argument

is not a valid pointer to an ACL entry.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

Derived from the FreeBSD manual pages written by

and adapted for Linux by
