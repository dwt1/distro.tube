#+TITLE: Manpages - __gnu_pbds_detail_mask_based_range_hashing.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_mask_based_range_hashing.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::mask_based_range_hashing< Size_Type > - Range
hashing policy.

* SYNOPSIS
\\

=#include <mask_based_range_hashing.hpp>=

** Protected Types
typedef Size_Type *size_type*\\

** Protected Member Functions
void *notify_resized* (size_type size)\\

size_type *range_hash* (size_type hash) const\\

void *swap* (*mask_based_range_hashing* &other)\\

* Detailed Description
** "template<typename Size_Type>
\\
class __gnu_pbds::detail::mask_based_range_hashing< Size_Type >"Range
hashing policy.

Definition at line *50* of file *mask_based_range_hashing.hpp*.

* Member Typedef Documentation
** template<typename Size_Type > typedef Size_Type
*__gnu_pbds::detail::mask_based_range_hashing*< Size_Type
>::size_type= [protected]=
Definition at line *53* of file *mask_based_range_hashing.hpp*.

* Member Function Documentation
** template<typename Size_Type > void
*__gnu_pbds::detail::mask_based_range_hashing*< Size_Type
>::notify_resized (size_type size)= [protected]=
Definition at line *83* of file *mask_based_range_hashing.hpp*.

** template<typename Size_Type > size_type
*__gnu_pbds::detail::mask_based_range_hashing*< Size_Type >::range_hash
(size_type hash) const= [inline]=, = [protected]=
Definition at line *63* of file *mask_based_range_hashing.hpp*.

** template<typename Size_Type > void
*__gnu_pbds::detail::mask_based_range_hashing*< Size_Type >::swap
(*mask_based_range_hashing*< Size_Type > & other)= [inline]=,
= [protected]=
Definition at line *56* of file *mask_based_range_hashing.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
