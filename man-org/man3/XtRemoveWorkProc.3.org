#+TITLE: Manpages - XtRemoveWorkProc.3
#+DESCRIPTION: Linux manpage for XtRemoveWorkProc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtRemoveWorkProc.3 is found in manpage for: [[../man3/XtAppAddWorkProc.3][man3/XtAppAddWorkProc.3]]