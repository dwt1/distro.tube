#+TITLE: Manpages - hwloc_obj_attr_u_hwloc_cache_attr_s.3
#+DESCRIPTION: Linux manpage for hwloc_obj_attr_u_hwloc_cache_attr_s.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_obj_attr_u::hwloc_cache_attr_s

* SYNOPSIS
\\

=#include <hwloc.h>=

** Data Fields
hwloc_uint64_t *size*\\

unsigned *depth*\\

unsigned *linesize*\\

int *associativity*\\

*hwloc_obj_cache_type_t* *type*\\

* Detailed Description
Cache-specific Object Attributes.

* Field Documentation
** int hwloc_obj_attr_u::hwloc_cache_attr_s::associativity
Ways of associativity, -1 if fully associative, 0 if unknown.

** unsigned hwloc_obj_attr_u::hwloc_cache_attr_s::depth
Depth of cache (e.g., L1, L2, ...etc.)

** unsigned hwloc_obj_attr_u::hwloc_cache_attr_s::linesize
Cache-line size in bytes. 0 if unknown.

** hwloc_uint64_t hwloc_obj_attr_u::hwloc_cache_attr_s::size
Size of cache in bytes.

** *hwloc_obj_cache_type_t* hwloc_obj_attr_u::hwloc_cache_attr_s::type
Cache type.

* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
