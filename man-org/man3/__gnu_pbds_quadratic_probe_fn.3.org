#+TITLE: Manpages - __gnu_pbds_quadratic_probe_fn.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_quadratic_probe_fn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::quadratic_probe_fn< Size_Type > - A probe sequence policy
using square increments.

* SYNOPSIS
\\

=#include <hash_policy.hpp>=

** Public Types
typedef Size_Type *size_type*\\

** Public Member Functions
void *swap* (*quadratic_probe_fn*< Size_Type > &other)\\

** Protected Member Functions
size_type *operator()* (size_type i) const\\
Returns the i-th offset from the hash value.

* Detailed Description
** "template<typename Size_Type = std::size_t>
\\
class __gnu_pbds::quadratic_probe_fn< Size_Type >"A probe sequence
policy using square increments.

Definition at line *85* of file *hash_policy.hpp*.

* Member Typedef Documentation
** template<typename Size_Type = std::size_t> typedef Size_Type
*__gnu_pbds::quadratic_probe_fn*< Size_Type >::size_type
Definition at line *88* of file *hash_policy.hpp*.

* Member Function Documentation
** template<typename Size_Type = std::size_t> size_type
*__gnu_pbds::quadratic_probe_fn*< Size_Type >::operator() (size_type i)
const= [inline]=, = [protected]=
Returns the i-th offset from the hash value.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
