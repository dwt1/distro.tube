#+TITLE: Manpages - CURLOPT_NOPROGRESS.3
#+DESCRIPTION: Linux manpage for CURLOPT_NOPROGRESS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_NOPROGRESS - switch off the progress meter

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOPROGRESS, long onoff);

* DESCRIPTION
If /onoff/ is to 1, it tells the library to shut off the progress meter
completely for requests done with this /handle/. It will also prevent
the /CURLOPT_XFERINFOFUNCTION(3)/ or /CURLOPT_PROGRESSFUNCTION(3)/ from
getting called.

* DEFAULT
1, meaning it normally runs without a progress meter.

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    /* enable progress meter */
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

    /* Perform the request */
    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK.

* SEE ALSO
*CURLOPT_XFERINFOFUNCTION*(3), *CURLOPT_PROGRESSFUNCTION*(3),
*CURLOPT_VERBOSE*(3),
