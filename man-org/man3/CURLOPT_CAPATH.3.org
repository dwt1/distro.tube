#+TITLE: Manpages - CURLOPT_CAPATH.3
#+DESCRIPTION: Linux manpage for CURLOPT_CAPATH.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CAPATH - directory holding CA certificates

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAPATH, char *capath);

* DESCRIPTION
Pass a char * to a null-terminated string naming a directory holding
multiple CA certificates to verify the peer with. If libcurl is built
against OpenSSL, the certificate directory must be prepared using the
openssl c_rehash utility. This makes sense only when used in combination
with the /CURLOPT_SSL_VERIFYPEER(3)/ option.

The /CURLOPT_CAPATH(3)/ function apparently does not work in Windows due
to some limitation in openssl.

The application does not have to keep the string around after setting
this option.

* DEFAULT
A default path detected at build time.

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    curl_easy_setopt(curl, CURLOPT_CAPATH, "/etc/cert-dir");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
This option is supported by the OpenSSL, GnuTLS and mbedTLS (since
7.56.0) backends. The NSS backend provides the option only for backward
compatibility.

* RETURN VALUE
CURLE_OK if supported; or an error such as:

CURLE_NOT_BUILT_IN - Not supported by the SSL backend

CURLE_UNKNOWN_OPTION

CURLE_OUT_OF_MEMORY

* SEE ALSO
*CURLOPT_CAINFO*(3), *CURLOPT_STDERR*(3), *CURLOPT_DEBUGFUNCTION*(3),
