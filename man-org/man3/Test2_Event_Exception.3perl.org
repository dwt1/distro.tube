#+TITLE: Manpages - Test2_Event_Exception.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Exception.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Exception - Exception event

* DESCRIPTION
An exception event will display to STDERR, and will prevent the overall
test file from passing.

* SYNOPSIS
use Test2::API qw/context/; use Test2::Event::Exception; my $ctx =
context(); my $event = $ctx->send_event(Exception, error => Stuff is
broken);

* METHODS
Inherits from Test2::Event. Also defines:

- $reason = $e->error :: The reason for the exception.

* CAVEATS
Be aware that all exceptions are stringified during construction.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
