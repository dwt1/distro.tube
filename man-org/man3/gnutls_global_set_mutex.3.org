#+TITLE: Manpages - gnutls_global_set_mutex.3
#+DESCRIPTION: Linux manpage for gnutls_global_set_mutex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_global_set_mutex - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*void gnutls_global_set_mutex(mutex_init_func */init/*,
mutex_deinit_func */deinit/*, mutex_lock_func */lock/*,
mutex_unlock_func */unlock/*);*

* ARGUMENTS
- mutex_init_func init :: mutex initialization function

- mutex_deinit_func deinit :: mutex deinitialization function

- mutex_lock_func lock :: mutex locking function

- mutex_unlock_func unlock :: mutex unlocking function

* DESCRIPTION
With this function you are allowed to override the default mutex locks
used in some parts of gnutls and dependent libraries. This function
should be used if you have complete control of your program and
libraries. Do not call this function from a library, or preferably from
any application unless really needed to. GnuTLS will use the appropriate
locks for the running system.

Note that since the move to implicit initialization of GnuTLS on library
load, calling this function will deinitialize the library, and
re-initialize it after the new locking functions are set.

This function must be called prior to any other gnutls function.

* SINCE
2.12.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
