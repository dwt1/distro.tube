#+TITLE: Manpages - std_is_placeholder.3
#+DESCRIPTION: Linux manpage for std_is_placeholder.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::is_placeholder< _Tp > - Determines if the given type _Tp is a
placeholder in a bind() expression and, if so, which placeholder it is.

* SYNOPSIS
\\

Inherits *std::integral_constant< int, 0 >*.

** Public Types
typedef *integral_constant*< int, __v > *type*\\

typedef int *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr int *value*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::is_placeholder< _Tp >"Determines if the given type _Tp is a
placeholder in a bind() expression and, if so, which placeholder it is.

C++11 [func.bind.isplace].

Definition at line *204* of file *std/functional*.

* Member Typedef Documentation
** typedef *integral_constant*<int , __v> *std::integral_constant*< int
, __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** typedef int *std::integral_constant*< int , __v
>::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** constexpr *std::integral_constant*< int , __v >::operator value_type
() const= [inline]=, = [constexpr]=, = [noexcept]=, = [inherited]=
Definition at line *62* of file *std/type_traits*.

** constexpr value_type *std::integral_constant*< int , __v
>::operator() () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *67* of file *std/type_traits*.

* Member Data Documentation
** constexpr int *std::integral_constant*< int , __v
>::value= [static]=, = [constexpr]=, = [inherited]=
Definition at line *59* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
