#+TITLE: Manpages - SDL_GetVideoInfo.3
#+DESCRIPTION: Linux manpage for SDL_GetVideoInfo.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetVideoInfo - returns a pointer to information about the video
hardware

* SYNOPSIS
*#include "SDL.h"*

*SDL_VideoInfo *SDL_GetVideoInfo*(*void*);

* DESCRIPTION
This function returns a read-only pointer to /information/ about the
video hardware. If this is called before /SDL_SetVideoMode/, the *vfmt*
member of the returned structure will contain the pixel format of the
"best" video mode.

* SEE ALSO
*SDL_SetVideoMode*, *SDL_VideoInfo*
