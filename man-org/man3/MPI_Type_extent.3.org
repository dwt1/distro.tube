#+TITLE: Manpages - MPI_Type_extent.3
#+DESCRIPTION: Linux manpage for MPI_Type_extent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Type_extent* - Returns the extent of a data type, the difference
between the upper and lower bounds of the data type -- use of this
routine is deprecated.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Type_extent(MPI_Datatype datatype, MPI_Aint *extent)
#+end_example

* Fortran Syntax
#+begin_example
  INCLUDE 'mpif.h'
  MPI_TYPE_EXTENT(DATATYPE, EXTENT, IERROR)
  	INTEGER	DATATYPE, EXTENT, IERROR
#+end_example

* INPUT PARAMETER
- datatype :: Datatype (handle).

* OUTPUT PARAMETERS
- extent :: Datatype extent (integer).

- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
Note that use of this routine is /deprecated/ as of MPI-2. Please use
MPI_Type_get_extent instead.

This deprecated routine is not available in C++.

MPI_Type_extent returns the extent of a data type, the difference
between the upper and lower bounds of the data type.

In general, if

#+begin_example
      Typemap = {(type(0), disp(0)), ..., (type(n-1), disp(n-1))}
#+end_example

then the lower bound of Typemap is defined to be

#+begin_example
                ( min(j) disp(j)                         if no entry has
    lb(Typemap)=(                                        basic type lb
                (min(j) {disp(j) such that type(j) = lb} otherwise
#+end_example

Similarly, the upper bound of Typemap is defined to be

#+begin_example
                (max(j) disp(j) + sizeof(type(j)) + e    if no entry has
    ub(Typemap)=(                                        basic type ub
                (max(j) {disp(j) such that type(j) = ub} otherwise
#+end_example

Then

#+begin_example
      extent(Typemap) = ub(Typemap) - lb(Typemap)
#+end_example

If type(i) requires alignment to a byte address that is a multiple of
k(i), then e is the least nonnegative increment needed to round
extent(Typemap) to the next multiple of max(i) k(i).

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
MPI_Type_get_extent\\
