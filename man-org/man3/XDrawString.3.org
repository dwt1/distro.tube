#+TITLE: Manpages - XDrawString.3
#+DESCRIPTION: Linux manpage for XDrawString.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XDrawString, XDrawString16 - draw text characters

* SYNTAX
int XDrawString ( Display */display/ , Drawable /d/ , GC /gc/ , int
/x/ , int /y/ , _Xconst char */string/ , int /length/ );

int XDrawString16 ( Display */display/ , Drawable /d/ , GC /gc/ , int
/x/ , int /y/ , _Xconst XChar2b */string/ , int /length/ );

* ARGUMENTS

4. Specifies the drawable.

- display :: Specifies the connection to the X server.

- gc :: Specifies the GC.

- length :: Specifies the number of characters in the string argument.

- string :: Specifies the character string.

24. \\

25. Specify the x and y coordinates, which are relative to the origin of
    the specified drawable and define the origin of the first character.

* DESCRIPTION
Each character image, as defined by the font in the GC, is treated as an
additional mask for a fill operation on the drawable. The drawable is
modified only where the font character has a bit set to 1. For fonts
defined with 2-byte matrix indexing and used with *XDrawString16*, each
byte is used as a byte2 with a byte1 of zero.

Both functions use these GC components: function, plane-mask,
fill-style, font, subwindow-mode, clip-x-origin, clip-y-origin, and
clip-mask. They also use these GC mode-dependent components: foreground,
background, tile, stipple, tile-stipple-x-origin, and
tile-stipple-y-origin.

*XDrawString* and *XDrawString16* can generate *BadDrawable*, *BadGC*,
and *BadMatch* errors.

* DIAGNOSTICS
- *BadDrawable* :: A value for a Drawable argument does not name a
  defined Window or Pixmap.

- *BadGC* :: A value for a GContext argument does not name a defined
  GContext.

- *BadMatch* :: An *InputOnly* window is used as a Drawable.

- *BadMatch* :: Some argument or pair of arguments has the correct type
  and range but fails to match in some other way required by the
  request.

* SEE ALSO
XDrawImageString(3), XDrawText(3), XLoadFont(3)\\
/Xlib - C Language X Interface/
