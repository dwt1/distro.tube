#+TITLE: Manpages - rtcGetGeometryBufferData.3embree3
#+DESCRIPTION: Linux manpage for rtcGetGeometryBufferData.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcGetGeometryBufferData - gets pointer to
    the first buffer view element
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void* rtcGetGeometryBufferData(
    RTCGeometry geometry,
    enum RTCBufferType type,
    unsigned int slot
  );
#+end_example

** DESCRIPTION
The =rtcGetGeometryBufferData= function returns a pointer to the first
element of the buffer view attached to the specified buffer type and
slot (=type= and =slot= argument) of the geometry (=geometry= argument).

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcSetGeometryBuffer], [rtcSetSharedGeometryBuffer],
[rtcSetNewGeometryBuffer]
