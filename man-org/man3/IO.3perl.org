#+TITLE: Manpages - IO.3perl
#+DESCRIPTION: Linux manpage for IO.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
IO - load various IO modules

* SYNOPSIS
use IO qw(Handle File); # loads IO modules, here IO::Handle, IO::File
use IO; # DEPRECATED

* DESCRIPTION
=IO= provides a simple mechanism to load several of the IO modules in
one go. The IO modules belonging to the core are:

IO::Handle IO::Seekable IO::File IO::Pipe IO::Socket IO::Dir IO::Select
IO::Poll

Some other IO modules don't belong to the perl core but can be loaded as
well if they have been installed from CPAN. You can discover which ones
exist with this query: <https://metacpan.org/search?q=IO%3A%3A>.

For more information on any of these modules, please see its respective
documentation.

* DEPRECATED
use IO; # loads all the modules listed below

The loaded modules are IO::Handle, IO::Seekable, IO::File, IO::Pipe,
IO::Socket, IO::Dir. You should instead explicitly import the IO modules
you want.
