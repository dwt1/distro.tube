#+TITLE: Manpages - archive_read_open.3
#+DESCRIPTION: Linux manpage for archive_read_open.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

The same as

except that the skip callback is assumed to be

Freeze the settings, open the archive, and prepare for reading entries.
This is the most generic version of this call, which accepts four
callback functions. Most clients will want to use

or

instead. The library invokes the client-provided functions to obtain raw
bytes from the archive.

Like

except that it accepts a

pointer. This function should not be used with tape drives or other
devices that require strict I/O blocking.

Like

except that it accepts a file descriptor and block size rather than a
set of function pointers. Note that the file descriptor will not be
automatically closed at end-of-archive. This function is safe for use
with tape drives or other blocked devices.

This is a deprecated synonym for

Like

except that it accepts a simple filename and a block size. A NULL
filename represents standard input. This function is safe for use with
tape drives or other blocked devices.

Like

except that it accepts a pointer and size of a block of memory
containing the archive data.

A complete description of the

and

objects can be found in the overview manual page for

The callback functions must match the following prototypes:

The open callback is invoked by

It should return

if the underlying file or data source is successfully opened. If the
open fails, it should call

to register an error code and message and return

The read callback is invoked whenever the library requires raw bytes
from the archive. The read callback should read data into a buffer, set
the

argument to point to the available data, and return a count of the
number of bytes available. The library will invoke the read callback
again only after it has consumed this data. The library imposes no
constraints on the size of the data blocks returned. On end-of-file, the
read callback should return zero. On error, the read callback should
invoke

to register an error code and message and return -1.

The skip callback is invoked when the library wants to ignore a block of
data. The return value is the number of bytes actually skipped, which
may differ from the request. If the callback cannot skip data, it should
return zero. If the skip callback is not provided (the function pointer
is

the library will invoke the read function instead and simply discard the
result. A skip callback can provide significant performance gains when
reading uncompressed archives from slow disk drives or other media that
can skip quickly.

The close callback is invoked by archive_close when the archive
processing is complete. The callback should return

on success. On failure, the callback should invoke

to register an error code and message and return

These functions return

on success, or

Detailed error codes and textual descriptions are available from the

and

functions.
