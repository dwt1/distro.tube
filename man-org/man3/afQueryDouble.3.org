#+TITLE: Manpages - afQueryDouble.3
#+DESCRIPTION: Linux manpage for afQueryDouble.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afQueryDouble.3 is found in manpage for: [[../afQuery.3][afQuery.3]]