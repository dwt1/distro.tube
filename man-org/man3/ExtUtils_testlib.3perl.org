#+TITLE: Manpages - ExtUtils_testlib.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_testlib.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::testlib - add blib/* directories to @INC

* SYNOPSIS
use ExtUtils::testlib;

* DESCRIPTION
After an extension has been built and before it is installed it may be
desirable to test it bypassing =make test=. By adding

use ExtUtils::testlib;

to a test program the intermediate directories used by =make= are added
to =@INC=.
