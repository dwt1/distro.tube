#+TITLE: Manpages - fparseln.3bsd
#+DESCRIPTION: Linux manpage for fparseln.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

function returns a pointer to the next logical line from the stream
referenced by

This string is

terminated and it is dynamically allocated on each invocation. It is the
responsibility of the caller to free the pointer.

By default, if a character is escaped, both it and the preceding escape
character will be present in the returned string. Various

alter this behaviour.

The meaning of the arguments is as follows:

The stream to read from.

If not

the length of the string is stored in the memory location to which it
points.

If not

the value of the memory location to which is pointed to, is incremented
by the number of lines actually read from the file.

Contains the escape, continuation, and comment characters. If a
character is

then processing for that character is disabled. If

all characters default to values specified below. The contents of

is as follows:

The escape character, which defaults to

is used to remove any special meaning from the next character.

The continuation character, which defaults to

is used to indicate that the next line should be concatenated with the
current one if this character is the last character on the current line
and is not escaped.

The comment character, which defaults to

if not escaped indicates the beginning of a comment that extends until
the end of the current line.

If non-zero, alter the operation of

The various flags, which may be

together, are:

Remove escape preceding an escaped comment.

Remove escape preceding an escaped continuation.

Remove escape preceding an escaped escape.

Remove escape preceding any other character.

All of the above.

Upon successful completion a pointer to the parsed line is returned;
otherwise,

is returned.

The

function uses internally

so all error conditions that apply to

apply to

In addition

may set

to

and return

if it runs out of memory.

The

function first appeared in
