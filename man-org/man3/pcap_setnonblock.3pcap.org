#+TITLE: Manpages - pcap_setnonblock.3pcap
#+DESCRIPTION: Linux manpage for pcap_setnonblock.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_setnonblock, pcap_getnonblock - set or get the state of
non-blocking mode on a capture device

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  char errbuf[PCAP_ERRBUF_SIZE];
  int pcap_setnonblock(pcap_t *p, int nonblock, char *errbuf);
  int pcap_getnonblock(pcap_t *p, char *errbuf);
#+end_example

* DESCRIPTION
*pcap_setnonblock*() puts a capture handle into ``non-blocking'' mode,
or takes it out of ``non-blocking'' mode, depending on whether the
/nonblock/ argument is non-zero or zero. It has no effect on
``savefiles''. If there is an error, *PCAP_ERROR* is returned and
/errbuf/ is filled in with an appropriate error message; otherwise, *0*
is returned.

In ``non-blocking'' mode, an attempt to read from the capture descriptor
with *pcap_dispatch*(3PCAP) and *pcap_next_ex*(3PCAP) will, if no
packets are currently available to be read, return *0* immediately
rather than blocking waiting for packets to arrive.

*pcap_loop*(3PCAP) will loop forever, consuming CPU time when no packets
are currently available; *pacp_dispatch*() should be used instead.
*pcap_next*(3PCAP) will return *NULL* if there are no packets currently
available to read; this is indistinguishable from an error, so
*pcap_next_ex*() should be used instead.

When first activated with *pcap_activate*(3PCAP) or opened with
*pcap_open_live*(3PCAP)*,* a capture handle is not in ``non-blocking
mode''; a call to *pcap_setnonblock*() is required in order to put it
into ``non-blocking'' mode.

* RETURN VALUE
*pcap_getnonblock*() returns the current ``non-blocking'' state of the
capture descriptor; it always returns *0* on ``savefiles''. If there is
an error, *PCAP_ERROR* is returned and /errbuf/ is filled in with an
appropriate error message.

/errbuf/ is assumed to be able to hold at least *PCAP_ERRBUF_SIZE*
chars.

* SEE ALSO
*pcap*(3PCAP), *pcap_next_ex*(3PCAP), *pcap_geterr*(3PCAP)
