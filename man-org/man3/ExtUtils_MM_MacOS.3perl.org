#+TITLE: Manpages - ExtUtils_MM_MacOS.3perl
#+DESCRIPTION: Linux manpage for ExtUtils_MM_MacOS.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
ExtUtils::MM_MacOS - once produced Makefiles for MacOS Classic

* SYNOPSIS
# MM_MacOS no longer contains any code. This is just a stub.

* DESCRIPTION
Once upon a time, MakeMaker could produce an approximation of a correct
Makefile on MacOS Classic (MacPerl). Due to a lack of maintainers, this
fell out of sync with the rest of MakeMaker and hadn't worked in years.
Since there's little chance of it being repaired, MacOS Classic is
fading away, and the code was icky to begin with, the code has been
deleted to make maintenance easier.

Anyone interested in resurrecting this file should pull the old version
from the MakeMaker CVS repository and contact makemaker@perl.org.
