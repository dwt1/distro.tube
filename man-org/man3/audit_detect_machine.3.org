#+TITLE: Manpages - audit_detect_machine.3
#+DESCRIPTION: Linux manpage for audit_detect_machine.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_detect_machine - Detects the current machine type

* SYNOPSIS
*#include <libaudit.h>*

int audit_detect_machine(void);

* DESCRIPTION
audit_detect_machine queries uname and converts the kernel machine
string to an enum value defined in machine_t. The machine type is needed
for any use of the audit_name_to_syscall function.

* RETURN VALUE
Returns -1 if an error occurs; otherwise, the return value is the
machine's type.

* SEE ALSO
*uname*(3), *audit_name_to_syscall*(3).

* AUTHOR
Steve Grubb
