#+TITLE: Manpages - gnutls_alert_get_name.3
#+DESCRIPTION: Linux manpage for gnutls_alert_get_name.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_alert_get_name - API function

* SYNOPSIS
*#include <gnutls/gnutls.h>*

*const char * gnutls_alert_get_name(gnutls_alert_description_t
*/alert/*);*

* ARGUMENTS
- gnutls_alert_description_t alert :: is an alert number.

* DESCRIPTION
This function will return a string that describes the given alert
number, or *NULL*. See *gnutls_alert_get()*.

* RETURNS
string corresponding to *gnutls_alert_description_t* value.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
