#+TITLE: Manpages - __gnu_debug__Safe_unordered_container.3
#+DESCRIPTION: Linux manpage for __gnu_debug__Safe_unordered_container.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_debug::_Safe_unordered_container< _Container > - Base class for
constructing a /safe/ unordered container type that tracks iterators
that reference it.

* SYNOPSIS
\\

=#include <safe_unordered_container.h>=

Inherits *__gnu_debug::_Safe_unordered_container_base*.

Inherited by *__gnu_debug::_Safe_container< unordered_map< _Key, _Tp,
std::hash< _Key >, std::equal_to< _Key >, std::allocator< std::pair<
const _Key, _Tp > > >, std::allocator< std::pair< const _Key, _Tp > >,
__gnu_debug::_Safe_unordered_container >*,
*__gnu_debug::_Safe_container< unordered_multimap< _Key, _Tp, std::hash<
_Key >, std::equal_to< _Key >, std::allocator< std::pair< const _Key,
_Tp > > >, std::allocator< std::pair< const _Key, _Tp > >,
__gnu_debug::_Safe_unordered_container >*,
*__gnu_debug::_Safe_container< unordered_multiset< _Value, std::hash<
_Value >, std::equal_to< _Value >, std::allocator< _Value > >,
std::allocator< _Value >, __gnu_debug::_Safe_unordered_container >*, and
*__gnu_debug::_Safe_container< unordered_set< _Value, std::hash< _Value
>, std::equal_to< _Value >, std::allocator< _Value > >, std::allocator<
_Value >, __gnu_debug::_Safe_unordered_container >*.

** Public Attributes
*_Safe_iterator_base* * *_M_const_iterators*\\
The list of constant iterators that reference this container.

*_Safe_iterator_base* * *_M_const_local_iterators*\\
The list of constant local iterators that reference this container.

*_Safe_iterator_base* * *_M_iterators*\\
The list of mutable iterators that reference this container.

*_Safe_iterator_base* * *_M_local_iterators*\\
The list of mutable local iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

template<typename _Predicate > void *_M_invalidate_local_if* (_Predicate
__pred)\\

void *_M_invalidate_locals* ()\\

void *_M_revalidate_singular* ()\\

void *_M_swap* (*_Safe_sequence_base* &__x) noexcept\\

void *_M_swap* (*_Safe_unordered_container_base* &__x) noexcept\\

* Detailed Description
** "template<typename _Container>
\\
class __gnu_debug::_Safe_unordered_container< _Container >"Base class
for constructing a /safe/ unordered container type that tracks iterators
that reference it.

The class template _Safe_unordered_container simplifies the construction
of /safe/ unordered containers that track the iterators that reference
the container, so that the iterators are notified of changes in the
container that may affect their operation, e.g., if the container
invalidates its iterators or is destructed. This class template may only
be used by deriving from it and passing the name of the derived class as
its template parameter via the curiously recurring template pattern. The
derived class must have =iterator= and =const_iterator= types that are
instantiations of class template _Safe_iterator for this container and
=local_iterator= and =const_local_iterator= types that are
instantiations of class template _Safe_local_iterator for this
container. Iterators will then be tracked automatically.

Definition at line *58* of file *safe_unordered_container.h*.

* Member Function Documentation
** void __gnu_debug::_Safe_unordered_container_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_all ()= [inline]=, = [protected]=
Definition at line *76* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container >::_M_invalidate_if
(_Predicate __pred)= [protected]=
Invalidates all iterators =x= that reference this container, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_unordered_container.tcc*.

** template<typename _Container > template<typename _Predicate > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_local_if (_Predicate __pred)= [protected]=
Invalidates all local iterators =x= that reference this container, are
not singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal local iterators nested in the safe ones.

Definition at line *69* of file *safe_unordered_container.tcc*.

** template<typename _Container > void
*__gnu_debug::_Safe_unordered_container*< _Container
>::_M_invalidate_locals ()= [inline]=, = [protected]=
Definition at line *67* of file *safe_unordered_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** void __gnu_debug::_Safe_unordered_container_base::_M_swap
(*_Safe_unordered_container_base* & __x)= [protected]=, = [noexcept]=,
= [inherited]=
Swap this container with the given container. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

* Member Data Documentation
** *_Safe_iterator_base**
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** *_Safe_iterator_base**
__gnu_debug::_Safe_unordered_container_base::_M_const_local_iterators= [inherited]=
The list of constant local iterators that reference this container.

Definition at line *130* of file *safe_unordered_base.h*.

** *_Safe_iterator_base**
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** *_Safe_iterator_base**
__gnu_debug::_Safe_unordered_container_base::_M_local_iterators= [inherited]=
The list of mutable local iterators that reference this container.

Definition at line *127* of file *safe_unordered_base.h*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
