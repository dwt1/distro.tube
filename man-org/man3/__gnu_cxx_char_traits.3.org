#+TITLE: Manpages - __gnu_cxx_char_traits.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_char_traits.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::char_traits< _CharT > - Base class used to implement
std::char_traits.

* SYNOPSIS
\\

=#include <char_traits.h>=

Inherited by *std::char_traits< _CharT >*.

** Public Types
typedef _CharT *char_type*\\

typedef *_Char_types*< _CharT >::int_type *int_type*\\

typedef *_Char_types*< _CharT >::off_type *off_type*\\

typedef *_Char_types*< _CharT >::*pos_type* *pos_type*\\

typedef *_Char_types*< _CharT >::state_type *state_type*\\

** Static Public Member Functions
static constexpr void *assign* (char_type &__c1, const char_type
&__c2)\\

static constexpr char_type * *assign* (char_type *__s, std::size_t __n,
char_type __a)\\

static constexpr int *compare* (const char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr char_type * *copy* (char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr int_type *eof* ()\\

static constexpr bool *eq* (const char_type &__c1, const char_type
&__c2)\\

static constexpr bool *eq_int_type* (const int_type &__c1, const
int_type &__c2)\\

static constexpr const char_type * *find* (const char_type *__s,
std::size_t __n, const char_type &__a)\\

static constexpr std::size_t *length* (const char_type *__s)\\

static constexpr bool *lt* (const char_type &__c1, const char_type
&__c2)\\

static constexpr char_type * *move* (char_type *__s1, const char_type
*__s2, std::size_t __n)\\

static constexpr int_type *not_eof* (const int_type &__c)\\

static constexpr char_type *to_char_type* (const int_type &__c)\\

static constexpr int_type *to_int_type* (const char_type &__c)\\

* Detailed Description
** "template<typename _CharT>
\\
struct __gnu_cxx::char_traits< _CharT >"Base class used to implement
std::char_traits.

*Note*

#+begin_quote
  For any given actual character type, this definition is probably
  wrong. (Most of the member functions are likely to be right, but the
  int_type and state_type typedefs, and the eof() member function, are
  likely to be wrong.) The reason this class exists is so users can
  specialize it. Classes in namespace std may not be specialized for
  fundamental types, but classes in namespace __gnu_cxx may be.
#+end_quote

See
https://gcc.gnu.org/onlinedocs/libstdc++/manual/strings.html#strings.string.character_types
for advice on how to make use of this class for /unusual/ character
types. Also, check out include/ext/pod_char_traits.h.\\

Definition at line *90* of file *char_traits.h*.

* Member Typedef Documentation
** template<typename _CharT > typedef _CharT *__gnu_cxx::char_traits*<
_CharT >::char_type
Definition at line *92* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::int_type
*__gnu_cxx::char_traits*< _CharT >::int_type
Definition at line *93* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::off_type
*__gnu_cxx::char_traits*< _CharT >::off_type
Definition at line *95* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::*pos_type*
*__gnu_cxx::char_traits*< _CharT >::*pos_type*
Definition at line *94* of file *char_traits.h*.

** template<typename _CharT > typedef *_Char_types*<_CharT>::state_type
*__gnu_cxx::char_traits*< _CharT >::state_type
Definition at line *96* of file *char_traits.h*.

* Member Function Documentation
** template<typename _CharT > static constexpr void
*__gnu_cxx::char_traits*< _CharT >::assign (char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=
Definition at line *102* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::assign (char_type *
__s, std::size_t __n, char_type __a)= [static]=, = [constexpr]=
Definition at line *223* of file *char_traits.h*.

** template<typename _CharT > constexpr int *__gnu_cxx::char_traits*<
_CharT >::compare (const char_type * __s1, const char_type * __s2,
std::size_t __n)= [static]=, = [constexpr]=
Definition at line *154* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::copy (char_type *
__s1, const char_type * __s2, std::size_t __n)= [static]=,
= [constexpr]=
Definition at line *212* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::eof ()= [inline]=, = [static]=,
= [constexpr]=
Definition at line *144* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::eq (const char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=
Definition at line *106* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::eq_int_type (const int_type & __c1,
const int_type & __c2)= [inline]=, = [static]=, = [constexpr]=
Definition at line *140* of file *char_traits.h*.

** template<typename _CharT > constexpr const *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::find (const char_type
* __s, std::size_t __n, const char_type & __a)= [static]=,
= [constexpr]=
Definition at line *178* of file *char_traits.h*.

** template<typename _CharT > constexpr std::size_t
*__gnu_cxx::char_traits*< _CharT >::length (const char_type *
__s)= [static]=, = [constexpr]=
Definition at line *167* of file *char_traits.h*.

** template<typename _CharT > static constexpr bool
*__gnu_cxx::char_traits*< _CharT >::lt (const char_type & __c1, const
char_type & __c2)= [inline]=, = [static]=, = [constexpr]=
Definition at line *110* of file *char_traits.h*.

** template<typename _CharT > constexpr *char_traits*< _CharT
>::char_type * *__gnu_cxx::char_traits*< _CharT >::move (char_type *
__s1, const char_type * __s2, std::size_t __n)= [static]=,
= [constexpr]=
Definition at line *190* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::not_eof (const int_type &
__c)= [inline]=, = [static]=, = [constexpr]=
Definition at line *148* of file *char_traits.h*.

** template<typename _CharT > static constexpr char_type
*__gnu_cxx::char_traits*< _CharT >::to_char_type (const int_type &
__c)= [inline]=, = [static]=, = [constexpr]=
Definition at line *132* of file *char_traits.h*.

** template<typename _CharT > static constexpr int_type
*__gnu_cxx::char_traits*< _CharT >::to_int_type (const char_type &
__c)= [inline]=, = [static]=, = [constexpr]=
Definition at line *136* of file *char_traits.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
