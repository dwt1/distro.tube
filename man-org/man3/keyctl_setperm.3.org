#+TITLE: Manpages - keyctl_setperm.3
#+DESCRIPTION: Linux manpage for keyctl_setperm.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_setperm - change the permissions mask on a key

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_setperm(key_serial_t key, key_perm_t perm);
#+end_example

* DESCRIPTION
*keyctl_setperm*() changes the permissions mask on a key.

A process that does not have the *SysAdmin* capability may not change
the permissions mask on a key that doesn't have the same UID as the
caller.

The caller must have *setattr* permission on a key to be able to change
its permissions mask.

The permissions mask is a bitwise-OR of the following flags:

- *KEY_xxx_VIEW* :: Grant permission to view the attributes of a key.

- *KEY_xxx_READ* :: Grant permission to read the payload of a key or to
  list a keyring.

- *KEY_xxx_WRITE* :: Grant permission to modify the payload of a key or
  to add or remove links to/from a keyring.

- *KEY_xxx_SEARCH* :: Grant permission to find a key or to search a
  keyring.

- *KEY_xxx_LINK* :: Grant permission to make links to a key.

- *KEY_xxx_SETATTR* :: Grant permission to change the ownership and
  permissions attributes of a key.

- *KEY_xxx_ALL* :: Grant all the above.

The '*xxx*' in the above should be replaced by one of:

- *POS* :: Grant the permission to a process that possesses the key (has
  it attached searchably to one of the process's keyrings).

- *USR* :: Grant the permission to a process with the same UID as the
  key.

- *GRP* :: Grant the permission to a process with the same GID as the
  key, or with a match for the key's GID amongst that process's Groups
  list.

- *OTH* :: Grant the permission to any other process.

Examples include: *KEY_POS_VIEW*, *KEY_USR_READ*, *KEY_GRP_SEARCH* and
*KEY_OTH_ALL*.

User, group and other grants are exclusive: if a process qualifies in
the 'user' category, it will not qualify in the 'groups' category; and
if a process qualifies in either 'user' or 'groups' then it will not
qualify in the 'other' category.

Possessor grants are cumulative with the grants from the 'user',
'groups' and 'other' categories.

* RETURN VALUE
On success *keyctl_setperm*() returns *0 .* On error, the value *-1*
will be returned and /errno/ will have been set to an appropriate error.

* ERRORS
- *ENOKEY* :: The specified key does not exist.

- *EKEYEXPIRED* :: The specified key has expired.

- *EKEYREVOKED* :: The specified key has been revoked.

- *EACCES* :: The named key exists, but does not grant *setattr*
  permission to the calling process.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *add_key*(2), *keyctl*(2), *request_key*(2), *keyctl*(3),
*keyrings*(7), *keyutils*(7)
