#+TITLE: Manpages - FcConfigFileInfoIterNext.3
#+DESCRIPTION: Linux manpage for FcConfigFileInfoIterNext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigFileInfoIterNext - Set the iterator to point to the next list

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigFileInfoIterNext (FcConfig */config/*,
FcConfigFileInfoIter **/iter/*);*

* DESCRIPTION
Set 'iter' to point to the next node in the config file information
list. If there is no next node, FcFalse is returned.

This function isn't MT-safe. *FcConfigReference* must be called before
using *FcConfigFileInfoIterInit* and then *FcConfigDestroy* when the
relevant values are no longer referenced.

* SINCE
version 2.12.91
