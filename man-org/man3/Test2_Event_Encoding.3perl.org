#+TITLE: Manpages - Test2_Event_Encoding.3perl
#+DESCRIPTION: Linux manpage for Test2_Event_Encoding.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::Event::Encoding - Set the encoding for the output stream

* DESCRIPTION
The encoding event is generated when a test file wants to specify the
encoding to be used when formatting its output. This event is intended
to be produced by formatter classes and used for interpreting test
names, message contents, etc.

* SYNOPSIS
use Test2::API qw/context/; use Test2::Event::Encoding; my $ctx =
context(); my $event = $ctx->send_event(Encoding, encoding => UTF-8);

* METHODS
Inherits from Test2::Event. Also defines:

- $encoding = $e->encoding :: The encoding being specified.

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
