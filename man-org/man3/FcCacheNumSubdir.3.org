#+TITLE: Manpages - FcCacheNumSubdir.3
#+DESCRIPTION: Linux manpage for FcCacheNumSubdir.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCacheNumSubdir - Return the number of subdirectories in cache.

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcCacheNumSubdir (const FcCache */cache/*);*

* DESCRIPTION
This returns the total number of subdirectories in the cache.
