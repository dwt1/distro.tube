#+TITLE: Manpages - XFontSetExtents.3
#+DESCRIPTION: Linux manpage for XFontSetExtents.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XFontSetExtents - XFontSetExtents structure

* STRUCTURES
The *XFontSetExtents* structure contains:

#+begin_example
  typedef struct {
          XRectangle max_ink_extent;      /* over all drawable characters */
          XRectangle max_logical_extent;  /* over all drawable characters */
  } XFontSetExtents;
#+end_example

The *XRectangle* structures used to return font set metrics are the
usual Xlib screen-oriented rectangles with x, y giving the upper left
corner, and width and height always positive.

The max_ink_extent member gives the maximum extent, over all drawable
characters, of the rectangles that bound the character glyph image drawn
in the foreground color, relative to a constant origin. See
*XmbTextExtents* and *XwcTextExtents* for detailed semantics.

The max_logical_extent member gives the maximum extent, over all
drawable characters, of the rectangles that specify minimum spacing to
other graphical features, relative to a constant origin. Other graphical
features drawn by the client, for example, a border surrounding the
text, should not intersect this rectangle. The max_logical_extent member
should be used to compute minimum interline spacing and the minimum area
that must be allowed in a text field to draw a given number of arbitrary
characters.

Due to context-dependent rendering, appending a given character to a
string may change the string's extent by an amount other than that
character's individual extent.

* SEE ALSO
XCreateFontSet(3), XExtentsOfFontSet(3), XFontsOfFontSet(3)\\
/Xlib - C Language X Interface/
