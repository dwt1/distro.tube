#+TITLE: Manpages - std_lognormal_distribution_param_type.3
#+DESCRIPTION: Linux manpage for std_lognormal_distribution_param_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::lognormal_distribution< _RealType >::param_type

* SYNOPSIS
\\

=#include <random.h>=

** Public Types
typedef *lognormal_distribution*< _RealType > *distribution_type*\\

** Public Member Functions
*param_type* (_RealType __m, _RealType __s=_RealType(1))\\

_RealType *m* () const\\

_RealType *s* () const\\

** Friends
bool *operator!=* (const *param_type* &__p1, const *param_type* &__p2)\\

bool *operator==* (const *param_type* &__p1, const *param_type* &__p2)\\

* Detailed Description
** "template<typename _RealType = double>
\\
struct std::lognormal_distribution< _RealType >::param_type"Parameter
type.

Definition at line *2203* of file *random.h*.

* Member Typedef Documentation
** template<typename _RealType = double> typedef
*lognormal_distribution*<_RealType> *std::lognormal_distribution*<
_RealType >::*param_type::distribution_type*
Definition at line *2205* of file *random.h*.

* Constructor & Destructor Documentation
** template<typename _RealType = double> *std::lognormal_distribution*<
_RealType >::param_type::param_type ()= [inline]=
Definition at line *2207* of file *random.h*.

** template<typename _RealType = double> *std::lognormal_distribution*<
_RealType >::param_type::param_type (_RealType __m, _RealType __s =
=_RealType(1)=)= [inline]=, = [explicit]=
Definition at line *2210* of file *random.h*.

* Member Function Documentation
** template<typename _RealType = double> _RealType
*std::lognormal_distribution*< _RealType >::param_type::m ()
const= [inline]=
Definition at line *2215* of file *random.h*.

** template<typename _RealType = double> _RealType
*std::lognormal_distribution*< _RealType >::param_type::s ()
const= [inline]=
Definition at line *2219* of file *random.h*.

* Friends And Related Function Documentation
** template<typename _RealType = double> bool operator!= (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2227* of file *random.h*.

** template<typename _RealType = double> bool operator== (const
*param_type* & __p1, const *param_type* & __p2)= [friend]=
Definition at line *2223* of file *random.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
