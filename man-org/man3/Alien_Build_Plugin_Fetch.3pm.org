#+TITLE: Manpages - Alien_Build_Plugin_Fetch.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Fetch.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Fetch - Fetch Alien::Build plugins

* VERSION
version 2.44

* SYNOPSIS
use alienfile; share { start_url http://ftp.gnu.org/gnu/make; plugin
Download; };

* DESCRIPTION
Fetch plugins retrieve single resources from the internet. The
difference between a Fetch plugin and a Download plugin is that Download
plugin may fetch several resources from the internet (usually using a
Fetch plugin), before finding the final archive. Normally you will not
need to use Fetch plugins directly but should instead use the
Alien::Build::Plugin::Download::Negotiate plugin, which will pick the
best plugins for your given URL.

- Alien::Build::Plugin::Fetch::HTTPTiny :: 

- Alien::Build::Plugin::Fetch::Local :: 

- Alien::Build::Plugin::Fetch::LWP :: 

- Alien::Build::Plugin::Fetch::NetFTP :: 

* SEE ALSO
Alien::Build, Alien::Build::Plugin

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
