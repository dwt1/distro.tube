#+TITLE: Manpages - auparse_next_record.3
#+DESCRIPTION: Linux manpage for auparse_next_record.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_next_record - move record cursor

* SYNOPSIS
*#include <auparse.h>*

int auparse_next_record(auparse_state_t *au);

* DESCRIPTION
auparse_next_record will move the internal library cursors to point to
the next record of the current event. You should not call this function
from a feed interface callback function. Doing so will deadlock the
code. In that scenario, you should check the number of records in the
current event with auparse_get_num_records and only call this if there
are more records.

* RETURN VALUE
Returns -1 if an error occurs, 0 if no more records in current event, or
1 for success.

* SEE ALSO
*auparse_next_event*(3),*auparse_get_num_records*(3).

* AUTHOR
Steve Grubb
