#+TITLE: Manpages - pcre2_jit_free_unused_memory.3
#+DESCRIPTION: Linux manpage for pcre2_jit_free_unused_memory.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  void pcre2_jit_free_unused_memory(pcre2_general_context *gcontext);
#+end_example

* DESCRIPTION
This function frees unused JIT executable memory. The argument is a
general context, for custom memory management, or NULL for standard
memory management. JIT memory allocation retains some memory in order to
improve future JIT compilation speed. In low memory conditions,
*pcre2_jit_free_unused_memory()* can be used to cause this memory to be
freed.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
