#+TITLE: Manpages - gnutls_x509_crt_set_crq_extension_by_oid.3
#+DESCRIPTION: Linux manpage for gnutls_x509_crt_set_crq_extension_by_oid.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_crt_set_crq_extension_by_oid - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*int gnutls_x509_crt_set_crq_extension_by_oid(gnutls_x509_crt_t */crt/*,
gnutls_x509_crq_t */crq/*, const char * */oid/*, unsigned */flags/*);*

* ARGUMENTS
- gnutls_x509_crt_t crt :: a certificate of type *gnutls_x509_crt_t*

- gnutls_x509_crq_t crq :: holds a certificate request

- const char * oid :: the object identifier of the OID to copy

- unsigned flags :: should be zero

* DESCRIPTION
This function will set the extension specify by /oid/ from the given
request to the certificate.

* RETURNS
On success, *GNUTLS_E_SUCCESS* (0) is returned, otherwise a negative
error value.

* SINCE
3.5.1

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
