#+TITLE: Manpages - acl_check.3
#+DESCRIPTION: Linux manpage for acl_check.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function checks the ACL referred to by the argument

for validity.

The three required entries ACL_USER_OBJ, ACL_GROUP_OBJ, and ACL_OTHER
must exist exactly once in the ACL. If the ACL contains any ACL_USER or
ACL_GROUP entries, then an ACL_MASK entry is also required. The ACL may
contain at most one ACL_MASK entry.

The user identifiers must be unique among all entries of type ACL_USER.
The group identifiers must be unique among all entries of type
ACL_GROUP.

If the ACL referred to by

is invalid,

returns a positive error code that indicates which type of error was
detected. The following symbolic error codes are defined:

The ACL contains multiple entries that have a tag type that may occur at
most once.

The ACL contains multiple ACL_USER entries with the same user ID, or
multiple ACL_GROUP entries with the same group ID.

A required entry is missing.

The ACL contains an invalid entry tag type.

The

function can be used to translate error codes to text messages.

In addition, if the pointer

is not

assigns the number of the ACL entry at which the error was detected to
the value pointed to by

Entries are numbered starting with zero, in the order in which they
would be returned by the

function.

If successful, the

function returns

if the ACL referred to by

is valid, and a positive error code if the ACL is invalid. Otherwise, a
value of

is returned and the global variable

is set to indicate the error.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

The argument

is not a valid pointer to an ACL.

This is a non-portable, Linux specific extension to the ACL manipulation
functions defined in IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned).

Written by
