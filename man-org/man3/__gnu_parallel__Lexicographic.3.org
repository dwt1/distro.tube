#+TITLE: Manpages - __gnu_parallel__Lexicographic.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__Lexicographic.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_Lexicographic< _T1, _T2, _Compare > - Compare __a pair
of types lexicographically, ascending.

* SYNOPSIS
\\

=#include <multiseq_selection.h>=

Inherits *std::binary_function< std::pair< _T1, _T2 >, std::pair< _T1,
_T2 >, bool >*.

** Public Types
typedef *std::pair*< _T1, _T2 > *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef bool *result_type*\\
=result_type= is the return type

typedef *std::pair*< _T1, _T2 > *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
*_Lexicographic* (_Compare &__comp)\\

bool *operator()* (const *std::pair*< _T1, _T2 > &__p1, const
*std::pair*< _T1, _T2 > &__p2) const\\

* Detailed Description
** "template<typename _T1, typename _T2, typename _Compare>
\\
class __gnu_parallel::_Lexicographic< _T1, _T2, _Compare >"Compare __a
pair of types lexicographically, ascending.

Definition at line *53* of file *multiseq_selection.h*.

* Member Typedef Documentation
** typedef *std::pair*< _T1, _T2 > *std::binary_function*< *std::pair*<
_T1, _T2 > , *std::pair*< _T1, _T2 > , bool
>::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef bool *std::binary_function*< *std::pair*< _T1, _T2 > ,
*std::pair*< _T1, _T2 > , bool >::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef *std::pair*< _T1, _T2 > *std::binary_function*< *std::pair*<
_T1, _T2 > , *std::pair*< _T1, _T2 > , bool
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Constructor & Destructor Documentation
** template<typename _T1 , typename _T2 , typename _Compare >
*__gnu_parallel::_Lexicographic*< _T1, _T2, _Compare >::*_Lexicographic*
(_Compare & __comp)= [inline]=
Definition at line *61* of file *multiseq_selection.h*.

* Member Function Documentation
** template<typename _T1 , typename _T2 , typename _Compare > bool
*__gnu_parallel::_Lexicographic*< _T1, _T2, _Compare >::operator()
(const *std::pair*< _T1, _T2 > & __p1, const *std::pair*< _T1, _T2 > &
__p2) const= [inline]=
Definition at line *64* of file *multiseq_selection.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
