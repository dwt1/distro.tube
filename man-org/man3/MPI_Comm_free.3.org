#+TITLE: Manpages - MPI_Comm_free.3
#+DESCRIPTION: Linux manpage for MPI_Comm_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Comm_free * - Mark a communicator object for deallocation.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Comm_free(MPI_Comm *comm)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_COMM_FREE(COMM, IERROR)
  	INTEGER	COMM, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Comm_free(comm, ierror)
  	TYPE(MPI_Comm), INTENT(INOUT) :: comm
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void Comm::Free()
#+end_example

* INPUT PARAMETER
- comm :: Communicator to be destroyed (handle).

* OUTPUT PARAMETER
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
This operation marks the communicator object for deallocation. The
handle is set to MPI_COMM_NULL. Any pending operations that use this
communicator will complete normally; the object is actually deallocated
only if there are no other active references to it. This call applies to
intracommunicators and intercommunicators. Upon actual deallocation, the
delete callback functions for all cached attributes (see Section 5.7 in
the MPI-1 Standard, "Caching") are called in arbitrary order.

* NOTES
Note that it is not defined by the MPI standard what happens if the
delete_fn callback invokes other MPI functions. In Open MPI, it is not
valid for delete_fn callbacks (or any of their children) to add or
delete attributes on the same object on which the delete_fn callback is
being invoked.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

Before the error value is returned, the current MPI error handler is
called. By default, this error handler aborts the MPI job, except for
I/O function errors. The error handler may be changed with
MPI_Comm_set_errhandler; the predefined error handler MPI_ERRORS_RETURN
may be used to cause error values to be returned. Note that MPI does not
guarantee that an MPI program can continue past an error.

* SEE ALSO
#+begin_example
  MPI_Comm_delete_attr
#+end_example
