#+TITLE: Manpages - setprogname.3bsd
#+DESCRIPTION: Linux manpage for setprogname.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setprogname.3bsd is found in manpage for: [[../man3/getprogname.3bsd][man3/getprogname.3bsd]]