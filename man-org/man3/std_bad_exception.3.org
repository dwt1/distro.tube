#+TITLE: Manpages - std_bad_exception.3
#+DESCRIPTION: Linux manpage for std_bad_exception.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::bad_exception

* SYNOPSIS
\\

Inherits *std::exception*.

** Public Member Functions
virtual const char * *what* () const noexcept\\

* Detailed Description
If an exception is thrown which is not listed in a function's exception
specification, one of these may be thrown.\\

Definition at line *50* of file *exception*.

* Constructor & Destructor Documentation
** std::bad_exception::bad_exception ()= [inline]=, = [noexcept]=
Definition at line *53* of file *exception*.

* Member Function Documentation
** virtual const char * std::bad_exception::what () const= [virtual]=,
= [noexcept]=
Returns a C-style character string describing the general cause of the
current error.\\

Reimplemented from *std::exception*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
