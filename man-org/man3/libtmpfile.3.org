#+TITLE: Manpages - libtmpfile.3
#+DESCRIPTION: Linux manpage for libtmpfile.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pm_tmpfile() - create a temporary unnamed file

* SYNOPSIS
#+begin_example
  #include <netpbm/pm.h>

  FILE *
  pm_tmpfile(void);
#+end_example

* EXAMPLE
This simple example creates a temporary file, writes 'hello world' to
it, then reads back and prints those contents.

#+begin_example
  #include <netpbm/pm.h>

  FILE * myfileP;

  myfile = pm_tmpfile();

  fprintf(myfile, 'hello world\n');

  fseek(myfileP, 0, SEEK_SET);

  fread(buffer, sizeof(buffer), 1, myfileP);

  fprintf(STDOUT, 'temp file contains '%s'\n', buffer);

  fclose(myfileP);
#+end_example

* DESCRIPTION
This library function is part of *Netpbm*(1)

*pm_tmpfile()* creates and opens an unnamed temporary file. It is
basically the same thing as the standard C library *tmpfile()* function,
except that it uses the *TMPFILE* environment variable to decide where
to create the temporary file. If *TMPFILE* is not set or is set to
something unusable (e.g. too long), *pm_tmpfile()* falls back to the
value of the standard C library symbol *P_tmpdir*, just like
*tmpfile()*.

Unlike *tmpfile()*, *pm_tmpfile()* never returns NULL. If it fails, it
issues a message to Standard Error and aborts the program, like most
libnetpbm routines do.

If you need to refer to the temporary file by name, use
*pm_make_tmpfile()* instead.

* HISTORY
*pm_tmpfile()* was introduced in Netpbm 10.20 (January 2004).
