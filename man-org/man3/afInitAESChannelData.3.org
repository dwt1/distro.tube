#+TITLE: Manpages - afInitAESChannelData.3
#+DESCRIPTION: Linux manpage for afInitAESChannelData.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about afInitAESChannelData.3 is found in manpage for: [[../afInitAESChannelDataTo.3][afInitAESChannelDataTo.3]]