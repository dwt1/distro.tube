#+TITLE: Manpages - XGetDeviceKeyMapping.3
#+DESCRIPTION: Linux manpage for XGetDeviceKeyMapping.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetDeviceKeyMapping, XChangeDeviceKeyMapping - query or change device
key mappings

* SYNOPSIS
#+begin_example
  #include <X11/extensions/XInput.h>
#+end_example

#+begin_example
  KeySym *XGetDeviceKeyMapping( Display *display,
                                XDevice *device,
                                KeyCode first_keycode,
                                int keycode_count,
                                int *keysyms_per_keycode_return);
  display
         Specifies the connection to the X server.
#+end_example

#+begin_example
  device
         Specifies the device whose key mapping is to be queried
         or modified.
#+end_example

#+begin_example
  first_keycode
         Specifies the first KeyCode to be returned.
#+end_example

#+begin_example
  keycode_count
         Specifies the number of KeyCodes to be returned or
         modified.
#+end_example

#+begin_example
  keysyms_per_keycode
         Specifies the number of KeySyms per KeyCode.
#+end_example

#+begin_example
  keysyms_per_keycode_return
         Specifies the address of a variable into which the
         number of KeySyms per KeyCode will be returned.
#+end_example

#+begin_example
  keysyms
         Specifies the address of an array of KeySyms.
#+end_example

* DESCRIPTION

#+begin_quote
  #+begin_example
    For the specified device, the XGetDeviceKeyMapping request
    returns the symbols for the specified number of KeyCodes
    starting with first_keycode. The value specified in
    first_keycode must be greater than or equal to min_keycode as
    returned by XListInputDevices, or a BadValue error results. In
    addition, the following expression must be less than or equal
    to max_keycode as returned by XListInputDevices:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    first_keycode + keycode_count - 1
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If this is not the case, a BadValue error results. The number
    of elements in the KeySyms list is:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    keycode_count * keysyms_per_keycode_return
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    KeySym number N, counting from zero, for KeyCode K has the
    following index in the list, counting from zero: (K -
    first_code) * keysyms_per_code_return + N
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The X server arbitrarily chooses the keysyms_per_keycode_return
    value to be large enough to report all requested symbols. A
    special KeySym value of NoSymbol is used to fill in unused
    elements for individual KeyCodes. To free the storage returned
    by XGetDeviceKeyMapping, use XFree.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the specified device does not support input class keys, a
    BadMatch error will result.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XGetDeviceKeyMapping can generate a BadDevice, BadMatch, or
    BadValue error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    For the specified device, the XChangeDeviceKeyMapping request
    defines the symbols for the specified number of KeyCodes
    starting with first_keycode. The symbols for KeyCodes outside
    this range remain unchanged. The number of elements in keysyms
    must be:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    num_codes * keysyms_per_keycode
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The specified first_keycode must be greater than or equal to
    min_keycode returned by XListInputDevices, or a BadValue error
    results. In addition, the following expression must be less
    than or equal to max_keycode as returned by XListInputDevices,
    or a BadValue error results:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    first_keycode + num_codes - 1
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    KeySym number N, counting from zero, for KeyCode K has the
    following index in keysyms, counting from zero:
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    (K - first_keycode) * keysyms_per_keycode + N
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    The specified keysyms_per_keycode can be chosen arbitrarily by
    the client to be large enough to hold all desired symbols. A
    special KeySym value of NoSymbol should be used to fill in
    unused elements for individual KeyCodes. It is legal for
    NoSymbol to appear in nontrailing positions of the effective
    list for a KeyCode. XChangeDeviceKeyMapping generates a
    DeviceMappingNotify event that is sent to all clients that have
    selected that type of event.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    There is no requirement that the X server interpret this
    mapping. It is merely stored for reading and writing by
    clients.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    If the specified device does not support input class keys, a
    BadMatch error results.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    XChangeDeviceKeyMapping can generate a BadDevice, BadMatch,
    BadAlloc, or BadValue error.
  #+end_example
#+end_quote

* DIAGNOSTICS

#+begin_quote
  #+begin_example
    BadDevice
           An invalid device was specified. The specified device
           does not exist or has not been opened by this client via
           XOpenInputDevice. This error may also occur if the
           specified device is the X keyboard or X pointer device.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadMatch
           This error may occur if an XGetDeviceKeyMapping or
           XChangeDeviceKeyMapping request was made specifying a
           device that has no keys.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadValue
           Some numeric value falls outside the range of values
           accepted by the request. Unless a specific range is
           specified for an argument, the full range defined by the
           arguments type is accepted. Any argument defined as a
           set of alternatives can generate this error.
  #+end_example
#+end_quote

#+begin_quote
  #+begin_example
    BadAlloc
           The server failed to allocate the requested resource or
           server memory.
  #+end_example
#+end_quote

* SEE ALSO

#+begin_quote
  #+begin_example
    XSetDeviceButtonMapping(3), XSetDeviceModifierMapping(__3_)
  #+end_example
#+end_quote
