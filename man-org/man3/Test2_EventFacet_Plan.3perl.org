#+TITLE: Manpages - Test2_EventFacet_Plan.3perl
#+DESCRIPTION: Linux manpage for Test2_EventFacet_Plan.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Test2::EventFacet::Plan - Facet for setting the plan

* DESCRIPTION
Events use this facet when they need to set the plan.

* FIELDS
- $string = $plan->{details} :: 

- $string = $plan->details() :: 

Human readable explanation for the plan being set. This is normally not
rendered by most formatters except when the =skip= field is also set.

- $positive_int = $plan->{count} :: 

- $positive_int = $plan->count() :: 

Set the number of expected assertions. This should usually be set to =0=
when =skip= or =none= are also set.

- $bool = $plan->{skip} :: 

- $bool = $plan->skip() :: 

When true the entire test should be skipped. This is usually paired with
an explanation in the =details= field, and a =control= facet that has
=terminate= set to =0=.

- $bool = $plan->{none} :: 

- $bool = $plan->none() :: 

This is mainly used by legacy Test::Builder tests which set the plan to
=no= plan, a construct that predates the much better =done_testing()=.
If you are using this in non-legacy code you may need to reconsider the
course of your life, maybe a hermitage would suite you?

* SOURCE
The source code repository for Test2 can be found at
/http://github.com/Test-More/test-more//.

* MAINTAINERS
- Chad Granum <exodist@cpan.org> :: 

* AUTHORS
- Chad Granum <exodist@cpan.org> :: 

* COPYRIGHT
Copyright 2020 Chad Granum <exodist@cpan.org>.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See /http://dev.perl.org/licenses//
