#+TITLE: Manpages - std_negate.3
#+DESCRIPTION: Linux manpage for std_negate.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::negate< _Tp > - One of the *math functors*.

* SYNOPSIS
\\

=#include <stl_function.h>=

Inherits *std::unary_function< _Tp, _Tp >*.

** Public Types
typedef _Tp *argument_type*\\
=argument_type= is the type of the argument

typedef _Tp *result_type*\\
=result_type= is the return type

** Public Member Functions
constexpr _Tp *operator()* (const _Tp &__x) const\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::negate< _Tp >"One of the *math functors*.

Definition at line *217* of file *stl_function.h*.

* Member Typedef Documentation
** typedef _Tp *std::unary_function*< _Tp , _Tp
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef _Tp *std::unary_function*< _Tp , _Tp
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Tp > constexpr _Tp *std::negate*< _Tp
>::operator() (const _Tp & __x) const= [inline]=, = [constexpr]=
Definition at line *221* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
