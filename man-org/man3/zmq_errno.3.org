#+TITLE: Manpages - zmq_errno.3
#+DESCRIPTION: Linux manpage for zmq_errno.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_errno - retrieve value of errno for the calling thread

* SYNOPSIS
*int zmq_errno (void);*

* DESCRIPTION
The /zmq_errno()/ function shall retrieve the value of the /errno/
variable for the calling thread.

The /zmq_errno()/ function is provided to assist users on non-POSIX
systems who are experiencing issues with retrieving the correct value of
/errno/ directly. Specifically, users on Win32 systems whose application
is using a different C run-time library from the C run-time library in
use by 0MQ will need to use /zmq_errno()/ for correct operation.

#+begin_quote
  \\

  *Important*

  \\

  Users not experiencing issues with retrieving the correct value of
  /errno/ should not use this function and should instead access the
  /errno/ variable directly.
#+end_quote

* RETURN VALUE
The /zmq_errno()/ function shall return the value of the /errno/
variable for the calling thread.

* ERRORS
No errors are defined.

* SEE ALSO
*zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
