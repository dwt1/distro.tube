#+TITLE: Manpages - FcDirCacheRead.3
#+DESCRIPTION: Linux manpage for FcDirCacheRead.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcDirCacheRead - read or construct a directory cache

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcCache * FcDirCacheRead (const FcChar8 */dir/*, FcBool */force/*,
FcConfig **/config/*);*

* DESCRIPTION
This returns a cache for /dir/. If /force/ is FcFalse, then an existing,
valid cache file will be used. Otherwise, a new cache will be created by
scanning the directory and that returned.
