#+TITLE: Manpages - lo_arg.3
#+DESCRIPTION: Linux manpage for lo_arg.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lo_arg - Union used to read values from incoming messages.

* SYNOPSIS
\\

** Data Fields
int32_t *i*\\

int32_t *i32*\\

int64_t *h*\\

int64_t *i64*\\

float *f*\\

float *f32*\\

double *d*\\

double *f64*\\

char *s*\\

char *S*\\

unsigned char *c*\\

uint8_t *m* [4]\\

*lo_timetag* *t*\\

struct {\\

} *blob*\\

* Detailed Description
Union used to read values from incoming messages.

Types can generally be read using argv[n]->t where n is the argument
number and t is the type character, with the exception of strings and
symbols which must be read with &argv[n]->t.

Definition at line 104 of file lo_osc_types.h.

* Field Documentation
** struct { ... } lo_arg::blob
Blob

** unsigned char lo_arg::c
Standard C, 8 bit, char.

Definition at line 127 of file lo_osc_types.h.

** double lo_arg::d
64 bit IEEE-754 double.

Definition at line 118 of file lo_osc_types.h.

** float lo_arg::f
32 bit IEEE-754 float.

Definition at line 114 of file lo_osc_types.h.

** float lo_arg::f32
32 bit IEEE-754 float.

Definition at line 116 of file lo_osc_types.h.

** double lo_arg::f64
64 bit IEEE-754 double.

Definition at line 120 of file lo_osc_types.h.

** int64_t lo_arg::h
64 bit signed integer.

Definition at line 110 of file lo_osc_types.h.

** int32_t lo_arg::i
32 bit signed integer.

Definition at line 106 of file lo_osc_types.h.

** int32_t lo_arg::i32
32 bit signed integer.

Definition at line 108 of file lo_osc_types.h.

** int64_t lo_arg::i64
64 bit signed integer.

Definition at line 112 of file lo_osc_types.h.

** uint8_t lo_arg::m[4]
A 4 byte MIDI packet.

Definition at line 129 of file lo_osc_types.h.

** char lo_arg::s
Standard C, NULL terminated string.

Definition at line 122 of file lo_osc_types.h.

** char lo_arg::S
Standard C, NULL terminated, string. Used in systems which distinguish
strings and symbols.

Definition at line 125 of file lo_osc_types.h.

** *lo_timetag* lo_arg::t
OSC TimeTag value.

Definition at line 131 of file lo_osc_types.h.

* Author
Generated automatically by Doxygen for liblo from the source code.
