#+TITLE: Manpages - pcre_copy_named_substring.3
#+DESCRIPTION: Linux manpage for pcre_copy_named_substring.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

#+begin_example
  int pcre_copy_named_substring(const pcre *code,
   const char *subject, int *ovector,
   int stringcount, const char *stringname,
   char *buffer, int buffersize);

  int pcre16_copy_named_substring(const pcre16 *code,
   PCRE_SPTR16 subject, int *ovector,
   int stringcount, PCRE_SPTR16 stringname,
   PCRE_UCHAR16 *buffer, int buffersize);

  int pcre32_copy_named_substring(const pcre32 *code,
   PCRE_SPTR32 subject, int *ovector,
   int stringcount, PCRE_SPTR32 stringname,
   PCRE_UCHAR32 *buffer, int buffersize);
#+end_example

* DESCRIPTION
This is a convenience function for extracting a captured substring,
identified by name, into a given buffer. The arguments are:

/code/ Pattern that was successfully matched /subject/ Subject that has
been successfully matched /ovector/ Offset vector that
*pcre[16|32]_exec()* used /stringcount/ Value returned by
*pcre[16|32]_exec()* /stringname/ Name of the required substring
/buffer/ Buffer to receive the string /buffersize/ Size of buffer

The yield is the length of the substring, PCRE_ERROR_NOMEMORY if the
buffer was too small, or PCRE_ERROR_NOSUBSTRING if the string name is
invalid.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
