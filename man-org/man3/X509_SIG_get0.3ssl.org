#+TITLE: Manpages - X509_SIG_get0.3ssl
#+DESCRIPTION: Linux manpage for X509_SIG_get0.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
X509_SIG_get0, X509_SIG_getm - DigestInfo functions

* SYNOPSIS
#include <openssl/x509.h> void X509_SIG_get0(const X509_SIG *sig, const
X509_ALGOR **palg, const ASN1_OCTET_STRING **pdigest); void
X509_SIG_getm(X509_SIG *sig, X509_ALGOR **palg, ASN1_OCTET_STRING
**pdigest,

* DESCRIPTION
*X509_SIG_get0()* returns pointers to the algorithm identifier and
digest value in *sig*. *X509_SIG_getm()* is identical to
*X509_SIG_get0()* except the pointers returned are not constant and can
be modified: for example to initialise them.

* RETURN VALUES
*X509_SIG_get0()* and *X509_SIG_getm()* return no values.

* SEE ALSO
*d2i_X509* (3)

* COPYRIGHT
Copyright 2002-2018 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
