#+TITLE: Manpages - Pod_Perldoc_ToTerm.3perl
#+DESCRIPTION: Linux manpage for Pod_Perldoc_ToTerm.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Pod::Perldoc::ToTerm - render Pod with terminal escapes

* SYNOPSIS
perldoc -o term Some::Modulename

* DESCRIPTION
This is a plug-in class that allows Perldoc to use Pod::Text as a
formatter class.

It supports the following options, which are explained in Pod::Text:
alt, indent, loose, quotes, sentence, width

For example:

perldoc -o term -w indent:5 Some::Modulename

* PAGER FORMATTING
Depending on the platform, and because this class emits terminal escapes
it will attempt to set the =-R= flag on your pager by injecting the flag
into your environment variable for =less= or =more=.

On Windows and DOS, this class will not modify any environment
variables.

* CAVEAT
This module may change to use a different text formatter class in the
future, and this may change what options are supported.

* SEE ALSO
Pod::Text, Pod::Text::Termcap, Pod::Perldoc

* COPYRIGHT AND DISCLAIMERS
Copyright (c) 2017 Mark Allen.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

* AUTHOR
Mark Allen =<mallen@cpan.org>=
