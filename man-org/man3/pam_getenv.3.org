#+TITLE: Manpages - pam_getenv.3
#+DESCRIPTION: Linux manpage for pam_getenv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_getenv - get a PAM environment variable

* SYNOPSIS
#+begin_example
  #include <security/pam_appl.h>
#+end_example

*const char *pam_getenv(pam_handle_t **/pamh/*, const char **/name/*);*

* DESCRIPTION
The *pam_getenv* function searches the PAM environment list as
associated with the handle /pamh/ for an item that matches the string
pointed to by /name/ and returns a pointer to the value of the
environment variable. The application is not allowed to free the data.

* RETURN VALUES
The *pam_getenv* function returns NULL on failure.

* SEE ALSO
*pam_start*(3), *pam_getenvlist*(3), *pam_putenv*(3), *pam*(8)
