#+TITLE: Manpages - XQueryBestSize.3
#+DESCRIPTION: Linux manpage for XQueryBestSize.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XQueryBestSize, XQueryBestTile, XQueryBestStipple - determine efficient
sizes

* SYNTAX
Status XQueryBestSize ( Display */display/ , int /class/ , Drawable
/which_screen/ , unsigned int /width/, unsigned int /height/ , unsigned
int */width_return/, unsigned int */height_return/ );

Status XQueryBestTile ( Display */display/ , Drawable /which_screen/ ,
unsigned int /width/, unsigned int /height/ , unsigned int
*/width_return/, unsigned int */height_return/ );

Status XQueryBestStipple ( Display */display/ , Drawable
/which_screen/ , unsigned int /width/, unsigned int /height/ , unsigned
int */width_return/, unsigned int */height_return/ );

* ARGUMENTS
- class :: Specifies the class that you are interested in. You can pass
  *TileShape*, *CursorShape*, or *StippleShape*.

- display :: Specifies the connection to the X server.

- width :: \\

- height :: Specify the width and height.

- which_screen :: Specifies any drawable on the screen.

- width_return :: \\

- height_return :: Return the width and height of the object best
  supported by the display hardware.

* DESCRIPTION
The *XQueryBestSize* function returns the best or closest size to the
specified size. For *CursorShape*, this is the largest size that can be
fully displayed on the screen specified by which_screen. For
*TileShape*, this is the size that can be tiled fastest. For
*StippleShape*, this is the size that can be stippled fastest. For
*CursorShape*, the drawable indicates the desired screen. For
*TileShape* and *StippleShape*, the drawable indicates the screen and
possibly the window class and depth. An *InputOnly* window cannot be
used as the drawable for *TileShape* or *StippleShape*, or a *BadMatch*
error results.

*XQueryBestSize* can generate *BadDrawable*, *BadMatch*, and *BadValue*
errors.

The *XQueryBestTile* function returns the best or closest size, that is,
the size that can be tiled fastest on the screen specified by
which_screen. The drawable indicates the screen and possibly the window
class and depth. If an *InputOnly* window is used as the drawable, a
*BadMatch* error results.

*XQueryBestTile* can generate *BadDrawable* and *BadMatch* errors.

The *XQueryBestStipple* function returns the best or closest size, that
is, the size that can be stippled fastest on the screen specified by
which_screen. The drawable indicates the screen and possibly the window
class and depth. If an *InputOnly* window is used as the drawable, a
*BadMatch* error results.

*XQueryBestStipple* can generate *BadDrawable* and *BadMatch* errors.

* DIAGNOSTICS
- *BadMatch* :: An *InputOnly* window is used as a Drawable.

- *BadDrawable* :: A value for a Drawable argument does not name a
  defined Window or Pixmap.

- *BadMatch* :: The values do not exist for an *InputOnly* window.

- *BadValue* :: Some numeric value falls outside the range of values
  accepted by the request. Unless a specific range is specified for an
  argument, the full range defined by the argument's type is accepted.
  Any argument defined as a set of alternatives can generate this error.

* SEE ALSO
XCreateGC(3), XSetArcMode(3), XSetClipOrigin(3), XSetFillStyle(3),
XSetFont(3), XSetLineAttributes(3), XSetState(3), XSetTile(3)\\
/Xlib - C Language X Interface/
