#+TITLE: Manpages - SDL_GetModState.3
#+DESCRIPTION: Linux manpage for SDL_GetModState.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_GetModState - Get the state of modifier keys.

* SYNOPSIS
*#include "SDL.h"*

*SDLMod SDL_GetModState*(*void*);

* DESCRIPTION
Returns the current state of the modifier keys (CTRL, ALT, etc.).

* RETURN VALUE
The return value can be an OR'd combination of the SDLMod enum.

#+begin_quote
  *SDLMod*

  #+begin_example
    typedef enum {
      KMOD_NONE  = 0x0000,
      KMOD_LSHIFT= 0x0001,
      KMOD_RSHIFT= 0x0002,
      KMOD_LCTRL = 0x0040,
      KMOD_RCTRL = 0x0080,
      KMOD_LALT  = 0x0100,
      KMOD_RALT  = 0x0200,
      KMOD_LMETA = 0x0400,
      KMOD_RMETA = 0x0800,
      KMOD_NUM   = 0x1000,
      KMOD_CAPS  = 0x2000,
      KMOD_MODE  = 0x4000,
    } SDLMod;
  #+end_example
#+end_quote

SDL also defines the following symbols for convenience:

#+begin_quote
  #+begin_example
    #define KMOD_CTRL (KMOD_LCTRL|KMOD_RCTRL)
    #define KMOD_SHIFT  (KMOD_LSHIFT|KMOD_RSHIFT)
    #define KMOD_ALT  (KMOD_LALT|KMOD_RALT)
    #define KMOD_META (KMOD_LMETA|KMOD_RMETA)
  #+end_example
#+end_quote

* SEE ALSO
*SDL_GetKeyState*
