#+TITLE: Manpages - timespecsub.3bsd
#+DESCRIPTION: Linux manpage for timespecsub.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about timespecsub.3bsd is found in manpage for: [[../man3/timeradd.3bsd][man3/timeradd.3bsd]]