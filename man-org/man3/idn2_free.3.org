#+TITLE: Manpages - idn2_free.3
#+DESCRIPTION: Linux manpage for idn2_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
idn2_free - API function

* SYNOPSIS
*#include <idn2.h>*

*void idn2_free(void * */ptr/*);*

* ARGUMENTS
- void * ptr :: pointer to deallocate

* DESCRIPTION
Call free(3) on the given pointer.

This function is typically only useful on systems where the library
malloc heap is different from the library caller malloc heap, which
happens on Windows when the library is a separate DLL.

* SEE ALSO
The full documentation for *libidn2* is maintained as a Texinfo manual.
If the *info* and *libidn2* programs are properly installed at your
site, the command

#+begin_quote
  *info libidn2*
#+end_quote

should give you access to the complete manual. As an alternative you may
obtain the manual from:

#+begin_quote
  *https://www.gnu.org/software/libidn/libidn2/manual/*
#+end_quote
