#+TITLE: Manpages - ldns_axfr_last_pkt.3
#+DESCRIPTION: Linux manpage for ldns_axfr_last_pkt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_axfr_start, ldns_axfr_next, ldns_axfr_abort, ldns_axfr_complete,
ldns_axfr_last_pkt - functions for full zone transfer

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

ldns_axfr_start();

ldns_rr* ldns_axfr_next(ldns_resolver *resolver);

void ldns_axfr_abort(ldns_resolver *resolver);

bool ldns_axfr_complete(const ldns_resolver *resolver);

ldns_pkt* ldns_axfr_last_pkt(const ldns_resolver *res);

* DESCRIPTION
/ldns_axfr_start/()

/ldns_axfr_next/() Get the next stream of RRs in a AXFR .br *resolver*:
the resolver to use. First ldns_axfr_start() must be called .br Returns
ldns_rr the next RR from the AXFR stream After you get this returned RR
(not NULL: on error), then check if ldns_axfr_complete() is true to see
if the zone transfer has completed.

/ldns_axfr_abort/() Abort a transfer that is in progress .br *resolver*:
the resolver that is used

/ldns_axfr_complete/() Returns true if the axfr transfer has completed
(i.e. 2 SOA RRs and no errors were encountered .br *resolver*: the
resolver that is used .br Returns bool true if axfr transfer was
completed without error

/ldns_axfr_last_pkt/() Returns a pointer to the last ldns_pkt that was
sent by the server in the AXFR transfer uasable for instance to get the
error code on failure .br *res*: the resolver that was used in the axfr
transfer .br Returns ldns_pkt the last packet sent

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
*perldoc Net::DNS*, *RFC1034*, *RFC1035*, *RFC4033*, *RFC4034* and
*RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
