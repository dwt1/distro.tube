#+TITLE: Manpages - rexec_af.3
#+DESCRIPTION: Linux manpage for rexec_af.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about rexec_af.3 is found in manpage for: [[../man3/rexec.3][man3/rexec.3]]