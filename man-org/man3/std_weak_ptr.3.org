#+TITLE: Manpages - std_weak_ptr.3
#+DESCRIPTION: Linux manpage for std_weak_ptr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::weak_ptr< _Tp > - A non-owning observer for a pointer owned by a
shared_ptr.

* SYNOPSIS
\\

=#include <shared_ptr.h>=

Inherits std::__weak_ptr< _Tp, _Lp >.

** Public Types
using *element_type* = typename *remove_extent*< _Tp >::type\\

** Public Member Functions
template<typename _Yp , typename = _Constructible<const
shared_ptr<_Yp>&>> *weak_ptr* (const *shared_ptr*< _Yp > &__r)
noexcept\\

*weak_ptr* (const *weak_ptr* &) noexcept=default\\

template<typename _Yp , typename = _Constructible<const weak_ptr<_Yp>&>>
*weak_ptr* (const *weak_ptr*< _Yp > &__r) noexcept\\

*weak_ptr* (*weak_ptr* &&) noexcept=default\\

template<typename _Yp , typename = _Constructible<weak_ptr<_Yp>>>
*weak_ptr* (*weak_ptr*< _Yp > &&__r) noexcept\\

bool *expired* () const noexcept\\

*shared_ptr*< _Tp > *lock* () const noexcept\\

template<typename _Yp > _Assignable< const *shared_ptr*< _Yp > & >
*operator=* (const *shared_ptr*< _Yp > &__r) noexcept\\

*weak_ptr* & *operator=* (const *weak_ptr* &__r) noexcept=default\\

template<typename _Yp > _Assignable< const *weak_ptr*< _Yp > & >
*operator=* (const *weak_ptr*< _Yp > &__r) noexcept\\

*weak_ptr* & *operator=* (*weak_ptr* &&__r) noexcept=default\\

template<typename _Yp > _Assignable< *weak_ptr*< _Yp > > *operator=*
(*weak_ptr*< _Yp > &&__r) noexcept\\

template<typename _Tp1 > bool *owner_before* (const __shared_ptr< _Tp1,
_Lp > &__rhs) const noexcept\\

template<typename _Tp1 > bool *owner_before* (const __weak_ptr< _Tp1,
_Lp > &__rhs) const noexcept\\

void *reset* () noexcept\\

void *swap* (__weak_ptr &__s) noexcept\\

long *use_count* () const noexcept\\

** Related Functions
(Note that these are not member functions.)

template<typename _Tp > void *swap* (*weak_ptr*< _Tp > &__a, *weak_ptr*<
_Tp > &__b) noexcept\\
Swap overload for weak_ptr.

* Detailed Description
** "template<typename _Tp>
\\
class std::weak_ptr< _Tp >"A non-owning observer for a pointer owned by
a shared_ptr.

A weak_ptr provides a safe alternative to a raw pointer when you want a
non-owning reference to an object that is managed by a shared_ptr.

Unlike a raw pointer, a weak_ptr can be converted to a new shared_ptr
that shares ownership with every other shared_ptr that already owns the
pointer. In other words you can upgrade from a non-owning 'weak'
reference to an owning shared_ptr, without having access to any of the
existing shared_ptr objects.

Also unlike a raw pointer, a weak_ptr does not become 'dangling' after
the object it points to has been destroyed. Instead, a weak_ptr becomes
/expired/ and can no longer be converted to a shared_ptr that owns the
freed pointer, so you cannot accidentally access the pointed-to object
after it has been destroyed.

Definition at line *686* of file *bits/shared_ptr.h*.

* Member Typedef Documentation
** template<typename _Tp , _Lock_policy _Lp> using std::__weak_ptr< _Tp,
_Lp >::element_type = typename *remove_extent*<_Tp>::type= [inherited]=
Definition at line *1597* of file *shared_ptr_base.h*.

* Constructor & Destructor Documentation
** template<typename _Tp > template<typename _Yp , typename =
_Constructible<const shared_ptr<_Yp>&>> *std::weak_ptr*< _Tp
>::*weak_ptr* (const *shared_ptr*< _Yp > & __r)= [inline]=,
= [noexcept]=
Definition at line *703* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<const weak_ptr<_Yp>&>> *std::weak_ptr*< _Tp >::*weak_ptr*
(const *weak_ptr*< _Yp > & __r)= [inline]=, = [noexcept]=
Definition at line *709* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp , typename =
_Constructible<weak_ptr<_Yp>>> *std::weak_ptr*< _Tp >::*weak_ptr*
(*weak_ptr*< _Yp > && __r)= [inline]=, = [noexcept]=
Definition at line *715* of file *bits/shared_ptr.h*.

* Member Function Documentation
** template<typename _Tp , _Lock_policy _Lp> bool std::__weak_ptr< _Tp,
_Lp >::expired () const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1689* of file *shared_ptr_base.h*.

** template<typename _Tp > *shared_ptr*< _Tp > *std::weak_ptr*< _Tp
>::lock () const= [inline]=, = [noexcept]=
Definition at line *749* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp > _Assignable< const
*shared_ptr*< _Yp > & > *std::weak_ptr*< _Tp >::operator= (const
*shared_ptr*< _Yp > & __r)= [inline]=, = [noexcept]=
Definition at line *731* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp > _Assignable< const
*weak_ptr*< _Yp > & > *std::weak_ptr*< _Tp >::operator= (const
*weak_ptr*< _Yp > & __r)= [inline]=, = [noexcept]=
Definition at line *723* of file *bits/shared_ptr.h*.

** template<typename _Tp > template<typename _Yp > _Assignable<
*weak_ptr*< _Yp > > *std::weak_ptr*< _Tp >::operator= (*weak_ptr*< _Yp >
&& __r)= [inline]=, = [noexcept]=
Definition at line *742* of file *bits/shared_ptr.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Tp1 >
bool std::__weak_ptr< _Tp, _Lp >::owner_before (const __shared_ptr<
_Tp1, _Lp > & __rhs) const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1694* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> template<typename _Tp1 >
bool std::__weak_ptr< _Tp, _Lp >::owner_before (const __weak_ptr< _Tp1,
_Lp > & __rhs) const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1699* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> void std::__weak_ptr< _Tp,
_Lp >::reset ()= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1703* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> void std::__weak_ptr< _Tp,
_Lp >::swap (__weak_ptr< _Tp, _Lp > & __s)= [inline]=, = [noexcept]=,
= [inherited]=
Definition at line *1707* of file *shared_ptr_base.h*.

** template<typename _Tp , _Lock_policy _Lp> long std::__weak_ptr< _Tp,
_Lp >::use_count () const= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *1685* of file *shared_ptr_base.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
