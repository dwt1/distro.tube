#+TITLE: Manpages - acl_set_file.3
#+DESCRIPTION: Linux manpage for acl_set_file.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Linux Access Control Lists library (libacl, -lacl).

The

function associates an access ACL with a file or directory, or
associates a default ACL with a directory. The pathname for the file or
directory is pointed to by the argument

The effective user ID of the process must match the owner of the file or
directory or the process must have the CAP_FOWNER capability for the
request to succeed.

The value of the argument

is used to indicate whether the access ACL or the default ACL associated
with

is being set. If the

parameter is ACL_TYPE_ACCESS, the access ACL of

shall be set. If the

parameter is ACL_TYPE_DEFAULT, the default ACL of

shall be set. If the argument

specifies a type of ACL that cannot be associated with

then the function fails.

The

parameter must reference a valid ACL according to the rules described on
the

manual page if the

parameter is ACL_TYPE_ACCESS, and must either reference a valid ACL or
an ACL with zero ACL entries if the

parameter is ACL_TYPE_DEFAULT. If the

parameter references an empty ACL, then the

function removes any default ACL associated with the directory referred
to by the

parameter.

If any of the following conditions occur, the

function returns

and sets

to the corresponding value:

Search permission is denied for a component of the path prefix or the
object exists and the process does not have appropriate access rights.

Argument

specifies a type of ACL that cannot be associated with

The argument

does not point to a valid ACL.

The ACL has more entries than the file referred to by

can obtain.

The

parameter is not ACL_TYPE_ACCESS or ACL_TYPE_DEFAULT.

The

parameter is ACL_TYPE_DEFAULT, but the file referred to by

is not a directory.

The length of the argument

is too long.

The named object does not exist or the argument

points to an empty string.

The directory or file system that would contain the new ACL cannot be
extended or the file system is out of file allocation resources.

A component of the path prefix is not a directory.

The file identified by

cannot be associated with the ACL because the file system on which the
file is located does not support this.

The process does not have appropriate privilege to perform the operation
to set the ACL.

This function requires modification of a file system which is currently
read-only.

IEEE Std 1003.1e draft 17 (“POSIX.1e”, abandoned)

The behavior of

when the

parameter refers to an empty ACL and the

parameter is ACL_TYPE_DEFAULT is an extension in the Linux
implementation, in order that all values returned by

can be passed to

The POSIX.1e function for removing a default ACL is

Derived from the FreeBSD manual pages written by

and adapted for Linux by
