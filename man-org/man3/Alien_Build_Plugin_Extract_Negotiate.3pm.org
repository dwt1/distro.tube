#+TITLE: Manpages - Alien_Build_Plugin_Extract_Negotiate.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Extract_Negotiate.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Extract::Negotiate - Extraction negotiation plugin

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Extract => ( format => tar.gz, );

* DESCRIPTION
This is a negotiator plugin for extracting packages downloaded from the
internet. This plugin picks the best Extract plugin to do the actual
work. Which plugins are picked depend on the properties you specify,
your platform and environment. It is usually preferable to use a
negotiator plugin rather than using a specific Extract Plugin from your
alienfile.

* PROPERTIES
** format
The expected format for the download. Possible values include: =tar=,
=tar.gz=, =tar.bz2=, =tar.xz=, =zip=, =d=.

* METHODS
** pick
my $name = Alien::Build::Plugin::Extract::Negotiate->pick($format);

Returns the name of the best plugin for the given format.

* SEE ALSO
Alien::Build, alienfile, Alien::Build::MM, Alien

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
