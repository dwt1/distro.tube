#+TITLE: Manpages - FcWeightToOpenType.3
#+DESCRIPTION: Linux manpage for FcWeightToOpenType.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcWeightToOpenType - Convert from fontconfig weight values to OpenType
ones

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

int FcWeightToOpenType (int/ot_weight/*);*

* DESCRIPTION
*FcWeightToOpenType* is like *FcWeightToOpenTypeDouble* but with integer
arguments. Use the other function instead.

* SINCE
version 2.11.91
