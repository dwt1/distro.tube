#+TITLE: Manpages - FcUtf8Len.3
#+DESCRIPTION: Linux manpage for FcUtf8Len.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcUtf8Len - count UTF-8 encoded chars

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcUtf8Len (FcChar8 */src/*, int */len/*, int **/nchar/*, int
**/wchar/*);*

* DESCRIPTION
Counts the number of Unicode chars in /len/ bytes of /src/. Places that
count in /nchar/. /wchar/ contains 1, 2 or 4 depending on the number of
bytes needed to hold the largest Unicode char counted. The return value
indicates whether /src/ is a well-formed UTF8 string.
