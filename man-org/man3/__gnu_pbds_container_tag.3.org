#+TITLE: Manpages - __gnu_pbds_container_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_container_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::container_tag - Base data structure tag.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherited by *__gnu_pbds::associative_tag*,
*__gnu_pbds::priority_queue_tag*, and *__gnu_pbds::sequence_tag*.

* Detailed Description
Base data structure tag.

Definition at line *125* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
