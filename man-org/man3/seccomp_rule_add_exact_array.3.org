#+TITLE: Manpages - seccomp_rule_add_exact_array.3
#+DESCRIPTION: Linux manpage for seccomp_rule_add_exact_array.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about seccomp_rule_add_exact_array.3 is found in manpage for: [[../man3/seccomp_rule_add.3][man3/seccomp_rule_add.3]]