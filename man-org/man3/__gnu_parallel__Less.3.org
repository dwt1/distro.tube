#+TITLE: Manpages - __gnu_parallel__Less.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__Less.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_Less< _T1, _T2 > - Similar to std::less, but allows two
different types.

* SYNOPSIS
\\

=#include <base.h>=

Inherits *std::binary_function< _T1, _T2, bool >*.

** Public Types
typedef _T1 *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef bool *result_type*\\
=result_type= is the return type

typedef _T2 *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
bool *operator()* (const _T1 &__t1, const _T2 &__t2) const\\

bool *operator()* (const _T2 &__t2, const _T1 &__t1) const\\

* Detailed Description
** "template<typename _T1, typename _T2>
\\
struct __gnu_parallel::_Less< _T1, _T2 >"Similar to std::less, but
allows two different types.

Definition at line *252* of file *base.h*.

* Member Typedef Documentation
** typedef _T1 *std::binary_function*< _T1 , _T2 , bool
>::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef bool *std::binary_function*< _T1 , _T2 , bool
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _T2 *std::binary_function*< _T1 , _T2 , bool
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Member Function Documentation
** template<typename _T1 , typename _T2 > bool *__gnu_parallel::_Less*<
_T1, _T2 >::operator() (const _T1 & __t1, const _T2 & __t2)
const= [inline]=
Definition at line *255* of file *base.h*.

** template<typename _T1 , typename _T2 > bool *__gnu_parallel::_Less*<
_T1, _T2 >::operator() (const _T2 & __t2, const _T1 & __t1)
const= [inline]=
Definition at line *259* of file *base.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
