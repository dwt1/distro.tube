#+TITLE: Manpages - pcre2_set_substitute_callout.3
#+DESCRIPTION: Linux manpage for pcre2_set_substitute_callout.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE2 - Perl-compatible regular expressions (revised API)

* SYNOPSIS
*#include <pcre2.h>*

#+begin_example
  int pcre2_set_substitute_callout(pcre2_match_context *mcontext,
   int (*callout_function)(pcre2_substitute_callout_block *),
   void *callout_data);
#+end_example

* DESCRIPTION
This function sets the substitute callout fields in a match context (the
first argument). The second argument specifies a callout function, and
the third argument is an opaque data item that is passed to it. The
result of this function is always zero.

There is a complete description of the PCRE2 native API in the
*pcre2api* page and a description of the POSIX API in the *pcre2posix*
page.
