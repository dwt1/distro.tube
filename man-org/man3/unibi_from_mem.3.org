#+TITLE: Manpages - unibi_from_mem.3
#+DESCRIPTION: Linux manpage for unibi_from_mem.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_from_mem - construct a terminal object from a compiled terminfo
entry

* SYNOPSIS
#include <unibilium.h> unibi_term *unibi_from_mem(const char *p, size_t
n);

* DESCRIPTION
This function parses a compiled terminfo entry that starts at /p/ and is
/n/ bytes long, and constructs a =unibi_term= object from it. When
you're done with this object, you should call =unibi_destroy= to free
it.

=unibi_from_mem= parses both the traditional ncurses terminfo format
(starting with the bytes =1A 01=) and the newer wide integer format
(starting with the bytes =1E 02=).

* RETURN VALUE
A pointer to a new =unibi_term=. In case of failure, =NULL= is returned
and =errno= is set.

* ERRORS
- "EINVAL" :: The bytes /p/ points at don't look like a valid terminfo
  entry.

- "EFAULT" :: /n/ is too small for a valid terminfo entry.

* SEE ALSO
*unibilium.h* (3), *unibi_dump* (3), *unibi_destroy* (3),
*unibi_from_fp* (3), *unibi_from_fd* (3), *unibi_from_file* (3),
*unibi_from_term* (3), *unibi_from_env* (3)
