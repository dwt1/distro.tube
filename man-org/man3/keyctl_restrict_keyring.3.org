#+TITLE: Manpages - keyctl_restrict_keyring.3
#+DESCRIPTION: Linux manpage for keyctl_restrict_keyring.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
keyctl_restrict_keyring - restrict keys that may be linked to a keyring

* SYNOPSIS
#+begin_example
  #include <keyutils.h>

  long keyctl_restrict_keyring(key_serial_t keyring,
  const char *type, const char *restriction);
#+end_example

* DESCRIPTION
*keyctl_restrict_keyring*() limits the linkage of keys to the given
/keyring/ using a provided key /type/ and /restriction/ scheme. The
available options vary depending on the key type, and typically contain
a restriction name possibly followed by key ids or other data relevant
to the restriction. If the type and restriction are both *NULL,* the
keyring will reject all links.

* RETURN VALUE
On success *keyctl_restrict_keyring*() returns *0*. On error, the value
*-1* will be returned and /errno/ will have been set to an appropriate
error.

* ERRORS
- *EDEADLK* :: A restriction cycle was avoided. Two keyrings cannot
  restrict each other.

- *EEXIST* :: The keyring is already restricted.

- *EINVAL* :: The restriction string is invalid or too large.

- *ENOKEY* :: The key type in the restriction is invalid or not
  available.

- *ENOTDIR* :: The provided key id references an item that is not a
  keyring.

- *ENOENT* :: The key type exists but does not support restrictions.

* LINKING
This is a library function that can be found in /libkeyutils/. When
linking, *-lkeyutils* should be specified to the linker.

* SEE ALSO
*keyctl*(1), *keyctl*(2), *keyctl*(3), *keyutils*(7)
