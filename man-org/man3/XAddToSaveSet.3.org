#+TITLE: Manpages - XAddToSaveSet.3
#+DESCRIPTION: Linux manpage for XAddToSaveSet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XAddToSaveSet.3 is found in manpage for: [[../man3/XChangeSaveSet.3][man3/XChangeSaveSet.3]]