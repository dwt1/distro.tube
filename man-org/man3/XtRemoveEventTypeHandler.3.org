#+TITLE: Manpages - XtRemoveEventTypeHandler.3
#+DESCRIPTION: Linux manpage for XtRemoveEventTypeHandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtRemoveEventTypeHandler.3 is found in manpage for: [[../man3/XtInsertEventTypeHandler.3][man3/XtInsertEventTypeHandler.3]]