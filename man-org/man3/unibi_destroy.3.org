#+TITLE: Manpages - unibi_destroy.3
#+DESCRIPTION: Linux manpage for unibi_destroy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
unibi_destroy - free a terminal object

* SYNOPSIS
#include <unibilium.h> void unibi_destroy(unibi_term *ut);

* DESCRIPTION
This function frees a terminal object created by =unibi_dummy= or
=unibi_from_mem=.

* SEE ALSO
*unibilium.h* (3), *unibi_dummy* (3), *unibi_from_mem* (3)
