#+TITLE: Manpages - CURLOPT_PROXY_CAPATH.3
#+DESCRIPTION: Linux manpage for CURLOPT_PROXY_CAPATH.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_PROXY_CAPATH - directory holding HTTPS proxy CA certificates

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CAPATH, char
*capath);

* DESCRIPTION
Pass a char * to a null-terminated string naming a directory holding
multiple CA certificates to verify the HTTPS proxy with. If libcurl is
built against OpenSSL, the certificate directory must be prepared using
the openssl c_rehash utility. This makes sense only when
/CURLOPT_PROXY_SSL_VERIFYPEER(3)/ is enabled (which it is by default).

The application does not have to keep the string around after setting
this option.

* DEFAULT
NULL

* PROTOCOLS
Everything used over an HTTPS proxy

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* using an HTTPS proxy */
    curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
    curl_easy_setopt(curl, CURLOPT_PROXY_CAPATH, "/etc/cert-dir");
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.52.0

This option is supported by the OpenSSL, GnuTLS, and mbedTLS (since
7.56.0) backends. The NSS backend provides the option only for backward
compatibility.

* RETURN VALUE
CURLE_OK if supported; or an error such as:

CURLE_NOT_BUILT_IN - Not supported by the SSL backend

CURLE_UNKNOWN_OPTION

CURLE_OUT_OF_MEMORY

* SEE ALSO
*CURLOPT_PROXY_CAINFO*(3), *CURLOPT_CAINFO*(3),
*CURLOPT_PROXY_SSL_VERIFYHOST*(3), *CURLOPT_STDERR*(3),
*CURLOPT_DEBUGFUNCTION*(3),
