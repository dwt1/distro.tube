#+TITLE: Manpages - __fpurge.3
#+DESCRIPTION: Linux manpage for __fpurge.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about __fpurge.3 is found in manpage for: [[../man3/fpurge.3][man3/fpurge.3]]