#+TITLE: Manpages - rpc_gss_is_installed.3t
#+DESCRIPTION: Linux manpage for rpc_gss_is_installed.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This function indicates whether the GSS_API mechanism named "mech" is
installed and enabled.

The name of a GSS_API mechanism. "kerberos_v5" is currently the only
supported mechanism.

Returns

if the named GSS_API mechanism is installed and enabled,

otherwise.

The

function is part of libtirpc.

This manual page was written by
