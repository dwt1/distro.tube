#+TITLE: Manpages - ne_xml_create.3
#+DESCRIPTION: Linux manpage for ne_xml_create.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_xml_create, ne_xml_destroy - create and destroy an XML parser

* SYNOPSIS
#+begin_example
  #include <ne_xml.h>
#+end_example

*ne_xml_parser *ne_xml_create(void);*

*void ne_xml_destroy(ne_xml_parser **/parser/*);*

* DESCRIPTION
The *ne_xml_create* function creates an XML parser object, which can be
used for parsing XML documents using stacked SAX handlers.

* RETURN VALUE
*ne_xml_create* returns a pointer to an XML parser object, and never
NULL

* SEE ALSO
XXX

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
