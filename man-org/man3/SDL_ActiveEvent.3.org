#+TITLE: Manpages - SDL_ActiveEvent.3
#+DESCRIPTION: Linux manpage for SDL_ActiveEvent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_ActiveEvent - Application visibility event structure

* STRUCTURE DEFINITION
#+begin_example
  typedef struct{
    Uint8 type;
    Uint8 gain;
    Uint8 state;
  } SDL_ActiveEvent;
#+end_example

* STRUCTURE DATA
- *type* :: *SDL_ACTIVEEVENT.*

- *gain* :: 0 if the event is a loss or 1 if it is a gain.

- *state* :: *SDL_APPMOUSEFOCUS* if mouse focus was gained or lost,
  *SDL_APPINPUTFOCUS* if input focus was gained or lost, or
  *SDL_APPACTIVE* if the application was iconified (*gain*=0) or
  restored(*gain*=1).

* DESCRIPTION
*SDL_ActiveEvent* is a member of the *SDL_Event* union and is used when
an event of type *SDL_ACTIVEEVENT* is reported.

When the mouse leaves or enters the window area a *SDL_APPMOUSEFOCUS*
type activation event occurs, if the mouse entered the window then
*gain* will be 1, otherwise *gain* will be 0. A *SDL_APPINPUTFOCUS* type
activation event occurs when the application loses or gains keyboard
focus. This usually occurs when another application is made active.
Finally, a *SDL_APPACTIVE* type event occurs when the application is
either minimised/iconified (*gain*=0) or restored.

#+begin_quote
  *Note: *

  This event does not occur when an application window is first created.
#+end_quote

* SEE ALSO
*SDL_Event*, *SDL_GetAppState*
