#+TITLE: Manpages - RSA_blinding_on.3ssl
#+DESCRIPTION: Linux manpage for RSA_blinding_on.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
RSA_blinding_on, RSA_blinding_off - protect the RSA operation from
timing attacks

* SYNOPSIS
#include <openssl/rsa.h> int RSA_blinding_on(RSA *rsa, BN_CTX *ctx);
void RSA_blinding_off(RSA *rsa);

* DESCRIPTION
RSA is vulnerable to timing attacks. In a setup where attackers can
measure the time of RSA decryption or signature operations, blinding
must be used to protect the RSA operation from that attack.

*RSA_blinding_on()* turns blinding on for key *rsa* and generates a
random blinding factor. *ctx* is *NULL* or a preallocated and
initialized *BN_CTX*.

*RSA_blinding_off()* turns blinding off and frees the memory used for
the blinding factor.

* RETURN VALUES
*RSA_blinding_on()* returns 1 on success, and 0 if an error occurred.

*RSA_blinding_off()* returns no value.

* COPYRIGHT
Copyright 2000-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
