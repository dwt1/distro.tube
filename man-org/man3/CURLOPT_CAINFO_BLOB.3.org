#+TITLE: Manpages - CURLOPT_CAINFO_BLOB.3
#+DESCRIPTION: Linux manpage for CURLOPT_CAINFO_BLOB.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CAINFO_BLOB - Certificate Authority (CA) bundle in PEM format

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAINFO_BLOB, struct
curl_blob *stblob);

* DESCRIPTION
Pass a pointer to a curl_blob structure, which contains information
(pointer and size) about a memory block with binary data of PEM encoded
content holding one or more certificates to verify the HTTPS server
with.

If /CURLOPT_SSL_VERIFYPEER(3)/ is zero and you avoid verifying the
server's certificate, /CURLOPT_CAINFO_BLOB(3)/ is not needed.

This option overrides /CURLOPT_CAINFO(3)/.

* DEFAULT
NULL

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  char *strpem; /* strpem must point to a PEM string */
  CURL *curl = curl_easy_init();
  if(curl) {
    struct curl_blob blob;
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    blob.data = strpem;
    blob.len = strlen(strpem);
    blob.flags = CURL_BLOB_COPY;
    curl_easy_setopt(curl, CURLOPT_CAINFO_BLOB, &blob);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.77.0.

This option is supported by the BearSSL (since 7.79.0), OpenSSL, Secure
Transport and Schannel backends.

* RETURN VALUE
Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if
not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

* SEE ALSO
*CURLOPT_CAINFO*(3), *CURLOPT_CAPATH*(3), *CURLOPT_SSL_VERIFYPEER*(3),
*CURLOPT_SSL_VERIFYHOST*(3),
