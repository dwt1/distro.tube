#+TITLE: Manpages - rtcSetGeometryOccludedFilterFunction.3embree3
#+DESCRIPTION: Linux manpage for rtcSetGeometryOccludedFilterFunction.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcSetGeometryOccludedFilterFunction - sets the occlusion filter
    for the geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcSetGeometryOccludedFilterFunction(
    RTCGeometry geometry,
    RTCFilterFunctionN filter
  );
#+end_example

** DESCRIPTION
The =rtcSetGeometryOccludedFilterFunction= function registers an
occlusion filter callback function (=filter= argument) for the specified
geometry (=geometry= argument).

Only a single callback function can be registered per geometry, and
further invocations overwrite the previously set callback function.
Passing =NULL= as function pointer disables the registered callback
function.

The registered intersection filter function is invoked for every hit
encountered during the =rtcOccluded=-type ray queries and can accept or
reject that hit. The feature can be used to define a silhouette for a
primitive and reject hits that are outside the silhouette. E.g. a tree
leaf could be modeled with an alpha texture that decides whether hit
points lie inside or outside the leaf.

Please see the description of the
=rtcSetGeometryIntersectFilterFunction= for a description of the filter
callback function.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcSetGeometryIntersectFilterFunction]
