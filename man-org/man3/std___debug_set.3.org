#+TITLE: Manpages - std___debug_set.3
#+DESCRIPTION: Linux manpage for std___debug_set.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::__debug::set< _Key, _Compare, _Allocator > - Class std::set with
safety/checking/debug instrumentation.

* SYNOPSIS
\\

=#include <set.h>=

Inherits *__gnu_debug::_Safe_container< set< _Key, std::less< _Key >,
std::allocator< _Key > >, std::allocator< _Key >,
__gnu_debug::_Safe_node_sequence >*, and set< _Key, std::less< _Key >,
std::allocator< _Key > >.

** Public Types
typedef _Allocator *allocator_type*\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_const_iterator*, *set* >
*const_iterator*\\

typedef _Base::const_pointer *const_pointer*\\

typedef _Base::const_reference *const_reference*\\

typedef *std::reverse_iterator*< *const_iterator* >
*const_reverse_iterator*\\

typedef _Base::difference_type *difference_type*\\

using *insert_return_type* = *_Node_insert_return*< *iterator*,
node_type >\\

typedef *__gnu_debug::_Safe_iterator*< *_Base_iterator*, *set* >
*iterator*\\

typedef _Compare *key_compare*\\

typedef _Key *key_type*\\

using *node_type* = typename _Base::node_type\\

typedef _Base::pointer *pointer*\\

typedef _Base::reference *reference*\\

typedef *std::reverse_iterator*< *iterator* > *reverse_iterator*\\

typedef _Base::size_type *size_type*\\

typedef _Compare *value_compare*\\

typedef _Key *value_type*\\

** Public Member Functions
*set* (_Base_ref __x)\\

template<typename _InputIterator > *set* (_InputIterator __first,
_InputIterator __last, const _Compare &__comp=_Compare(), const
_Allocator &__a=_Allocator())\\

template<typename _InputIterator > *set* (_InputIterator __first,
_InputIterator __last, const allocator_type &__a)\\

*set* (const _Compare &__comp, const _Allocator &__a=_Allocator())\\

*set* (const allocator_type &__a)\\

*set* (const *set* &)=default\\

*set* (const *set* &__x, const allocator_type &__a)\\

*set* (*initializer_list*< value_type > __l, const _Compare
&__comp=_Compare(), const allocator_type &__a=allocator_type())\\

*set* (*initializer_list*< value_type > __l, const allocator_type
&__a)\\

*set* (*set* &&)=default\\

*set* (*set* &&__x, const allocator_type &__a)
noexcept(noexcept(*_Base*(*std::move*(__x._M_base()), __a)))\\

const *_Base* & *_M_base* () const noexcept\\

*_Base* & *_M_base* () noexcept\\

template<typename _Predicate > void *_M_invalidate_if* (_Predicate
__pred)\\

void *_M_swap* (_Safe_container &__x) noexcept\\

template<typename _Predicate > void *_M_transfer_from_if*
(_Safe_sequence &__from, _Predicate __pred)\\

*const_iterator* *begin* () const noexcept\\

*iterator* *begin* () noexcept\\

*const_iterator* *cbegin* () const noexcept\\

*const_iterator* *cend* () const noexcept\\

void *clear* () noexcept\\

*const_reverse_iterator* *crbegin* () const noexcept\\

*const_reverse_iterator* *crend* () const noexcept\\

template<typename... _Args> *std::pair*< *iterator*, bool > *emplace*
(_Args &&... __args)\\

template<typename... _Args> *iterator* *emplace_hint* (*const_iterator*
__pos, _Args &&... __args)\\

*const_iterator* *end* () const noexcept\\

*iterator* *end* () noexcept\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *std::pair*< *iterator*,
*iterator* > *equal_range* (const _Kt &__x)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *std::pair*<
*const_iterator*, *const_iterator* > *equal_range* (const _Kt &__x)
const\\

*std::pair*< *iterator*, *iterator* > *equal_range* (const key_type
&__x)\\

*std::pair*< *const_iterator*, *const_iterator* > *equal_range* (const
key_type &__x) const\\

size_type *erase* (const key_type &__x)\\

_GLIBCXX_ABI_TAG_CXX11 *iterator* *erase* (*const_iterator* __first,
*const_iterator* __last)\\

_GLIBCXX_ABI_TAG_CXX11 *iterator* *erase* (*const_iterator*
__position)\\

node_type *extract* (const key_type &__key)\\

node_type *extract* (*const_iterator* __position)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *iterator* *find* (const _Kt
&__x)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *const_iterator* *find*
(const _Kt &__x) const\\

*iterator* *find* (const key_type &__x)\\

*const_iterator* *find* (const key_type &__x) const\\

template<typename _InputIterator > void *insert* (_InputIterator
__first, _InputIterator __last)\\

*std::pair*< *iterator*, bool > *insert* (const value_type &__x)\\

*iterator* *insert* (*const_iterator* __hint, node_type &&__nh)\\

*iterator* *insert* (*const_iterator* __position, const value_type
&__x)\\

*iterator* *insert* (*const_iterator* __position, value_type &&__x)\\

void *insert* (*initializer_list*< value_type > __l)\\

*insert_return_type* *insert* (node_type &&__nh)\\

*std::pair*< *iterator*, bool > *insert* (value_type &&__x)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *iterator* *lower_bound*
(const _Kt &__x)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *const_iterator*
*lower_bound* (const _Kt &__x) const\\

*iterator* *lower_bound* (const key_type &__x)\\

*const_iterator* *lower_bound* (const key_type &__x) const\\

*set* & *operator=* (const *set* &)=default\\

*set* & *operator=* (*initializer_list*< value_type > __l)\\

*set* & *operator=* (*set* &&)=default\\

*const_reverse_iterator* *rbegin* () const noexcept\\

*reverse_iterator* *rbegin* () noexcept\\

*const_reverse_iterator* *rend* () const noexcept\\

*reverse_iterator* *rend* () noexcept\\

void *swap* (*set* &__x) noexcept(/**conditional* */)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *iterator* *upper_bound*
(const _Kt &__x)\\

template<typename _Kt , typename _Req = typename
__has_is_transparent<_Compare, _Kt>::type> *const_iterator*
*upper_bound* (const _Kt &__x) const\\

*iterator* *upper_bound* (const key_type &__x)\\

*const_iterator* *upper_bound* (const key_type &__x) const\\

** Public Attributes
_Safe_iterator_base * *_M_const_iterators*\\
The list of constant iterators that reference this container.

_Safe_iterator_base * *_M_iterators*\\
The list of mutable iterators that reference this container.

unsigned int *_M_version*\\
The container version number. This number may never be 0.

** Protected Member Functions
void *_M_detach_all* ()\\

void *_M_detach_singular* ()\\

__gnu_cxx::__mutex & *_M_get_mutex* () throw ()\\

void *_M_invalidate_all* ()\\

void *_M_invalidate_all* () const\\

void *_M_revalidate_singular* ()\\

_Safe_container & *_M_safe* () noexcept\\

void *_M_swap* (_Safe_sequence_base &__x) noexcept\\

** Friends
template<typename _ItT , typename _SeqT , typename _CatT > class
*::__gnu_debug::_Safe_iterator*\\

* Detailed Description
** "template<typename _Key, typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>>
\\
class std::__debug::set< _Key, _Compare, _Allocator >"Class std::set
with safety/checking/debug instrumentation.

Definition at line *44* of file *set.h*.

* Member Typedef Documentation
** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Allocator
*std::__debug::set*< _Key, _Compare, _Allocator >::allocator_type
Definition at line *77* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
*__gnu_debug::_Safe_iterator*<*_Base_const_iterator*, *set*>
*std::__debug::set*< _Key, _Compare, _Allocator >::*const_iterator*
Definition at line *84* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Base::const_pointer
*std::__debug::set*< _Key, _Compare, _Allocator >::const_pointer
Definition at line *89* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
_Base::const_reference *std::__debug::set*< _Key, _Compare, _Allocator
>::const_reference
Definition at line *79* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
*std::reverse_iterator*<*const_iterator*> *std::__debug::set*< _Key,
_Compare, _Allocator >::*const_reverse_iterator*
Definition at line *91* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
_Base::difference_type *std::__debug::set*< _Key, _Compare, _Allocator
>::difference_type
Definition at line *87* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> using *std::__debug::set*<
_Key, _Compare, _Allocator >::*insert_return_type* =
*_Node_insert_return*<*iterator*, node_type>
Definition at line *313* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
*__gnu_debug::_Safe_iterator*<*_Base_iterator*, *set*>
*std::__debug::set*< _Key, _Compare, _Allocator >::*iterator*
Definition at line *82* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Compare
*std::__debug::set*< _Key, _Compare, _Allocator >::key_compare
Definition at line *75* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Key
*std::__debug::set*< _Key, _Compare, _Allocator >::key_type
Definition at line *73* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> using *std::__debug::set*<
_Key, _Compare, _Allocator >::node_type = typename _Base::node_type
Definition at line *312* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Base::pointer
*std::__debug::set*< _Key, _Compare, _Allocator >::pointer
Definition at line *88* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Base::reference
*std::__debug::set*< _Key, _Compare, _Allocator >::reference
Definition at line *78* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef
*std::reverse_iterator*<*iterator*> *std::__debug::set*< _Key, _Compare,
_Allocator >::*reverse_iterator*
Definition at line *90* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Base::size_type
*std::__debug::set*< _Key, _Compare, _Allocator >::size_type
Definition at line *86* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Compare
*std::__debug::set*< _Key, _Compare, _Allocator >::value_compare
Definition at line *76* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> typedef _Key
*std::__debug::set*< _Key, _Compare, _Allocator >::value_type
Definition at line *74* of file *set.h*.

* Constructor & Destructor Documentation
** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (*initializer_list*< value_type > __l,
const _Compare & __comp = =_Compare()=, const allocator_type & __a =
=allocator_type()=)= [inline]=
Definition at line *107* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (const allocator_type & __a)= [inline]=,
= [explicit]=
Definition at line *113* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (const *set*< _Key, _Compare, _Allocator >
& __x, const allocator_type & __a)= [inline]=
Definition at line *116* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (*set*< _Key, _Compare, _Allocator > &&
__x, const allocator_type & __a)= [inline]=, = [noexcept]=
Definition at line *119* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (*initializer_list*< value_type > __l,
const allocator_type & __a)= [inline]=
Definition at line *124* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename
_InputIterator > *std::__debug::set*< _Key, _Compare, _Allocator
>::*set* (_InputIterator __first, _InputIterator __last, const
allocator_type & __a)= [inline]=
Definition at line *128* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (const _Compare & __comp, const _Allocator
& __a = =_Allocator()=)= [inline]=, = [explicit]=
Definition at line *137* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename
_InputIterator > *std::__debug::set*< _Key, _Compare, _Allocator
>::*set* (_InputIterator __first, _InputIterator __last, const _Compare
& __comp = =_Compare()=, const _Allocator & __a =
=_Allocator()=)= [inline]=
Definition at line *142* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::__debug::set*< _Key,
_Compare, _Allocator >::*set* (_Base_ref __x)= [inline]=
Definition at line *150* of file *set.h*.

* Member Function Documentation
** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> const *_Base* &
*std::__debug::set*< _Key, _Compare, _Allocator >::_M_base ()
const= [inline]=, = [noexcept]=
Definition at line *569* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *_Base* &
*std::__debug::set*< _Key, _Compare, _Allocator >::_M_base
()= [inline]=, = [noexcept]=
Definition at line *566* of file *set.h*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_all
()= [protected]=, = [inherited]=
Detach all iterators, leaving them singular.

Referenced by
*__gnu_debug::_Safe_sequence_base::~_Safe_sequence_base()*.

** void __gnu_debug::_Safe_sequence_base::_M_detach_singular
()= [protected]=, = [inherited]=
Detach all singular iterators.

*Postcondition*

#+begin_quote
  for all iterators i attached to this sequence, i->_M_version ==
  _M_version.
#+end_quote

** __gnu_cxx::__mutex & __gnu_debug::_Safe_sequence_base::_M_get_mutex
()= [protected]=, = [inherited]=
For use in _Safe_sequence.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** template<typename _Sequence > void
*__gnu_debug::_Safe_node_sequence*< _Sequence >::_M_invalidate_all
()= [inline]=, = [protected]=, = [inherited]=
Definition at line *136* of file *safe_sequence.h*.

** void __gnu_debug::_Safe_sequence_base::_M_invalidate_all ()
const= [inline]=, = [protected]=, = [inherited]=
Invalidates all iterators.

Definition at line *256* of file *safe_base.h*.

References *__gnu_debug::_Safe_sequence_base::_M_version*.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_invalidate_if (_Predicate
__pred)= [inherited]=
Invalidates all iterators =x= that reference this sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *37* of file *safe_sequence.tcc*.

** void __gnu_debug::_Safe_sequence_base::_M_revalidate_singular
()= [protected]=, = [inherited]=
Revalidates all attached singular iterators. This method may be used to
validate iterators that were invalidated before (but for some reason,
such as an exception, need to become valid again).

** _Safe_container & *__gnu_debug::_Safe_container*< *set*< _Key,
*std::less*< _Key >, *std::allocator*< _Key > > , *std::allocator*< _Key
> , *__gnu_debug::_Safe_node_sequence* , true >::_M_safe ()= [inline]=,
= [protected]=, = [noexcept]=, = [inherited]=
Definition at line *52* of file *safe_container.h*.

** void *__gnu_debug::_Safe_container*< *set*< _Key, *std::less*< _Key
>, *std::allocator*< _Key > > , *std::allocator*< _Key > ,
*__gnu_debug::_Safe_node_sequence* , true >::_M_swap (*_Safe_container*<
*set*< _Key, *std::less*< _Key >, *std::allocator*< _Key > >,
*std::allocator*< _Key >, *__gnu_debug::_Safe_node_sequence* > &
__x)= [inline]=, = [noexcept]=, = [inherited]=
Definition at line *111* of file *safe_container.h*.

** void __gnu_debug::_Safe_sequence_base::_M_swap (*_Safe_sequence_base*
& __x)= [protected]=, = [noexcept]=, = [inherited]=
Swap this sequence with the given sequence. This operation also swaps
ownership of the iterators, so that when the operation is complete all
iterators that originally referenced one container now reference the
other container.

** template<typename _Sequence > template<typename _Predicate > void
*__gnu_debug::_Safe_sequence*< _Sequence >::_M_transfer_from_if
(*_Safe_sequence*< _Sequence > & __from, _Predicate
__pred)= [inherited]=
Transfers all iterators =x= that reference =from= sequence, are not
singular, and for which =__pred(x)= returns =true=. =__pred= will be
invoked with the normal iterators nested in the safe ones.

Definition at line *68* of file *safe_sequence.tcc*.

References *std::__addressof()*,
*__gnu_debug::_Safe_sequence_base::_M_const_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_detach_single()*,
*__gnu_debug::_Safe_sequence_base::_M_get_mutex()*,
*__gnu_debug::_Safe_sequence_base::_M_iterators*,
*__gnu_debug::_Safe_iterator_base::_M_next*,
*__gnu_debug::_Safe_iterator_base::_M_prior*,
*__gnu_debug::_Safe_iterator_base::_M_sequence*, and
*__gnu_debug::_Safe_iterator_base::_M_version*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::begin ()
const= [inline]=, = [noexcept]=
Definition at line *185* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::begin ()= [inline]=,
= [noexcept]=
Definition at line *181* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::cbegin ()
const= [inline]=, = [noexcept]=
Definition at line *214* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::cend ()
const= [inline]=, = [noexcept]=
Definition at line *218* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> void *std::__debug::set*<
_Key, _Compare, _Allocator >::clear ()= [inline]=, = [noexcept]=
Definition at line *432* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::crbegin ()
const= [inline]=, = [noexcept]=
Definition at line *222* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::crend ()
const= [inline]=, = [noexcept]=
Definition at line *226* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename... _Args>
*std::pair*< *iterator*, bool > *std::__debug::set*< _Key, _Compare,
_Allocator >::emplace (_Args &&... __args)= [inline]=
Definition at line *239* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename... _Args>
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator
>::emplace_hint (*const_iterator* __pos, _Args &&... __args)= [inline]=
Definition at line *247* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::end ()
const= [inline]=, = [noexcept]=
Definition at line *193* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::end ()= [inline]=,
= [noexcept]=
Definition at line *189* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*std::pair*< *iterator*, *iterator* > *std::__debug::set*< _Key,
_Compare, _Allocator >::equal_range (const _Kt & __x)= [inline]=
Definition at line *548* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*std::pair*< *const_iterator*, *const_iterator* > *std::__debug::set*<
_Key, _Compare, _Allocator >::equal_range (const _Kt & __x)
const= [inline]=
Definition at line *558* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::pair*< *iterator*,
*iterator* > *std::__debug::set*< _Key, _Compare, _Allocator
>::equal_range (const key_type & __x)= [inline]=
Definition at line *524* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::pair*<
*const_iterator*, *const_iterator* > *std::__debug::set*< _Key,
_Compare, _Allocator >::equal_range (const key_type & __x)
const= [inline]=
Definition at line *535* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> size_type
*std::__debug::set*< _Key, _Compare, _Allocator >::erase (const key_type
& __x)= [inline]=
Definition at line *370* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> _GLIBCXX_ABI_TAG_CXX11
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator >::erase
(*const_iterator* __first, *const_iterator* __last)= [inline]=
Definition at line *386* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> _GLIBCXX_ABI_TAG_CXX11
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator >::erase
(*const_iterator* __position)= [inline]=
Definition at line *353* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> node_type
*std::__debug::set*< _Key, _Compare, _Allocator >::extract (const
key_type & __key)= [inline]=
Definition at line *324* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> node_type
*std::__debug::set*< _Key, _Compare, _Allocator >::extract
(*const_iterator* __position)= [inline]=
Definition at line *316* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator >::find
(const _Kt & __x)= [inline]=
Definition at line *458* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*const_iterator* *std::__debug::set*< _Key, _Compare, _Allocator >::find
(const _Kt & __x) const= [inline]=
Definition at line *465* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::find (const key_type
& __x)= [inline]=
Definition at line *444* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::find (const key_type
& __x) const= [inline]=
Definition at line *450* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename
_InputIterator > void *std::__debug::set*< _Key, _Compare, _Allocator
>::insert (_InputIterator __first, _InputIterator __last)= [inline]=
Definition at line *293* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::pair*< *iterator*,
bool > *std::__debug::set*< _Key, _Compare, _Allocator >::insert (const
value_type & __x)= [inline]=
Definition at line *259* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::insert
(*const_iterator* __hint, node_type && __nh)= [inline]=
Definition at line *341* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::insert
(*const_iterator* __position, const value_type & __x)= [inline]=
Definition at line *276* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::insert
(*const_iterator* __position, value_type && __x)= [inline]=
Definition at line *284* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> void *std::__debug::set*<
_Key, _Compare, _Allocator >::insert (*initializer_list*< value_type >
__l)= [inline]=
Definition at line *307* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *insert_return_type*
*std::__debug::set*< _Key, _Compare, _Allocator >::insert (node_type &&
__nh)= [inline]=
Definition at line *333* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *std::pair*< *iterator*,
bool > *std::__debug::set*< _Key, _Compare, _Allocator >::insert
(value_type && __x)= [inline]=
Definition at line *268* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator
>::lower_bound (const _Kt & __x)= [inline]=
Definition at line *486* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*const_iterator* *std::__debug::set*< _Key, _Compare, _Allocator
>::lower_bound (const _Kt & __x) const= [inline]=
Definition at line *493* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::lower_bound (const
key_type & __x)= [inline]=
Definition at line *472* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::lower_bound (const
key_type & __x) const= [inline]=
Definition at line *478* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *set* & *std::__debug::set*<
_Key, _Compare, _Allocator >::operator= (*initializer_list*< value_type
> __l)= [inline]=
Definition at line *169* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::rbegin ()
const= [inline]=, = [noexcept]=
Definition at line *201* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::rbegin ()= [inline]=,
= [noexcept]=
Definition at line *197* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::rend ()
const= [inline]=, = [noexcept]=
Definition at line *209* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *reverse_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::rend ()= [inline]=,
= [noexcept]=
Definition at line *205* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> void *std::__debug::set*<
_Key, _Compare, _Allocator >::swap (*set*< _Key, _Compare, _Allocator >
& __x)= [inline]=, = [noexcept]=
Definition at line *424* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*iterator* *std::__debug::set*< _Key, _Compare, _Allocator
>::upper_bound (const _Kt & __x)= [inline]=
Definition at line *512* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _Kt ,
typename _Req = typename __has_is_transparent<_Compare, _Kt>::type>
*const_iterator* *std::__debug::set*< _Key, _Compare, _Allocator
>::upper_bound (const _Kt & __x) const= [inline]=
Definition at line *519* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::upper_bound (const
key_type & __x)= [inline]=
Definition at line *498* of file *set.h*.

** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> *const_iterator*
*std::__debug::set*< _Key, _Compare, _Allocator >::upper_bound (const
key_type & __x) const= [inline]=
Definition at line *504* of file *set.h*.

* Friends And Related Function Documentation
** template<typename _Key , typename _Compare = std::less<_Key>,
typename _Allocator = std::allocator<_Key>> template<typename _ItT ,
typename _SeqT , typename _CatT > friend class
::*__gnu_debug::_Safe_iterator*= [friend]=
Definition at line *59* of file *set.h*.

* Member Data Documentation
** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_const_iterators= [inherited]=
The list of constant iterators that reference this container.

Definition at line *197* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** _Safe_iterator_base*
__gnu_debug::_Safe_sequence_base::_M_iterators= [inherited]=
The list of mutable iterators that reference this container.

Definition at line *194* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence< _Sequence
>::_M_transfer_from_if()*.

** unsigned int
__gnu_debug::_Safe_sequence_base::_M_version= [mutable]=, = [inherited]=
The container version number. This number may never be 0.

Definition at line *200* of file *safe_base.h*.

Referenced by *__gnu_debug::_Safe_sequence_base::_M_invalidate_all()*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
