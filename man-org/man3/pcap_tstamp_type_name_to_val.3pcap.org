#+TITLE: Manpages - pcap_tstamp_type_name_to_val.3pcap
#+DESCRIPTION: Linux manpage for pcap_tstamp_type_name_to_val.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_tstamp_type_name_to_val - get the time stamp type value
corresponding to a time stamp type name

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  int pcap_tstamp_type_name_to_val(const char *name);
#+end_example

* DESCRIPTION
*pcap_tstamp_type_name_to_val*() translates a time stamp type name to
the corresponding time stamp type value. The translation is
case-insensitive.

* RETURN VALUE
*pcap_tstamp_type_name_to_val*() returns time stamp type value on
success and *PCAP_ERROR* on failure.

* BACKWARD COMPATIBILITY
This function became available in libpcap release 1.2.1.

* SEE ALSO
*pcap*(3PCAP), *pcap_tstamp_type_val_to_name*(3PCAP)
