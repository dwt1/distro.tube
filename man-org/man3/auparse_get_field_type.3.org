#+TITLE: Manpages - auparse_get_field_type.3
#+DESCRIPTION: Linux manpage for auparse_get_field_type.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
auparse_get_field_type - get current field's data type

* SYNOPSIS
*#include <auparse.h>*

int auparse_get_field_type(auparse_state_t *au);

* DESCRIPTION
auparse_get_field_type returns a value from the auparse_type_t enum that
describes the kind of data in the current field of the current record in
the current event.

* RETURN VALUE
Returns AUPARSE_TYPE_UNCLASSIFIED if the field's data type has no known
description or is an integer. Otherwise it returns another enum. Fields
with the type AUPARSE_TYPE_ESCAPED must be interpreted to access their
value since those field's raw value is encoded.

* SEE ALSO
*auparse_get_field_name*(3).

* AUTHOR
Steve Grubb
