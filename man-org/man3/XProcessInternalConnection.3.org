#+TITLE: Manpages - XProcessInternalConnection.3
#+DESCRIPTION: Linux manpage for XProcessInternalConnection.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XProcessInternalConnection.3 is found in manpage for: [[../man3/XAddConnectionWatch.3][man3/XAddConnectionWatch.3]]