#+TITLE: Manpages - libssh2_agent_disconnect.3
#+DESCRIPTION: Linux manpage for libssh2_agent_disconnect.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_agent_disconnect - close a connection to an ssh-agent

* SYNOPSIS
#include <libssh2.h>

int libssh2_agent_disconnect(LIBSSH2_AGENT *agent);

* DESCRIPTION
Close a connection to an ssh-agent.

* RETURN VALUE
Returns 0 if succeeded, or a negative value for error.

* AVAILABILITY
Added in libssh2 1.2

* SEE ALSO
*libssh2_agent_connect(3)* *libssh2_agent_free(3)*
