#+TITLE: Manpages - archive_entry_time.3
#+DESCRIPTION: Linux manpage for archive_entry_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

These functions create and manipulate the time fields in an

Supported time fields are atime (access time), birthtime (creation
time), ctime (last time an inode property was changed) and mtime
(modification time).

provides a high-resolution interface. The timestamps are truncated
automatically depending on the archive format (for archiving) or the
filesystem capabilities (for restoring).

All timestamp fields are optional. The

functions can be used to mark the corresponding field as missing. The
current state can be queried using

Unset time fields have a second and nanosecond field of 0.

The

library first appeared in

The

library was written by
