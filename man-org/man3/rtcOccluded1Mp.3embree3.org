#+TITLE: Manpages - rtcOccluded1Mp.3embree3
#+DESCRIPTION: Linux manpage for rtcOccluded1Mp.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcOccluded1Mp - find any hits for a stream of M pointers to
    single rays
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcOccluded1M(
    RTCScene scene,
    struct RTCIntersectContext* context,
    struct RTCRay** ray,
    unsigned int M
  );
#+end_example

** DESCRIPTION
The =rtcOccluded1Mp= function checks whether there are any hits for a
stream of =M= single rays (=ray= argument) with the scene (=scene=
argument). The =ray= argument points to an array of pointers to rays.
Section [rtcOccluded1] for a description of how to set up and trace a
occlusion rays.

#+begin_example
#+end_example

#+begin_example
#+end_example

A ray in a ray stream is considered inactive if its =tnear= value is
larger than its =tfar= value.

The stream size =M= can be an arbitrary positive integer including 0.
Each ray must be aligned to 16 bytes.

** EXIT STATUS
For performance reasons this function does not do any error checks, thus
will not set any error flags on failure.

** SEE ALSO
[rtcIntersect1Mp]
