#+TITLE: Manpages - csinh.3
#+DESCRIPTION: Linux manpage for csinh.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
csinh, csinhf, csinhl - complex hyperbolic sine

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex csinh(double complex z);
  float complex csinhf(float complex z);
  long double complex csinhl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate the complex hyperbolic sine of /z/.

The complex hyperbolic sine function is defined as:

#+begin_example
      csinh(z) = (exp(z)-exp(-z))/2
#+end_example

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                         | Attribute     | Value   |
| *csinh*(), *csinhf*(), *csinhl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *casinh*(3), *ccosh*(3), *ctanh*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
