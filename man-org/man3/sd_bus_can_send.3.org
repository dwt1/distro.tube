#+TITLE: Manpages - sd_bus_can_send.3
#+DESCRIPTION: Linux manpage for sd_bus_can_send.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_can_send - Check which types can be sent over a bus object

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*void sd_bus_can_send(sd_bus **/bus/*, char */type/*);*

* DESCRIPTION
*sd_bus_can_send()* is mostly used for checking if file descriptor
passing is available on the given bus. /type/ can be any of the
*SD_BUS_TYPE* constants.

* RETURN VALUE
On failure, *sd_bus_can_send()* returns a negative errno-style error
code. If values of the given type can be sent over the given bus, it
returns a positive integer. Otherwise, it returns zero.

** Errors
Returned errors may indicate the following problems:

*-ENOPKG*

#+begin_quote
  The bus object /bus/ could not be resolved.
#+end_quote

*-ENOTCONN*

#+begin_quote
  The input parameter /bus/ is *NULL* or the bus is not connected.
#+end_quote

*-ECHILD*

#+begin_quote
  The bus object /bus/ was created in a different process.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* SEE ALSO
*systemd*(1), *sd-bus*(3)
