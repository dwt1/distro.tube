#+TITLE: Manpages - TAP_Formatter_Session.3perl
#+DESCRIPTION: Linux manpage for TAP_Formatter_Session.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Formatter::Session - Abstract base class for harness output
delegate

* VERSION
Version 3.43

* METHODS
** Class Methods
/=new=/

my %args = ( formatter => $self, ) my $harness =
TAP::Formatter::Console::Session->new( \%args );

The constructor returns a new =TAP::Formatter::Console::Session= object.

- =formatter=

- =parser=

- =name=

- =show_count=

/=header=/

Output test preamble

/=result=/

Called by the harness for each line of TAP it receives.

/=close_test=/

Called to close a test session.

/=clear_for_close=/

Called by =close_test= to clear the line showing test progress, or the
parallel test ruler, prior to printing the final test result.

/=time_report=/

Return a formatted string about the elapsed (wall-clock) time and about
the consumed CPU time.
