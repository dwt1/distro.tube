#+TITLE: Manpages - scalbn.3
#+DESCRIPTION: Linux manpage for scalbn.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about scalbn.3 is found in manpage for: [[../man3/scalbln.3][man3/scalbln.3]]