#+TITLE: Manpages - __gnu_parallel__Multiplies.3
#+DESCRIPTION: Linux manpage for __gnu_parallel__Multiplies.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::_Multiplies< _Tp1, _Tp2, _Result > - Similar to
std::multiplies, but allows two different types.

* SYNOPSIS
\\

=#include <base.h>=

Inherits *std::binary_function< _Tp1, _Tp2, __typeof__(*static_cast<
_Tp1 * >(0) **static_cast< _Tp2 * >(0)) >*.

** Public Types
typedef _Tp1 *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef __typeof__(*< * >0_Tp1 **static_cast< _Tp2 * >(0)
*result_type*\\
=result_type= is the return type

typedef _Tp2 *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
_Result *operator()* (const _Tp1 &__x, const _Tp2 &__y) const\\

* Detailed Description
** "template<typename _Tp1, typename _Tp2, typename _Result =
__typeof__(*static_cast<_Tp1*>(0) * *static_cast<_Tp2*>(0))>
\\
struct __gnu_parallel::_Multiplies< _Tp1, _Tp2, _Result >"Similar to
std::multiplies, but allows two different types.

Definition at line *288* of file *base.h*.

* Member Typedef Documentation
** typedef _Tp1 *std::binary_function*< _Tp1 , _Tp2 , __typeof__(*< *
>0_Tp1 **static_cast< _Tp2 * >(0) >::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef __typeof__(*< * >0_Tp1 **static_cast< _Tp2 * >(0)
*std::binary_function*< _Tp1 , _Tp2 , __typeof__(*< * >0_Tp1
**static_cast< _Tp2 * >(0) >::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _Tp2 *std::binary_function*< _Tp1 , _Tp2 , __typeof__(*< *
>0_Tp1 **static_cast< _Tp2 * >(0)
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Tp1 , typename _Tp2 , typename _Result =
__typeof__(*static_cast<_Tp1*>(0) * *static_cast<_Tp2*>(0))> _Result
*__gnu_parallel::_Multiplies*< _Tp1, _Tp2, _Result >::operator() (const
_Tp1 & __x, const _Tp2 & __y) const= [inline]=
Definition at line *291* of file *base.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
