#+TITLE: Manpages - zmq_connect.3
#+DESCRIPTION: Linux manpage for zmq_connect.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zmq_connect - create outgoing connection from socket

* SYNOPSIS
*int zmq_connect (void */*socket/*, const char */*endpoint/*);*

* DESCRIPTION
The /zmq_connect()/ function connects the /socket/ to an /endpoint/ and
then accepts incoming connections on that endpoint.

The /endpoint/ is a string consisting of a /transport/:// followed by an
/address/. The /transport/ specifies the underlying protocol to use. The
/address/ specifies the transport-specific address to connect to.

0MQ provides the the following transports:

/tcp/

#+begin_quote
  unicast transport using TCP, see *zmq_tcp*(7)
#+end_quote

/ipc/

#+begin_quote
  local inter-process communication transport, see *zmq_ipc*(7)
#+end_quote

/inproc/

#+begin_quote
  local in-process (inter-thread) communication transport, see
  *zmq_inproc*(7)
#+end_quote

/pgm/, /epgm/

#+begin_quote
  reliable multicast transport using PGM, see *zmq_pgm*(7)
#+end_quote

/vmci/

#+begin_quote
  virtual machine communications interface (VMCI), see *zmq_vmci*(7)
#+end_quote

/udp/

#+begin_quote
  unreliable unicast and multicast using UDP, see *zmq_udp*(7)
#+end_quote

Every 0MQ socket type except /ZMQ_PAIR/ and /ZMQ_CHANNEL/ supports
one-to-many and many-to-one semantics. The precise semantics depend on
the socket type and are defined in *zmq_socket*(3).

#+begin_quote
  \\

  *Note*

  \\

  for most transports and socket types the connection is not performed
  immediately but as needed by 0MQ. Thus a successful call to
  /zmq_connect()/ does not mean that the connection was or could
  actually be established. Because of this, for most transports and
  socket types the order in which a /server/ socket is bound and a
  /client/ socket is connected to it does not matter. The /ZMQ_PAIR/ and
  /ZMQ_CHANNEL/ sockets are an exception, as they do not automatically
  reconnect to endpoints.
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  following a /zmq_connect()/, for socket types except for ZMQ_ROUTER,
  the socket enters its normal /ready/ state. By contrast, following a
  /zmq_bind()/ alone, the socket enters a /mute/ state in which the
  socket blocks or drops messages according to the socket type, as
  defined in *zmq_socket*(3). A ZMQ_ROUTER socket enters its normal
  /ready/ state for a specific peer only when handshaking is complete
  for that peer, which may take an arbitrary time.
#+end_quote

#+begin_quote
  \\

  *Note*

  \\

  for some socket types, multiple connections to the same endpoint don't
  really make sense (see *https://github.com/zeromq/libzmq/issues/788*).
  For those socket types, any attempt to connect to an already connected
  endpoint is silently ignored (i.e., returns zero). This behavior
  applies to ZMQ_DEALER, ZMQ_SUB, ZMQ_PUB, and ZMQ_REQ socket types.
#+end_quote

* RETURN VALUE
The /zmq_connect()/ function returns zero if successful. Otherwise it
returns -1 and sets /errno/ to one of the values defined below.

* ERRORS
*EINVAL*

#+begin_quote
  The endpoint supplied is invalid.
#+end_quote

*EPROTONOSUPPORT*

#+begin_quote
  The requested /transport/ protocol is not supported.
#+end_quote

*ENOCOMPATPROTO*

#+begin_quote
  The requested /transport/ protocol is not compatible with the socket
  type.
#+end_quote

*ETERM*

#+begin_quote
  The 0MQ /context/ associated with the specified /socket/ was
  terminated.
#+end_quote

*ENOTSOCK*

#+begin_quote
  The provided /socket/ was invalid.
#+end_quote

*EMTHREAD*

#+begin_quote
  No I/O thread is available to accomplish the task.
#+end_quote

* EXAMPLE
*Connecting a subscriber socket to an in-process and a TCP transport*.

#+begin_quote
  #+begin_example
    /* Create a ZMQ_SUB socket */
    void *socket = zmq_socket (context, ZMQ_SUB);
    assert (socket);
    /* Connect it to an in-process transport with the address my_publisher */
    int rc = zmq_connect (socket, "inproc://my_publisher");
    assert (rc == 0);
    /* Connect it to the host server001, port 5555 using a TCP transport */
    rc = zmq_connect (socket, "tcp://server001:5555");
    assert (rc == 0);
  #+end_example
#+end_quote

* SEE ALSO
*zmq_bind*(3) *zmq_socket*(3) *zmq*(7)

* AUTHORS
This page was written by the 0MQ community. To make a change please read
the 0MQ Contribution Policy at
*http://www.zeromq.org/docs:contributing*.
