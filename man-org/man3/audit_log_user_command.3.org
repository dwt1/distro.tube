#+TITLE: Manpages - audit_log_user_command.3
#+DESCRIPTION: Linux manpage for audit_log_user_command.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_log_user_command - log a user command

* SYNOPSIS
*#include <libaudit.h>*

*int audit_log_user_command(int audit_fd, int type, const char *command,
const char *tty, int result);*

* DESCRIPTION
This function will log a command to the audit system using a predefined
message format. It encodes the command as the audit system expects for
untrusted strings. This function should be used by all apps need to
record commands. The function parameters are as follows:

#+begin_example
  audit_fd - The fd returned by audit_open
  type - type of message, ex: AUDIT_USYS_CONFIG, AUDIT_USER_LOGIN
  command - the command being logged
  tty - The tty of the user, if NULL will attempt to figure out
  result - 1 is "success" and 0 is "failed"
#+end_example

* RETURN VALUE
It returns the sequence number which is > 0 on success or <= 0 on error.

* ERRORS
This function returns -1 on failure. Examine errno for more info.

* SEE ALSO
*audit_log_user_message*(3), *audit_log_user_comm_message*(3),
*audit_log_acct_message*(3), *audit_log_user_avc_message*(3),
*audit_log_semanage_message*(3).

* AUTHOR
Steve Grubb
