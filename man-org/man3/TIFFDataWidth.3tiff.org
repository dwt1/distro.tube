#+TITLE: Manpages - TIFFDataWidth.3tiff
#+DESCRIPTION: Linux manpage for TIFFDataWidth.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFDataWidth - Get the size of TIFF data types

* SYNOPSIS
*#include <tiffio.h>*

*int TIFFDataWidth(TIFFDataType */type/*)*

* DESCRIPTION
/TIFFDataWidth/ returns a size of /type/ in bytes. Currently following
data types are supported:\\
/TIFF_BYTE/\\
/TIFF_ASCII/\\
/TIFF_SBYTE/\\
/TIFF_UNDEFINED/\\
/TIFF_SHORT/\\
/TIFF_SSHORT/\\
/TIFF_LONG/\\
/TIFF_SLONG/\\
/TIFF_FLOAT/\\
/TIFF_IFD/\\
/TIFF_RATIONAL/\\
/TIFF_SRATIONAL/\\
/TIFF_DOUBLE/\\

* RETURN VALUES
\\
/TIFFDataWidth/ returns a number of bytes occupied by the item of given
type. 0 returned when unknown data type supplied.

* SEE ALSO
*libtiff*(3TIFF),

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
