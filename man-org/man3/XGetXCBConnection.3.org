#+TITLE: Manpages - XGetXCBConnection.3
#+DESCRIPTION: Linux manpage for XGetXCBConnection.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGetXCBConnection - get the XCB connection for an Xlib Display

* SYNTAX
#include <X11/Xlib-xcb.h>

xcb_connection_t *XGetXCBConnection(Display */dpy/);

* ARGUMENTS
- dpy :: Specifies the connection to the X server.

* DESCRIPTION
The /XGetXCBConnection/ function returns the XCB connection associated
with an Xlib Display. Clients can use this XCB connection with functions
from the XCB library, just as they would with an XCB connection created
with XCB. Callers of this function must link to libX11-xcb and a version
of Xlib built with XCB support.

* SEE ALSO
XOpenDisplay(3), XSetEventQueueOwner(3),\\
/Xlib - C Language X Interface/
