#+TITLE: Manpages - FcAtomicReplaceOrig.3
#+DESCRIPTION: Linux manpage for FcAtomicReplaceOrig.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcAtomicReplaceOrig - replace original with new

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcAtomicReplaceOrig (FcAtomic */atomic/*);*

* DESCRIPTION
Replaces the original file referenced by /atomic/ with the new file.
Returns FcFalse if the file cannot be replaced due to permission issues
in the filesystem. Otherwise returns FcTrue.
