#+TITLE: Manpages - srand.3
#+DESCRIPTION: Linux manpage for srand.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about srand.3 is found in manpage for: [[../man3/rand.3][man3/rand.3]]