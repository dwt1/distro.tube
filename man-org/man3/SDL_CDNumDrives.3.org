#+TITLE: Manpages - SDL_CDNumDrives.3
#+DESCRIPTION: Linux manpage for SDL_CDNumDrives.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CDNumDrives - Returns the number of CD-ROM drives on the system.

* SYNOPSIS
*#include "SDL.h"*

*int SDL_CDNumDrives*(*void*)

* DESCRIPTION
Returns the number of CD-ROM drives on the system.

* SEE ALSO
*SDL_CDOpen*
