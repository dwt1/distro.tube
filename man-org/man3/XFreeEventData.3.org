#+TITLE: Manpages - XFreeEventData.3
#+DESCRIPTION: Linux manpage for XFreeEventData.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeEventData.3 is found in manpage for: [[../man3/XGetEventData.3][man3/XGetEventData.3]]