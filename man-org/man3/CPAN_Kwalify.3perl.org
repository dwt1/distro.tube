#+TITLE: Manpages - CPAN_Kwalify.3perl
#+DESCRIPTION: Linux manpage for CPAN_Kwalify.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
CPAN::Kwalify - Interface between CPAN.pm and Kwalify.pm

* SYNOPSIS
use CPAN::Kwalify; validate($schema_name, $data, $file, $doc);

* DESCRIPTION
- _validate($schema_name, $data, $file, $doc) :: =$schema_name= is the
  name of a supported schema. Currently only =distroprefs= is supported.
  =$data= is the data to be validated. =$file= is the absolute path to
  the file the data are coming from. =$doc= is the index of the document
  within =$doc= that is to be validated. The last two arguments are only
  there for better error reporting. Relies on being called from within
  CPAN.pm. Dies if something fails. Does not return anything useful.

- yaml($schema_name) :: Returns the YAML text of that schema. Dies if
  something fails.

* AUTHOR
Andreas Koenig =<andk@cpan.org>=

* LICENSE
This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

See <http://www.perl.com/perl/misc/Artistic.html>
