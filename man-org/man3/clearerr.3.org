#+TITLE: Manpages - clearerr.3
#+DESCRIPTION: Linux manpage for clearerr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about clearerr.3 is found in manpage for: [[../man3/ferror.3][man3/ferror.3]]