#+TITLE: Manpages - if_freenameindex.3
#+DESCRIPTION: Linux manpage for if_freenameindex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about if_freenameindex.3 is found in manpage for: [[../man3/if_nameindex.3][man3/if_nameindex.3]]