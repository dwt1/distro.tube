#+TITLE: Manpages - FcFontSetSort.3
#+DESCRIPTION: Linux manpage for FcFontSetSort.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFontSetSort - Add to a font set

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcFontSet * FcFontSetSort (FcConfig */config/*, FcFontSet ***/sets/*,
int*/nsets/*, FcPattern **/pattern/*, FcBool */trim/*, FcCharSet
***/csp/*, FcResult **/result/*);*

* DESCRIPTION
Returns the list of fonts from /sets/ sorted by closeness to /pattern/.
If /trim/ is FcTrue, elements in the list which don't include Unicode
coverage not provided by earlier elements in the list are elided. The
union of Unicode coverage of all of the fonts is returned in /csp/, if
/csp/ is not NULL. This function should be called only after
FcConfigSubstitute and FcDefaultSubstitute have been called for /p/;
otherwise the results will not be correct.

The returned FcFontSet references FcPattern structures which may be
shared by the return value from multiple FcFontSort calls, applications
cannot modify these patterns. Instead, they should be passed, along with
/pattern/ to *FcFontRenderPrepare* which combines them into a complete
pattern.

The FcFontSet returned by FcFontSetSort is destroyed by calling
FcFontSetDestroy.
