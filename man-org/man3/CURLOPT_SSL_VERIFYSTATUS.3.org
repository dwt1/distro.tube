#+TITLE: Manpages - CURLOPT_SSL_VERIFYSTATUS.3
#+DESCRIPTION: Linux manpage for CURLOPT_SSL_VERIFYSTATUS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_SSL_VERIFYSTATUS - verify the certificate's status

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_VERIFYSTATUS, long
verify);

* DESCRIPTION
Pass a long as parameter set to 1 to enable or 0 to disable.

This option determines whether libcurl verifies the status of the server
cert using the "Certificate Status Request" TLS extension (aka. OCSP
stapling).

Note that if this option is enabled but the server does not support the
TLS extension, the verification will fail.

* DEFAULT
0

* PROTOCOLS
All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
    /* ask for OCSP stapling! */
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 1L);
    ret = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.41.0. This option is currently only supported by the OpenSSL,
GnuTLS and NSS TLS backends.

* RETURN VALUE
Returns CURLE_OK if OCSP stapling is supported by the SSL backend,
otherwise returns CURLE_NOT_BUILT_IN.

* SEE ALSO
*CURLOPT_SSL_VERIFYHOST*(3), *CURLOPT_SSL_VERIFYPEER*(3),
*CURLOPT_CAINFO*(3),
