#+TITLE: Manpages - archive_write_filter.3
#+DESCRIPTION: Linux manpage for archive_write_filter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Streaming Archive Library (libarchive, -larchive)

The resulting archive will be compressed as specified. Note that the
compressed output is always properly blocked.

The output will be encoded as specified. The encoded output is always
properly blocked.

This is never necessary. It is provided only for backwards
compatibility.

The archive will be fed into the specified compression program. The
output of that program is blocked and written to the client write
callbacks.

These functions return

on success, or

Detailed error codes and textual descriptions are available from the

and

functions.
