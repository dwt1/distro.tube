#+TITLE: Manpages - Unicode_Collate_CJK_Big5.3perl
#+DESCRIPTION: Linux manpage for Unicode_Collate_CJK_Big5.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Unicode::Collate::CJK::Big5 - weighting CJK Unified Ideographs for
Unicode::Collate

* SYNOPSIS
use Unicode::Collate; use Unicode::Collate::CJK::Big5; my $collator =
Unicode::Collate->new( overrideCJK =>
\&Unicode::Collate::CJK::Big5::weightBig5 );

* DESCRIPTION
=Unicode::Collate::CJK::Big5= provides =weightBig5()=, that is adequate
for =overrideCJK= of =Unicode::Collate= and makes tailoring of CJK
Unified Ideographs in the order of CLDR's big5han ordering.

* SEE ALSO
- CLDR - Unicode Common Locale Data
  Repository :: <http://cldr.unicode.org/>

- Unicode Locale Data Markup Language (LDML) - UTS
  #35 :: <http://www.unicode.org/reports/tr35/>

- Unicode::Collate :: 

- Unicode::Collate::Locale :: 
