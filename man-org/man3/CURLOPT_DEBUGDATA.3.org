#+TITLE: Manpages - CURLOPT_DEBUGDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_DEBUGDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_DEBUGDATA - pointer passed to the debug callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DEBUGDATA, void
*pointer);

* DESCRIPTION
Pass a /pointer/ to whatever you want passed in to your
/CURLOPT_DEBUGFUNCTION(3)/ in the last void * argument. This pointer is
not used by libcurl, it is only passed to the callback.

* DEFAULT
NULL

* PROTOCOLS
All

* EXAMPLE
#+begin_example
  int main(void)
  {
    CURL *curl;
    CURLcode res;
    struct data my_tracedata;

    curl = curl_easy_init();
    if(curl) {
      curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);

      curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &my_tracedata);

      /* the DEBUGFUNCTION has no effect until we enable VERBOSE */
      curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

      curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
      res = curl_easy_perform(curl);

      /* always cleanup */
      curl_easy_cleanup(curl);
    }
    return 0;
  }
#+end_example

* AVAILABILITY
Always

* RETURN VALUE
Returns CURLE_OK

* SEE ALSO
*CURLOPT_STDERR*(3), *CURLOPT_DEBUGFUNCTION*(3),
