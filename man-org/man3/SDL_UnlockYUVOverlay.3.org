#+TITLE: Manpages - SDL_UnlockYUVOverlay.3
#+DESCRIPTION: Linux manpage for SDL_UnlockYUVOverlay.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_UnlockYUVOverlay - Unlock an overlay

* SYNOPSIS
*#include "SDL.h"*

*void SDL_UnlockYUVOverlay*(*SDL_Overlay *overlay*);

* DESCRIPTION
The opposite to *SDL_LockYUVOverlay*. Unlocks a previously locked
overlay. An overlay must be unlocked before it can be displayed.

* SEE ALSO
*SDL_UnlockYUVOverlay*, *SDL_CreateYUVOverlay*, *SDL_Overlay*
