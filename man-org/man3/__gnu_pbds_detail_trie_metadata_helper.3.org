#+TITLE: Manpages - __gnu_pbds_detail_trie_metadata_helper.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_trie_metadata_helper.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::trie_metadata_helper< Node_Update, _BTp > - Trie
metadata helper.

* SYNOPSIS
\\

* Detailed Description
** "template<typename Node_Update, bool _BTp>
\\
struct __gnu_pbds::detail::trie_metadata_helper< Node_Update, _BTp
>"Trie metadata helper.

Definition at line *58* of file
*trie_policy/node_metadata_selector.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
