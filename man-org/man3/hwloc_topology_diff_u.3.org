#+TITLE: Manpages - hwloc_topology_diff_u.3
#+DESCRIPTION: Linux manpage for hwloc_topology_diff_u.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
hwloc_topology_diff_u

* SYNOPSIS
\\

=#include <diff.h>=

** Data Structures
struct *hwloc_topology_diff_generic_s*\\

struct *hwloc_topology_diff_obj_attr_s*\\

struct *hwloc_topology_diff_too_complex_s*\\

** Data Fields
struct *hwloc_topology_diff_u::hwloc_topology_diff_generic_s*
*generic*\\

struct *hwloc_topology_diff_u::hwloc_topology_diff_obj_attr_s*
*obj_attr*\\

struct *hwloc_topology_diff_u::hwloc_topology_diff_too_complex_s*
*too_complex*\\

* Detailed Description
One element of a difference list between two topologies.

* Field Documentation
** struct *hwloc_topology_diff_u::hwloc_topology_diff_generic_s*
hwloc_topology_diff_u::generic
** struct *hwloc_topology_diff_u::hwloc_topology_diff_obj_attr_s*
hwloc_topology_diff_u::obj_attr
** struct *hwloc_topology_diff_u::hwloc_topology_diff_too_complex_s*
hwloc_topology_diff_u::too_complex
* Author
Generated automatically by Doxygen for Hardware Locality (hwloc) from
the source code.
