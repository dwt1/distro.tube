#+TITLE: Manpages - XML_LibXML_Number.3pm
#+DESCRIPTION: Linux manpage for XML_LibXML_Number.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
XML::LibXML::Number - Simple numeric values.

* DESCRIPTION
This class holds simple numeric values. It doesn't support -0, +/-
Infinity, or NaN, as the XPath spec says it should, but I'm not hurting
anyone I don't think.

* API
** new($num)
Creates a new XML::LibXML::Number object, with the value in =$num=. Does
some rudimentary numeric checking on =$num= to ensure it actually is a
number.

** *value()*
Also as overloaded stringification. Returns the numeric value held.
