#+TITLE: Manpages - ieee1284_claim.3
#+DESCRIPTION: Linux manpage for ieee1284_claim.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ieee1284_claim - claim access to the port

* SYNOPSIS
#+begin_example
  #include <ieee1284.h>
#+end_example

*int ieee1284_claim(struct parport **/port/*);*

* DESCRIPTION
With the exception of *ieee1284_get_deviceid*(3), *ieee1284_claim* must
be called on an open port before any other libieee1284 function for
accessing a device on it.

* RETURN VALUE
*E1284_OK*

#+begin_quote
  Success. Note that, unless the *F1284_EXCL* flag was specified to
  start with, the port should be released within a “reasonable” amount
  of time.
#+end_quote

*E1284_NOMEM*

#+begin_quote
  There is not enough memory.
#+end_quote

*E1284_INVALIDPORT*

#+begin_quote
  The /port/ parameter is invalid (for instance, it might not have been
  opened yet).
#+end_quote

*E1284_SYS*

#+begin_quote
  There was a problem at the operating system level. The global variable
  /errno/ has been set appropriately.
#+end_quote

* SEE ALSO
*ieee1284_release*(3)

* AUTHOR
*Tim Waugh* <twaugh@redhat.com>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2001-2003 Tim Waugh\\
