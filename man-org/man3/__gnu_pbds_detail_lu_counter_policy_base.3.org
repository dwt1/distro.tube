#+TITLE: Manpages - __gnu_pbds_detail_lu_counter_policy_base.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_detail_lu_counter_policy_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::detail::lu_counter_policy_base< Size_Type > - Base class for
list-update counter policy.

* SYNOPSIS
\\

=#include <lu_counter_metadata.hpp>=

** Protected Types
typedef Size_Type *size_type*\\

** Protected Member Functions
template<typename Metadata_Reference > bool *operator()*
(Metadata_Reference r_data, size_type m_max_count) const\\

*lu_counter_metadata*< size_type > *operator()* (size_type max_size)
const\\

* Detailed Description
** "template<typename Size_Type>
\\
class __gnu_pbds::detail::lu_counter_policy_base< Size_Type >"Base class
for list-update counter policy.

Definition at line *67* of file *lu_counter_metadata.hpp*.

* Member Typedef Documentation
** template<typename Size_Type > typedef Size_Type
*__gnu_pbds::detail::lu_counter_policy_base*< Size_Type
>::size_type= [protected]=
Definition at line *70* of file *lu_counter_metadata.hpp*.

* Member Function Documentation
** template<typename Size_Type > template<typename Metadata_Reference >
bool *__gnu_pbds::detail::lu_counter_policy_base*< Size_Type
>::operator() (Metadata_Reference r_data, size_type m_max_count)
const= [inline]=, = [protected]=
Definition at line *78* of file *lu_counter_metadata.hpp*.

** template<typename Size_Type > *lu_counter_metadata*< size_type >
*__gnu_pbds::detail::lu_counter_policy_base*< Size_Type >::operator()
(size_type max_size) const= [inline]=, = [protected]=
Definition at line *73* of file *lu_counter_metadata.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
