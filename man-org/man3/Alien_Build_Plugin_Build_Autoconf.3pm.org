#+TITLE: Manpages - Alien_Build_Plugin_Build_Autoconf.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_Plugin_Build_Autoconf.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::Plugin::Build::Autoconf - Autoconf plugin for Alien::Build

* VERSION
version 2.44

* SYNOPSIS
use alienfile; plugin Build::Autoconf;

* DESCRIPTION
This plugin provides some tools for building projects that use autoconf.
The main thing this provides is a =configure= helper, documented below
and the default build stage, which is:

%{configure} --disable-shared, %{make}, %{make} install,

On Windows, this plugin also pulls in the
Alien::Build::Plugin::Build::MSYS which is required for autoconf style
projects on windows.

The other thing that this plugin does is that it does a double staged
=DESTDIR= install. The author has found this improves the overall
reliability of Alien modules that are based on autoconf packages.

This plugin supports out-of-source builds (known in autoconf terms as
VPATH builds) via the meta property =out_of_source=.

* PROPERTIES
** with_pic
Adds =--with-pic= option when running =configure=. If supported by your
package, it will generate position independent code on platforms that
support it. This is required to XS modules, and generally what you want.

autoconf normally ignores options that it does not understand, so it is
usually a safe and reasonable default to include it. A small number of
projects look like they use autoconf, but are really an autoconf style
interface with a different implementation. They may fail if you try to
provide it with options such as =--with-pic= that they do not recognize.
Such packages are the rationale for this property.

** msys_version
The version of Alien::MSYS required if it is deemed necessary. If
Alien::MSYS isn't needed (if running under Unix, or MSYS2, for example)
this will do nothing.

** config_site
The content for the generated =config.site=.

* HELPERS
** configure
%{configure}

The correct incantation to start an autoconf style =configure= script on
your platform. Some reasonable default flags will be provided.

* ENVIRONMENT
- "ALIEN_BUILD_SITE_CONFIG" :: This plugin needs to alter the behavior
  of autotools via the =site.config= file and so sets and possibly
  overrides any existing =SITE_CONFIG=. Normally that is what you want
  but you can also insert your own =site.config= in addition by using
  this environment variable.

* SEE ALSO
Alien::Build::Plugin::Build::MSYS, Alien::Build::Plugin, Alien::Build,
Alien::Base, Alien

<https://www.gnu.org/software/autoconf/autoconf.html>

<https://www.gnu.org/prep/standards/html_node/DESTDIR.html>

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
