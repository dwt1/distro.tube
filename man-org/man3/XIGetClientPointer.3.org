#+TITLE: Manpages - XIGetClientPointer.3
#+DESCRIPTION: Linux manpage for XIGetClientPointer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIGetClientPointer.3 is found in manpage for: [[../man3/XISetClientPointer.3][man3/XISetClientPointer.3]]