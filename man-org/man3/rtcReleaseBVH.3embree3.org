#+TITLE: Manpages - rtcReleaseBVH.3embree3
#+DESCRIPTION: Linux manpage for rtcReleaseBVH.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcReleaseBVH - decrements the BVH reference count
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcReleaseBVH(RTCBVH bvh);
#+end_example

** DESCRIPTION
BVH objects are reference counted. The =rtcReleaseBVH= function
decrements the reference count of the passed BVH object (=bvh=
argument). When the reference count falls to 0, the BVH gets destroyed.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewBVH], [rtcRetainBVH]
