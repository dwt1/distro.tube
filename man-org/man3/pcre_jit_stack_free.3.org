#+TITLE: Manpages - pcre_jit_stack_free.3
#+DESCRIPTION: Linux manpage for pcre_jit_stack_free.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
PCRE - Perl-compatible regular expressions

* SYNOPSIS
*#include <pcre.h>*

*void pcre_jit_stack_free(pcre_jit_stack */stack/);*

*void pcre16_jit_stack_free(pcre16_jit_stack */stack/);*

*void pcre32_jit_stack_free(pcre32_jit_stack */stack/);*

* DESCRIPTION
This function is used to free a JIT stack that was created by
*pcre[16|32]_jit_stack_alloc()* when it is no longer needed. For more
details, see the *pcrejit* page.

There is a complete description of the PCRE native API in the *pcreapi*
page and a description of the POSIX API in the *pcreposix* page.
