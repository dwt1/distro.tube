#+TITLE: Manpages - std_codecvt_base.3
#+DESCRIPTION: Linux manpage for std_codecvt_base.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::codecvt_base - Empty base class for codecvt facet [22.2.1.5].

* SYNOPSIS
\\

=#include <codecvt.h>=

Inherited by *std::__codecvt_abstract_base< _InternT, _ExternT,
encoding_state >*, *std::__codecvt_abstract_base< char, char, mbstate_t
>*, *std::__codecvt_abstract_base< char16_t, char, mbstate_t >*,
*std::__codecvt_abstract_base< char32_t, char, mbstate_t >*,
*std::__codecvt_abstract_base< wchar_t, char, mbstate_t >*, and
*std::__codecvt_abstract_base< _InternT, _ExternT, _StateT >*.

** Public Types
enum *result* { *ok*, *partial*, *error*, *noconv* }\\

* Detailed Description
Empty base class for codecvt facet [22.2.1.5].

Definition at line *49* of file *codecvt.h*.

* Member Enumeration Documentation
** enum std::codecvt_base::result
Definition at line *52* of file *codecvt.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
