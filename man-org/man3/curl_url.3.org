#+TITLE: Manpages - curl_url.3
#+DESCRIPTION: Linux manpage for curl_url.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_url - returns a new CURLU handle

* SYNOPSIS
*#include <curl/curl.h>*

CURLU *curl_url();

* DESCRIPTION
This function will allocates and returns a pointer to a fresh CURLU
handle, to be used for further use of the URL API.

* EXAMPLE
#+begin_example
    CURLUcode rc;
    CURLU *url = curl_url();
    rc = curl_url_set(url, CURLUPART_URL, "https://example.com", 0);
    if(!rc) {
      char *scheme;
      rc = curl_url_get(url, CURLUPART_SCHEME, &scheme, 0);
      if(!rc) {
        printf("the scheme is %s\n", scheme);
        curl_free(scheme);
      }
      curl_url_cleanup(url);
    }
#+end_example

* AVAILABILITY
Added in 7.62.0

* RETURN VALUE
Returns a *CURLU ** if successful, or NULL if out of memory.

* SEE ALSO
*curl_url_cleanup*(3), *curl_url_get*(3), *curl_url_set*(3),
*curl_url_dup*(3), *curl_url_strerror*(3), *CURLOPT_CURLU*(3)
