#+TITLE: Manpages - unvis.3bsd
#+DESCRIPTION: Linux manpage for unvis.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
(See

for include usage.)

The

and

functions are used to decode a visual representation of characters, as
produced by the

function, back into the original form.

The

function is called with successive characters in

until a valid sequence is recognized, at which time the decoded
character is available at the character pointed to by

The

function decodes the characters pointed to by

into the buffer pointed to by

The

function simply copies

to

decoding any escape sequences along the way, and returns the number of
characters placed into

or -1 if an invalid escape sequence was detected. The size of

should be equal to the size of

(that is, no expansion takes place during decoding).

The

function does the same as the

function, but it allows you to add a flag that specifies the style the
string

is encoded with. Currently, the supported flags are:

and

The

function implements a state machine that can be used to decode an
arbitrary stream of bytes. All state associated with the bytes being
decoded is stored outside the

function (that is, a pointer to the state is passed in), so calls
decoding different streams can be freely intermixed. To start decoding a
stream of bytes, first initialize an integer to zero. Call

with each successive byte, along with a pointer to this integer, and a
pointer to a destination character. The

function has several return codes that must be handled properly. They
are:

Another character is necessary; nothing has been recognized yet.

A valid character has been recognized and is available at the location
pointed to by

A valid character has been recognized and is available at the location
pointed to by

however, the character currently passed in should be passed in again.

A valid sequence was detected, but no character was produced. This
return code is necessary to indicate a logical break between characters.

An invalid escape sequence was detected, or the decoder is in an unknown
state. The decoder is placed into the starting state.

When all bytes in the stream have been processed, call

one more time with flag set to

to extract any remaining character (the character passed in is ignored).

The

argument is also used to specify the encoding style of the source. If
set to

or

will decode URI strings as specified in RFC 1808. If set to

will decode entity references and numeric character references as
specified in RFC 1866. If set to

will decode MIME Quoted-Printable strings as specified in RFC 2045. If
set to

will not decode

quoted characters.

The following code fragment illustrates a proper use of

int state = 0; char out;

while ((ch = getchar()) != EOF) { again: switch(unvis(&out, ch, &state,
0)) { case 0: case UNVIS_NOCHAR: break; case UNVIS_VALID:
(void)putchar(out); break; case UNVIS_VALIDPUSH: (void)putchar(out);
goto again; case UNVIS_SYNBAD: errx(EXIT_FAILURE, "Bad character
sequence!"); } } if (unvis(&out, '\0', &state, UNVIS_END) ==
UNVIS_VALID) (void)putchar(out);

The functions

and

will return -1 on error and set

to:

An invalid escape sequence was detected, or the decoder is in an unknown
state.

In addition the functions

and

will can also set

on error to:

Not enough space to perform the conversion.

The

function first appeared in

The

and

functions appeared in

The names

and

are wrong. Percent-encoding was defined in RFC 1738, the original RFC
for URL. RFC 1866 defines HTML 2.0, an application of SGML, from which
it inherits concepts of numeric character references and entity
references.
