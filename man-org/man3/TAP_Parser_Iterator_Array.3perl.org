#+TITLE: Manpages - TAP_Parser_Iterator_Array.3perl
#+DESCRIPTION: Linux manpage for TAP_Parser_Iterator_Array.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
TAP::Parser::Iterator::Array - Iterator for array-based TAP sources

* VERSION
Version 3.43

* SYNOPSIS
use TAP::Parser::Iterator::Array; my @data = (foo, bar, baz); my $it =
TAP::Parser::Iterator::Array->new(\@data); my $line = $it->next;

* DESCRIPTION
This is a simple iterator wrapper for arrays of scalar content, used by
TAP::Parser. Unless you're writing a plugin or subclassing, you probably
won't need to use this module directly.

* METHODS
** Class Methods
/=new=/

Create an iterator. Takes one argument: an =$array_ref=

** Instance Methods
/=next=/

Iterate through it, of course.

/=next_raw=/

Iterate raw input without applying any fixes for quirky input syntax.

/=wait=/

Get the wait status for this iterator. For an array iterator this will
always be zero.

/=exit=/

Get the exit status for this iterator. For an array iterator this will
always be zero.

* ATTRIBUTION
Originally ripped off from Test::Harness.

* SEE ALSO
TAP::Object, TAP::Parser, TAP::Parser::Iterator,
