#+TITLE: Manpages - XIUndefineCursor.3
#+DESCRIPTION: Linux manpage for XIUndefineCursor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XIUndefineCursor.3 is found in manpage for: [[../man3/XIDefineCursor.3][man3/XIDefineCursor.3]]