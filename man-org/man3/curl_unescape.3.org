#+TITLE: Manpages - curl_unescape.3
#+DESCRIPTION: Linux manpage for curl_unescape.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_unescape - URL decodes the given string

* SYNOPSIS
*#include <curl/curl.h>*

*char *curl_unescape( const char **/url/*, int */length/* );*

* DESCRIPTION
Obsolete function. Use /curl_easy_unescape(3)/ instead!

This function will convert the given URL encoded input string to a
"plain string" and return that as a new allocated string. All input
characters that are URL encoded (%XX where XX is a two-digit hexadecimal
number) will be converted to their plain text versions.

If the 'length' argument is set to 0, curl_unescape() will use strlen()
on the input 'url' string to find out the size.

You must /curl_free(3)/ the returned string when you are done with it.

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    int decodelen;
    char *decoded = curl_unescape("%63%75%72%6c", 12, &decodelen);
    if(decoded) {
      /* do not assume printf() works on the decoded data! */
      printf("Decoded: ");
      /* ... */
      curl_free(decoded);
    }
  }
#+end_example

* AVAILABILITY
Since 7.15.4, /curl_easy_unescape(3)/ should be used. This function will
be removed in a future release.

* RETURN VALUE
A pointer to a null-terminated string or NULL if it failed.

* SEE ALSO
\\
