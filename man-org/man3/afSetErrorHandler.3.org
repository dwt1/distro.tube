#+TITLE: Manpages - afSetErrorHandler.3
#+DESCRIPTION: Linux manpage for afSetErrorHandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
afSetErrorHandler - install a custom error handling routine

* SYNOPSIS
#+begin_example
  #include <audiofile.h>
#+end_example

#+begin_example
  AFerrfunc afSetErrorHandler (AFerrfunc errorFunction);
#+end_example

* PARAMETERS
/errorFunction/ is a pointer to an error handling function.

* DESCRIPTION
afSetErrorHandler allows the user to install an error handling function
overriding the default function.

* RETURN VALUE
The value returned from afSetErrorHandler is a pointer to the previous
error handling function.

* AUTHOR
Michael Pruett <michael@68k.org>
