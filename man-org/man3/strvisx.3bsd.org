#+TITLE: Manpages - strvisx.3bsd
#+DESCRIPTION: Linux manpage for strvisx.3bsd
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about strvisx.3bsd is found in manpage for: [[../man3/vis.3bsd][man3/vis.3bsd]]