#+TITLE: Manpages - Net_Domain.3perl
#+DESCRIPTION: Linux manpage for Net_Domain.3perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Net::Domain - Attempt to evaluate the current host's internet name and
domain

* SYNOPSIS
use Net::Domain qw(hostname hostfqdn hostdomain domainname);

* DESCRIPTION
Using various methods *attempt* to find the Fully Qualified Domain Name
(FQDN) of the current host. From this determine the host-name and the
host-domain.

Each of the functions will return /undef/ if the FQDN cannot be
determined.

** Functions
- "hostfqdn()" :: Identify and return the FQDN of the current host.

- "domainname()" :: An alias for *hostfqdn()*.

- "hostname()" :: Returns the smallest part of the FQDN which can be
  used to identify the host.

- "hostdomain()" :: Returns the remainder of the FQDN after the
  /hostname/ has been removed.

* EXPORTS
The following symbols are, or can be, exported by this module:

- Default Exports :: /None/.

- Optional Exports :: =hostname=, =hostdomain=, =hostfqdn=,
  =domainname=.

- Export Tags :: /None/.

* KNOWN BUGS
See <https://rt.cpan.org/Dist/Display.html?Status=Active&Queue=libnet>.

* AUTHOR
Graham Barr <gbarr@pobox.com <mailto:gbarr@pobox.com>>.

Adapted from Sys::Hostname by David Sundstrom <sunds@asictest.sc.ti.com
<mailto:sunds@asictest.sc.ti.com>>.

Steve Hay <shay@cpan.org <mailto:shay@cpan.org>> is now maintaining
libnet as of version 1.22_02.

* COPYRIGHT
Copyright (C) 1995-1998 Graham Barr. All rights reserved.

Copyright (C) 2013-2014, 2020 Steve Hay. All rights reserved.

* LICENCE
This module is free software; you can redistribute it and/or modify it
under the same terms as Perl itself, i.e. under the terms of either the
GNU General Public License or the Artistic License, as specified in the
/LICENCE/ file.

* VERSION
Version 3.13

* DATE
23 Dec 2020

* HISTORY
See the /Changes/ file.
