#+TITLE: Manpages - curl_multi_strerror.3
#+DESCRIPTION: Linux manpage for curl_multi_strerror.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
curl_multi_strerror - return string describing error code

* SYNOPSIS
#+begin_example
  #include <curl/curl.h>
  const char *curl_multi_strerror(CURLMcode errornum);
#+end_example

* DESCRIPTION
The curl_multi_strerror() function returns a string describing the
CURLMcode error code passed in the argument /errornum/.

* EXAMPLE
#+begin_example
  int still_running;

  CURLMcode mc = curl_multi_perform(multi_handle, &still_running);
  if(mc)
    printf("error: %s\n", curl_multi_strerror(mc));
#+end_example

* AVAILABILITY
This function was added in libcurl 7.12.0

* RETURN VALUE
A pointer to a null-terminated string.

* SEE ALSO
*libcurl-errors*(3), *curl_easy_strerror*(3), *curl_share_strerror*(3),
*curl_url_strerror*(3)
