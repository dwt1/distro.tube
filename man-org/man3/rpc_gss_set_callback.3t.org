#+TITLE: Manpages - rpc_gss_set_callback.3t
#+DESCRIPTION: Linux manpage for rpc_gss_set_callback.3t
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Register a function which will be called when new security contexts are
created on a server. This function will be called on the first RPC
request which uses that context and has the opportunity of rejecting the
request (for instance after matching the request credentials to an
access control list). To accept the new security context, the callback
should return

otherwise

If the callback accepts a context, it becomes responsible for the
lifetime of the delegated client credentials (if any).

It is also possible to 'lock' the values of service and quality of
protection used by the context. If a context is locked, any subsequent
requests which use different values for service and quality of
protection will be rejected.

A structure containing the RPC program and version for this callback and
a function which will be called when new contexts are created for the
given RPC program and version

The RPC request using the new context

GSS-API delegated credentials (if any)

The GSS-API context

A structure used to enforce a particular QOP and service. Set

to

to lock the service and QOP values

The callback function may set

to any pointer sized value. This value can be accessed during the
lifetime of the context via

Returns

if the callback was registered successfully or

otherwise

The

function is part of libtirpc.

This manual page was written by

There is no mechanism for informing a server when a security context has
been deleted. This makes it difficult to allocate resources (e.g. to
return via the callback's

argument).
