#+TITLE: Manpages - closelog.3
#+DESCRIPTION: Linux manpage for closelog.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about closelog.3 is found in manpage for: [[../man3/syslog.3][man3/syslog.3]]