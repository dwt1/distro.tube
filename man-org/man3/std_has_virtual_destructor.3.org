#+TITLE: Manpages - std_has_virtual_destructor.3
#+DESCRIPTION: Linux manpage for std_has_virtual_destructor.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::has_virtual_destructor< _Tp > - has_virtual_destructor

* SYNOPSIS
\\

Inherits *std::integral_constant< bool, __has_virtual_destructor(_Tp)>*.

** Public Types
typedef *integral_constant*< bool, __v > *type*\\

typedef bool *value_type*\\

** Public Member Functions
constexpr *operator value_type* () const noexcept\\

constexpr value_type *operator()* () const noexcept\\

** Static Public Attributes
static constexpr bool *value*\\

* Detailed Description
** "template<typename _Tp>
\\
struct std::has_virtual_destructor< _Tp >"has_virtual_destructor

Definition at line *1298* of file *std/type_traits*.

* Member Typedef Documentation
** typedef *integral_constant*<bool , __v> *std::integral_constant*<
bool , __v >::*type*= [inherited]=
Definition at line *61* of file *std/type_traits*.

** typedef bool *std::integral_constant*< bool , __v
>::value_type= [inherited]=
Definition at line *60* of file *std/type_traits*.

* Member Function Documentation
** constexpr *std::integral_constant*< bool , __v >::operator value_type
() const= [inline]=, = [constexpr]=, = [noexcept]=, = [inherited]=
Definition at line *62* of file *std/type_traits*.

** constexpr value_type *std::integral_constant*< bool , __v
>::operator() () const= [inline]=, = [constexpr]=, = [noexcept]=,
= [inherited]=
Definition at line *67* of file *std/type_traits*.

* Member Data Documentation
** constexpr bool *std::integral_constant*< bool , __v
>::value= [static]=, = [constexpr]=, = [inherited]=
Definition at line *59* of file *std/type_traits*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
