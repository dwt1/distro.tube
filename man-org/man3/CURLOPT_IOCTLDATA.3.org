#+TITLE: Manpages - CURLOPT_IOCTLDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_IOCTLDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_IOCTLDATA - pointer passed to I/O callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IOCTLDATA, void
*pointer);

* DESCRIPTION
Pass the /pointer/ that will be untouched by libcurl and passed as the
3rd argument in the ioctl callback set with /CURLOPT_IOCTLFUNCTION(3)/.

* DEFAULT
By default, the value of this parameter is NULL.

* PROTOCOLS
Used with HTTP

* EXAMPLE
#+begin_example
  static curlioerr ioctl_callback(CURL *handle, int cmd, void *clientp)
  {
    struct data *io = (struct data *)clientp;
    if(cmd == CURLIOCMD_RESTARTREAD) {
      lseek(fd, 0, SEEK_SET);
      current_offset = 0;
      return CURLIOE_OK;
    }
    return CURLIOE_UNKNOWNCMD;
  }
  {
    struct data ioctl_data;
    curl_easy_setopt(curl, CURLOPT_IOCTLFUNCTION, ioctl_callback);
    curl_easy_setopt(curl, CURLOPT_IOCTLDATA, &ioctl_data);
  }
#+end_example

* AVAILABILITY
Added in 7.12.3

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_IOCTLFUNCTION*(3), *CURLOPT_SEEKFUNCTION*(3),
