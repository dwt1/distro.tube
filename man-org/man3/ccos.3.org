#+TITLE: Manpages - ccos.3
#+DESCRIPTION: Linux manpage for ccos.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ccos, ccosf, ccosl - complex cosine function

* SYNOPSIS
#+begin_example
  #include <complex.h>

  double complex ccos(double complex z);
  float complex ccosf(float complex z);
  long double complex ccosl(long double complex z);

  Link with -lm.
#+end_example

* DESCRIPTION
These functions calculate the complex cosine of /z/.

The complex cosine function is defined as:

#+begin_example
      ccos(z) = (exp(i * z) + exp(-i * z)) / 2
#+end_example

* VERSIONS
These functions first appeared in glibc in version 2.1.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface                      | Attribute     | Value   |
| *ccos*(), *ccosf*(), *ccosl*() | Thread safety | MT-Safe |

* CONFORMING TO
C99, POSIX.1-2001, POSIX.1-2008.

* SEE ALSO
*cabs*(3), *cacos*(3), *csin*(3), *ctan*(3), *complex*(7)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
