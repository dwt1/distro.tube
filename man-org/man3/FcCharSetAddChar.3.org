#+TITLE: Manpages - FcCharSetAddChar.3
#+DESCRIPTION: Linux manpage for FcCharSetAddChar.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcCharSetAddChar - Add a character to a charset

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcCharSetAddChar (FcCharSet */fcs/*, FcChar32 */ucs4/*);*

* DESCRIPTION
*FcCharSetAddChar* adds a single Unicode char to the set, returning
FcFalse on failure, either as a result of a constant set or from running
out of memory.
