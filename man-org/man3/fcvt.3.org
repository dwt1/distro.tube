#+TITLE: Manpages - fcvt.3
#+DESCRIPTION: Linux manpage for fcvt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about fcvt.3 is found in manpage for: [[../man3/ecvt.3][man3/ecvt.3]]