#+TITLE: Manpages - ldns_rr_set_push_rr.3
#+DESCRIPTION: Linux manpage for ldns_rr_set_push_rr.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_set_push_rr, ldns_rr_set_pop_rr - push and pop rr on a rrset

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

bool ldns_rr_set_push_rr(ldns_rr_list *rr_list, ldns_rr *rr);

ldns_rr* ldns_rr_set_pop_rr(ldns_rr_list *rr_list);

* DESCRIPTION
/ldns_rr_set_push_rr/() pushes an rr to an rrset (which really are
rr_list's). .br **rr_list*: the rrset to push the rr to .br **rr*: the
rr to push .br Returns true if the push succeeded otherwise false

/ldns_rr_set_pop_rr/() pops the last rr from an rrset. This function is
there only for the symmetry. .br *rr_list*: the rr_list to pop from .br
Returns NULL if nothing to pop. Otherwise the popped RR

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/, /ldns_rr_list/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
