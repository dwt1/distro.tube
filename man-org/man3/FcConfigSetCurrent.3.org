#+TITLE: Manpages - FcConfigSetCurrent.3
#+DESCRIPTION: Linux manpage for FcConfigSetCurrent.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcConfigSetCurrent - Set configuration as default

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcBool FcConfigSetCurrent (FcConfig */config/*);*

* DESCRIPTION
Sets the current default configuration to /config/. Implicitly calls
FcConfigBuildFonts if necessary, and FcConfigReference() to inrease the
reference count in /config/ since 2.12.0, returning FcFalse if that call
fails.
