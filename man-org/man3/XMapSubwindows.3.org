#+TITLE: Manpages - XMapSubwindows.3
#+DESCRIPTION: Linux manpage for XMapSubwindows.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XMapSubwindows.3 is found in manpage for: [[../man3/XMapWindow.3][man3/XMapWindow.3]]