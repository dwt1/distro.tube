#+TITLE: Manpages - nearbyint.3
#+DESCRIPTION: Linux manpage for nearbyint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about nearbyint.3 is found in manpage for: [[../man3/rint.3][man3/rint.3]]