#+TITLE: Manpages - pam_sm_setcred.3
#+DESCRIPTION: Linux manpage for pam_sm_setcred.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_sm_setcred - PAM service function to alter credentials

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_sm_setcred(pam_handle_t **/pamh/*, int */flags/*, int */argc/*,
const char ***/argv/*);*

* DESCRIPTION
The *pam_sm_setcred* function is the service modules implementation of
the *pam_setcred*(3) interface.

This function performs the task of altering the credentials of the user
with respect to the corresponding authorization scheme. Generally, an
authentication module may have access to more information about a user
than their authentication token. This function is used to make such
information available to the application. It should only be called
/after/ the user has been authenticated but before a session has been
established.

Valid flags, which may be logically ORd with /PAM_SILENT/, are:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_ESTABLISH_CRED

#+begin_quote
  Initialize the credentials for the user.
#+end_quote

PAM_DELETE_CRED

#+begin_quote
  Delete the credentials associated with the authentication service.
#+end_quote

PAM_REINITIALIZE_CRED

#+begin_quote
  Reinitialize the user credentials.
#+end_quote

PAM_REFRESH_CRED

#+begin_quote
  Extend the lifetime of the user credentials.
#+end_quote

The way the *auth* stack is navigated in order to evaluate the
*pam_setcred*() function call, independent of the *pam_sm_setcred*()
return codes, is exactly the same way that it was navigated when
evaluating the *pam_authenticate*() library call. Typically, if a stack
entry was ignored in evaluating *pam_authenticate*(), it will be ignored
when libpam evaluates the *pam_setcred*() function call. Otherwise, the
return codes from each module specific *pam_sm_setcred*() call are
treated as *required*.

* RETURN VALUES
PAM_CRED_UNAVAIL

#+begin_quote
  This module cannot retrieve the users credentials.
#+end_quote

PAM_CRED_EXPIRED

#+begin_quote
  The users credentials have expired.
#+end_quote

PAM_CRED_ERR

#+begin_quote
  This module was unable to set the credentials of the user.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The user credential was successfully set.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  The user is not known to this authentication module.
#+end_quote

These, non-/PAM_SUCCESS/, return values will typically lead to the
credential stack /failing/. The first such error will dominate in the
return value of *pam_setcred*().

* SEE ALSO
*pam*(3), *pam_authenticate*(3), *pam_setcred*(3),
*pam_sm_authenticate*(3), *pam_strerror*(3), *PAM*(8)
