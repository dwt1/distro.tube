#+TITLE: Manpages - CURLOPT_DOH_SSL_VERIFYSTATUS.3
#+DESCRIPTION: Linux manpage for CURLOPT_DOH_SSL_VERIFYSTATUS.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_DOH_SSL_VERIFYSTATUS - verify the DoH SSL certificate's status

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_SSL_VERIFYSTATUS,
long verify);

* DESCRIPTION
Pass a long as parameter set to 1 to enable or 0 to disable.

This option determines whether libcurl verifies the status of the DoH
(DNS-over-HTTPS) server cert using the "Certificate Status Request" TLS
extension (aka. OCSP stapling).

This option is the DoH equivalent of /CURLOPT_SSL_VERIFYSTATUS(3)/ and
only affects requests to the DoH server.

Note that if this option is enabled but the server does not support the
TLS extension, the verification will fail.

* DEFAULT
0

* PROTOCOLS
DoH

* EXAMPLE
#+begin_example
  CURL *curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

    curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://cloudflare-dns.com/dns-query");

    /* Ask for OCSP stapling when verifying the DoH server */
    curl_easy_setopt(curl, CURLOPT_DOH_SSL_VERIFYSTATUS, 1L);

    curl_easy_perform(curl);
  }
#+end_example

* AVAILABILITY
Added in 7.76.0. This option is currently only supported by the OpenSSL,
GnuTLS and NSS TLS backends.

* RETURN VALUE
Returns CURLE_OK if OCSP stapling is supported by the SSL backend,
otherwise returns CURLE_NOT_BUILT_IN.

* SEE ALSO
*CURLOPT_DOH_SSL_VERIFYHOST*(3), *CURLOPT_DOH_SSL_VERIFYPEER*(3),
*CURLOPT_SSL_VERIFYSTATUS*(3),
