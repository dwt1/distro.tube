#+TITLE: Manpages - __gnu_parallel_sequential_tag.3
#+DESCRIPTION: Linux manpage for __gnu_parallel_sequential_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::sequential_tag - Forces sequential execution at compile
time.

* SYNOPSIS
\\

=#include <tags.h>=

* Detailed Description
Forces sequential execution at compile time.

Definition at line *42* of file *tags.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
