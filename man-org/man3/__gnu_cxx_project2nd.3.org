#+TITLE: Manpages - __gnu_cxx_project2nd.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_project2nd.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::project2nd< _Arg1, _Arg2 > - An *SGI extension *.

* SYNOPSIS
\\

Inherits __gnu_cxx::_Project2nd< _Arg1, _Arg2 >.

** Public Types
typedef _Arg1 *first_argument_type*\\
=first_argument_type= is the type of the first argument

typedef _Arg2 *result_type*\\
=result_type= is the return type

typedef _Arg2 *second_argument_type*\\
=second_argument_type= is the type of the second argument

** Public Member Functions
_Arg2 *operator()* (const _Arg1 &, const _Arg2 &__y) const\\

* Detailed Description
** "template<class _Arg1, class _Arg2>
\\
struct __gnu_cxx::project2nd< _Arg1, _Arg2 >"An *SGI extension *.

Definition at line *233* of file *ext/functional*.

* Member Typedef Documentation
** typedef _Arg1 *std::binary_function*< _Arg1, _Arg2, _Arg2
>::*first_argument_type*= [inherited]=
=first_argument_type= is the type of the first argument

Definition at line *121* of file *stl_function.h*.

** typedef _Arg2 *std::binary_function*< _Arg1, _Arg2, _Arg2
>::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *127* of file *stl_function.h*.

** typedef _Arg2 *std::binary_function*< _Arg1, _Arg2, _Arg2
>::*second_argument_type*= [inherited]=
=second_argument_type= is the type of the second argument

Definition at line *124* of file *stl_function.h*.

* Member Function Documentation
** template<class _Arg1 , class _Arg2 > _Arg2 __gnu_cxx::_Project2nd<
_Arg1, _Arg2 >::operator() (const _Arg1 &, const _Arg2 & __y)
const= [inline]=, = [inherited]=
Definition at line *215* of file *ext/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
