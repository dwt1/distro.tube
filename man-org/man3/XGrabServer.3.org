#+TITLE: Manpages - XGrabServer.3
#+DESCRIPTION: Linux manpage for XGrabServer.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XGrabServer, XUngrabServer - grab the server

* SYNTAX
int XGrabServer ( Display */display/ );

int XUngrabServer ( Display */display/ );

* ARGUMENTS
- display :: Specifies the connection to the X server.

* DESCRIPTION
The *XGrabServer* function disables processing of requests and close
downs on all other connections than the one this request arrived on. You
should not grab the X server any more than is absolutely necessary.

The *XUngrabServer* function restarts processing of requests and close
downs on other connections. You should avoid grabbing the X server as
much as possible.

* SEE ALSO
XGrabButton(3), XGrabKey(3), XGrabKeyboard(3), XGrabPointer(3)\\
/Xlib - C Language X Interface/
