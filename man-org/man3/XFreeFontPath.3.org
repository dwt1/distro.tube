#+TITLE: Manpages - XFreeFontPath.3
#+DESCRIPTION: Linux manpage for XFreeFontPath.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFreeFontPath.3 is found in manpage for: [[../man3/XSetFontPath.3][man3/XSetFontPath.3]]