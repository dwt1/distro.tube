#+TITLE: Manpages - setusershell.3
#+DESCRIPTION: Linux manpage for setusershell.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about setusershell.3 is found in manpage for: [[../man3/getusershell.3][man3/getusershell.3]]