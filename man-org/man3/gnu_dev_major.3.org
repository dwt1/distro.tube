#+TITLE: Manpages - gnu_dev_major.3
#+DESCRIPTION: Linux manpage for gnu_dev_major.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about gnu_dev_major.3 is found in manpage for: [[../man3/makedev.3][man3/makedev.3]]