#+TITLE: Manpages - XtAddCallback.3
#+DESCRIPTION: Linux manpage for XtAddCallback.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtAddCallback, XtAddCallbacks, XtRemoveCallback, XtRemoveCallbacks,
XtRemoveAllCallbacks - add and remove callback procedures

* SYNTAX
#include <X11/Intrinsic.h>

void XtAddCallback(Widget /w/, const char */callback_name/,
XtCallbackProc /callback/, XtPointer /client_data/);

void XtAddCallbacks(Widget /w/, const char */callback_name/,
XtCallbackList /callbacks/);

void XtRemoveCallback(Widget /w/, const char */callback_name/,
XtCallbackProc /callback/, XtPointer /client_data/);

void XtRemoveCallbacks(Widget /w/, const char */callback_name/,
XtCallbackList /callbacks/);

void XtRemoveAllCallbacks(Widget /w/, const char */callback_name/);

* ARGUMENTS
- callback :: Specifies the callback procedure.

- callbacks :: Specifies the null-terminated list of callback procedures
  and corresponding client data.

- callback_name :: Specifies the callback list to which the procedure is
  to be appended or deleted.

- client_data :: Specifies the argument that is to be passed to the
  specified procedure when it is invoked by XtCallbacks or NULL, or the
  client data to match on the registered callback procedures.

23. Specifies the widget.

* DESCRIPTION
The *XtAddCallback* function adds the specified callback procedure to
the specified widget's callback list.

The *XtAddCallbacks* add the specified list of callbacks to the
specified widget's callback list.

The *XtRemoveCallback* function removes a callback only if both the
procedure and the client data match.

The *XtRemoveCallbacks* function removes the specified callback
procedures from the specified widget's callback list.

The *XtRemoveAllCallbacks* function removes all the callback procedures
from the specified widget's callback list.

* SEE ALSO
XtCallCallbacks(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
