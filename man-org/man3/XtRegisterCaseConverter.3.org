#+TITLE: Manpages - XtRegisterCaseConverter.3
#+DESCRIPTION: Linux manpage for XtRegisterCaseConverter.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtRegisterCaseConverter.3 is found in manpage for: [[../man3/XtSetKeyTranslator.3][man3/XtSetKeyTranslator.3]]