#+TITLE: Manpages - XGetOMValues.3
#+DESCRIPTION: Linux manpage for XGetOMValues.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetOMValues.3 is found in manpage for: [[../man3/XOpenOM.3][man3/XOpenOM.3]]