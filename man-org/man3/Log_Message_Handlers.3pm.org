#+TITLE: Manpages - Log_Message_Handlers.3pm
#+DESCRIPTION: Linux manpage for Log_Message_Handlers.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Log::Message::Handlers - Message handlers for Log::Message

* SYNOPSIS
# Implicitly used by Log::Message to serve as handlers for #
Log::Message::Item objects # Create your own file with a package called
# Log::Message::Handlers to add to the existing ones, or to even #
overwrite them $item->carp; $item->trace;

* DESCRIPTION
Log::Message::Handlers provides handlers for Log::Message::Item objects.
The handler corresponding to the level (see Log::Message::Item manpage
for an explanation about levels) will be called automatically upon
storing the error.

Handlers may also explicitly be called on an Log::Message::Item object
if one so desires (see the Log::Message manpage on how to retrieve the
Item objects).

* Default Handlers
** log
Will simply log the error on the stack, and do nothing special

** carp
Will carp (see the Carp manpage) with the error, and add the timestamp
of when it occurred.

** croak
Will croak (see the Carp manpage) with the error, and add the timestamp
of when it occurred.

** cluck
Will cluck (see the Carp manpage) with the error, and add the timestamp
of when it occurred.

** confess
Will confess (see the Carp manpage) with the error, and add the
timestamp of when it occurred

** die
Will simply die with the error message of the item

** warn
Will simply warn with the error message of the item

** trace
Will provide a traceback of this error item back to the first one that
occurred, clucking with every item as it comes across it.

* Custom Handlers
If you wish to provide your own handlers, you can simply do the
following:

- Create a file that holds a package by the name of
  =Log::Message::Handlers=

- Create subroutines with the same name as the levels you wish to handle
  in the Log::Message module (see the Log::Message manpage for
  explanation on levels)

- Require that file in your program, or add it in your configuration
  (see the Log::Message::Config manpage for explanation on how to use a
  config file)

And that is it, the handler will now be available to handle messages for
you.

The arguments a handler may receive are those specified by the =extra=
key, when storing the message. See the Log::Message manpage for details
on the arguments.

* SEE ALSO
Log::Message, Log::Message::Item, Log::Message::Config

* AUTHOR
This module by Jos Boumans <kane@cpan.org>.

* Acknowledgements
Thanks to Ann Barcomb for her suggestions.

* COPYRIGHT
This module is copyright (c) 2002 Jos Boumans <kane@cpan.org>. All
rights reserved.

This library is free software; you may redistribute and/or modify it
under the same terms as Perl itself.
