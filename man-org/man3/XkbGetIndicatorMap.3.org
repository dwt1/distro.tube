#+TITLE: Manpages - XkbGetIndicatorMap.3
#+DESCRIPTION: Linux manpage for XkbGetIndicatorMap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetIndicatorMap - Gets the map for one or more indicators, using a
mask to specify the indicators

* SYNOPSIS
*Status XkbGetIndicatorMap* *( Display **/dpy/* ,* *unsigned int
*/which/* ,* *XkbDescPtr */desc/* );*

* ARGUMENTS
- /- dpy/ :: connection to the X server

- /- which/ :: mask of indicators for which maps should be returned

- /- desc/ :: keyboard description to be updated

* DESCRIPTION
Xkb allows applications to obtain information about indicators using two
different methods. The first method, which is similar to the core X
implementation, uses a mask to specify the indicators. The second
method, which is more suitable for applications concerned with
interoperability, uses indicator names. The correspondence between the
indicator name and the bit position in masks is as follows: one of the
parameters returned from /XkbGetNamedIndicators/ is an index that is the
bit position to use in any function call that requires a mask of
indicator bits, as well as the indicator's index into the
XkbIndicatorRec array of indicator maps.

/XkbGetIndicatorMap/ obtains the maps from the server for only those
indicators specified by the /which/ mask and copies the values into the
keyboard description specified by /desc./ If the /indicators/ field of
the /desc/ parameter is NULL, /XkbGetIndicatorMap/ allocates and
initializes it.

To free the indicator maps, use /XkbFreeIndicatorMaps./

* DIAGNOSTICS
- *BadAlloc* :: Unable to allocate storage

- *BadMatch* :: A compatible version of Xkb was not available in the
  server or an argument has correct type and range, but is otherwise
  invalid

- *BadImplementation* :: Invalid reply from server

* SEE ALSO
*XkbFreeIndicatorMaps*(3), *XkbGetNamedIndicators*(3)
