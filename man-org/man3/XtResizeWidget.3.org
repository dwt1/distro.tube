#+TITLE: Manpages - XtResizeWidget.3
#+DESCRIPTION: Linux manpage for XtResizeWidget.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtResizeWidget.3 is found in manpage for: [[../man3/XtConfigureWidget.3][man3/XtConfigureWidget.3]]