#+TITLE: Manpages - ne_i18n_init.3
#+DESCRIPTION: Linux manpage for ne_i18n_init.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ne_i18n_init - functions to initialize internationalization support

* SYNOPSIS
#+begin_example
  #include <ne_i18n.h>
#+end_example

*void ne_i18n_init(const char **/encoding/*);*

* DESCRIPTION
The *ne_i18n_init* function can be used to enable support for translated
messages in the neon library. The /encoding/ parameter, if non-NULL,
specifies the character encoding required for generated translated
string. If it is NULL, the appropriate character encoding for the
process locale will be used.

This call is only strictly necessary if either:

#+begin_quote
  1.

  neon has been installed into a different prefix than the gettext
  implementation on which it depends for i18n purposes, or
#+end_quote

#+begin_quote
  2.

  the caller requires that translated messages are in a particular
  character encoding.
#+end_quote

If *ne_i18n_init* is never called, the message catalogs will not be
found if case (a) applies (and so English error messages will be used),
and will use the default character encoding specified by the process
locale. The library will otherwise operate correctly.

Note that the encoding used is a process-global setting and so results
may be unexpected if other users of neon within the process call
*ne_i18n_init* with a different encoding parameter.

* SEE ALSO
ne_sock_init

* AUTHOR
*Joe Orton* <neon@lists.manyfish.co.uk>

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
