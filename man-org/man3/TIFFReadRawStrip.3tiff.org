#+TITLE: Manpages - TIFFReadRawStrip.3tiff
#+DESCRIPTION: Linux manpage for TIFFReadRawStrip.3tiff
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
TIFFReadRawStrip - return the undecoded contents of a strip of data from
an open

file

* SYNOPSIS
*#include <tiffio.h>*

*tsize_t TIFFReadRawStrip(TIFF **/tif/*, tstrip_t */strip/*, tdata_t
*/buf/*, tsize_t */size/*)*

* DESCRIPTION
Read the contents of the specified strip into the (user supplied) data
buffer. Note that the value of /strip/ is a ``raw strip number.'' That
is, the caller must take into account whether or not the data is
organized in separate planes ( /PlanarConfiguration/=2). To read a full
strip of data the data buffer should typically be at least as large as
the number returned by /TIFFStripSize/.

* RETURN VALUES
The actual number of bytes of data that were placed in /buf/ is
returned; /TIFFReadEncodedStrip/ returns -1 if an error was encountered.

* DIAGNOSTICS
All error messages are directed to the *TIFFError*(3TIFF) routine.

* SEE ALSO
*TIFFOpen*(3TIFF), *TIFFReadEncodedStrip*(3TIFF),
*TIFFReadScanline*(3TIFF), *TIFFStripSize*(3TIFF), *libtiff*(3TIFF)

Libtiff library home page: *http://www.simplesystems.org/libtiff/*
