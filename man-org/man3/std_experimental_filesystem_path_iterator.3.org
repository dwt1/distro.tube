#+TITLE: Manpages - std_experimental_filesystem_path_iterator.3
#+DESCRIPTION: Linux manpage for std_experimental_filesystem_path_iterator.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::experimental::filesystem::path::iterator - An iterator for the
components of a path.

* SYNOPSIS
\\

=#include <fs_path.h>=

** Public Types
using *difference_type* = std::ptrdiff_t\\

using *iterator_category* = *std::bidirectional_iterator_tag*\\

using *pointer* = const *path* *\\

using *reference* = const *path* &\\

using *value_type* = *path*\\

** Public Member Functions
*iterator* (const *iterator* &)=default\\

*reference* *operator** () const\\

*iterator* & *operator++* ()\\

*iterator* *operator++* (int)\\

*iterator* & *operator--* ()\\

*iterator* *operator--* (int)\\

*pointer* *operator->* () const\\

*iterator* & *operator=* (const *iterator* &)=default\\

** Friends
bool *operator!=* (const *iterator* &__lhs, const *iterator* &__rhs)\\

bool *operator==* (const *iterator* &__lhs, const *iterator* &__rhs)\\

class *path*\\

* Detailed Description
An iterator for the components of a path.

Definition at line *854* of file *experimental/bits/fs_path.h*.

* Member Typedef Documentation
** using std::experimental::filesystem::path::iterator::difference_type
= std::ptrdiff_t
Definition at line *857* of file *experimental/bits/fs_path.h*.

** using
*std::experimental::filesystem::path::iterator::iterator_category* =
*std::bidirectional_iterator_tag*
Definition at line *861* of file *experimental/bits/fs_path.h*.

** using *std::experimental::filesystem::path::iterator::pointer* =
const *path**
Definition at line *860* of file *experimental/bits/fs_path.h*.

** using *std::experimental::filesystem::path::iterator::reference* =
const *path*&
Definition at line *859* of file *experimental/bits/fs_path.h*.

** using *std::experimental::filesystem::path::iterator::value_type* =
*path*
Definition at line *858* of file *experimental/bits/fs_path.h*.

* Constructor & Destructor Documentation
** std::experimental::filesystem::path::iterator::iterator ()= [inline]=
Definition at line *863* of file *experimental/bits/fs_path.h*.

* Member Function Documentation
** *iterator* std::experimental::filesystem::path::iterator::operator++
(int)= [inline]=
Definition at line *872* of file *experimental/bits/fs_path.h*.

** *iterator* std::experimental::filesystem::path::iterator::operator--
(int)= [inline]=
Definition at line *875* of file *experimental/bits/fs_path.h*.

** *pointer* std::experimental::filesystem::path::iterator::operator->
() const= [inline]=
Definition at line *869* of file *experimental/bits/fs_path.h*.

* Friends And Related Function Documentation
** bool operator!= (const *iterator* & __lhs, const *iterator* &
__rhs)= [friend]=
Definition at line *880* of file *experimental/bits/fs_path.h*.

** bool operator== (const *iterator* & __lhs, const *iterator* &
__rhs)= [friend]=
Definition at line *877* of file *experimental/bits/fs_path.h*.

** friend class *path*= [friend]=
Definition at line *884* of file *experimental/bits/fs_path.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
