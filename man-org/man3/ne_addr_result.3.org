#+TITLE: Manpages - ne_addr_result.3
#+DESCRIPTION: Linux manpage for ne_addr_result.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ne_addr_result.3 is found in manpage for: [[../ne_addr_resolve.3][ne_addr_resolve.3]]