#+TITLE: Manpages - FcFreeTypeCharSet.3
#+DESCRIPTION: Linux manpage for FcFreeTypeCharSet.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFreeTypeCharSet - compute Unicode coverage

* SYNOPSIS
*#include <fontconfig.h>* #include <fcfreetype.h>

FcCharSet * FcFreeTypeCharSet (FT_Face /face/*, FcBlanks **/blanks/*);*

* DESCRIPTION
Scans a FreeType face and returns the set of encoded Unicode chars.
FcBlanks is deprecated, /blanks/ is ignored and accepted only for
compatibility with older code.
