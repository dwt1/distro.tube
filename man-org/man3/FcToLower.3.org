#+TITLE: Manpages - FcToLower.3
#+DESCRIPTION: Linux manpage for FcToLower.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcToLower - convert upper case ASCII to lower case

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

FcChar8 FcToLower (FcChar8/c/*);*

* DESCRIPTION
This macro converts upper case ASCII /c/ to the equivalent lower case
letter.
