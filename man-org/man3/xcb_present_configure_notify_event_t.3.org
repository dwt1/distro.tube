#+TITLE: Manpages - xcb_present_configure_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_present_configure_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_present_configure_notify_event_t -

* SYNOPSIS
*#include <xcb/present.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_present_configure_notify_event_t {
      uint8_t             response_type;
      uint8_t             extension;
      uint16_t            sequence;
      uint32_t            length;
      uint16_t            event_type;
      uint8_t             pad0[2];
      xcb_present_event_t event;
      xcb_window_t        window;
      int16_t             x;
      int16_t             y;
      uint16_t            width;
      uint16_t            height;
      int16_t             off_x;
      int16_t             off_y;
      uint32_t            full_sequence;
      uint16_t            pixmap_width;
      uint16_t            pixmap_height;
      uint32_t            pixmap_flags;
  } xcb_present_configure_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_PRESENT_CONFIGURE_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- extension :: NOT YET DOCUMENTED.

- length :: NOT YET DOCUMENTED.

- event_type :: NOT YET DOCUMENTED.

- event :: NOT YET DOCUMENTED.

- window :: NOT YET DOCUMENTED.

24. NOT YET DOCUMENTED.

25. NOT YET DOCUMENTED.

- width :: NOT YET DOCUMENTED.

- height :: NOT YET DOCUMENTED.

- off_x :: NOT YET DOCUMENTED.

- off_y :: NOT YET DOCUMENTED.

- full_sequence :: NOT YET DOCUMENTED.

- pixmap_width :: NOT YET DOCUMENTED.

- pixmap_height :: NOT YET DOCUMENTED.

- pixmap_flags :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from present.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
