#+TITLE: Manpages - log2f.3
#+DESCRIPTION: Linux manpage for log2f.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about log2f.3 is found in manpage for: [[../man3/log2.3][man3/log2.3]]