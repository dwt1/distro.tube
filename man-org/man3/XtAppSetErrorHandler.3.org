#+TITLE: Manpages - XtAppSetErrorHandler.3
#+DESCRIPTION: Linux manpage for XtAppSetErrorHandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtAppSetErrorHandler.3 is found in manpage for: [[../man3/XtAppError.3][man3/XtAppError.3]]