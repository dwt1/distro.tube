#+TITLE: Manpages - XRestackWindows.3
#+DESCRIPTION: Linux manpage for XRestackWindows.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XRestackWindows.3 is found in manpage for: [[../man3/XRaiseWindow.3][man3/XRaiseWindow.3]]