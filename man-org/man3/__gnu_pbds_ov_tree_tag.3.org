#+TITLE: Manpages - __gnu_pbds_ov_tree_tag.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_ov_tree_tag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::ov_tree_tag - Ordered-vector tree.

* SYNOPSIS
\\

=#include <tag_and_trait.hpp>=

Inherits *__gnu_pbds::tree_tag*.

* Detailed Description
Ordered-vector tree.

Definition at line *159* of file *tag_and_trait.hpp*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
