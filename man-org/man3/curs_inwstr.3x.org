#+TITLE: Manpages - curs_inwstr.3x
#+DESCRIPTION: Linux manpage for curs_inwstr.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*inwstr*, *innwstr*, *winwstr*, *winnwstr*, *mvinwstr*, *mvinnwstr*,
*mvwinwstr*, *mvwinnwstr* - get a string of *wchar_t* characters from a
curses window

* SYNOPSIS
#+begin_example
  #include <curses.h> 

  int inwstr(wchar_t *wstr);


  int innwstr(wchar_t *wstr, int n);


  int winwstr(WINDOW *win, wchar_t *wstr);


  int winnwstr(WINDOW *win, wchar_t *wstr, int n);

  int mvinwstr(int y, int x, wchar_t *wstr);


  int mvinnwstr(int y, int x, wchar_t *wstr, int n);


  int mvwinwstr(WINDOW *win, int y, int x, wchar_t *wstr);


  int mvwinnwstr(WINDOW *win, int y, int x, wchar_t *wstr, int n);
#+end_example

* DESCRIPTION
These routines return a string of *wchar_t* wide characters in /wstr/,
extracted starting at the current cursor position in the named window.

The four functions with /n/ as the last argument return a leading
substring at most /n/ characters long (exclusive of the trailing NUL).
Transfer stops at the end of the current line, or when /n/ characters
have been stored at the location referenced by /wstr/.

If the size /n/ is not large enough to store a complete complex
character, an error is generated.

* NOTES
All routines except *winnwstr* may be macros.

Each cell in the window holds a complex character (i.e., base- and
combining-characters) together with attributes and color. These
functions store only the wide characters, ignoring attributes and color.
Use *in_wchstr* to return the complex characters from a window.

* RETURN VALUE
All routines return *ERR* upon failure. Upon successful completion, the
**inwstr* routines return *OK*, and the **innwstr* routines return the
number of characters read into the string.

Functions with a mv prefix first perform a cursor movement using
*wmove*, and return an error if the position is outside the window, or
if the window pointer is null.

* SEE ALSO
*curses*(3X), *curs_instr*(3X), *curs_in_wchstr*(3X)
