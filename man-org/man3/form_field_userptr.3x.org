#+TITLE: Manpages - form_field_userptr.3x
#+DESCRIPTION: Linux manpage for form_field_userptr.3x
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*set_field_userptr*, *field_userptr* - associate application data with a
form field

* SYNOPSIS
*#include <form.h>*

*int set_field_userptr(FIELD **/field/*, void **/userptr/*);*\\
*void *field_userptr(const FIELD **/field/*);*

* DESCRIPTION
Every form field has a field that can be used to hold
application-specific data (that is, the form-driver code leaves it
alone). These functions get and set that field.

* RETURN VALUE
The function *field_userptr* returns a pointer (which may be *NULL*). It
does not set *errno*.

The function *set_field_userptr* returns *E_OK* (success).

* SEE ALSO
*curses*(3X), *form*(3X).

* NOTES
The header file *<form.h>* automatically includes the header file
*<curses.h>*.

* PORTABILITY
These routines emulate the System V forms library. They were not
supported on Version 7 or BSD versions.

The user pointer is a void pointer. We chose not to leave it as a char
pointer for SVr4 compatibility.

* AUTHORS
Juergen Pfeifer. Manual pages and adaptation for new curses by Eric S.
Raymond.
