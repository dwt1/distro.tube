#+TITLE: Manpages - Alien_Base_Wrapper.3pm
#+DESCRIPTION: Linux manpage for Alien_Base_Wrapper.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Base::Wrapper - Compiler and linker wrapper for Alien

* VERSION
version 2.44

* SYNOPSIS
From the command line:

% perl -MAlien::Base::Wrapper=Alien::Foo,Alien::Bar -e cc -- -o foo.o -c
foo.c % perl -MAlien::Base::Wrapper=Alien::Foo,Alien::Bar -e ld -- -o
foo foo.o

From Makefile.PL (static):

use ExtUtils::MakeMaker; use Alien::Base::Wrapper (); WriteMakefile(
Alien::Base::Wrapper->new( Alien::Foo, Alien::Bar)->mm_args2( NAME =>
Foo::XS, VERSION_FROM => lib/Foo/XS.pm, ), );

From Makefile.PL (static with wrapper)

use Alien::Base::Wrapper qw( WriteMakefile); WriteMakefile( NAME =>
Foo::XS, VERSION_FROM => lib/Foo/XS.pm, alien_requires => { Alien::Foo
=> 0, Alien::Bar => 0, }, );

From Makefile.PL (dynamic):

use Devel::CheckLib qw( check_lib ); use ExtUtils::MakeMaker 6.52; my
@mm_args; my @libs; if(check_lib( lib => [ foo ] ) { push @mm_args, LIBS
=> [ -lfoo ]; } else { push @mm_args, CC => $(FULLPERL)
-MAlien::Base::Wrapper=Alien::Foo -e cc --, LD => $(FULLPERL)
-MAlien::Base::Wrapper=Alien::Foo -e ld --, BUILD_REQUIRES => {
Alien::Foo => 0, Alien::Base::Wrapper => 0, } ; } WriteMakefile( NAME =>
Foo::XS, VERSION_FROM => lib/Foo/XS.pm, CONFIGURE_REQUIRES => {
ExtUtils::MakeMaker => 6.52, }, @mm_args, );

* DESCRIPTION
This module acts as a wrapper around one or more Alien modules. It is
designed to work with Alien::Base based aliens, but it should work with
any Alien which uses the same essential API.

In the first example (from the command line), this class acts as a
wrapper around the compiler and linker that Perl is configured to use.
It takes the normal compiler and linker flags and adds the flags
provided by the Aliens specified, and then executes the command. It will
print the command to the console so that you can see exactly what is
happening.

In the second example (from Makefile.PL non-dynamic), this class is used
to generate the appropriate ExtUtils::MakeMaker (EUMM) arguments needed
to =WriteMakefile=.

In the third example (from Makefile.PL dynamic), we do a quick check to
see if the simple linker flag =-lfoo= will work, if so we use that. If
not, we use a wrapper around the compiler and linker that will use the
alien flags that are known at build time. The problem that this form
attempts to solve is that compiler and linker flags typically need to be
determined at /configure/ time, when a distribution is installed,
meaning if you are going to use an Alien module then it needs to be a
configure prerequisite, even if the library is already installed and
easily detected on the operating system.

The author of this module believes that the third (from Makefile.PL
dynamic) form is somewhat unnecessary. Alien modules based on
Alien::Base have a few prerequisites, but they are well maintained and
reliable, so while there is a small cost in terms of extra dependencies,
the overall reliability thanks to reduced overall complexity.

* CONSTRUCTOR
** new
my $abw = Alien::Base::Wrapper->new(@aliens);

Instead of passing the aliens you want to use into this modules import
you can create a non-global instance of =Alien::Base::Wrapper= using the
OO interface.

* FUNCTIONS
** cc
% perl -MAlien::Base::Wrapper=Alien::Foo -e cc -- cflags

Invoke the C compiler with the appropriate flags from =Alien::Foo= and
what is provided on the command line.

** ld
% perl -MAlien::Base::Wrapper=Alien::Foo -e ld -- ldflags

Invoke the linker with the appropriate flags from =Alien::Foo= and what
is provided on the command line.

** mm_args
my %args = $abw->mm_args; my %args = Alien::Base::Wrapper->mm_args;

Returns arguments that you can pass into =WriteMakefile= to compile/link
against the specified Aliens. Note that this does not set
=CONFIGURE_REQUIRES=. You probably want to use =mm_args2= below instead
for that reason.

** mm_args2
my %args = $abw->mm_args2(%args); my %args =
Alien::Base::Wrapper->mm_args2(%args);

Returns arguments that you can pass into =WriteMakefile= to compile/link
against. It works a little differently from =mm_args= above in that you
can pass in arguments. It also adds the appropriate =CONFIGURE_REQUIRES=
for you so you do not have to do that explicitly.

** mb_args
my %args = $abw->mb_args; my %args = Alien::Base::Wrapper->mb_args;

Returns arguments that you can pass into the constructor to
Module::Build.

** WriteMakefile
use Alien::Base::Wrapper qw( WriteMakefile ); WriteMakefile(%args,
alien_requires => %aliens); WriteMakefile(%args, alien_requires =>
@aliens);

This is a thin wrapper around =WriteMakefile= from ExtUtils::MakeMaker,
which adds the given aliens to the configure requirements and sets the
appropriate compiler and linker flags.

If the aliens are specified as a hash reference, then the keys are the
module names and the values are the versions. For a list it is just the
name of the aliens.

For the list form you can specify a version by appending ==version= to
the name of the Aliens, that is:

WriteMakefile( alien_requires => [ Alien::libfoo=1.23,
Alien::libbar=4.56 ], );

The list form is recommended if the ordering of the aliens matter. The
aliens are sorted in the hash form to make it consistent, but it may not
be the order that you want.

* ENVIRONMENT
Alien::Base::Wrapper responds to these environment variables:

- ALIEN_BASE_WRAPPER_QUIET :: If set to true, do not print the command
  before executing

* SEE ALSO
Alien::Base, Alien::Base

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
