#+TITLE: Manpages - MPI_Comm_call_errhandler.3
#+DESCRIPTION: Linux manpage for MPI_Comm_call_errhandler.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Comm_call_errhandler* - Passes the supplied error code to the error
handler assigned to a communicator

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_Comm_call_errhandler(MPI_Comm comm, int errorcode)
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  MPI_COMM_CALL_ERRHANDLER(COMM, ERRORCODE, IERROR)
  	INTEGER	COMM, ERRORCODE, IERROR
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  MPI_Comm_call_errhandler(comm, errorcode, ierror)
  	TYPE(MPI_Comm), INTENT(IN) :: comm
  	INTEGER, INTENT(IN) :: errorcode
  	INTEGER, OPTIONAL, INTENT(OUT) :: ierror
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  void MPI::Comm::Call_errhandler(int errorcode) const
#+end_example

* INPUT PARAMETER
- comm :: communicator with error handler (handle).

- errorcode :: error code (integer).

* OUTPUT PARAMETERS
- IERROR :: Fortran only: Error status (integer).

* DESCRIPTION
This function invokes the error handler assigned to the communicator
/comm/ with the supplied error code /errorcode/. If the error handler
was successfully called, the process is not aborted, and the error
handler returns, this function returns MPI_SUCCESS.

* NOTES
Users should note that the default error handler is
MPI_ERRORS_ARE_FATAL. Thus, calling this function will abort the
processes in /comm/ if the default error handler has not been changed.

* ERRORS
Almost all MPI routines return an error value; C routines as the value
of the function and Fortran routines in the last argument. C++ functions
do not return errors. If the default error handler is set to
MPI::ERRORS_THROW_EXCEPTIONS, then on error the C++ exception mechanism
will be used to throw an MPI::Exception object.

See the MPI man page for a full list of MPI error codes.

* SEE ALSO
#+begin_example
  MPI_Comm_create_errhandler
  MPI_Comm_set_errhandler
#+end_example
