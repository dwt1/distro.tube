#+TITLE: Manpages - XRRConfigSizes.3
#+DESCRIPTION: Linux manpage for XRRConfigSizes.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XRRConfigSizes.3 is found in manpage for: [[../man3/Xrandr.3][man3/Xrandr.3]]