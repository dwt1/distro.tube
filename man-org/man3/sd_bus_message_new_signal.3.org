#+TITLE: Manpages - sd_bus_message_new_signal.3
#+DESCRIPTION: Linux manpage for sd_bus_message_new_signal.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sd_bus_message_new_signal - Create a signal message

* SYNOPSIS
#+begin_example
  #include <systemd/sd-bus.h>
#+end_example

*int sd_bus_message_new_signal(sd_bus **/bus/*, sd_bus_message ***/m/*,
const char **/path/*, const char **/interface/*, const char
**/member/*);*

* DESCRIPTION
The *sd_bus_message_new_signal()* function creates a new bus message
object that encapsulates a D-Bus signal, and returns it in the /m/
output parameter. The signal will be sent to path /path/, on the
interface /interface/, member /member/. When this message is sent, no
reply is expected. See *sd_bus_message_new_method_call*(1) for a short
description of the meaning of the /path/, /interface/, and /member/
parameters.

* RETURN VALUE
This function returns 0 if the message object was successfully created,
and a negative errno-style error code otherwise.

** Errors
Returned errors may indicate the following problems:

*-EINVAL*

#+begin_quote
  The output parameter /m/ is *NULL*.

  The /path/ parameter is not a valid D-Bus path ("/an/object/path"),
  the /interface/ parameter is not a valid D-Bus interface name
  ("an.interface.name"), or the /member/ parameter is not a valid D-Bus
  member ("Name").
#+end_quote

*-ENOTCONN*

#+begin_quote
  The bus parameter /bus/ is *NULL* or the bus is not connected.
#+end_quote

*-ENOMEM*

#+begin_quote
  Memory allocation failed.
#+end_quote

* NOTES
These APIs are implemented as a shared library, which can be compiled
and linked to with the *libsystemd* *pkg-config*(1) file.

* EXAMPLES
*Example 1. Send a simple signal*

#+begin_quote
  #+begin_example
    #include <systemd/sd-bus.h>
    #define _cleanup_(f) __attribute__((cleanup(f)))

    int send_unit_files_changed(sd_bus *bus) {
      _cleanup_(sd_bus_message_unrefp) sd_bus_message *message = NULL;
      int r;

      r = sd_bus_message_new_signal(bus, &message,
                                    "/org/freedesktop/systemd1",
                                    "org.freedesktop.systemd1.Manager",
                                    "UnitFilesChanged");
      if (r < 0)
        return r;

      return sd_bus_send(bus, message, NULL);
    }
  #+end_example
#+end_quote

This function in systemd sources is used to emit the "UnitFilesChanged"
signal when the unit files have been changed.

* SEE ALSO
*systemd*(1), *sd-bus*(3), *sd_bus_emit_signal*(3)
