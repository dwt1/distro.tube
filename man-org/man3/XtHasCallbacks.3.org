#+TITLE: Manpages - XtHasCallbacks.3
#+DESCRIPTION: Linux manpage for XtHasCallbacks.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtHasCallbacks.3 is found in manpage for: [[../man3/XtCallCallbacks.3][man3/XtCallCallbacks.3]]