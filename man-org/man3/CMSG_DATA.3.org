#+TITLE: Manpages - CMSG_DATA.3
#+DESCRIPTION: Linux manpage for CMSG_DATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about CMSG_DATA.3 is found in manpage for: [[../man3/cmsg.3][man3/cmsg.3]]