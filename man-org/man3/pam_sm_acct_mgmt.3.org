#+TITLE: Manpages - pam_sm_acct_mgmt.3
#+DESCRIPTION: Linux manpage for pam_sm_acct_mgmt.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_sm_acct_mgmt - PAM service function for account management

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_sm_acct_mgmt(pam_handle_t **/pamh/*, int */flags/*, int
*/argc/*, const char ***/argv/*);*

* DESCRIPTION
The *pam_sm_acct_mgmt* function is the service modules implementation of
the *pam_acct_mgmt*(3) interface.

This function performs the task of establishing whether the user is
permitted to gain access at this time. It should be understood that the
user has previously been validated by an authentication module. This
function checks for other things. Such things might be: the time of day
or the date, the terminal line, remote hostname, etc. This function may
also determine things like the expiration on passwords, and respond that
the user change it before continuing.

Valid flags, which may be logically ORd with /PAM_SILENT/, are:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

PAM_DISALLOW_NULL_AUTHTOK

#+begin_quote
  Return *PAM_AUTH_ERR* if the database of authentication tokens for
  this authentication mechanism has a /NULL/ entry for the user.
#+end_quote

* RETURN VALUES
PAM_ACCT_EXPIRED

#+begin_quote
  User account has expired.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  Authentication failure.
#+end_quote

PAM_NEW_AUTHTOK_REQD

#+begin_quote
  The users authentication token has expired. Before calling this
  function again the application will arrange for a new one to be given.
  This will likely result in a call to *pam_sm_chauthtok()*.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Permission denied.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The authentication token was successfully updated.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User unknown to password service.
#+end_quote

* SEE ALSO
*pam*(3), *pam_acct_mgmt*(3), *pam_sm_chauthtok*(3), *pam_strerror*(3),
*PAM*(8)
