#+TITLE: Manpages - xcb_sync_counter_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_sync_counter_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_sync_counter_notify_event_t -

* SYNOPSIS
*#include <xcb/sync.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_sync_counter_notify_event_t {
      uint8_t            response_type;
      uint8_t            kind;
      uint16_t           sequence;
      xcb_sync_counter_t counter;
      xcb_sync_int64_t   wait_value;
      xcb_sync_int64_t   counter_value;
      xcb_timestamp_t    timestamp;
      uint16_t           count;
      uint8_t            destroyed;
      uint8_t            pad0;
  } xcb_sync_counter_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_SYNC_COUNTER_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- kind :: NOT YET DOCUMENTED.

- counter :: NOT YET DOCUMENTED.

- wait_value :: NOT YET DOCUMENTED.

- counter_value :: NOT YET DOCUMENTED.

- timestamp :: NOT YET DOCUMENTED.

- count :: NOT YET DOCUMENTED.

- destroyed :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from sync.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
