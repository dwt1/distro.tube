#+TITLE: Manpages - pam_sm_close_session.3
#+DESCRIPTION: Linux manpage for pam_sm_close_session.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_sm_close_session - PAM service function to terminate session
management

* SYNOPSIS
#+begin_example
  #include <security/pam_modules.h>
#+end_example

*int pam_sm_close_session(pam_handle_t **/pamh/*, int */flags/*, int
*/argc/*, const char ***/argv/*);*

* DESCRIPTION
The *pam_sm_close_session* function is the service modules
implementation of the *pam_close_session*(3) interface.

This function is called to terminate a session. The only valid value for
/flags/ is zero or:

PAM_SILENT

#+begin_quote
  Do not emit any messages.
#+end_quote

* RETURN VALUES
PAM_SESSION_ERR

#+begin_quote
  Cannot make/remove an entry for the specified session.
#+end_quote

PAM_SUCCESS

#+begin_quote
  The session was successfully terminated.
#+end_quote

* SEE ALSO
*pam*(3), *pam_close_session*(3), *pam_sm_close_session*(3),
*pam_strerror*(3), *PAM*(8)
