#+TITLE: Manpages - XtAppAddWorkProc.3
#+DESCRIPTION: Linux manpage for XtAppAddWorkProc.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtAppAddWorkProc, XtRemoveWorkProc - Add and remove background
processing procedures

* SYNTAX
#include <X11/Intrinsic.h>

XtWorkProcId XtAppAddWorkProc(XtAppContext /app_context/, XtWorkProc
/proc/, XtPointer /client_data/);

void XtRemoveWorkProc(XtWorkProcId /id/);

* ARGUMENTS
- app_context :: Specifies the application context that identifies the
  application.

- client_data :: Specifies the argument that is to be passed to the
  specified procedure when it is called.

- proc :: Specifies the procedure that is to be called.

- id :: Specifies which work procedure to remove.

* DESCRIPTION
The *XtAppAddWorkProc* function adds the specified work procedure for
the application identified by app_context.

The *XtRemoveWorkProc* function explicitly removes the specified
background work procedure.

* SEE ALSO
XtAppNextEvent(3)\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
