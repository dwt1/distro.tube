#+TITLE: Manpages - SDL_mutexV.3
#+DESCRIPTION: Linux manpage for SDL_mutexV.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_mutexV - Unlock a mutex

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*int SDL_mutexV*(*SDL_mutex *mutex*);

* DESCRIPTION
Unlocks the *mutex*, which was previously created with
*SDL_CreateMutex*. Returns *0* on success, or *-1* on an error.

SDL also defines a macro *#define SDL_UnlockMutex(m) SDL_mutexV(m)*.

* SEE ALSO
*SDL_CreateMutex*, *SDL_mutexP*
