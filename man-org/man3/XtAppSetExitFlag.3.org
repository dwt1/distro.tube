#+TITLE: Manpages - XtAppSetExitFlag.3
#+DESCRIPTION: Linux manpage for XtAppSetExitFlag.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XtAppSetExitFlag, XtAppGetExitFlag - thread support functions

* SYNTAX
#include <X11/Intrinsic.h>

void XtAppSetExitFlag(XtAppContext /app_context/);

Boolean XtAppGetExitFlag(XtAppContext /app_context/);

* ARGUMENTS
- app_context :: Specifies the application context.

* DESCRIPTION
To indicate that an application context should exit, use
*XtAppSetExitFlag*.

To test the exit status of an application context, use
*XtAppGetExitFlag*.

* SEE ALSO
\\
/X Toolkit Intrinsics - C Language Interface/\\
/Xlib - C Language X Interface/
