#+TITLE: Manpages - zip_source_make_command_bitmap.3
#+DESCRIPTION: Linux manpage for zip_source_make_command_bitmap.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
libzip (-lzip)

The

function returns a bitmap of source commands suitable as return value
for

It includes all the commands from the argument list, which must be
terminated by -1.

was added in libzip 1.0.

and
