#+TITLE: Manpages - CURLOPT_CLOSESOCKETDATA.3
#+DESCRIPTION: Linux manpage for CURLOPT_CLOSESOCKETDATA.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
CURLOPT_CLOSESOCKETDATA - pointer passed to the socket close callback

* SYNOPSIS
#include <curl/curl.h>

CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CLOSESOCKETDATA, void
*pointer);

* DESCRIPTION
Pass a /pointer/ that will be untouched by libcurl and passed as the
first argument in the closesocket callback set with
/CURLOPT_CLOSESOCKETFUNCTION(3)/.

* DEFAULT
The default value of this parameter is NULL.

* PROTOCOLS
All except file:

* EXAMPLE
#+begin_example
  static int closesocket(void *clientp, curl_socket_t item)
  {
    printf("libcurl wants to close %d now\n", (int)item);
    return 0;
  }

  /* call this function to close sockets */
  curl_easy_setopt(curl, CURLOPT_CLOSESOCKETFUNCTION, closesocket);
  curl_easy_setopt(curl, CURLOPT_CLOSESOCKETDATA, &sockfd);
#+end_example

* AVAILABILITY
Added in 7.21.7

* RETURN VALUE
Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if
not.

* SEE ALSO
*CURLOPT_CLOSESOCKETFUNCTION*(3), *CURLOPT_OPENSOCKETFUNCTION*(3),
