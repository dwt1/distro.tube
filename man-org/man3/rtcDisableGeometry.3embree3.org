#+TITLE: Manpages - rtcDisableGeometry.3embree3
#+DESCRIPTION: Linux manpage for rtcDisableGeometry.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcDisableGeometry - disables the geometry
#+end_example

** SYNOPSIS
#+begin_example
  #include <embree3/rtcore.h>

  void rtcDisableGeometry(RTCGeometry geometry);
#+end_example

** DESCRIPTION
The =rtcDisableGeometry= function disables the specified geometry
(=geometry= argument). A disabled geometry is not rendered. Each
geometry is enabled by default at construction time.

After disabling a geometry, the scene containing that geometry must be
committed using =rtcCommitScene= for the change to have effect.

** EXIT STATUS
On failure an error code is set that can be queried using
=rtcGetDeviceError=.

** SEE ALSO
[rtcNewGeometry], [rtcEnableGeometry], [rtcCommitScene]
