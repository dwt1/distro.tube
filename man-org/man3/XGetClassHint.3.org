#+TITLE: Manpages - XGetClassHint.3
#+DESCRIPTION: Linux manpage for XGetClassHint.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XGetClassHint.3 is found in manpage for: [[../man3/XAllocClassHint.3][man3/XAllocClassHint.3]]