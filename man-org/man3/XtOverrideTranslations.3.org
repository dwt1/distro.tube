#+TITLE: Manpages - XtOverrideTranslations.3
#+DESCRIPTION: Linux manpage for XtOverrideTranslations.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtOverrideTranslations.3 is found in manpage for: [[../man3/XtParseTranslationTable.3][man3/XtParseTranslationTable.3]]