#+TITLE: Manpages - XtGetErrorDatabaseText.3
#+DESCRIPTION: Linux manpage for XtGetErrorDatabaseText.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XtGetErrorDatabaseText.3 is found in manpage for: [[../man3/XtGetErrorDatabase.3][man3/XtGetErrorDatabase.3]]