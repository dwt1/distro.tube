#+TITLE: Manpages - ldiv.3
#+DESCRIPTION: Linux manpage for ldiv.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ldiv.3 is found in manpage for: [[../man3/div.3][man3/div.3]]