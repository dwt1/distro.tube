#+TITLE: Manpages - MPI_Wtick.3
#+DESCRIPTION: Linux manpage for MPI_Wtick.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_Wtick* - Returns the resolution of MPI_Wtime.

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  double MPI_Wtick()
#+end_example

* Fortran Syntax
#+begin_example
  USE MPI
  ! or the older form: INCLUDE 'mpif.h'
  DOUBLE PRECISION MPI_WTICK()
#+end_example

* Fortran 2008 Syntax
#+begin_example
  USE mpi_f08
  DOUBLE PRECISION MPI_WTICK()
#+end_example

* C++ Syntax
#+begin_example
  #include <mpi.h>
  double MPI::Wtick()
#+end_example

* RETURN VALUE
Time in seconds of resolution of MPI_Wtime.

* DESCRIPTION
MPI_Wtick returns the resolution of MPI_Wtime in seconds. That is, it
returns, as a double-precision value, the number of seconds between
successive clock ticks. For example, if the clock is implemented by the
hardware as a counter that is incremented every millisecond, the value
returned by MPI_Wtick should be 10^-3.

* NOTE
This function does not return an error value. Consequently, the result
of calling it before MPI_Init or after MPI_Finalize is undefined.

* SEE ALSO
MPI_Wtime
