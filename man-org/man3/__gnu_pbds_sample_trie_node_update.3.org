#+TITLE: Manpages - __gnu_pbds_sample_trie_node_update.3
#+DESCRIPTION: Linux manpage for __gnu_pbds_sample_trie_node_update.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_pbds::sample_trie_node_update< Node_CItr, Node_Itr, _ATraits,
_Alloc > - A sample node updator.

* SYNOPSIS
\\

=#include <sample_trie_node_update.hpp>=

** Public Types
typedef std::size_t *metadata_type*\\

** Protected Member Functions
*sample_trie_node_update* ()\\
Default constructor.

void *operator()* (node_iterator, node_const_iterator) const\\
Updates the rank of a node through a node_iterator node_it; end_nd_it is
the end node iterator.

* Detailed Description
** "template<typename Node_CItr, typename Node_Itr, typename _ATraits,
typename _Alloc>
\\
class __gnu_pbds::sample_trie_node_update< Node_CItr, Node_Itr,
_ATraits, _Alloc >"A sample node updator.

Definition at line *49* of file *sample_trie_node_update.hpp*.

* Member Typedef Documentation
** template<typename Node_CItr , typename Node_Itr , typename _ATraits ,
typename _Alloc > typedef std::size_t
*__gnu_pbds::sample_trie_node_update*< Node_CItr, Node_Itr, _ATraits,
_Alloc >::metadata_type
Definition at line *52* of file *sample_trie_node_update.hpp*.

* Constructor & Destructor Documentation
** template<typename Node_CItr , typename Node_Itr , typename _ATraits ,
typename _Alloc > *__gnu_pbds::sample_trie_node_update*< Node_CItr,
Node_Itr, _ATraits, _Alloc >::*sample_trie_node_update* ()= [protected]=
Default constructor.

* Member Function Documentation
** template<typename Node_CItr , typename Node_Itr , typename _ATraits ,
typename _Alloc > void *__gnu_pbds::sample_trie_node_update*< Node_CItr,
Node_Itr, _ATraits, _Alloc >::operator() (node_iterator,
node_const_iterator) const= [inline]=, = [protected]=
Updates the rank of a node through a node_iterator node_it; end_nd_it is
the end node iterator.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
