#+TITLE: Manpages - IsKeypadKey.3
#+DESCRIPTION: Linux manpage for IsKeypadKey.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about IsKeypadKey.3 is found in manpage for: [[../man3/IsCursorKey.3][man3/IsCursorKey.3]]