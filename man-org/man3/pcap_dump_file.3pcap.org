#+TITLE: Manpages - pcap_dump_file.3pcap
#+DESCRIPTION: Linux manpage for pcap_dump_file.3pcap
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pcap_dump_file - get the standard I/O stream for a savefile being
written

* SYNOPSIS
#+begin_example
  #include <pcap/pcap.h>
  FILE *pcap_dump_file(pcap_dumper_t *p);
#+end_example

* DESCRIPTION
*pcap_dump_file*() returns the standard I/O stream of the ``savefile''
opened by *pcap_dump_open*(3PCAP).

* SEE ALSO
*pcap*(3PCAP)
