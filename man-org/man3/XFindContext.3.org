#+TITLE: Manpages - XFindContext.3
#+DESCRIPTION: Linux manpage for XFindContext.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about XFindContext.3 is found in manpage for: [[../man3/XSaveContext.3][man3/XSaveContext.3]]