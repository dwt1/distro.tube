#+TITLE: Manpages - std__Placeholder.3
#+DESCRIPTION: Linux manpage for std__Placeholder.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::_Placeholder< _Num > - The type of placeholder objects defined by
libstdc++.

* SYNOPSIS
\\

* Detailed Description
** "template<int _Num>
\\
struct std::_Placeholder< _Num >"The type of placeholder objects defined
by libstdc++.

Definition at line *81* of file *std/functional*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
