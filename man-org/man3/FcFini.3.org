#+TITLE: Manpages - FcFini.3
#+DESCRIPTION: Linux manpage for FcFini.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
FcFini - finalize fontconfig library

* SYNOPSIS
*#include <fontconfig/fontconfig.h>*

void FcFini (void*);*

* DESCRIPTION
Frees all data structures allocated by previous calls to fontconfig
functions. Fontconfig returns to an uninitialized state, requiring a new
call to one of the FcInit functions before any other fontconfig function
may be called.
