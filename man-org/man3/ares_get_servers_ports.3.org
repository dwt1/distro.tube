#+TITLE: Manpages - ares_get_servers_ports.3
#+DESCRIPTION: Linux manpage for ares_get_servers_ports.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about ares_get_servers_ports.3 is found in manpage for: [[../man3/ares_get_servers.3][man3/ares_get_servers.3]]