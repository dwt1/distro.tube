#+TITLE: Manpages - std_any.3
#+DESCRIPTION: Linux manpage for std_any.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
std::any - A type-safe container of any type.

* SYNOPSIS
\\

** Public Member Functions
constexpr *any* () noexcept\\
Default constructor, creates an empty object.

template<typename _Tp , typename _VTp = _Decay_if_not_any<_Tp>, typename
_Mgr = _Manager<_VTp>, *enable_if_t*< *is_copy_constructible*< _VTp
>::value &&!__is_in_place_type< _VTp >::value, bool > = true> *any* (_Tp
&&__value)\\
Construct with a copy of =__value= as the contained object.

*any* (*any* &&__other) noexcept\\
Move constructor, transfer the state from =__other=.

*any* (const *any* &__other)\\
Copy constructor, copies the state of =__other=.

template<typename _Tp , typename... _Args, typename _VTp = decay_t<_Tp>,
typename _Mgr = _Manager<_VTp>, __any_constructible_t< _VTp, _Args &&...
> = false> *any* (in_place_type_t< _Tp >, _Args &&... __args)\\
Construct with an object created from =__args= as the contained object.

template<typename _Tp , typename _Up , typename... _Args, typename _VTp
= decay_t<_Tp>, typename _Mgr = _Manager<_VTp>, __any_constructible_t<
_VTp, *initializer_list*< _Up >, _Args &&... > = false> *any*
(in_place_type_t< _Tp >, *initializer_list*< _Up > __il, _Args &&...
__args)\\
Construct with an object created from =__il= and =__args= as the
contained object.

*~any* ()\\
Destructor, calls =reset()=

template<typename _Tp , typename... _Args> __emplace_t< *decay_t*< _Tp
>, _Args... > *emplace* (_Args &&... __args)\\
Emplace with an object created from =__args= as the contained object.

template<typename _Tp , typename _Up , typename... _Args> __emplace_t<
*decay_t*< _Tp >, *initializer_list*< _Up >, _Args &&... > *emplace*
(*initializer_list*< _Up > __il, _Args &&... __args)\\
Emplace with an object created from =__il= and =__args= as the contained
object.

bool *has_value* () const noexcept\\
Reports whether there is a contained object or not.

template<typename _Tp > *enable_if_t*< *is_copy_constructible*<
_Decay_if_not_any< _Tp > >::value, *any* & > *operator=* (_Tp &&__rhs)\\
Store a copy of =__rhs= as the contained object.

*any* & *operator=* (*any* &&__rhs) noexcept\\
Move assignment operator.

*any* & *operator=* (const *any* &__rhs)\\
Copy the state of another object.

void *reset* () noexcept\\
If not empty, destroy the contained object.

void *swap* (*any* &__rhs) noexcept\\
Exchange state with another object.

const *type_info* & *type* () const noexcept\\
The =typeid= of the contained object, or =typeid(void)= if empty.

** Static Public Member Functions
template<typename _Tp > static constexpr bool *__is_valid_cast* ()\\

** Friends
template<typename _Tp > void * *__any_caster* (const *any* *__any)\\

* Detailed Description
A type-safe container of any type.

An =any= object's state is either empty or it stores a contained object
of CopyConstructible type.

Definition at line *77* of file *std/any*.

* Constructor & Destructor Documentation
** constexpr std::any::any ()= [inline]=, = [constexpr]=, = [noexcept]=
Default constructor, creates an empty object.

Definition at line *150* of file *std/any*.

** std::any::any (const *any* & __other)= [inline]=
Copy constructor, copies the state of =__other=.

Definition at line *153* of file *std/any*.

** std::any::any (*any* && __other)= [inline]=, = [noexcept]=
Move constructor, transfer the state from =__other=.

*Postcondition*

#+begin_quote
  =!__other=.has_value() (this postcondition is a GNU extension)
#+end_quote

Definition at line *170* of file *std/any*.

** template<typename _Tp , typename _VTp = _Decay_if_not_any<_Tp>,
typename _Mgr = _Manager<_VTp>, *enable_if_t*< *is_copy_constructible*<
_VTp >::value &&!__is_in_place_type< _VTp >::value, bool > = true>
std::any::any (_Tp && __value)= [inline]=
Construct with a copy of =__value= as the contained object.

Definition at line *187* of file *std/any*.

** template<typename _Tp , typename... _Args, typename _VTp =
decay_t<_Tp>, typename _Mgr = _Manager<_VTp>, __any_constructible_t<
_VTp, _Args &&... > = false> std::any::any (in_place_type_t< _Tp >,
_Args &&... __args)= [inline]=, = [explicit]=
Construct with an object created from =__args= as the contained object.

Definition at line *198* of file *std/any*.

** template<typename _Tp , typename _Up , typename... _Args, typename
_VTp = decay_t<_Tp>, typename _Mgr = _Manager<_VTp>,
__any_constructible_t< _VTp, *initializer_list*< _Up >, _Args &&... > =
false> std::any::any (in_place_type_t< _Tp >, *initializer_list*< _Up >
__il, _Args &&... __args)= [inline]=, = [explicit]=
Construct with an object created from =__il= and =__args= as the
contained object.

Definition at line *211* of file *std/any*.

** std::any::~any ()= [inline]=
Destructor, calls =reset()=

Definition at line *218* of file *std/any*.

* Member Function Documentation
** template<typename _Tp > static constexpr bool
std::any::__is_valid_cast ()= [inline]=, = [static]=, = [constexpr]=
Definition at line *344* of file *std/any*.

** template<typename _Tp , typename... _Args> __emplace_t< *decay_t*<
_Tp >, _Args... > std::any::emplace (_Args &&... __args)= [inline]=
Emplace with an object created from =__args= as the contained object.

Definition at line *262* of file *std/any*.

** template<typename _Tp , typename _Up , typename... _Args>
__emplace_t< *decay_t*< _Tp >, *initializer_list*< _Up >, _Args &&... >
std::any::emplace (*initializer_list*< _Up > __il, _Args &&...
__args)= [inline]=
Emplace with an object created from =__il= and =__args= as the contained
object.

Definition at line *275* of file *std/any*.

** bool std::any::has_value () const= [inline]=, = [noexcept]=
Reports whether there is a contained object or not.

Definition at line *329* of file *std/any*.

** template<typename _Tp > *enable_if_t*< *is_copy_constructible*<
_Decay_if_not_any< _Tp > >::value, *any* & > std::any::operator= (_Tp &&
__rhs)= [inline]=
Store a copy of =__rhs= as the contained object.

Definition at line *253* of file *std/any*.

** *any* & std::any::operator= (*any* && __rhs)= [inline]=,
= [noexcept]=
Move assignment operator.

*Postcondition*

#+begin_quote
  =!__rhs=.has_value() (not guaranteed for other implementations)
#+end_quote

Definition at line *236* of file *std/any*.

** *any* & std::any::operator= (const *any* & __rhs)= [inline]=
Copy the state of another object.

Definition at line *224* of file *std/any*.

** void std::any::reset ()= [inline]=, = [noexcept]=
If not empty, destroy the contained object.

Definition at line *287* of file *std/any*.

** void std::any::swap (*any* & __rhs)= [inline]=, = [noexcept]=
Exchange state with another object.

Definition at line *297* of file *std/any*.

** const *type_info* & std::any::type () const= [inline]=, = [noexcept]=
The =typeid= of the contained object, or =typeid(void)= if empty.

Definition at line *333* of file *std/any*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
