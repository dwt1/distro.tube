#+TITLE: Manpages - EVP_DigestSignInit.3ssl
#+DESCRIPTION: Linux manpage for EVP_DigestSignInit.3ssl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
EVP_DigestSignInit, EVP_DigestSignUpdate, EVP_DigestSignFinal,
EVP_DigestSign - EVP signing functions

* SYNOPSIS
#include <openssl/evp.h> int EVP_DigestSignInit(EVP_MD_CTX *ctx,
EVP_PKEY_CTX **pctx, const EVP_MD *type, ENGINE *e, EVP_PKEY *pkey); int
EVP_DigestSignUpdate(EVP_MD_CTX *ctx, const void *d, size_t cnt); int
EVP_DigestSignFinal(EVP_MD_CTX *ctx, unsigned char *sig, size_t
*siglen); int EVP_DigestSign(EVP_MD_CTX *ctx, unsigned char *sigret,
size_t *siglen, const unsigned char *tbs, size_t tbslen);

* DESCRIPTION
The EVP signature routines are a high-level interface to digital
signatures.

*EVP_DigestSignInit()* sets up signing context *ctx* to use digest
*type* from ENGINE *e* and private key *pkey*. *ctx* must be created
with *EVP_MD_CTX_new()* before calling this function. If *pctx* is not
NULL, the EVP_PKEY_CTX of the signing operation will be written to
**pctx*: this can be used to set alternative signing options. Note that
any existing value in **pctx* is overwritten. The EVP_PKEY_CTX value
returned must not be freed directly by the application if *ctx* is not
assigned an EVP_PKEY_CTX value before being passed to
*EVP_DigestSignInit()* (which means the EVP_PKEY_CTX is created inside
*EVP_DigestSignInit()* and it will be freed automatically when the
EVP_MD_CTX is freed).

The digest *type* may be NULL if the signing algorithm supports it.

No *EVP_PKEY_CTX* will be created by *EVP_DigestSignInit()* if the
passed *ctx* has already been assigned one via
*EVP_MD_CTX_set_pkey_ctx* (3). See also *SM2* (7).

Only EVP_PKEY types that support signing can be used with these
functions. This includes MAC algorithms where the MAC generation is
considered as a form of signing. Built-in EVP_PKEY types supported by
these functions are CMAC, Poly1305, DSA, ECDSA, HMAC, RSA, SipHash,
Ed25519 and Ed448.

Not all digests can be used for all key types. The following
combinations apply.

- DSA :: Supports SHA1, SHA224, SHA256, SHA384 and SHA512

- ECDSA :: Supports SHA1, SHA224, SHA256, SHA384, SHA512 and SM3

- RSA with no padding :: Supports no digests (the digest *type* must be
  NULL)

- RSA with X931 padding :: Supports SHA1, SHA256, SHA384 and SHA512

- All other RSA padding types :: Support SHA1, SHA224, SHA256, SHA384,
  SHA512, MD5, MD5_SHA1, MD2, MD4, MDC2, SHA3-224, SHA3-256, SHA3-384,
  SHA3-512

- Ed25519 and Ed448 :: Support no digests (the digest *type* must be
  NULL)

- HMAC :: Supports any digest

- CMAC, Poly1305 and SipHash :: Will ignore any digest provided.

If RSA-PSS is used and restrictions apply then the digest must match.

*EVP_DigestSignUpdate()* hashes *cnt* bytes of data at *d* into the
signature context *ctx*. This function can be called several times on
the same *ctx* to include additional data. This function is currently
implemented using a macro.

*EVP_DigestSignFinal()* signs the data in *ctx* and places the signature
in *sig*. If *sig* is *NULL* then the maximum size of the output buffer
is written to the *siglen* parameter. If *sig* is not *NULL* then before
the call the *siglen* parameter should contain the length of the *sig*
buffer. If the call is successful the signature is written to *sig* and
the amount of data written to *siglen*.

*EVP_DigestSign()* signs *tbslen* bytes of data at *tbs* and places the
signature in *sig* and its length in *siglen* in a similar way to
*EVP_DigestSignFinal()*.

* RETURN VALUES
*EVP_DigestSignInit()*, *EVP_DigestSignUpdate()*,
*EVP_DigestSignFinal()* and *EVP_DigestSign()* return 1 for success and
0 for failure.

The error codes can be obtained from *ERR_get_error* (3).

* NOTES
The *EVP* interface to digital signatures should almost always be used
in preference to the low-level interfaces. This is because the code then
becomes transparent to the algorithm used and much more flexible.

*EVP_DigestSign()* is a one shot operation which signs a single block of
data in one function. For algorithms that support streaming it is
equivalent to calling *EVP_DigestSignUpdate()* and
*EVP_DigestSignFinal()*. For algorithms which do not support streaming
(e.g. PureEdDSA) it is the only way to sign data.

In previous versions of OpenSSL there was a link between message digest
types and public key algorithms. This meant that clone digests such as
*EVP_dss1()* needed to be used to sign using SHA1 and DSA. This is no
longer necessary and the use of clone digest is now discouraged.

For some key types and parameters the random number generator must be
seeded. If the automatic seeding or reseeding of the OpenSSL CSPRNG
fails due to external circumstances (see *RAND* (7)), the operation will
fail.

The call to *EVP_DigestSignFinal()* internally finalizes a copy of the
digest context. This means that calls to *EVP_DigestSignUpdate()* and
*EVP_DigestSignFinal()* can be called later to digest and sign
additional data.

Since only a copy of the digest context is ever finalized, the context
must be cleaned up after use by calling *EVP_MD_CTX_free()* or a memory
leak will occur.

The use of *EVP_PKEY_size()* with these functions is discouraged because
some signature operations may have a signature length which depends on
the parameters set. As a result *EVP_PKEY_size()* would have to return a
value which indicates the maximum possible signature for any set of
parameters.

* SEE ALSO
*EVP_DigestVerifyInit* (3), *EVP_DigestInit* (3), *evp* (7), *HMAC* (3),
*MD2* (3), *MD5* (3), *MDC2* (3), *RIPEMD160* (3), *SHA1* (3),
*dgst* (1), *RAND* (7)

* HISTORY
*EVP_DigestSignInit()*, *EVP_DigestSignUpdate()* and
*EVP_DigestSignFinal()* were added in OpenSSL 1.0.0.

* COPYRIGHT
Copyright 2006-2020 The OpenSSL Project Authors. All Rights Reserved.

Licensed under the OpenSSL license (the License). You may not use this
file except in compliance with the License. You can obtain a copy in the
file LICENSE in the source distribution or at
<https://www.openssl.org/source/license.html>.
