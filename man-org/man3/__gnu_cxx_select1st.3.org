#+TITLE: Manpages - __gnu_cxx_select1st.3
#+DESCRIPTION: Linux manpage for __gnu_cxx_select1st.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_cxx::select1st< _Pair > - An *SGI extension *.

* SYNOPSIS
\\

Inherits std::_Select1st< _Pair >.

** Public Types
typedef _Pair *argument_type*\\
=argument_type= is the type of the argument

typedef _Pair::first_type *result_type*\\
=result_type= is the return type

** Public Member Functions
_Pair::first_type & *operator()* (_Pair &__x) const\\

template<typename _Pair2 > _Pair2::first_type & *operator()* (_Pair2
&__x) const\\

const _Pair::first_type & *operator()* (const _Pair &__x) const\\

template<typename _Pair2 > const _Pair2::first_type & *operator()*
(const _Pair2 &__x) const\\

* Detailed Description
** "template<class _Pair>
\\
struct __gnu_cxx::select1st< _Pair >"An *SGI extension *.

Definition at line *192* of file *ext/functional*.

* Member Typedef Documentation
** typedef _Pair *std::unary_function*< _Pair , _Pair::first_type
>::*argument_type*= [inherited]=
=argument_type= is the type of the argument

Definition at line *108* of file *stl_function.h*.

** typedef _Pair::first_type *std::unary_function*< _Pair ,
_Pair::first_type >::*result_type*= [inherited]=
=result_type= is the return type

Definition at line *111* of file *stl_function.h*.

* Member Function Documentation
** template<typename _Pair > _Pair::first_type & std::_Select1st< _Pair
>::operator() (_Pair & __x) const= [inline]=, = [inherited]=
Definition at line *1151* of file *stl_function.h*.

** template<typename _Pair > template<typename _Pair2 >
_Pair2::first_type & std::_Select1st< _Pair >::operator() (_Pair2 & __x)
const= [inline]=, = [inherited]=
Definition at line *1161* of file *stl_function.h*.

** template<typename _Pair > const _Pair::first_type & std::_Select1st<
_Pair >::operator() (const _Pair & __x) const= [inline]=, = [inherited]=
Definition at line *1155* of file *stl_function.h*.

** template<typename _Pair > template<typename _Pair2 > const
_Pair2::first_type & std::_Select1st< _Pair >::operator() (const _Pair2
& __x) const= [inline]=, = [inherited]=
Definition at line *1166* of file *stl_function.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
