#+TITLE: Manpages - audit_is_enabled.3
#+DESCRIPTION: Linux manpage for audit_is_enabled.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
audit_is_enabled - judge whether auditing is enabled or not

* SYNOPSIS
#+begin_example
  #include <libaudit.h>

  int audit_is_enabled(int fd");
#+end_example

* DESCRIPTION
*audit_is_enabled*() judges whether auditing is enabled or not. /fd/
must have been returned by *audit_open*(3).

* RETURN VALUE
This function will return 0 if auditing is NOT enabled and 1 if enabled,
and -1 on error.

* SEE ALSO
*audit_set_enabled*(3).

* AUTHOR
Steve Grubb
