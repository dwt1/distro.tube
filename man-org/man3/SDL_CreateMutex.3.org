#+TITLE: Manpages - SDL_CreateMutex.3
#+DESCRIPTION: Linux manpage for SDL_CreateMutex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
SDL_CreateMutex - Create a mutex

* SYNOPSIS
*#include "SDL.h"* #include "SDL_thread.h"

*SDL_mutex *SDL_CreateMutex*(*void*);

* DESCRIPTION
Create a new, unlocked mutex.

* EXAMPLES
#+begin_example
  SDL_mutex *mut;

  mut=SDL_CreateMutex();
  .
  .
  if(SDL_mutexP(mut)==-1){
    fprintf(stderr, "Couldn't lock mutex
  ");
    exit(-1);
  }
  .
  /* Do stuff while mutex is locked */
  .
  .
  if(SDL_mutexV(mut)==-1){
    fprintf(stderr, "Couldn't unlock mutex
  ");
    exit(-1);
  }

  SDL_DestroyMutex(mut);
#+end_example

* SEE ALSO
*SDL_mutexP*, *SDL_mutexV*, *SDL_DestroyMutex*
