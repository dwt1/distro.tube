#+TITLE: Manpages - DPMSEnable.3
#+DESCRIPTION: Linux manpage for DPMSEnable.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
DPMSEnable - enables DPMS on the specified display

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/dpms.h>
  Status DPMSEnable ( Display *display  );
#+end_example

* ARGUMENTS
- /display/ :: Specifies the connection to the X server

* DESCRIPTION
The /DPMSEnable/ function enables Display Power Management Signaling
(DPMS) on the specified /display./ When DPMS is enabled, DPMS will use
the currently saved timeout values. It will trigger the appropriate DPMS
power level based on the timeout values. Refer to /DPMSSetTimeouts./ All
physical screens are affected by /DPMSEnable/ at the same time.

If /DPMSEnable/ is invoked on a display which has DPMS already enabled,
or on a display which does not support DPMS, no change is made and no
error is returned.

* RETURN VALUES
- TRUE :: The /DPMSEnable/ function always returns TRUE.

* SEE ALSO
*DPMSCapable*(3), *DPMSDisable*(3), *DPMSGetTimeouts*(3),
*DPMSSetTimeouts*(3)
