#+TITLE: Manpages - hwloc_obj_type_is_io.3
#+DESCRIPTION: Linux manpage for hwloc_obj_type_is_io.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about hwloc_obj_type_is_io.3 is found in manpage for: [[../man3/hwlocality_helper_types.3][man3/hwlocality_helper_types.3]]