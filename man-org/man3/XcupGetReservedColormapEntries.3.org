#+TITLE: Manpages - XcupGetReservedColormapEntries.3
#+DESCRIPTION: Linux manpage for XcupGetReservedColormapEntries.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XcupGetReservedColormapEntries - list colormap entries reserved by the
system

* SYNOPSIS
#+begin_example
  cc [ flag ... ] file ... -lXext [ library ... ]
  #include <X11/extensions/Xcup.h>
  Status XcupGetReservedColormapEntries ( Display *display ,
  int screen , XColor **colors_out , int *ncolors );
#+end_example

* ARGUMENTS
- display :: Specifies the connection to the X server.

- screen :: Screen number on the host server.

- colors_out :: Returns the values reserved by the server.

- ncolors :: Returns the number of items in /colors_out/.

* DESCRIPTION
The / XcupGetReservedColormapEntries / function returns a list of
colormap entries (pixels) that are reserved by the system. This list
will, at a minimum, contain entries for the BlackPixel and WhitePixel of
the specified screen. Use / XFree / to free /colors_out./

To minimize colormap flash, an application which installs its own
private colormap should query the special colors by calling
/ XCupGetReservedColormapEntries /, and can then store those entries (in
the proper location) in its private colormap using / XCupStoreColors /.

* SEE ALSO
*XcupQueryVersion*(3Xext), *XcupStoreColors*(3Xext), *XFree*(3X11),\\
/Colormap Utilization Policy and Extension/
