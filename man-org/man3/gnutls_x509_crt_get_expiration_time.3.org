#+TITLE: Manpages - gnutls_x509_crt_get_expiration_time.3
#+DESCRIPTION: Linux manpage for gnutls_x509_crt_get_expiration_time.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_x509_crt_get_expiration_time - API function

* SYNOPSIS
*#include <gnutls/x509.h>*

*time_t gnutls_x509_crt_get_expiration_time(gnutls_x509_crt_t
*/cert/*);*

* ARGUMENTS
- gnutls_x509_crt_t cert :: should contain a *gnutls_x509_crt_t* type

* DESCRIPTION
This function will return the time this certificate was or will be
expired.

* RETURNS
expiration time, or (time_t)-1 on error.

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
