#+TITLE: Manpages - ldns_rr_set_class.3
#+DESCRIPTION: Linux manpage for ldns_rr_set_class.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ldns_rr_set_owner, ldns_rr_set_ttl, ldns_rr_set_type,
ldns_rr_set_rd_count, ldns_rr_set_class, ldns_rr_set_rdf - set ldns_rr
attributes

* SYNOPSIS
#include <stdint.h>\\
#include <stdbool.h>\\

#include <ldns/ldns.h>

void ldns_rr_set_owner(ldns_rr *rr, ldns_rdf *owner);

void ldns_rr_set_ttl(ldns_rr *rr, uint32_t ttl);

void ldns_rr_set_type(ldns_rr *rr, ldns_rr_type rr_type);

void ldns_rr_set_rd_count(ldns_rr *rr, size_t count);

void ldns_rr_set_class(ldns_rr *rr, ldns_rr_class rr_class);

ldns_rdf* ldns_rr_set_rdf(ldns_rr *rr, const ldns_rdf *f, size_t
position);

* DESCRIPTION
/ldns_rr_set_owner/() sets the owner in the rr structure. .br **rr*: rr
to operate on .br **owner*: set to this owner .br Returns void

/ldns_rr_set_ttl/() sets the ttl in the rr structure. .br **rr*: rr to
operate on .br *ttl*: set to this ttl .br Returns void

/ldns_rr_set_type/() sets the type in the rr. .br **rr*: rr to operate
on .br *rr_type*: set to this type .br Returns void

/ldns_rr_set_rd_count/() sets the rd_count in the rr. .br **rr*: rr to
operate on .br *count*: set to this count .br Returns void

/ldns_rr_set_class/() sets the class in the rr. .br **rr*: rr to operate
on .br *rr_class*: set to this class .br Returns void

/ldns_rr_set_rdf/() sets a rdf member, it will be set on the position
given. The old value is returned, like pop. .br **rr*: the rr to operate
on .br **f*: the rdf to set .br *position*: the position the set the rdf
.br Returns the old value in the rr, NULL on failyre

* AUTHOR
The ldns team at NLnet Labs.

* REPORTING BUGS
Please report bugs to ldns-team@nlnetlabs.nl or in our bugzilla at
http://www.nlnetlabs.nl/bugs/index.html

* COPYRIGHT
Copyright (c) 2004 - 2006 NLnet Labs.

Licensed under the BSD License. There is NO warranty; not even for
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* SEE ALSO
/ldns_rr/, /ldns_rr_list/. And *perldoc Net::DNS*, *RFC1034*, *RFC1035*,
*RFC4033*, *RFC4034* and *RFC4035*.

* REMARKS
This manpage was automatically generated from the ldns source code.
