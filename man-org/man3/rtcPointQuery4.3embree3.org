#+TITLE: Manpages - rtcPointQuery4.3embree3
#+DESCRIPTION: Linux manpage for rtcPointQuery4.3embree3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
** NAME
#+begin_example
  rtcPointQuery4/8/16 - traverses the BVH with a point query object for a ray packet
#+end_example

** DESCRIPTION
The =rtcPointQuery4/8/16= function unrolls the ray packet internally and
calls [rtcPointQuery].

** SEE ALSO
[rtcPointQuery]
