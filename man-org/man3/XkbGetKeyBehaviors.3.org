#+TITLE: Manpages - XkbGetKeyBehaviors.3
#+DESCRIPTION: Linux manpage for XkbGetKeyBehaviors.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetKeyBehaviors - Obtain the behaviors (the behaviors array) for a
subset of the keys in a keyboard description from the server

* SYNOPSIS
*Status XkbGetKeyBehaviors* *( Display **/dpy/* ,* *unsigned int
*/first/* ,* *unsigned int */num/* ,* *XkbDescPtr */xkb/* );*

* ARGUMENTS
- /- dpy/ :: connection to server

- /- first/ :: keycode of first key to get

- /- num/ :: number of keys for which behaviors are desired

- /- xkb/ :: Xkb description to contain the result

* DESCRIPTION
/XkbGetKeyBehaviors/ sends a request to the server to obtain the
behaviors for /num/ keys on the keyboard starting with the key whose
keycode is /first./ It waits for a reply and returns the behaviors in
the /server->behaviors/ field of /xkb./ If successful,
/XkbGetKeyBehaviors/ returns Success.

If the /server/ map in the /xkb/ parameter has not been allocated,
/XkbGetKeyBehaviors/ allocates and initializes it before obtaining the
actions.

If the server does not have a compatible version of Xkb, or the Xkb
extension has not been properly initialized, /XkbGetKeyBehaviors/
returns BadAccess. If /num/ is less than 1 or greater than
XkbMaxKeyCount, /XkbGetKeyBehaviors/ returns BadValue. If any allocation
errors occur, /XkbGetKeyBehaviors/ returns BadAlloc.

* DIAGNOSTICS
- *BadAccess* :: The Xkb extension has not been properly initialized

- *BadAlloc* :: Unable to allocate storage

- *BadValue* :: An argument is out of range
