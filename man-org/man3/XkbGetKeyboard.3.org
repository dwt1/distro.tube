#+TITLE: Manpages - XkbGetKeyboard.3
#+DESCRIPTION: Linux manpage for XkbGetKeyboard.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
XkbGetKeyboard - Retrieves one or more components of a keyboard device
description

* SYNOPSIS
*XkbDescPtr XkbGetKeyboard* *( Display **/display/* ,* *unsigned int
*/which/* ,* *unsigned int */device_spec/* );*

* ARGUMENTS
- /display/ :: connection to the X server

- /device_spec/ :: device ID, or XkbUseCoreKbd

- /bits_to_change/ :: determines events to be selected / deselected

- /values_for_bits/ :: 1=>select, 0->deselect; for events in
  bits_to_change

* DESCRIPTION
/XkbGetKeyboard/ allocates and returns a pointer to a keyboard
description. It queries the server for those components specified in the
/which/ parameter for device /device_spec/ and copies the results to the
XkbDescRec it allocated. The remaining fields in the keyboard
description are set to NULL. The valid masks for /which/ are those
listed in Table 1.

TABLE

/XkbGetKeyboard/ is used to read the current description for one or more
components of a keyboard device. It calls /XkbGetKeyboardByName/ as
follows:

/XkbGetKeyboardByName(dpy, device_spec,/ NULL, /which, which,/ False).

* DIAGNOSTICS
Unable to allocate storage
