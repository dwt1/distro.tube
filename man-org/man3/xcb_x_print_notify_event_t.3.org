#+TITLE: Manpages - xcb_x_print_notify_event_t.3
#+DESCRIPTION: Linux manpage for xcb_x_print_notify_event_t.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xcb_x_print_notify_event_t -

* SYNOPSIS
*#include <xcb/xprint.h>*

** Event datastructure
#+begin_example

  typedef struct xcb_x_print_notify_event_t {
      uint8_t                response_type;
      uint8_t                detail;
      uint16_t               sequence;
      xcb_x_print_pcontext_t context;
      uint8_t                cancel;
  } xcb_x_print_notify_event_t;
#+end_example

\\

* EVENT FIELDS
- response_type :: The type of this event, in this case
  /XCB_X_PRINT_NOTIFY/. This field is also present in the
  /xcb_generic_event_t/ and can be used to tell events apart from each
  other.

- sequence :: The sequence number of the last request processed by the
  X11 server.

- detail :: NOT YET DOCUMENTED.

- context :: NOT YET DOCUMENTED.

- cancel :: NOT YET DOCUMENTED.

* DESCRIPTION
* SEE ALSO
* AUTHOR
Generated from xprint.xml. Contact xcb@lists.freedesktop.org for
corrections and improvements.
