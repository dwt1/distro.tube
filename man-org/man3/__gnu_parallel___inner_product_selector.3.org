#+TITLE: Manpages - __gnu_parallel___inner_product_selector.3
#+DESCRIPTION: Linux manpage for __gnu_parallel___inner_product_selector.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
__gnu_parallel::__inner_product_selector< _It, _It2, _Tp > -
std::inner_product() selector.

* SYNOPSIS
\\

=#include <for_each_selectors.h>=

Inherits *__gnu_parallel::__generic_for_each_selector< _It >*.

** Public Member Functions
*__inner_product_selector* (_It __b1, _It2 __b2)\\
Constructor.

template<typename _Op > _Tp *operator()* (_Op __mult, _It __current)\\
Functor execution.

** Public Attributes
_It *__begin1_iterator*\\
Begin iterator of first sequence.

_It2 *__begin2_iterator*\\
Begin iterator of second sequence.

_It *_M_finish_iterator*\\
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

* Detailed Description
** "template<typename _It, typename _It2, typename _Tp>
\\
struct __gnu_parallel::__inner_product_selector< _It, _It2, _Tp
>"std::inner_product() selector.

Definition at line *222* of file *for_each_selectors.h*.

* Constructor & Destructor Documentation
** template<typename _It , typename _It2 , typename _Tp >
*__gnu_parallel::__inner_product_selector*< _It, _It2, _Tp
>::*__inner_product_selector* (_It __b1, _It2 __b2)= [inline]=,
= [explicit]=
Constructor.

*Parameters*

#+begin_quote
  /__b1/ Begin iterator of first sequence.\\
  /__b2/ Begin iterator of second sequence.
#+end_quote

Definition at line *234* of file *for_each_selectors.h*.

* Member Function Documentation
** template<typename _It , typename _It2 , typename _Tp >
template<typename _Op > _Tp *__gnu_parallel::__inner_product_selector*<
_It, _It2, _Tp >::operator() (_Op __mult, _It __current)= [inline]=
Functor execution.

*Parameters*

#+begin_quote
  /__mult/ Multiplication functor.\\
  /__current/ iterator referencing object.
#+end_quote

*Returns*

#+begin_quote
  Inner product elemental __result.
#+end_quote

Definition at line *243* of file *for_each_selectors.h*.

References *__gnu_parallel::__inner_product_selector< _It, _It2, _Tp
>::__begin1_iterator*, and *__gnu_parallel::__inner_product_selector<
_It, _It2, _Tp >::__begin2_iterator*.

* Member Data Documentation
** template<typename _It , typename _It2 , typename _Tp > _It
*__gnu_parallel::__inner_product_selector*< _It, _It2, _Tp
>::__begin1_iterator
Begin iterator of first sequence.

Definition at line *225* of file *for_each_selectors.h*.

Referenced by *__gnu_parallel::__inner_product_selector< _It, _It2, _Tp
>::operator()()*.

** template<typename _It , typename _It2 , typename _Tp > _It2
*__gnu_parallel::__inner_product_selector*< _It, _It2, _Tp
>::__begin2_iterator
Begin iterator of second sequence.

Definition at line *228* of file *for_each_selectors.h*.

Referenced by *__gnu_parallel::__inner_product_selector< _It, _It2, _Tp
>::operator()()*.

** template<typename _It > _It
*__gnu_parallel::__generic_for_each_selector*< _It
>::_M_finish_iterator= [inherited]=
_Iterator on last element processed; needed for some algorithms (e. g.
std::transform()).

Definition at line *47* of file *for_each_selectors.h*.

* Author
Generated automatically by Doxygen for libstdc++ from the source code.
