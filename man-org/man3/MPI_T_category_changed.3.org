#+TITLE: Manpages - MPI_T_category_changed.3
#+DESCRIPTION: Linux manpage for MPI_T_category_changed.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*MPI_T_category_changed* - Get a timestamp for the categories

* SYNTAX
* C Syntax
#+begin_example
  #include <mpi.h>
  int MPI_T_category_changed(int *stamp)
#+end_example

* INPUT PARAMETERS
- stamp :: A virtual time stamp to indicate the last change to the
  categories.

* DESCRIPTION
If two subsequent calls to this routine return the same timestamp, it is
guaranteed that no categories have been changed or added. If the
timestamp from the second call is higher than some categories have been
added or changed.

* ERRORS
MPI_T_category_changed() will fail if:

- [MPI_T_ERR_NOT_INITIALIZED] :: The MPI Tools interface not initialized
