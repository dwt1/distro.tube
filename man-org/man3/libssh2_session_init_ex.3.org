#+TITLE: Manpages - libssh2_session_init_ex.3
#+DESCRIPTION: Linux manpage for libssh2_session_init_ex.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
libssh2_session_init_ex - initializes an SSH session object

* SYNOPSIS
#include <libssh2.h>

LIBSSH2_SESSION *
libssh2_session_init_ex(LIBSSH2_ALLOC_FUNC((*myalloc)),
LIBSSH2_FREE_FUNC((*myfree)), LIBSSH2_REALLOC_FUNC((*myrealloc)), void
*abstract);

LIBSSH2_SESSION * libssh2_session_init(void);

* DESCRIPTION
/myalloc/ - Custom allocator function. Refer to the section on Callbacks
for implementing an allocator callback. Pass a value of NULL to use the
default system allocator.

/myfree/ - Custom de-allocator function. Refer to the section on
Callbacks for implementing a deallocator callback. Pass a value of NULL
to use the default system deallocator.

/myrealloc/ - Custom re-allocator function. Refer to the section on
Callbacks for implementing a reallocator callback. Pass a value of NULL
to use the default system reallocator.

/abstract/ - Arbitrary pointer to application specific callback data.
This value will be passed to any callback function associated with the
named session instance.

Initializes an SSH session object. By default system memory allocators
(malloc(), free(), realloc()) will be used for any dynamically allocated
memory blocks. Alternate memory allocation functions may be specified
using the extended version of this API call, and/or optional application
specific data may be attached to the session object.

This method must be called first, prior to configuring session options
or starting up an SSH session with a remote server.

* RETURN VALUE
Pointer to a newly allocated LIBSSH2_SESSION instance, or NULL on
errors.

* SEE ALSO
*libssh2_session_free(3)* *libssh2_session_handshake(3)*
