#+TITLE: Manpages - getservbyname.3
#+DESCRIPTION: Linux manpage for getservbyname.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getservbyname.3 is found in manpage for: [[../man3/getservent.3][man3/getservent.3]]