#+TITLE: Manpages - Alien_Build_rc.3pm
#+DESCRIPTION: Linux manpage for Alien_Build_rc.3pm
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
Alien::Build::rc - Alien::Build local config

* VERSION
version 2.44

* SYNOPSIS
in your =~/.alienbuild/rc.pl=:

preload Foo::Bar; postload Baz::Frooble;

* DESCRIPTION
Alien::Build will load your =~/.alienbuild/rc.pl= file, if it exists
before running the alienfile recipe. This allows you to alter the
behavior of Alien::Build based Aliens if you have local configuration
requirements. For example you can prompt before downloading remote
content or fetch from a local mirror.

* FUNCTIONS
** logx
log $message;

Send a message to the Alien::Build log.

** preload
preload $plugin;

Preload the given plugin.

** postload
postload $plugin;

Postload the given plugin.

* SEE ALSO
- Alien::Build::Plugin::Fetch::Cache :: 

- Alien::Build::Plugin::Fetch::Prompt :: 

- Alien::Build::Plugin::Fetch::Rewrite :: 

- Alien::Build::Plugin::Probe::Override :: 

* AUTHOR
Author: Graham Ollis <plicease@cpan.org>

Contributors:

Diab Jerius (DJERIUS)

Roy Storey (KIWIROY)

Ilya Pavlov

David Mertens (run4flat)

Mark Nunberg (mordy, mnunberg)

Christian Walde (Mithaldu)

Brian Wightman (MidLifeXis)

Zaki Mughal (zmughal)

mohawk (mohawk2, ETJ)

Vikas N Kumar (vikasnkumar)

Flavio Poletti (polettix)

Salvador Fandiño (salva)

Gianni Ceccarelli (dakkar)

Pavel Shaydo (zwon, trinitum)

Kang-min Liu (劉康民, gugod)

Nicholas Shipp (nshp)

Juan Julián Merelo Guervós (JJ)

Joel Berger (JBERGER)

Petr Písař (ppisar)

Lance Wicks (LANCEW)

Ahmad Fatoum (a3f, ATHREEF)

José Joaquín Atria (JJATRIA)

Duke Leto (LETO)

Shoichi Kaji (SKAJI)

Shawn Laffan (SLAFFAN)

Paul Evans (leonerd, PEVANS)

Håkon Hægland (hakonhagland, HAKONH)

nick nauwelaerts (INPHOBIA)

* COPYRIGHT AND LICENSE
This software is copyright (c) 2011-2020 by Graham Ollis.

This is free software; you can redistribute it and/or modify it under
the same terms as the Perl 5 programming language system itself.
