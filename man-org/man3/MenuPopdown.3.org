#+TITLE: Manpages - MenuPopdown.3
#+DESCRIPTION: Linux manpage for MenuPopdown.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about MenuPopdown.3 is found in manpage for: [[../man3/XtPopdown.3][man3/XtPopdown.3]]