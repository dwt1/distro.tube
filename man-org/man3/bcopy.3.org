#+TITLE: Manpages - bcopy.3
#+DESCRIPTION: Linux manpage for bcopy.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bcopy - copy byte sequence

* SYNOPSIS
#+begin_example
  #include <strings.h>

  void bcopy(const void *src, void *dest, size_t n);
#+end_example

* DESCRIPTION
The *bcopy*() function copies /n/ bytes from /src/ to /dest/. The result
is correct, even when both areas overlap.

* RETURN VALUE
None.

* ATTRIBUTES
For an explanation of the terms used in this section, see
*attributes*(7).

| Interface | Attribute     | Value   |
| *bcopy*() | Thread safety | MT-Safe |

* CONFORMING TO
4.3BSD. This function is deprecated (marked as LEGACY in POSIX.1-2001):
use *memcpy*(3) or *memmove*(3) in new programs. Note that the first two
arguments are interchanged for *memcpy*(3) and *memmove*(3).
POSIX.1-2008 removes the specification of *bcopy*().

* SEE ALSO
*bstring*(3), *memccpy*(3), *memcpy*(3), *memmove*(3), *strcpy*(3),
*strncpy*(3)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
