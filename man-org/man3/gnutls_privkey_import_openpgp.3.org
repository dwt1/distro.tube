#+TITLE: Manpages - gnutls_privkey_import_openpgp.3
#+DESCRIPTION: Linux manpage for gnutls_privkey_import_openpgp.3
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gnutls_privkey_import_openpgp - API function

* SYNOPSIS
*#include <gnutls/abstract.h>*

*int gnutls_privkey_import_openpgp(gnutls_privkey_t */pkey/*,
gnutls_openpgp_privkey_t */key/*, unsigned int */flags/*);*

* ARGUMENTS
- gnutls_privkey_t pkey :: The private key

- gnutls_openpgp_privkey_t key :: The private key to be imported

- unsigned int flags :: Flags for the import

* DESCRIPTION
This function is no-op.

* RETURNS
*GNUTLS_E_UNIMPLEMENTED_FEATURE*.

* SINCE
2.12.0

* REPORTING BUGS
Report bugs to <bugs@gnutls.org>.\\
Home page: https://www.gnutls.org

* COPYRIGHT
Copyright © 2001- Free Software Foundation, Inc., and others.\\
Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.

* SEE ALSO
The full documentation for *gnutls* is maintained as a Texinfo manual.
If the /usr/share/doc/gnutls/ directory does not contain the HTML form
visit

- https://www.gnutls.org/manual/ :: 
