#+TITLE: Man1 - pbmtoicon.1
#+DESCRIPTION: Linux manpage for pbmtoicon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtoicon - convert a Sun icon image to PBM

* SYNOPSIS
*pbmtoicon* [/iconfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtoicon* was obsoleted by **pbmtosunicon**(1) , introduced with
Netpbm 10.53 (December 2010). *pbmtosunicon* is backward compatible with
*pbmtoicon*, plus adds additional functions, including the ability to
convert a Depth=8 Sun icon, producing a PGM image.

Starting in Release 10.53, *pbmtoicon* is just an alias for
*pbmtosunicon*.

The point of the name change is that there are many kinds of icons in
the world besides Sun icons, and in 2010, the Sun variety isn't even
common.

In releases before 10.53, you can use the *pbmtosunicon* documentation
for *pbmtoicon*, as long as you recognize that any change the
*pbmtosunicon* manual says happened in or after Netpbm 10.53 doesn't
apply to *pbmtoicon*.
