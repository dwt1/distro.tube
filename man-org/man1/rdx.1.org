#+TITLE: Man1 - rdx.1
#+DESCRIPTION: Linux manpage for rdx.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rdx - load and execute an RDOFF object

* SYNOPSIS
*rdx* /object/

* DESCRIPTION
*rdx* loads an RDOFF /object/, and then calls `*_main*', which it
expects to be a C-style function, accepting two parameters, /argc/ and
/argv/ in normal C style.

* AUTHORS
Julian Hall <jules@earthcorp.com>.

This manual page was written by Matej Vela <vela@debian.org>.
