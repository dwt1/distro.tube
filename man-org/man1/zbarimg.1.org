#+TITLE: Man1 - zbarimg.1
#+DESCRIPTION: Linux manpage for zbarimg.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zbarimg - scan and decode bar codes from image file(s)

* SYNOPSIS
*zbarimg* [*-qv*] [*--quiet*] [*--verbose[=*/n/]]\\
{*-dD* | *--display* | *--nodisplay* | *--xml* | *--noxml* |
*-S[*/symbology/.]/config/*[=*/value/] | *--set
[*/symbology/.]/config/*[=*/value/] | /image/...}

*zbarimg* {*-h* | *--help* | *--version*}

* DESCRIPTION
For each specified /image/ file *zbarimg* scans the image for bar codes
and prints any decoded data to stdout. Images may optionally be
displayed to the screen.

The underlying library currently supports EAN-13 (including UPC and ISBN
subsets), EAN-8, DataBar, DataBar Expanded, Code 128, Code 93, Code 39,
Codabar, Interleaved 2 of 5 and QR Code symbologies. The specific type
of each detected symbol is printed with the decoded data.

Note that "/image/" in this context refers to any format supported by
ImageMagick, including many vector formats such as PDF and PostScript.
Keep in mind that vector formats are rasterized before scanning;
manually rasterize vector images before scanning to avoid
unintentionally corrupting embedded barcode bitmaps.

* OPTIONS
This program follows the usual GNU command line syntax. Single letter
options may be bundled, long options start with two dashes (`-).

*-h*, *--help*

#+begin_quote
  Print a short help message describing command line options to standard
  output and exit
#+end_quote

*--version*

#+begin_quote
  Print program version information to standard output and exit
#+end_quote

*-v*, *--verbose[=*/n/]

#+begin_quote
  Increase debug output level. Multiple *-v* options create more spew.
  Alternatively specify /n/ to set the debug level directly
#+end_quote

*-S[*/symbology/.]/config/*[=*/value/], *--set
[*/symbology/.]/config/*[=*/value/]

#+begin_quote
  Set decoder configuration option /config/ for /symbology/ to /value/.
  /value/ defaults to 1 if omitted. /symbology/ is one of *ean13*,
  *ean8*, *upca*, *upce*, *isbn13*, *isbn10*, *i25*, *codabar*,
  *code39*, *code93*, *code128*, *qrcode* or the special value ***. If
  /symbology/ is omitted or ***, the /config/ will be set for all
  applicable symbologies. These are the currently recognized /config/s.
  Prefix a config with "no-" to negate it. Not all configs are
  appropriate for every symbology.

  *enable*

  #+begin_quote
    Control decoding/reporting of a symbology. For symbologies which are
    just subsets of *ean13* (*upca*, *upce*, *isbn13*, *isbn10*), this
    config controls whether the subsets are detected and reported as
    such. These special cases are disabled by default, all other
    symbologies default to enabled
  #+end_quote

  *disable*

  #+begin_quote
    Antonym for *enable*
  #+end_quote

  *emit-check*

  #+begin_quote
    Control whether check digits are included in the decoded output.
    Enabled by default. This config does not apply for *code128*, which
    never returns the check digit. It also not apply for cases where the
    check digit is disabled (see *add-check*). Check digits are
    currently not implemented for *i25* or *code39*
  #+end_quote

  *add-check*

  #+begin_quote
    Enable decode and verification of a check digit for symbologies
    where it is optional: this will include *code39* and *i25*, neither
    of which implements the check digit yet
  #+end_quote

  *ascii*

  #+begin_quote
    Enable escape sequences that encode the full ASCII character set.
    This would apply to *code39*, except that its not implemented
    either...
  #+end_quote

  *position*

  #+begin_quote
    Enable collection of symbol position information. Enabled by
    default. Currently, the position information is unusable, so you can
    save a few cycles by disabling this.
  #+end_quote

  *test-inverted*

  #+begin_quote
    Specially for QR code images, sometimes the image is inverted, e. g.
    lines are written in white instead of black. This option makes ZBar
    to invert the image and parse again, in case it fails using the
    normal order. Enabling it affects all decoders.
  #+end_quote

  *min-length=*/n/, *max-length=*/n/

  #+begin_quote
    Bound the number of decoded characters in a valid symbol. If a
    decode result is outside the configured min/max range (inclusive),
    it will not be reported. Set to 0 to disable the corresponding
    check. This setting applies to variable-length symbologies: *i25*,
    *codabar*, *code39*, *code128* and *pdf417*. *min-length* defaults
    to 6 for *i25* and 1 for *code39* (per Code 39 autodiscrimination
    recommendation); all others default to 0
  #+end_quote

  *x-density=*/n/, *y-density=*/n/

  #+begin_quote
    Adjust the density of the scanner passes. Lower values scan more of
    the image at the cost of decreased performance. Setting to 0
    disables scanning along that axis. Defaults are both 1.
  #+end_quote
#+end_quote

*-q*, *--quiet*

#+begin_quote
  Quiet operation; only output decoded symbol data. specifically this
  disables the statistics line printed (to stderr) before exiting, as
  well as the warning message printed (also to stderr) when no barcodes
  are found in an image
#+end_quote

*-d*, *--display*, *-D*, *--nodisplay*

#+begin_quote
  Enable/disable display of subsequent /image/ files, until next
  *--display* or *--nodisplay* is encountered. This option may appear
  multiple times to enable display of specific images. Image display is
  disabled by default
#+end_quote

*--xml*, *--noxml*

#+begin_quote
  Enable/disable result output using an XML format. This format wraps
  the raw data from the symbol with information about the scan (such as
  page indices) in an easy to parse format. The latest schema is
  available from *http://zbar.sourceforge.net/2008/barcode.xsd*.
#+end_quote

*--raw*

#+begin_quote
  Enable raw symbol data output. This format prints symbol data
  separated by newlines without the additional symbology type
  information that is printed by default
#+end_quote

* EXAMPLES
Scan a PNG image of a UPC bar code symbol and pass resulting data to a
script that searches for the code in a database and does something
useful with it:

#+begin_quote
  #+begin_example
    zbarimg product.png | upcrpc.py
  #+end_example
#+end_quote

The *upcrpc.py* example script included in the examples/ subdirectory of
the distribution will make an XMLRPC call to a popular internet UPC
database and print the product description if found.

Scan a JPEG image containing several barcodes and display the image in a
window, also disabling recognition of Interleaved 2 of 5 codes to
prevent confusion with other symbologies or background noise:

#+begin_quote
  #+begin_example
    zbarimg --display -Si25.disable label.jpg
  #+end_example
#+end_quote

Look in a scanned document only for Code 39, using XML output format so
the page numbers are available. To enable only Code 39, first all
symbologies are disabled, then Code 39 is re-enabled:

#+begin_quote
  #+begin_example
    zbarimg --xml -Sdisable -Scode39.enable scan.tiff
  #+end_example
#+end_quote

* EXIT STATUS
*zbarimg* returns an exit code to indicate the status of the program
execution. Current exit codes are:

0

#+begin_quote
  Barcodes successfully detected in all images. Warnings may have been
  generated, but no errors.
#+end_quote

1

#+begin_quote
  An error occurred while processing some image(s). This includes bad
  arguments, I/O errors and image handling errors from ImageMagick.
#+end_quote

2

#+begin_quote
  ImageMagick fatal error.
#+end_quote

3

#+begin_quote
  The user quit the program before all images were scanned. Only applies
  when running in interactive mode (with *--display*)
#+end_quote

4

#+begin_quote
  No barcode was detected in one or more of the images. No other errors
  occurred.
#+end_quote

* SEE ALSO
zbarcam(1)

*http://zbar.sf.net/*

* BUGS
See *http://sf.net/tracker/?group_id=189236&atid=928515*

* AUTHOR
*Jeff Brown* <spadix@users.sourceforge.net>

#+begin_quote
  Lead developer
#+end_quote

* COPYRIGHT
\\
Copyright © 2007-2010 Jeff Brown\\

All Rights Reserved
