#+TITLE: Manpages - pwait.1
#+DESCRIPTION: Linux manpage for pwait.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about pwait.1 is found in manpage for: [[../man1/pgrep.1][man1/pgrep.1]]