#+TITLE: Man1 - smbcacls.1
#+DESCRIPTION: Linux manpage for smbcacls.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
smbcacls - Set or get ACLs on an NT file or directory names

* SYNOPSIS
smbcacls {//server/share} {/filename} [-D|--delete=ACL]
[-M|--modify=ACL] [-a|--add=ACL] [-S|--set=ACLS] [-C|--chown=USERNAME]
[-G|--chgrp=GROUPNAME] [-I|--inherit=STRING] [--propagate-inheritance]
[--numeric] [--sddl] [--query-security-info=INT]
[--set-security-info=INT] [-t|--test-args] [--domain-sid=SID]
[-x|--maximum-access] [-?|--help] [--usage] [-d|--debuglevel=DEBUGLEVEL]
[--debug-stdout] [--configfile=CONFIGFILE] [--option=name=value]
[-l|--log-basename=LOGFILEBASE] [--leak-report] [--leak-report-full]
[-R|--name-resolve=NAME-RESOLVE-ORDER]
[-O|--socket-options=SOCKETOPTIONS] [-m|--max-protocol=MAXPROTOCOL]
[-n|--netbiosname=NETBIOSNAME] [--netbios-scope=SCOPE]
[-W|--workgroup=WORKGROUP] [--realm=REALM]
[-U|--user=[DOMAIN/]USERNAME[%PASSWORD]] [-N|--no-pass]
[--password=STRING] [--pw-nt-hash] [-A|--authentication-file=FILE]
[-P|--machine-pass] [--simple-bind-dn=DN]
[--use-kerberos=desired|required|off] [--use-krb5-ccache=CCACHE]
[--use-winbind-ccache] [--client-protection=sign|encrypt|off]
[-V|--version]

* DESCRIPTION
This tool is part of the *samba*(7) suite.

The smbcacls program manipulates NT Access Control Lists (ACLs) on SMB
file shares. An ACL is comprised zero or more Access Control Entries
(ACEs), which define access restrictions for a specific user or group.

* OPTIONS
The following options are available to the smbcacls program. The format
of ACLs is described in the section ACL FORMAT

-a|--add acl

#+begin_quote
  Add the entries specified to the ACL. Existing access control entries
  are unchanged.
#+end_quote

-M|--modify acl

#+begin_quote
  Modify the mask value (permissions) for the ACEs specified on the
  command line. An error will be printed for each ACE specified that was
  not already present in the objects ACL.
#+end_quote

-D|--delete acl

#+begin_quote
  Delete any ACEs specified on the command line. An error will be
  printed for each ACE specified that was not already present in the
  objects ACL.
#+end_quote

-S|--set acl

#+begin_quote
  This command sets the ACL on the object with only what is specified on
  the command line. Any existing ACL is erased. Note that the ACL
  specified must contain at least a revision, type, owner and group for
  the call to succeed.
#+end_quote

-C|--chown name

#+begin_quote
  The owner of a file or directory can be changed to the name given
  using the /-C/ option. The name can be a sid in the form S-1-x-y-z or
  a name resolved against the server specified in the first argument.

  This command is a shortcut for -M OWNER:name.
#+end_quote

-G|--chgrp name

#+begin_quote
  The group owner of a file or directory can be changed to the name
  given using the /-G/ option. The name can be a sid in the form
  S-1-x-y-z or a name resolved against the server specified n the first
  argument.

  This command is a shortcut for -M GROUP:name.
#+end_quote

-I|--inherit allow|remove|copy

#+begin_quote
  Set or unset the windows "Allow inheritable permissions" check box
  using the /-I/ option. To set the check box pass allow. To unset the
  check box pass either remove or copy. Remove will remove all inherited
  ACEs. Copy will copy all the inherited ACEs.
#+end_quote

--propagate-inheritance

#+begin_quote
  Add, modify, delete or set ACEs on an entire directory tree according
  to the inheritance flags. Refer to the INHERITANCE section for
  details.
#+end_quote

--numeric

#+begin_quote
  This option displays all ACL information in numeric format. The
  default is to convert SIDs to names and ACE types and masks to a
  readable string format.
#+end_quote

-m|--max-protocol PROTOCOL_NAME

#+begin_quote
  This allows the user to select the highest SMB protocol level that
  smbcacls will use to connect to the server. By default this is set to
  NT1, which is the highest available SMB1 protocol. To connect using
  SMB2 or SMB3 protocol, use the strings SMB2 or SMB3 respectively. Note
  that to connect to a Windows 2012 server with encrypted transport
  selecting a max-protocol of SMB3 is required.
#+end_quote

-t|--test-args

#+begin_quote
  Dont actually do anything, only validate the correctness of the
  arguments.
#+end_quote

--query-security-info FLAGS

#+begin_quote
  The security-info flags for queries.
#+end_quote

--set-security-info FLAGS

#+begin_quote
  The security-info flags for queries.
#+end_quote

--sddl

#+begin_quote
  Output and input acls in sddl format.
#+end_quote

--domain-sid SID

#+begin_quote
  SID used for sddl processing.
#+end_quote

-x|--maximum-access

#+begin_quote
  When displaying an ACL additionally query the server for effective
  maximum permissions. Note that this is only supported with SMB
  protocol version 2 or higher.
#+end_quote

-?|--help

#+begin_quote
  Print a summary of command line options.
#+end_quote

--usage

#+begin_quote
  Display brief usage message.
#+end_quote

-d|--debuglevel=DEBUGLEVEL

#+begin_quote
  /level/ is an integer from 0 to 10. The default value if this
  parameter is not specified is 1 for client applications.

  The higher this value, the more detail will be logged to the log files
  about the activities of the server. At level 0, only critical errors
  and serious warnings will be logged. Level 1 is a reasonable level for
  day-to-day running - it generates a small amount of information about
  operations carried out.

  Levels above 1 will generate considerable amounts of log data, and
  should only be used when investigating a problem. Levels above 3 are
  designed for use only by developers and generate HUGE amounts of log
  data, most of which is extremely cryptic.

  Note that specifying this parameter here will override the *log level*
  parameter in the smb.conf file.
#+end_quote

--debug-stdout

#+begin_quote
  This will redirect debug output to STDOUT. By default all clients are
  logging to STDERR.
#+end_quote

--configfile=<configuration file>

#+begin_quote
  The file specified contains the configuration details required by the
  client. The information in this file can be general for client and
  server or only provide client specific like options such as *client
  smb encrypt*. See smb.conf for more information. The default
  configuration file name is determined at compile time.
#+end_quote

--option=<name>=<value>

#+begin_quote
  Set the *smb.conf*(5) option "<name>" to value "<value>" from the
  command line. This overrides compiled-in defaults and options read
  from the configuration file. If a name or a value includes a space,
  wrap whole --option=name=value into quotes.
#+end_quote

-l|--log-basename=logdirectory

#+begin_quote
  Base directory name for log/debug files. The extension *".progname"*
  will be appended (e.g. log.smbclient, log.smbd, etc...). The log file
  is never removed by the client.
#+end_quote

--leak-report

#+begin_quote
  Enable talloc leak reporting on exit.
#+end_quote

--leak-report-full

#+begin_quote
  Enable full talloc leak reporting on exit.
#+end_quote

-V|--version

#+begin_quote
  Prints the program version number.
#+end_quote

-R|--name-resolve=NAME-RESOLVE-ORDER

#+begin_quote
  This option is used to determine what naming services and in what
  order to resolve host names to IP addresses. The option takes a
  space-separated string of different name resolution options. The best
  ist to wrap the whole --name-resolve=NAME-RESOLVE-ORDER into quotes.

  The options are: "lmhosts", "host", "wins" and "bcast". They cause
  names to be resolved as follows:

  #+begin_quote

    #+begin_quote
      ·

      *lmhosts*: Lookup an IP address in the Samba lmhosts file. If the
      line in lmhosts has no name type attached to the NetBIOS name (see
      the *lmhosts*(5) for details) then any name type matches for
      lookup.
    #+end_quote

    #+begin_quote
      ·

      *host*: Do a standard host name to IP address resolution, using
      the system /etc/hosts, NIS, or DNS lookups. This method of name
      resolution is operating system dependent, for instance on IRIX or
      Solaris this may be controlled by the /etc/nsswitch.conf file).
      Note that this method is only used if the NetBIOS name type being
      queried is the 0x20 (server) name type, otherwise it is ignored.
    #+end_quote

    #+begin_quote
      ·

      *wins*: Query a name with the IP address listed in the /wins
      server/ parameter. If no WINS server has been specified this
      method will be ignored.
    #+end_quote

    #+begin_quote
      ·

      *bcast*: Do a broadcast on each of the known local interfaces
      listed in the /interfaces/ parameter. This is the least reliable
      of the name resolution methods as it depends on the target host
      being on a locally connected subnet.
    #+end_quote
  #+end_quote

  If this parameter is not set then the name resolve order defined in
  the smb.conf file parameter (*name resolve order*) will be used.

  The default order is lmhosts, host, wins, bcast. Without this
  parameter or any entry in the *name resolve order* parameter of the
  smb.conf file, the name resolution methods will be attempted in this
  order.
#+end_quote

-O|--socket-options=SOCKETOPTIONS

#+begin_quote
  TCP socket options to set on the client socket. See the socket options
  parameter in the smb.conf manual page for the list of valid options.
#+end_quote

-m|--max-protocol=MAXPROTOCOL

#+begin_quote
  The value of the parameter (a string) is the highest protocol level
  that will be supported by the client.

  Note that specifying this parameter here will override the *client max
  protocol* parameter in the smb.conf file.
#+end_quote

-n|--netbiosname=NETBIOSNAME

#+begin_quote
  This option allows you to override the NetBIOS name that Samba uses
  for itself. This is identical to setting the *netbios name* parameter
  in the smb.conf file. However, a command line setting will take
  precedence over settings in smb.conf.
#+end_quote

--netbios-scope=SCOPE

#+begin_quote
  This specifies a NetBIOS scope that nmblookup will use to communicate
  with when generating NetBIOS names. For details on the use of NetBIOS
  scopes, see rfc1001.txt and rfc1002.txt. NetBIOS scopes are /very/
  rarely used, only set this parameter if you are the system
  administrator in charge of all the NetBIOS systems you communicate
  with.
#+end_quote

-W|--workgroup=WORKGROUP

#+begin_quote
  Set the SMB domain of the username. This overrides the default domain
  which is the domain defined in smb.conf. If the domain specified is
  the same as the servers NetBIOS name, it causes the client to log on
  using the servers local SAM (as opposed to the Domain SAM).

  Note that specifying this parameter here will override the *workgroup*
  parameter in the smb.conf file.
#+end_quote

-r|--realm=REALM

#+begin_quote
  Set the realm for the domain.

  Note that specifying this parameter here will override the *realm*
  parameter in the smb.conf file.
#+end_quote

-U|--user=[DOMAIN\]USERNAME[%PASSWORD]

#+begin_quote
  Sets the SMB username or username and password.

  If %PASSWORD is not specified, the user will be prompted. The client
  will first check the *USER* environment variable (which is also
  permitted to also contain the password seperated by a %), then the
  *LOGNAME* variable (which is not permitted to contain a password) and
  if either exists, the value is used. If these environmental variables
  are not found, the username found in a Kerberos Credentials cache may
  be used.

  A third option is to use a credentials file which contains the
  plaintext of the username and password. This option is mainly provided
  for scripts where the admin does not wish to pass the credentials on
  the command line or via environment variables. If this method is used,
  make certain that the permissions on the file restrict access from
  unwanted users. See the /-A/ for more details.

  Be cautious about including passwords in scripts or passing
  user-supplied values onto the command line. For security it is better
  to let the Samba client tool ask for the password if needed, or obtain
  the password once with kinit.

  While Samba will attempt to scrub the password from the process title
  (as seen in ps), this is after startup and so is subject to a race.
#+end_quote

-N|--no-pass

#+begin_quote
  If specified, this parameter suppresses the normal password prompt
  from the client to the user. This is useful when accessing a service
  that does not require a password.

  Unless a password is specified on the command line or this parameter
  is specified, the client will request a password.

  If a password is specified on the command line and this option is also
  defined the password on the command line will be silently ignored and
  no password will be used.
#+end_quote

--password

#+begin_quote
  Specify the password on the commandline.

  Be cautious about including passwords in scripts or passing
  user-supplied values onto the command line. For security it is better
  to let the Samba client tool ask for the password if needed, or obtain
  the password once with kinit.

  If --password is not specified, the tool will check the *PASSWD*
  environment variable, followed by *PASSWD_FD* which is expected to
  contain an open file descriptor (FD) number.

  Finally it will check *PASSWD_FILE* (containing a file path to be
  opened). The file should only contain the password. Make certain that
  the permissions on the file restrict access from unwanted users!

  While Samba will attempt to scrub the password from the process title
  (as seen in ps), this is after startup and so is subject to a race.
#+end_quote

--pw-nt-hash

#+begin_quote
  The supplied password is the NT hash.
#+end_quote

-A|--authentication-file=filename

#+begin_quote
  This option allows you to specify a file from which to read the
  username and password used in the connection. The format of the file
  is:

  #+begin_quote
    #+begin_example
      				username = <value>
      				password = <value>
      				domain   = <value>
      			
    #+end_example
  #+end_quote

  Make certain that the permissions on the file restrict access from
  unwanted users!
#+end_quote

-P|--machine-pass

#+begin_quote
  Use stored machine account password.
#+end_quote

--simple-bind-dn=DN

#+begin_quote
  DN to use for a simple bind.
#+end_quote

--use-kerberos=desired|required|off

#+begin_quote
  This parameter determines whether Samba client tools will try to
  authenticate using Kerberos. For Kerberos authentication you need to
  use dns names instead of IP addresses when connnecting to a service.

  Note that specifying this parameter here will override the *client use
  kerberos* parameter in the smb.conf file.
#+end_quote

--use-krb5-ccache=CCACHE

#+begin_quote
  Specifies the credential cache location for Kerberos authentication.

  This will set --use-kerberos=required too.
#+end_quote

--use-winbind-ccache

#+begin_quote
  Try to use the credential cache by winbind.
#+end_quote

--client-protection=sign|encrypt|off

#+begin_quote
  Sets the connection protection the client tool should use.

  Note that specifying this parameter here will override the *client
  protection* parameter in the smb.conf file.

  In case you need more fine grained control you can use:
  --option=clientsmbencrypt=OPTION, --option=clientipcsigning=OPTION,
  --option=clientsigning=OPTION.
#+end_quote

* ACL FORMAT
The format of an ACL is one or more entries separated by either commas
or newlines. An ACL entry is one of the following:

#+begin_quote
  #+begin_example
     
    REVISION:<revision number>
    OWNER:<sid or name>
    GROUP:<sid or name>
    ACL:<sid or name>:<type>/<flags>/<mask>
  #+end_example
#+end_quote

Control bits related to automatic inheritance

#+begin_quote

  #+begin_quote
    ·

    /OD/ - "Owner Defaulted" - Indicates that the SID of the owner of
    the security descriptor was provided by a default mechanism.
  #+end_quote

  #+begin_quote
    ·

    /GD/ - "Group Defaulted" - Indicates that the SID of the security
    descriptor group was provided by a default mechanism.
  #+end_quote

  #+begin_quote
    ·

    /DP/ - "DACL Present" - Indicates a security descriptor that has a
    discretionary access control list (DACL).
  #+end_quote

  #+begin_quote
    ·

    /DD/ - "DACL Defaulted" - Indicates a security descriptor with a
    default DACL.
  #+end_quote

  #+begin_quote
    ·

    /SP/ - "SACL Present" - Indicates a security descriptor that has a
    system access control list (SACL).
  #+end_quote

  #+begin_quote
    ·

    /SD/ - "SACL Defaulted" - A default mechanism, rather than the
    original provider of the security descriptor, provided the SACL.
  #+end_quote

  #+begin_quote
    ·

    /DT/ - "DACL Trusted"
  #+end_quote

  #+begin_quote
    ·

    /SS/ - "Server Security"
  #+end_quote

  #+begin_quote
    ·

    /DR/ - "DACL Inheritance Required" - Indicates a required security
    descriptor in which the DACL is set up to support automatic
    propagation of inheritable access control entries (ACEs) to existing
    child objects.
  #+end_quote

  #+begin_quote
    ·

    /SR/ - "SACL Inheritance Required" - Indicates a required security
    descriptor in which the SACL is set up to support automatic
    propagation of inheritable ACEs to existing child objects.
  #+end_quote

  #+begin_quote
    ·

    /DI/ - "DACL Auto Inherited" - Indicates a security descriptor in
    which the DACL is set up to support automatic propagation of
    inheritable access control entries (ACEs) to existing child objects.
  #+end_quote

  #+begin_quote
    ·

    /SI/ - "SACL Auto Inherited" - Indicates a security descriptor in
    which the SACL is set up to support automatic propagation of
    inheritable ACEs to existing child objects.
  #+end_quote

  #+begin_quote
    ·

    /PD/ - "DACL Protected" - Prevents the DACL of the security
    descriptor from being modified by inheritable ACEs.
  #+end_quote

  #+begin_quote
    ·

    /PS/ - "SACL Protected" - Prevents the SACL of the security
    descriptor from being modified by inheritable ACEs.
  #+end_quote

  #+begin_quote
    ·

    /RM/ - "RM Control Valid" - Indicates that the resource manager
    control is valid.
  #+end_quote

  #+begin_quote
    ·

    /SR/ - "Self Relative" - Indicates a self-relative security
    descriptor.
  #+end_quote
#+end_quote

The revision of the ACL specifies the internal Windows NT ACL revision
for the security descriptor. If not specified it defaults to 1. Using
values other than 1 may cause strange behaviour.

The owner and group specify the owner and group sids for the object. If
a SID in the format S-1-x-y-z is specified this is used, otherwise the
name specified is resolved using the server on which the file or
directory resides.

ACEs are specified with an "ACL:" prefix, and define permissions granted
to an SID. The SID again can be specified in S-1-x-y-z format or as a
name in which case it is resolved against the server on which the file
or directory resides. The type, flags and mask values determine the type
of access granted to the SID.

The type can be either ALLOWED or DENIED to allow/deny access to the
SID.

The flags field defines how the ACE should be considered when performing
inheritance. smbcacls uses these flags when run with
/--propagate-inheritance/.

Flags can be specified as decimal or hexadecimal values, or with the
respective (XX) aliases, separated by a vertical bar "|".

#+begin_quote

  #+begin_quote
    ·

    /(OI)/ Object Inherit 0x1
  #+end_quote

  #+begin_quote
    ·

    /(CI)/ Container Inherit 0x2
  #+end_quote

  #+begin_quote
    ·

    /(NP)/ No Propagate Inherit 0x4
  #+end_quote

  #+begin_quote
    ·

    /(IO)/ Inherit Only 0x8
  #+end_quote

  #+begin_quote
    ·

    /(I)/ ACE was inherited 0x10
  #+end_quote
#+end_quote

The mask is a value which expresses the access right granted to the SID.
It can be given as a decimal or hexadecimal value, or by using one of
the following text strings which map to the NT file permissions of the
same name.

#+begin_quote

  #+begin_quote
    ·

    /R/ - Allow read access
  #+end_quote

  #+begin_quote
    ·

    /W/ - Allow write access
  #+end_quote

  #+begin_quote
    ·

    /X/ - Execute permission on the object
  #+end_quote

  #+begin_quote
    ·

    /D/ - Delete the object
  #+end_quote

  #+begin_quote
    ·

    /P/ - Change permissions
  #+end_quote

  #+begin_quote
    ·

    /O/ - Take ownership
  #+end_quote
#+end_quote

The following combined permissions can be specified:

#+begin_quote

  #+begin_quote
    ·

    /READ/ - Equivalent to RX permissions
  #+end_quote

  #+begin_quote
    ·

    /CHANGE/ - Equivalent to RXWD permissions
  #+end_quote

  #+begin_quote
    ·

    /FULL/ - Equivalent to RWXDPO permissions
  #+end_quote
#+end_quote

* INHERITANCE
Per-ACE inheritance flags can be set in the ACE flags field. By default,
inheritable ACEs e.g. those marked for object inheritance (OI) or
container inheritance (CI), are not propagated to sub-files or folders.
However, with the /--propagate-inheritance/ argument specified, such
ACEs are automatically propagated according to some inheritance rules.

#+begin_quote

  #+begin_quote
    ·

    Inheritable (OI)(OI) ACE flags can only be applied to folders.
  #+end_quote

  #+begin_quote
    ·

    Any inheritable ACEs applied to sub-files or folders are marked with
    the inherited (I) flag. Inheritable ACE(s) are applied to folders
    unless the no propagation (NP) flag is set.
  #+end_quote

  #+begin_quote
    ·

    When an ACE with the (OI) flag alone set is progagated to a child
    folder the inheritance only flag (IO) is also applied. This
    indicates the permissions associated with the ACE dont apply to the
    folder itself (only to its child files). When applying the ACE to a
    child file the ACE is inherited as normal.
  #+end_quote

  #+begin_quote
    ·

    When an ace with the (CI) flag alone set is propagated to a child
    file there is no effect, when propagated to a child folder it is
    inherited as normal.
  #+end_quote

  #+begin_quote
    ·

    When an ACE that has both (OI) & (CI) flags set the ACE is inherited
    as normal by both folders and files.
  #+end_quote
#+end_quote

(OI)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-parent/        (OI)(READ)
    | +-file.1       (I)(READ)
    | +-nested/      (OI)(IO)(I)(READ)
      |   +-file.2   (I)(READ)
  #+end_example
#+end_quote

(CI)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-parent/        (CI)(READ)
    | +-file.1
    | +-nested/      (CI)(I)(READ)
      |   +-file.2
  #+end_example
#+end_quote

(OI)(CI)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-parent/        (OI)(CI)(READ)
    | +-file.1       (I)(READ)
    | +-nested/      (OI)(CI)(I)(READ)
      |   +-file.2   (I)(READ)
  #+end_example
#+end_quote

(OI)(NP)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-oi_dir/        (OI)(NP)(READ)
    | +-file.1       (I)(READ)
    | +-nested/
    |   +-file.2
  #+end_example
#+end_quote

(CI)(NP)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-oi_dir/        (CI)(NP)(READ)
    | +-file.1
    | +-nested/      (I)(READ)
    |   +-file.2
  #+end_example
#+end_quote

(OI)(CI)(NP)(READ) added to parent folder

#+begin_quote
  #+begin_example
    +-parent/        (CI)(OI)(NP)(READ)
    | +-file.1       (I)(READ)
    | +-nested/      (I)(READ)
    |   +-file.2
  #+end_example
#+end_quote

Files and folders with protected ACLs do not allow inheritable
permissions (set with /-I/). Such objects will not receive ACEs flagged
for inheritance with (CI) or (OI).

* EXIT STATUS
The smbcacls program sets the exit status depending on the success or
otherwise of the operations performed. The exit status may be one of the
following values.

If the operation succeeded, smbcacls returns and exit status of 0. If
smbcacls couldnt connect to the specified server, or there was an error
getting or setting the ACLs, an exit status of 1 is returned. If there
was an error parsing any command line arguments, an exit status of 2 is
returned.

* VERSION
This man page is part of version 4.15.3 of the Samba suite.

* AUTHOR
The original Samba software and related utilities were created by Andrew
Tridgell. Samba is now developed by the Samba Team as an Open Source
project similar to the way the Linux kernel is developed.

smbcacls was written by Andrew Tridgell and Tim Potter.

The conversion to DocBook for Samba 2.2 was done by Gerald Carter. The
conversion to DocBook XML 4.2 for Samba 3.0 was done by Alexander
Bokovoy.
