#+TITLE: Man1 - cefsconv.1
#+DESCRIPTION: Linux manpage for cefsconv.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cefsconv - convert a TeX document in SJIS encoding containing CEF macros
into `preprocessed' form.

* SYNOPSIS
*cefsconv* < /infile/ > /outfile/

* DESCRIPTION
The *Chinese Encoding Framework* (*CEF*) developed by Christian Wittern
<cwittern@conline.central.de> makes it possible to include CJK
characters in various encodings using the SGML macros *&CX-aabb;* for
Big 5 and CNS encoding and *&U-aabb;* for Unicode (*X* denotes a number
between 0 and 7; 0 represents Big 5 encoding, 1-7 CNS encoding planes
1-7. *aabb* is either a decimal or hexadecimal number).

*cefsconv* converts the CEF macros of a SJIS encoded document together
with the SJIS encoding itself into a form which can directly processed
with LaTeX 2e. It is identical to

#+begin_quote
  *sjisconv* < /infile/ | *cefconv* > /outfile/
#+end_quote

This filter is part of the *CJK* macro package for LaTeX 2e.

* SEE ALSO
*sjisconv*(1), *bg5conv*(1), *cefconv*(1), *cef5conv*(1),
*extconv*(1),\\
the *CJK* documentation files.

* AUTHOR
Werner Lemberg <wl@gnu.org>
