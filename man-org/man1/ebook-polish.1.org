#+TITLE: Man1 - ebook-polish.1
#+DESCRIPTION: Linux manpage for ebook-polish.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
ebook-polish - ebook-polish

#+begin_quote

  #+begin_quote
    #+begin_example
      ebook-polish [options] input_file [output_file]
    #+end_example
  #+end_quote
#+end_quote

Polishing books is all about putting the shine of perfection onto your
carefully crafted e-books.

Polishing tries to minimize the changes to the internal code of your
e-book. Unlike conversion, it does not flatten CSS, rename files, change
font sizes, adjust margins, etc. Every action performs only the minimum
set of changes needed for the desired effect.

You should use this tool as the last step in your e-book creation
process.

Note that polishing only works on files in the AZW3 or EPUB formats.

Whenever you pass arguments to *ebook-polish* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--add-soft-hyphens, -H* :: Add soft hyphens to all words in the
    book. This allows the book to be rendered better when the text is
    justified, in readers that do not support hyphenation.
#+end_quote

#+begin_quote
  - *--compress-images, -i* :: Losslessly compress images in the book,
    to reduce the filesize, without affecting image quality.
#+end_quote

#+begin_quote
  - *--cover, -c* :: Path to a cover image. Changes the cover specified
    in the e-book. If no cover is present, or the cover is not properly
    identified, inserts a new cover.
#+end_quote

#+begin_quote
  - *--embed-fonts, -e* :: Embed all fonts that are referenced in the
    document and are not already embedded. This will scan your computer
    for the fonts, and if they are found, they will be embedded into the
    document. Please ensure that you have the proper license for
    embedding the fonts used in this document.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--jacket, -j* :: Insert a *"*book jacket*"* page at the start of
    the book that contains all the book metadata such as title, tags,
    authors, series, comments, etc. Any previous book jacket will be
    replaced.
#+end_quote

#+begin_quote
  - *--opf, -o* :: Path to an OPF file. The metadata in the book is
    updated from the OPF file.
#+end_quote

#+begin_quote
  - *--remove-jacket* :: Remove a previous inserted book jacket page.
#+end_quote

#+begin_quote
  - *--remove-soft-hyphens* :: Remove soft hyphens from all text in the
    book.
#+end_quote

#+begin_quote
  - *--remove-unused-css, -u* :: Remove all unused CSS rules from
    stylesheets and <style> tags. Some books created from production
    templates can have a large number of extra CSS rules that don*'*t
    match any actual content. These extra rules can slow down readers
    that need to parse them all.
#+end_quote

#+begin_quote
  - *--smarten-punctuation, -p* :: Convert plain text dashes, ellipsis,
    quotes, multiple hyphens, etc. into their typographically correct
    equivalents. Note that the algorithm can sometimes generate
    incorrect results, especially when single quotes at the start of
    contractions are involved.
#+end_quote

#+begin_quote
  - *--subset-fonts, -f* :: Subsetting fonts means reducing an embedded
    font to contain only the characters used from that font in the book.
    This greatly reduces the size of the font files (halving the font
    file sizes is common). For example, if the book uses a specific font
    for headers, then subsetting will reduce that font to contain only
    the characters present in the actual headers in the book. Or if the
    book embeds the bold and italic versions of a font, but bold and
    italic text is relatively rare, or absent altogether, then the bold
    and italic fonts can either be reduced to only a few characters or
    completely removed. The only downside to subsetting fonts is that
    if, at a later date you decide to add more text to your books, the
    newly added text might not be covered by the subset font.
#+end_quote

#+begin_quote
  - *--upgrade-book, -U* :: Upgrade the internal structures of the book,
    if possible. For instance, upgrades EPUB 2 books to EPUB 3 books.
#+end_quote

#+begin_quote
  - *--verbose* :: Produce more verbose output, useful for debugging.
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
