#+TITLE: Man1 - cargo-vendor.1
#+DESCRIPTION: Linux manpage for cargo-vendor.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-vendor - Vendor all dependencies locally

* SYNOPSIS
*cargo vendor* [/options/] [/path/]

* DESCRIPTION
This cargo subcommand will vendor all crates.io and git dependencies for
a project into the specified directory at *<path>*. After this command
completes the vendor directory specified by *<path>* will contain all
remote sources from dependencies specified. Additional manifests beyond
the default one can be specified with the *-s* option.

The *cargo vendor* command will also print out the configuration
necessary to use the vendored sources, which you will need to add to
*.cargo/config.toml*.

* OPTIONS
** Vendor Options
*-s* /manifest/, *--sync* /manifest/

#+begin_quote
  Specify extra *Cargo.toml* manifests to workspaces which should also
  be vendored and synced to the output.
#+end_quote

*--no-delete*

#+begin_quote
  Don't delete the "vendor" directory when vendoring, but rather keep
  all existing contents of the vendor directory
#+end_quote

*--respect-source-config*

#+begin_quote
  Instead of ignoring *[source]* configuration by default in
  *.cargo/config.toml* read it and use it when downloading crates from
  crates.io, for example
#+end_quote

*--versioned-dirs*

#+begin_quote
  Normally versions are only added to disambiguate multiple versions of
  the same package. This option causes all directories in the "vendor"
  directory to be versioned, which makes it easier to track the history
  of vendored packages over time, and can help with the performance of
  re-vendoring when only a subset of the packages have changed.
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Vendor all dependencies into a local "vendor" folder

  #+begin_quote
    #+begin_example
      cargo vendor
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Vendor all dependencies into a local "third-party/vendor" folder

  #+begin_quote
    #+begin_example
      cargo vendor third-party/vendor
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  3.Vendor the current workspace as well as another to "vendor"

  #+begin_quote
    #+begin_example
      cargo vendor -s ../path/to/Cargo.toml
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1)
