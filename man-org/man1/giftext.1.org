#+TITLE: Man1 - giftext.1
#+DESCRIPTION: Linux manpage for giftext.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
giftext - dump GIF pixels and metadata as text

* SYNOPSIS
*giftext* [-v] [-c] [-e] [-z] [-p] [-r] [-h] [/gif-file/]

* DESCRIPTION
A program to dump (text only) general information about GIF file.

If no GIF file is given, giftext will try to read a GIF file from stdin.

* OPTIONS
-v

#+begin_quote
  Verbose mode (show progress). Enables printout of running scan lines.
#+end_quote

-c

#+begin_quote
  Dumps the color maps.
#+end_quote

-e

#+begin_quote
  Dumps encoded bytes - the pixels after compressed using LZ algorithm
  and chained to form bytes. This is the form the data is saved in the
  GIF file. Dumps in hex - 2 digit per byte.
#+end_quote

-z

#+begin_quote
  Dumps the LZ codes of the image. Dumps in hex - 3 digits per code (as
  we are limited to 12 bits).
#+end_quote

-p

#+begin_quote
  Dumps the pixels of the image. Dumps in hex - 2 digit per pixel
  (<=byte).
#+end_quote

-r

#+begin_quote
  Dumps raw pixels as one byte per pixel. This option inhibits all other
  options and only the pixels are dumped. This option may be used to
  convert GIF files into raw data. Note: the color map can be extracted
  by gifclrmp utility. If more than one image is included in the file,
  all images will be dumped in order.
#+end_quote

-h

#+begin_quote
  Print one line of command line help, similar to Usage above.
#+end_quote

* AUTHOR
Gershon Elber.
