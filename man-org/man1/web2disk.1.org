#+TITLE: Man1 - web2disk.1
#+DESCRIPTION: Linux manpage for web2disk.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
web2disk - web2disk

#+begin_quote

  #+begin_quote
    #+begin_example
      web2disk URL
    #+end_example
  #+end_quote
#+end_quote

Where URL is for example /https://google.com/

Whenever you pass arguments to *web2disk* that have spaces in them,
enclose the arguments in quotation marks. For example: "/some path/with
spaces"

* [OPTIONS]

#+begin_quote
  - *--base-dir, -d* :: Base folder into which URL is saved. Default
    is .
#+end_quote

#+begin_quote
  - *--delay* :: Minimum interval in seconds between consecutive
    fetches. Default is 0 s
#+end_quote

#+begin_quote
  - *--dont-download-stylesheets* :: Do not download CSS stylesheets.
#+end_quote

#+begin_quote
  - *--encoding* :: The character encoding for the websites you are
    trying to download. The default is to try and guess the encoding.
#+end_quote

#+begin_quote
  - *--filter-regexp* :: Any link that matches this regular expression
    will be ignored. This option can be specified multiple times, in
    which case as long as any regexp matches a link, it will be ignored.
    By default, no links are ignored. If both filter regexp and match
    regexp are specified, then filter regexp is applied first.
#+end_quote

#+begin_quote
  - *--help, -h* :: show this help message and exit
#+end_quote

#+begin_quote
  - *--match-regexp* :: Only links that match this regular expression
    will be followed. This option can be specified multiple times, in
    which case as long as a link matches any one regexp, it will be
    followed. By default all links are followed.
#+end_quote

#+begin_quote
  - *--max-files, -n* :: The maximum number of files to download. This
    only applies to files from <a href> tags. Default is
    9223372036854775807
#+end_quote

#+begin_quote
  - *--max-recursions, -r* :: Maximum number of levels to recurse i.e.
    depth of links to follow. Default 1
#+end_quote

#+begin_quote
  - *--timeout, -t* :: Timeout in seconds to wait for a response from
    the server. Default: 10.0 s
#+end_quote

#+begin_quote
  - *--verbose* :: Show detailed output information. Useful for
    debugging
#+end_quote

#+begin_quote
  - *--version* :: show program*'*s version number and exit
#+end_quote

* AUTHOR
Kovid Goyal

* COPYRIGHT
Kovid Goyal
