#+TITLE: Man1 - tracker3-import.1
#+DESCRIPTION: Linux manpage for tracker3-import.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tracker3-import - Import data into a Tracker database.

* SYNOPSIS
*tracker3 import* FILE.ttl

* DESCRIPTION
*tracker3 import* imports data into a Tracker database.

The data must conform to the existing ontology of the database.

The data must be in Turtle format. You can use a tool such as rapper(1)
to convert the data from other formats to Turtle.

* SEE ALSO
*tracker3-export*(1), *tracker3-sparql*(1).
