#+TITLE: Man1 - zstdless.1
#+DESCRIPTION: Linux manpage for zstdless.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*zstdless* - view zstandard-compressed files

* SYNOPSIS
*zstdless* [/flags/] [/file/ ...]

* DESCRIPTION
*zstdless* runs *less (1)* on files or stdin, if no files argument is
given, after decompressing them with *zstdcat (1)*.

* SEE ALSO
*zstd (1)*
