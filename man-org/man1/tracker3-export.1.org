#+TITLE: Man1 - tracker3-export.1
#+DESCRIPTION: Linux manpage for tracker3-export.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tracker3-export - Export all data from a Tracker database.

* SYNOPSIS
*tracker3 export* [/options/...] [*IRI*...]

* DESCRIPTION
*tracker3 export* exports data stored in a Tracker database, in Turtle
format. By default all data is exported, if any *IRI*, only those
resources will be printed.

The output is intended to be machine-readable, not human readable. Use a
tool such as rapper(1) to convert the data to different formats.

* OPTIONS
*-g, --show-graphs*

#+begin_quote
  Tracker can separate data into multiple graphs. This feature is used
  by the filesystem miner to separate different types of content. This
  flag causes the relevant GRAPH statements to be output along with the
  data.

  #+begin_quote
    #+begin_example
      In this mode the output is TriG syntax rather than Turtle, due to
      the extra GRAPH statements. Some tools which understand Turtle do not
      understand TriG.
    #+end_example
  #+end_quote
#+end_quote

*--2to3*

#+begin_quote
  Helper for migrating data from Tracker 2.x databases. This option
  takes an argument specifying the scope. Only the “files-starred”
  argument is available so far.
#+end_quote

*--keyfile*

#+begin_quote
  Outputs the data in a key file format. Only may be used with --2to3
#+end_quote

* EXAMPLES
Export all data from Tracker Index and prettify the output using
rapper(1).::

#+begin_quote
  #+begin_example
    $ tracker3 export -b org.freedesktop.Tracker3.Miner.Files | rapper - -I . -i turtle -o turtle
  #+end_example
#+end_quote

* SEE ALSO
*tracker3-import*(1), *tracker3-sparql*(1).
