#+TITLE: Man1 - manweb.1
#+DESCRIPTION: Linux manpage for manweb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
manweb - browse netpbm (and other) documentation

* SYNOPSIS
*manweb* *-help*

*manweb* [*-config=*/configfile]/ [/topic [ subtopic ... ] ]/

* EXAMPLES
#+begin_example
  manweb
#+end_example

This gets a master index of documentation.

#+begin_example
  manweb netpbm
#+end_example

This gets the main documentation page for the Netpbm package, with
hyperlinks to the rest of the documentation.

#+begin_example
  manweb netpbm pngtopam
#+end_example

This goes directly to the documentation page for the Pngtopam program in
the Netpbm package.

#+begin_example
  manweb pngtopam
#+end_example

This also goes directly to the documentation page for the Pngtopam
program in the Netpbm package, if that's what would run in response to a
*pngtopam* shell command (your *PATH*/ environment variable is
involved)./

#+begin_example
  manweb 3 fopen
#+end_example

This gets the traditional man page for the fopen() subroutine using
*man*/./

#+begin_example
  manweb cp
#+end_example

This gets the GNU Info manual for the *cp*/ program, using /*info*/./

* DESCRIPTION
*manweb*/ displays reference documentation via quick shell/ commands. It
is a replacement for the well-known *man*/./

* Differences Between Man and Manweb
*manweb*/'s advantages over /*man*/ are:/

-  You can access documentation that is on the worldwide web instead of
  having locally installed copies. This saves installation work and gets
  you more current documentation.

-  Documentation can be in HTML, which is more widely known, more widely
  useful, and more expressive than the nroff/troff format used by
  *man*/./

-  *manweb*/ puts your topics in a tree for multilevel documentation./
  *man*/ is intended for a single level of documentation. For/ example,
  you can have a man page for each shell command, but not for the
  subcommands of a shell command. And you cannot properly have man pages
  for the members of multiple subroutine libraries.

-  Documentation can be hyperlinked.

Web servers need not be involved -- the documentation can be in local
files. Graphics need not be involved -- the *lynx*/ browser works fine/
in the same kind of terminals in which *man*/ works./

*manweb*/ finds the documentation you specify and calls a web/ browser
of your choice to display it. The documentation *manweb* finds can be
either an HTML file on your system, in which case, *manweb*/ gives a
/*file:*/ URL to your browser, or an explicit/ URL. That explicit URL
might be an *http:*/ URL referring to an/ HTML file on a web server
somewhere, or anything else your browser understands.

If *manweb*/ finds neither an HTML file nor a URL, but your parameters/
look like they could mean something to *man*/, /*manweb*/ calls/ *man*/.
Therefore, you can use a single command to access the vast/ body of
traditional man pages, plus any newer *manweb*/ documentation./ You can
make "man" a shell alias of "manweb".

*manweb*/ finds Info documentation as well. It looks for the/ topic you
specify as an Info topic after looking for HTML and URL documentation
and before running *man*/. If /*manweb*/ finds a/ corresponding Info
topic, it runs the program *info*/ on it. Info/ is the documentation
system that the GNU project invented to, among other things, replace
traditional Unix man pages. However, HTML and the Worldwide Web were
invented shortly afterward, so Info fizzled. But there is still a lot of
GNU software that is documented as Info topics.

** How Manweb Finds Documentation
*manweb*/ passes a URL to a web browser. This section tells/ how your
*manweb*/ invocation parameters turn into that URL./

*manweb*/'s search starts in the "web directory" directory./ That's
either the value of the *webdir*/ keyword in your/ *manweb*/
configuration file, or the default /*/usr/man/web*/./

Your invocation parameters form a "topic chain." Going from left to
right, the first parameter is the main topic, the 2nd is a subtopic of
the main topic, and so on.

Let's look at the simple case where you specify exactly one parameter --
a main topic. We'll call it /maintopic and look at 4 ways/ *manweb*/
might find it:/

- If *manweb*/ finds a file named maintopic/*.html* in the web
  directory, the URL *manweb*/ passes to the/ browser is just a *file:*/
  URL that specifies that .html/ file.

- If there's no .html file, but there is a file named
  /maintopic/*.url*/, the contents of the first line of/ that .url file
  is what *manweb*/ passes to the browser. It/ doesn't interpret the
  contents at all. If it's garbage, the browser chokes on it.

- If there's neither a .html nor a .url file, but there is a directory
  named /maintopic, /*manweb*/ looks in the/ directory for a file named
  /index.html. If there is one,/ *manweb*/ passes a /*file:*/ URL
  specifying that/ index.html file to the browser. If there's no
  /index.html, /*manweb*/ uses a /*file:*/ URL that/ specifies the
  directory itself.

- If *manweb*/ doesn't find documentation in any of the/ above ways, it
  searches your executable search path (as defined by your *PATH*/
  environment variable) for a program named/ /maintopic. If it finds
  one, it looks in the directory/ that contains the program for a file
  named *doc.url*/. If/ it finds one, it appends /maintopic/*.html*/ to
  the/ first line of the file and passes that to the browser. Unless the
  first line does /not end with a slash -- in that / case, *manweb*/
  passes the first line of the file unmodified/ to the browser.

It gets a little more interesting when you have subtopics. Looking at
each of the 4 cases above:

-  Where /maintopic/*.html*/ exists, subtopics are invalid./ You get a
  warning message and the subtopics are ignored.

-  Where there's no .html file but /maintopic/*.url*/ exists,/ *manweb*/
  appends the subtopic chain to the URL it gets from the/ .url file as
  in the following example: .url file contains
  *http://acme.com/productxyz/*/ and subtopics are/ *create*/ and/
  *database*/. The URL /*manweb*/ passes to the browser is/
  *http://acme.com/productxyz/create/database.html*/./

*manweb*/ doesn't check that this kind of appendage makes/ any sense for
the URL in question, except that if the URL in the .url file doesn't end
with a slash (*/*/), /*manweb* issues a warning and doesn't append
anything (ignores the subtopics).

-  Where there's neither a .html file nor a .url file, but there's a
  /maintopic directory, /*manweb*/ recurses into that/ directory and
  begins a whole new search using the first subtopic as the main topic
  and the rest of the subtopics as subtopics of that.

-  When there are subtopics, the *PATH*/ thing doesn't make sense,/ so
  *manweb*/ doesn't do it./

If you give subtopics, the *PATH*/ thing described above for one/ topic
doesn't apply.

If you give no parameters at all, *manweb*/ generates a URL for the/ web
directory itself as described above for subdirectories.

The above is simplified by the assumption of a single web directory. In
reality, the *webdir*/ keyword in the configuration/ file can specify a
chain of web directories. *manweb*/ searches/ each one in turn, doing
all the kinds of searches in each web directory before moving on to the
next one.

** The Configuration File
The default location of the *manweb*/ configuration file is/
*/etc/manweb.conf*/. But you can override this with the environment/
variable *MANWEB_CONF_FILE*/, and override that with the/ *-config*/
invocation option./

Lines starting with "#" are comments and are ignored, as are blank
lines.

All other lines have the format /keyword=value. The/ keywords defined
are:

- webdir ::  A colon-delimited sequence of directories to search for
  documentation as described above. If you don't specify this, the
  default is */usr/man/web*/ alone./

- browser ::  The file specification *manweb*/ of the web browser
  /*manweb* is to invoke to display documentation (except when it uses
  *man*/ to display/ a conventional man page). If the file specification
  does not include a slash, *manweb* searches for the file in the PATH
  search path.

If you don't specify this, the default is the value of the *BROWSER*/
environment variable, and if that is not set,/ *lynx*/./

Example:

#+begin_example
  # Configuration file for Manweb

  webdir=/usr/share/manweb
  browser=netscape
#+end_example
