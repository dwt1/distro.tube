#+TITLE: Man1 - usbredirserver.1
#+DESCRIPTION: Linux manpage for usbredirserver.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
usbredirserver - exporting an USB device for use from another (virtual)
machine

* SYNOPSIS
*usbredirserver* [/-p|--port <port>/] [/-v|--verbose <0-5>/] [/-4
<ipv4_addr|I-6 <ipv6_addr>]/ /<busnum-devnum|vendorid:prodid>/

* DESCRIPTION
usbredirserver is a small standalone server for exporting an USB device
for use from another (virtual) machine through the usbredir protocol.

You can specify the USB device to export either by USB id in the form of
/<vendorid>:<prodid>/, or by USB bus number and device address in the
form of /<usbbus>-<usbaddr>/.

Notice that an instance of usbredirserver can only be used to export a
single USB device. If you want to export multiple devices you can start
multiple instances listening on different TCP ports.

* OPTIONS
- *-p*, *--port*=/PORT/ :: Set the TCP port to listen on to /PORT/

- *-v*, *--verbose*=/VERBOSE/ :: Set usbredirserver's verbosity level to
  /VERBOSE/, this mostly affects USB redirection related messages. Valid
  values are 0-5:\\
  0:Silent 1:Errors 2:Warnings 3:Info 4:Debug 5:Debug++

* AUTHOR
Written by Hans de Goede <hdegoede@redhat.com>

* REPORTING BUGS
You can report bugs to the spice-devel mailinglist:
http://lists.freedesktop.org/mailman/listinfo/spice-devel or filing an
issue at: https://gitlab.freedesktop.org/spice/usbredir/issues/new

* COPYRIGHT
Copyright 2010-2012 Red Hat, Inc. License GPLv2+: GNU GPL version 2 or
later <http://gnu.org/licenses/gpl.html>.\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.
