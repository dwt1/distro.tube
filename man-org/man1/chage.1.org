#+TITLE: Man1 - chage.1
#+DESCRIPTION: Linux manpage for chage.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chage - change user password expiry information

* SYNOPSIS
*chage* [/options/] /LOGIN/

* DESCRIPTION
The *chage* command changes the number of days between password changes
and the date of the last password change. This information is used by
the system to determine when a user must change their password.

* OPTIONS
The options which apply to the *chage* command are:

*-d*, *--lastday* /LAST_DAY/

#+begin_quote
  Set the number of days since January 1st, 1970 when the password was
  last changed. The date may also be expressed in the format YYYY-MM-DD
  (or the format more commonly used in your area).
#+end_quote

*-E*, *--expiredate* /EXPIRE_DATE/

#+begin_quote
  Set the date or number of days since January 1, 1970 on which the
  users account will no longer be accessible. The date may also be
  expressed in the format YYYY-MM-DD (or the format more commonly used
  in your area). A user whose account is locked must contact the system
  administrator before being able to use the system again.

  Passing the number /-1/ as the /EXPIRE_DATE/ will remove an account
  expiration date.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help message and exit.
#+end_quote

*-i*, *--iso8601*

#+begin_quote
  When printing dates, use YYYY-MM-DD format.
#+end_quote

*-I*, *--inactive* /INACTIVE/

#+begin_quote
  Set the number of days of inactivity after a password has expired
  before the account is locked. The /INACTIVE/ option is the number of
  days of inactivity. A user whose account is locked must contact the
  system administrator before being able to use the system again.

  Passing the number /-1/ as the /INACTIVE/ will remove an accounts
  inactivity.
#+end_quote

*-l*, *--list*

#+begin_quote
  Show account aging information.
#+end_quote

*-m*, *--mindays* /MIN_DAYS/

#+begin_quote
  Set the minimum number of days between password changes to /MIN_DAYS/.
  A value of zero for this field indicates that the user may change
  their password at any time.
#+end_quote

*-M*, *--maxdays* /MAX_DAYS/

#+begin_quote
  Set the maximum number of days during which a password is valid. When
  /MAX_DAYS/ plus /LAST_DAY/ is less than the current day, the user will
  be required to change their password before being able to use their
  account. This occurrence can be planned for in advance by use of the
  *-W* option, which provides the user with advance warning.

  Passing the number /-1/ as /MAX_DAYS/ will remove checking a passwords
  validity.
#+end_quote

*-R*, *--root* /CHROOT_DIR/

#+begin_quote
  Apply changes in the /CHROOT_DIR/ directory and use the configuration
  files from the /CHROOT_DIR/ directory.
#+end_quote

*-W*, *--warndays* /WARN_DAYS/

#+begin_quote
  Set the number of days of warning before a password change is
  required. The /WARN_DAYS/ option is the number of days prior to the
  password expiring that a user will be warned their password is about
  to expire.
#+end_quote

If none of the options are selected, *chage* operates in an interactive
fashion, prompting the user with the current values for all of the
fields. Enter the new value to change the field, or leave the line blank
to use the current value. The current value is displayed between a pair
of /[ ]/ marks.

* NOTE
The *chage* program requires a shadow password file to be available.

The *chage* command is restricted to the root user, except for the *-l*
option, which may be used by an unprivileged user to determine when
their password or account is due to expire.

* CONFIGURATION
The following configuration variables in /etc/login.defs change the
behavior of this tool:

* FILES
/etc/passwd

#+begin_quote
  User account information.
#+end_quote

/etc/shadow

#+begin_quote
  Secure user account information.
#+end_quote

* EXIT VALUES
The *chage* command exits with the following values:

/0/

#+begin_quote
  success
#+end_quote

/1/

#+begin_quote
  permission denied
#+end_quote

/2/

#+begin_quote
  invalid command syntax
#+end_quote

/15/

#+begin_quote
  cant find the shadow password file
#+end_quote

* SEE ALSO
*passwd*(5), *shadow*(5).
