#+TITLE: Man1 - cpaldjvu.1
#+DESCRIPTION: Linux manpage for cpaldjvu.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cpaldjvu - DjVuDocument encoder for low-color images.

* SYNOPSIS
*cpaldjvu [*/options/*] */inputppmfile/* */outputdjvufile/

* DESCRIPTION
Program *cpaldjvu* is a DjVuDocument encoder for images containing few
colors. It performs best on images containing large solid color areas
such as screen dumps. Compression ratios on such images can be much
higher than those achieved by

or

compression.

This program works by first reducing the number of distinct colors to a
small specified value using a simple color quantization algorithm. The
dominant color is encoded into the background layer. The other colors
are encoded into the foreground layer.

* OPTIONS
- *-dpi */n/ :: Specify the resolution information encoded into the
  output file expressed in dots per inch. The resolution information
  encoded in DjVu files determine how the decoder scales the image on a
  particular display. Meaningful resolutions range from 25 to 6000. The
  default value is 300 dpi.

- *-colors */n/ :: Specify a maximum number of distinct colors for the
  color quantization algorithm. process. The default value is 256.
  Smaller values can produce much smaller files.

- *-bgwhite* :: Cause the background layer to use the lightest
  quantified color instead of the dominant color.

- *-verbose* :: Display informational messages while running.

* REMARKS
The color quantization might introduce severe degradation if the image
contains photographic areas with a large number of very similar colors.
Color quantization problems might be solved by pre-processing the input
file with a different quantization program such as *ppmquant*. Avoid
using the error diffusion dithering algorithm. This algorithm generates
random dithering patterns that might be very costly to encode.

* BUGS
This program should be rewritten as a pre-processor for *csepdjvu*.

* CREDITS
This program was initially written by Léon Bottou
<leonb@users.sourceforge.net> and was improved by Bill Riemers
<docbill@sourceforge.net> and many others.

* SEE ALSO
*djvu*(1), *pbm*(5), *ppmquant*(1), *pnmtogif*(1), *pnmtopng*(1)
