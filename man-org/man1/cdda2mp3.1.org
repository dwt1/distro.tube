#+TITLE: Manpages - cdda2mp3.1
#+DESCRIPTION: Linux manpage for cdda2mp3.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about cdda2mp3.1 is found in manpage for: [[../man1/cdda2ogg.1][man1/cdda2ogg.1]]