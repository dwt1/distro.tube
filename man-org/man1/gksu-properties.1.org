#+TITLE: Man1 - gksu-properties.1
#+DESCRIPTION: Linux manpage for gksu-properties.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gksu-properties - Configure the behaviour of gksu

* SYNOPSIS
*gksu-properties*

* DESCRIPTION
This manual page documents briefly the *gksu-properties* command.

*gksu-properties* allows you to define how *gksu*(1) grants the
privileges and locks your input devices (mouse, keyboard...).

* SEE ALSO
*gksu*(1), *su* (1),* sudo*(1)

* AUTHORS
Gustavo Noronha Silva <kov@debian.org>\\
Allan Douglas <allan_douglas@gmx.net>

This manual page was written by Francois Wendling <frwendling@free.fr>
for the Debian GNU/Linux system (but may be used by others).
