#+TITLE: Man1 - roff2text.1
#+DESCRIPTION: Linux manpage for roff2text.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
roff2text - transform roff code into text mode

* SYNOPSIS
*roff2text* [/groffer-option/ . . .] [ *--* ] [/filespec/ . . .]
*roff2text* *-h* *roff2text* *--help* *roff2text* *-v* *roff2text*
*--version*

The options *-v* and *--version* print the version information of the
program to standard output and exit. The options *-h* and *--help* print
a usage information of the program to standard output and stop the
program instantly.

All other options are assumed to be *groffer* options. They are
internally passed to *groffer*. They override the behavior of the
program. The options are optional, they can be omitted.

The /filespec/ arguments correspond to the /filespec/ arguments of
*groffer*. So they are either the names of existing, readable files or
*-* for standard input, or the name of a man page or a *groffer*(1) man
page search pattern. If no /filespec/ is specified standard input is
assumed automatically.

* DESCRIPTION
*roff2text* transforms /roff/ code into

Print the result to standard output.

There are more of these programs for generating other formats of /roff/
input.

* AUTHORS
*roff2text* was written by [[mailto:groff-bernd.warken-72@web.de][Bernd
Warken]].

* SEE ALSO
*groff*(1), *groffer*(1), *gxditview*(1).
