#+TITLE: Man1 - cargo-fix.1
#+DESCRIPTION: Linux manpage for cargo-fix.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
cargo-fix - Automatically fix lint warnings reported by rustc

* SYNOPSIS
*cargo fix* [/options/]

* DESCRIPTION
This Cargo subcommand will automatically take rustc's suggestions from
diagnostics like warnings and apply them to your source code. This is
intended to help automate tasks that rustc itself already knows how to
tell you to fix!

Executing *cargo fix* will under the hood execute *cargo-check*(1). Any
warnings applicable to your crate will be automatically fixed (if
possible) and all remaining warnings will be displayed when the check
process is finished. For example if you'd like to apply all fixes to the
current package, you can run:

#+begin_quote
  #+begin_example
    cargo fix
  #+end_example
#+end_quote

which behaves the same as *cargo check --all-targets*.

*cargo fix* is only capable of fixing code that is normally compiled
with *cargo check*. If code is conditionally enabled with optional
features, you will need to enable those features for that code to be
analyzed:

#+begin_quote
  #+begin_example
    cargo fix --features foo
  #+end_example
#+end_quote

Similarly, other *cfg* expressions like platform-specific code will need
to pass *--target* to fix code for the given target.

#+begin_quote
  #+begin_example
    cargo fix --target x86_64-pc-windows-gnu
  #+end_example
#+end_quote

If you encounter any problems with *cargo fix* or otherwise have any
questions or feature requests please don't hesitate to file an issue at
<https://github.com/rust-lang/cargo>.

** Edition migration
The *cargo fix* subcommand can also be used to migrate a package from
one /edition/
<https://doc.rust-lang.org/edition-guide/editions/transitioning-an-existing-project-to-a-new-edition.html>
to the next. The general procedure is:

#+begin_quote
  1.Run *cargo fix --edition*. Consider also using the *--all-features*
  flag if your project has multiple features. You may also want to run
  *cargo fix --edition* multiple times with different *--target* flags
  if your project has platform-specific code gated by *cfg* attributes.
#+end_quote

#+begin_quote
  2.Modify *Cargo.toml* to set the /edition field/
  <https://doc.rust-lang.org/cargo/reference/manifest.html#the-edition-field>
  to the new edition.
#+end_quote

#+begin_quote
  3.Run your project tests to verify that everything still works. If new
  warnings are issued, you may want to consider running *cargo fix*
  again (without the *--edition* flag) to apply any suggestions given by
  the compiler.
#+end_quote

And hopefully that's it! Just keep in mind of the caveats mentioned
above that *cargo fix* cannot update code for inactive features or *cfg*
expressions. Also, in some rare cases the compiler is unable to
automatically migrate all code to the new edition, and this may require
manual changes after building with the new edition.

* OPTIONS
** Fix options
*--broken-code*

#+begin_quote
  Fix code even if it already has compiler errors. This is useful if
  *cargo fix* fails to apply the changes. It will apply the changes and
  leave the broken code in the working directory for you to inspect and
  manually fix.
#+end_quote

*--edition*

#+begin_quote
  Apply changes that will update the code to the next edition. This will
  not update the edition in the *Cargo.toml* manifest, which must be
  updated manually after *cargo fix --edition* has finished.
#+end_quote

*--edition-idioms*

#+begin_quote
  Apply suggestions that will update code to the preferred style for the
  current edition.
#+end_quote

*--allow-no-vcs*

#+begin_quote
  Fix code even if a VCS was not detected.
#+end_quote

*--allow-dirty*

#+begin_quote
  Fix code even if the working directory has changes.
#+end_quote

*--allow-staged*

#+begin_quote
  Fix code even if the working directory has staged changes.
#+end_quote

** Package Selection
By default, when no package selection options are given, the packages
selected depend on the selected manifest file (based on the current
working directory if *--manifest-path* is not given). If the manifest is
the root of a workspace then the workspaces default members are
selected, otherwise only the package defined by the manifest will be
selected.

The default members of a workspace can be set explicitly with the
*workspace.default-members* key in the root manifest. If this is not
set, a virtual workspace will include all workspace members (equivalent
to passing *--workspace*), and a non-virtual workspace will include only
the root crate itself.

*-p* /spec/..., *--package* /spec/...

#+begin_quote
  Fix only the specified packages. See *cargo-pkgid*(1) for the SPEC
  format. This flag may be specified multiple times and supports common
  Unix glob patterns like ***, *?* and *[]*. However, to avoid your
  shell accidentally expanding glob patterns before Cargo handles them,
  you must use single quotes or double quotes around each pattern.
#+end_quote

*--workspace*

#+begin_quote
  Fix all members in the workspace.
#+end_quote

*--all*

#+begin_quote
  Deprecated alias for *--workspace*.
#+end_quote

*--exclude* /SPEC/...

#+begin_quote
  Exclude the specified packages. Must be used in conjunction with the
  *--workspace* flag. This flag may be specified multiple times and
  supports common Unix glob patterns like ***, *?* and *[]*. However, to
  avoid your shell accidentally expanding glob patterns before Cargo
  handles them, you must use single quotes or double quotes around each
  pattern.
#+end_quote

** Target Selection
When no target selection options are given, *cargo fix* will fix all
targets (*--all-targets* implied). Binaries are skipped if they have
*required-features* that are missing.

Passing target selection flags will fix only the specified targets.

Note that *--bin*, *--example*, *--test* and *--bench* flags also
support common Unix glob patterns like ***, *?* and *[]*. However, to
avoid your shell accidentally expanding glob patterns before Cargo
handles them, you must use single quotes or double quotes around each
glob pattern.

*--lib*

#+begin_quote
  Fix the package's library.
#+end_quote

*--bin* /name/...

#+begin_quote
  Fix the specified binary. This flag may be specified multiple times
  and supports common Unix glob patterns.
#+end_quote

*--bins*

#+begin_quote
  Fix all binary targets.
#+end_quote

*--example* /name/...

#+begin_quote
  Fix the specified example. This flag may be specified multiple times
  and supports common Unix glob patterns.
#+end_quote

*--examples*

#+begin_quote
  Fix all example targets.
#+end_quote

*--test* /name/...

#+begin_quote
  Fix the specified integration test. This flag may be specified
  multiple times and supports common Unix glob patterns.
#+end_quote

*--tests*

#+begin_quote
  Fix all targets in test mode that have the *test = true* manifest flag
  set. By default this includes the library and binaries built as
  unittests, and integration tests. Be aware that this will also build
  any required dependencies, so the lib target may be built twice (once
  as a unittest, and once as a dependency for binaries, integration
  tests, etc.). Targets may be enabled or disabled by setting the *test*
  flag in the manifest settings for the target.
#+end_quote

*--bench* /name/...

#+begin_quote
  Fix the specified benchmark. This flag may be specified multiple times
  and supports common Unix glob patterns.
#+end_quote

*--benches*

#+begin_quote
  Fix all targets in benchmark mode that have the *bench = true*
  manifest flag set. By default this includes the library and binaries
  built as benchmarks, and bench targets. Be aware that this will also
  build any required dependencies, so the lib target may be built twice
  (once as a benchmark, and once as a dependency for binaries,
  benchmarks, etc.). Targets may be enabled or disabled by setting the
  *bench* flag in the manifest settings for the target.
#+end_quote

*--all-targets*

#+begin_quote
  Fix all targets. This is equivalent to specifying *--lib --bins
  --tests --benches --examples*.
#+end_quote

** Feature Selection
The feature flags allow you to control which features are enabled. When
no feature options are given, the *default* feature is activated for
every selected package.

See /the features documentation/
<https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options>
for more details.

*--features* /features/

#+begin_quote
  Space or comma separated list of features to activate. Features of
  workspace members may be enabled with *package-name/feature-name*
  syntax. This flag may be specified multiple times, which enables all
  specified features.
#+end_quote

*--all-features*

#+begin_quote
  Activate all available features of all selected packages.
#+end_quote

*--no-default-features*

#+begin_quote
  Do not activate the *default* feature of the selected packages.
#+end_quote

** Compilation Options
*--target* /triple/

#+begin_quote
  Fix for the given architecture. The default is the host architecture.
  The general format of the triple is
  *<arch><sub>-<vendor>-<sys>-<abi>*. Run *rustc --print target-list*
  for a list of supported targets.

  This may also be specified with the *build.target* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.

  Note that specifying this flag makes Cargo run in a different mode
  where the target artifacts are placed in a separate directory. See the
  /build cache/ <https://doc.rust-lang.org/cargo/guide/build-cache.html>
  documentation for more details.
#+end_quote

*--release*

#+begin_quote
  Fix optimized artifacts with the *release* profile. See also the
  *--profile* option for choosing a specific profile by name.
#+end_quote

*--profile* /name/

#+begin_quote
  Fix with the given profile.

  As a special case, specifying the *test* profile will also enable
  checking in test mode which will enable checking tests and enable the
  *test* cfg option. See /rustc tests/
  <https://doc.rust-lang.org/rustc/tests/index.html> for more detail.

  See the /the reference/
  <https://doc.rust-lang.org/cargo/reference/profiles.html> for more
  details on profiles.
#+end_quote

*--ignore-rust-version*

#+begin_quote
  Fix the target even if the selected Rust compiler is older than the
  required Rust version as configured in the project's *rust-version*
  field.
#+end_quote

** Output Options
*--target-dir* /directory/

#+begin_quote
  Directory for all generated artifacts and intermediate files. May also
  be specified with the *CARGO_TARGET_DIR* environment variable, or the
  *build.target-dir* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  *target* in the root of the workspace.
#+end_quote

** Display Options
*-v*, *--verbose*

#+begin_quote
  Use verbose output. May be specified twice for "very verbose" output
  which includes extra output such as dependency warnings and build
  script output. May also be specified with the *term.verbose* /config
  value/ <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*-q*, *--quiet*

#+begin_quote
  No output printed to stdout.
#+end_quote

*--color* /when/

#+begin_quote
  Control when colored output is used. Valid values:

  #+begin_quote
    ·*auto* (default): Automatically detect if color support is
    available on the terminal.
  #+end_quote

  #+begin_quote
    ·*always*: Always display colors.
  #+end_quote

  #+begin_quote
    ·*never*: Never display colors.
  #+end_quote

  May also be specified with the *term.color* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

*--message-format* /fmt/

#+begin_quote
  The output format for diagnostic messages. Can be specified multiple
  times and consists of comma-separated values. Valid values:

  #+begin_quote
    ·*human* (default): Display in a human-readable text format.
    Conflicts with *short* and *json*.
  #+end_quote

  #+begin_quote
    ·*short*: Emit shorter, human-readable text messages. Conflicts with
    *human* and *json*.
  #+end_quote

  #+begin_quote
    ·*json*: Emit JSON messages to stdout. See /the reference/
    <https://doc.rust-lang.org/cargo/reference/external-tools.html#json-messages>
    for more details. Conflicts with *human* and *short*.
  #+end_quote

  #+begin_quote
    ·*json-diagnostic-short*: Ensure the *rendered* field of JSON
    messages contains the "short" rendering from rustc. Cannot be used
    with *human* or *short*.
  #+end_quote

  #+begin_quote
    ·*json-diagnostic-rendered-ansi*: Ensure the *rendered* field of
    JSON messages contains embedded ANSI color codes for respecting
    rustc's default color scheme. Cannot be used with *human* or
    *short*.
  #+end_quote

  #+begin_quote
    ·*json-render-diagnostics*: Instruct Cargo to not include rustc
    diagnostics in in JSON messages printed, but instead Cargo itself
    should render the JSON diagnostics coming from rustc. Cargo's own
    JSON diagnostics and others coming from rustc are still emitted.
    Cannot be used with *human* or *short*.
  #+end_quote
#+end_quote

** Manifest Options
*--manifest-path* /path/

#+begin_quote
  Path to the *Cargo.toml* file. By default, Cargo searches for the
  *Cargo.toml* file in the current directory or any parent directory.
#+end_quote

*--frozen*, *--locked*

#+begin_quote
  Either of these flags requires that the *Cargo.lock* file is
  up-to-date. If the lock file is missing, or it needs to be updated,
  Cargo will exit with an error. The *--frozen* flag also prevents Cargo
  from attempting to access the network to determine if it is
  out-of-date.

  These may be used in environments where you want to assert that the
  *Cargo.lock* file is up-to-date (such as a CI build) or want to avoid
  network access.
#+end_quote

*--offline*

#+begin_quote
  Prevents Cargo from accessing the network for any reason. Without this
  flag, Cargo will stop with an error if it needs to access the network
  and the network is not available. With this flag, Cargo will attempt
  to proceed without the network if possible.

  Beware that this may result in different dependency resolution than
  online mode. Cargo will restrict itself to crates that are downloaded
  locally, even if there might be a newer version as indicated in the
  local copy of the index. See the *cargo-fetch*(1) command to download
  dependencies before going offline.

  May also be specified with the *net.offline* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>.
#+end_quote

** Common Options
*+*/toolchain/

#+begin_quote
  If Cargo has been installed with rustup, and the first argument to
  *cargo* begins with *+*, it will be interpreted as a rustup toolchain
  name (such as *+stable* or *+nightly*). See the /rustup documentation/
  <https://rust-lang.github.io/rustup/overrides.html> for more
  information about how toolchain overrides work.
#+end_quote

*-h*, *--help*

#+begin_quote
  Prints help information.
#+end_quote

*-Z* /flag/

#+begin_quote
  Unstable (nightly-only) flags to Cargo. Run *cargo -Z help* for
  details.
#+end_quote

** Miscellaneous Options
*-j* /N/, *--jobs* /N/

#+begin_quote
  Number of parallel jobs to run. May also be specified with the
  *build.jobs* /config value/
  <https://doc.rust-lang.org/cargo/reference/config.html>. Defaults to
  the number of CPUs.
#+end_quote

* ENVIRONMENT
See /the reference/
<https://doc.rust-lang.org/cargo/reference/environment-variables.html>
for details on environment variables that Cargo reads.

* EXIT STATUS

#+begin_quote
  ·*0*: Cargo succeeded.
#+end_quote

#+begin_quote
  ·*101*: Cargo failed to complete.
#+end_quote

* EXAMPLES

#+begin_quote
  1.Apply compiler suggestions to the local package:

  #+begin_quote
    #+begin_example
      cargo fix
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  2.Update a package to prepare it for the next edition:

  #+begin_quote
    #+begin_example
      cargo fix --edition
    #+end_example
  #+end_quote
#+end_quote

#+begin_quote
  3.Apply suggested idioms for the current edition:

  #+begin_quote
    #+begin_example
      cargo fix --edition-idioms
    #+end_example
  #+end_quote
#+end_quote

* SEE ALSO
*cargo*(1), *cargo-check*(1)
