#+TITLE: Man1 - ntptrace.1
#+DESCRIPTION: Linux manpage for ntptrace.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*ntptrace* - Trace peers of an NTP server

* SYNOPSIS
*ntptrace* [*-flags*] [*-flag* [/value/]] [*--option-name*[[=|
]/value/]] [host]

* DESCRIPTION
*ntptrace* is a perl script that uses the ntpq utility program to follow
the chain of NTP servers from a given host back to the primary time
source. For ntptrace to work properly, each of these servers must
implement the NTP Control and Monitoring Protocol specified in RFC 1305
and enable NTP Mode 6 packets.

If given no arguments, ntptrace starts with localhost. Here is an
example of the output from ntptrace:

\\

#+begin_example
  % ntptrace localhost: stratum 4, offset 0.0019529, synch distance 0.144135
  server2ozo.com: stratum 2, offset 0.0124263, synch distance 0.115784 usndh.edu:
  stratum 1, offset 0.0019298, synch distance 0.011993, refid 'WWVB'
#+end_example

On each line, the fields are (left to right): the host name, the host
stratum, the time offset between that host and the local host (as
measured by *ntptrace*; this is why it is not always zero for
"localhost"), the host synchronization distance, and (only for stratum-1
servers) the reference clock ID. All times are given in seconds. Note
that the stratum is the server hop count to the primary source, while
the synchronization distance is the estimated error relative to the
primary source. These terms are precisely defined in RFC-1305.

* OPTIONS
Print IP addresses instead of hostnames.

Output hosts as dotted-quad numeric format rather than converting to the
canonical host names.

Maximum number of peers to trace. This option takes an integer number as
its argument. The default /number/ for this option is:

99

This option has not been fully documented.

Single remote host. The default /string/ for this option is:

127.0.0.1

This option has not been fully documented.

Display usage information and exit.

Pass the extended usage information through a pager.

Output version of program and exit. The default mode is `v', a simple
version. The `c' mode will print copyright information and `n' will
print the full copyright notice.

* EXIT STATUS
One of the following exit values will be returned:

Successful program execution.

The operation failed or the command syntax was not valid.

libopts had an internal operational error. Please report it to
autogen-users@lists.sourceforge.net. Thank you.

* NOTES
This manual page was /AutoGen/-erated from the *ntptrace* option
definitions.
