#+TITLE: Man1 - startx.1
#+DESCRIPTION: Linux manpage for startx.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
startx - initialize an X session

* SYNOPSIS
*startx* [ [ /client/ ] /options/ . . . ] [ *- -* [ /server/ ] [
/display/ ] /options/ . . . ]

* DESCRIPTION
The *startx* script is a front end to *xinit*(1) that provides a
somewhat nicer user interface for running a single session of the X
Window System. It is often run with no arguments.

Arguments immediately following the /startx/ command are used to start a
client in the same manner as *xinit*(1). The special argument '- -'
marks the end of client arguments and the beginning of server options.
It may be convenient to specify server options with startx to change
them on a per-session basis. Some examples of specifying server
arguments follow; consult the manual page for your X server to determine
which arguments are legal.

#+begin_quote
  startx - - -depth 16

  startx - - -dpi 100

  startx - - -layout Multihead
#+end_quote

To determine the client to run, *startx* looks for the following files,
in order:

#+begin_quote
  /$(HOME)/.startxrc/

  //usr/lib/sys.startxrc/

  /$(HOME)/.xinitrc/

  //etc/X11/xinit/xinitrc/
#+end_quote

If command line client options are given, they override this behavior
and revert to the *xinit*(1) behavior. To determine the server to run,
*startx* first looks for a file called /.xserverrc/ in the user's home
directory. If that is not found, it uses the file /xserverrc/ in the
/xinit/ library directory. If command line server options are given,
they override this behavior and revert to the *xinit*(1) behavior. Users
rarely need to provide a /.xserverrc/ file. See the *xinit*(1) manual
page for more details on the arguments.

The system-wide /xinitrc/ and /xserverrc/ files are found in the
//etc/X11/xinit/ directory.

The /.xinitrc/ is typically a shell script which starts many clients
according to the user's preference. When this shell script exits,
*startx* kills the server and performs any other session shutdown
needed. Most of the clients started by /.xinitrc/ should be run in the
background. The last client should run in the foreground; when it exits,
the session will exit. People often choose a session manager, window
manager, or /xterm/ as the ''magic'' client.

* EXAMPLE
Below is a sample /.xinitrc/ that starts several applications and leaves
the window manager running as the ''last'' application. Assuming that
the window manager has been configured properly, the user then chooses
the ''Exit'' menu item to shut down X.

#+begin_example
  xrdb -load $HOME/.Xresources
  xsetroot -solid gray &
  xbiff -geometry -430+5 &
  oclock -geometry 75x75-0-0 &
  xload -geometry -80-0 &
  xterm -geometry +0+60 -ls &
  xterm -geometry +0-100 &
  xconsole -geometry -0+0 -fn 5x7 &
  exec twm
#+end_example

* ENVIRONMENT VARIABLES
- DISPLAY :: This variable gets set to the name of the display to which
  clients should connect. Note that this gets /set/, not read.

- XAUTHORITY :: This variable, if not already defined, gets set to
  /$(HOME)/.Xauthority/. This is to prevent the X server, if not given
  the /-auth/ argument, from automatically setting up insecure
  host-based authentication for the local host. See the *Xserver*(1) and
  /Xsecurity/(7) manual pages for more information on X client/server
  authentication.

* FILES
- /$(HOME)/.xinitrc/ :: Client to run. Typically a shell script which
  runs many programs in the background.

- /$(HOME)/.xserverrc/ :: Server to run. The default is /X/.

- //etc/X11/xinit/xinitrc/ :: Client to run if the user has no
  /.xinitrc/ file.

- //etc/X11/xinit/xserverrc/ :: Server to run if the user has no
  /.xserverrc/ file.

* SEE ALSO
*xinit*(1), *X*(7), *Xserver*(1), *Xorg*(1), *xorg.conf*(5)
