#+TITLE: Man1 - vacuumdb.1
#+DESCRIPTION: Linux manpage for vacuumdb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vacuumdb - garbage-collect and analyze a PostgreSQL database

* SYNOPSIS
*vacuumdb* [/connection-option/...] [/option/...] [ *-t* | *--table*
/table/ [( /column/ [,...] )] ]... [/dbname/]

*vacuumdb* [/connection-option/...] [/option/...] *-a* | *--all*

* DESCRIPTION
vacuumdb is a utility for cleaning a PostgreSQL database. vacuumdb will
also generate internal statistics used by the PostgreSQL query
optimizer.

vacuumdb is a wrapper around the SQL command *VACUUM*(7). There is no
effective difference between vacuuming and analyzing databases via this
utility and via other methods for accessing the server.

* OPTIONS
vacuumdb accepts the following command-line arguments:

*-a*\\
*--all*

#+begin_quote
  Vacuum all databases.
#+end_quote

*[-d] */dbname/\\
*[--dbname=]*/dbname/

#+begin_quote
  Specifies the name of the database to be cleaned or analyzed, when
  *-a*/*--all* is not used. If this is not specified, the database name
  is read from the environment variable *PGDATABASE*. If that is not
  set, the user name specified for the connection is used. The /dbname/
  can be a connection string. If so, connection string parameters will
  override any conflicting command line options.
#+end_quote

*--disable-page-skipping*

#+begin_quote
  Disable skipping pages based on the contents of the visibility map.

  #+begin_quote
    \\

    *Note*

    \\
    This option is only available for servers running PostgreSQL 9.6 and
    later.
  #+end_quote
#+end_quote

*-e*\\
*--echo*

#+begin_quote
  Echo the commands that vacuumdb generates and sends to the server.
#+end_quote

*-f*\\
*--full*

#+begin_quote
  Perform “full” vacuuming.
#+end_quote

*-F*\\
*--freeze*

#+begin_quote
  Aggressively “freeze” tuples.
#+end_quote

*-j */njobs/\\
*--jobs=*/njobs/

#+begin_quote
  Execute the vacuum or analyze commands in parallel by running /njobs/
  commands simultaneously. This option may reduce the processing time
  but it also increases the load on the database server.

  vacuumdb will open /njobs/ connections to the database, so make sure
  your max_connections setting is high enough to accommodate all
  connections.

  Note that using this mode together with the *-f* (FULL) option might
  cause deadlock failures if certain system catalogs are processed in
  parallel.
#+end_quote

*--min-mxid-age */mxid_age/

#+begin_quote
  Only execute the vacuum or analyze commands on tables with a multixact
  ID age of at least /mxid_age/. This setting is useful for prioritizing
  tables to process to prevent multixact ID wraparound (see Section
  24.1.5.1).

  For the purposes of this option, the multixact ID age of a relation is
  the greatest of the ages of the main relation and its associated TOAST
  table, if one exists. Since the commands issued by vacuumdb will also
  process the TOAST table for the relation if necessary, it does not
  need to be considered separately.

  #+begin_quote
    \\

    *Note*

    \\
    This option is only available for servers running PostgreSQL 9.6 and
    later.
  #+end_quote
#+end_quote

*--min-xid-age */xid_age/

#+begin_quote
  Only execute the vacuum or analyze commands on tables with a
  transaction ID age of at least /xid_age/. This setting is useful for
  prioritizing tables to process to prevent transaction ID wraparound
  (see Section 24.1.5).

  For the purposes of this option, the transaction ID age of a relation
  is the greatest of the ages of the main relation and its associated
  TOAST table, if one exists. Since the commands issued by vacuumdb will
  also process the TOAST table for the relation if necessary, it does
  not need to be considered separately.

  #+begin_quote
    \\

    *Note*

    \\
    This option is only available for servers running PostgreSQL 9.6 and
    later.
  #+end_quote
#+end_quote

*-P */parallel_workers/\\
*--parallel=*/parallel_workers/

#+begin_quote
  Specify the number of parallel workers for parallel vacuum. This
  allows the vacuum to leverage multiple CPUs to process indexes. See
  *VACUUM*(7).

  #+begin_quote
    \\

    *Note*

    \\
    This option is only available for servers running PostgreSQL 13 and
    later.
  #+end_quote
#+end_quote

*-q*\\
*--quiet*

#+begin_quote
  Do not display progress messages.
#+end_quote

*--skip-locked*

#+begin_quote
  Skip relations that cannot be immediately locked for processing.

  #+begin_quote
    \\

    *Note*

    \\
    This option is only available for servers running PostgreSQL 12 and
    later.
  #+end_quote
#+end_quote

*-t */table/* [ (*/column/* [,...]) ]*\\
*--table=*/table/* [ (*/column/* [,...]) ]*

#+begin_quote
  Clean or analyze /table/ only. Column names can be specified only in
  conjunction with the *--analyze* or *--analyze-only* options. Multiple
  tables can be vacuumed by writing multiple *-t* switches.

  #+begin_quote
    \\

    *Tip*

    \\
    If you specify columns, you probably have to escape the parentheses
    from the shell. (See examples below.)
  #+end_quote
#+end_quote

*-v*\\
*--verbose*

#+begin_quote
  Print detailed information during processing.
#+end_quote

*-V*\\
*--version*

#+begin_quote
  Print the vacuumdb version and exit.
#+end_quote

*-z*\\
*--analyze*

#+begin_quote
  Also calculate statistics for use by the optimizer.
#+end_quote

*-Z*\\
*--analyze-only*

#+begin_quote
  Only calculate statistics for use by the optimizer (no vacuum).
#+end_quote

*--analyze-in-stages*

#+begin_quote
  Only calculate statistics for use by the optimizer (no vacuum), like
  *--analyze-only*. Run several (currently three) stages of analyze with
  different configuration settings, to produce usable statistics faster.

  This option is useful to analyze a database that was newly populated
  from a restored dump or by *pg_upgrade*. This option will try to
  create some statistics as fast as possible, to make the database
  usable, and then produce full statistics in the subsequent stages.
#+end_quote

*-?*\\
*--help*

#+begin_quote
  Show help about vacuumdb command line arguments, and exit.
#+end_quote

vacuumdb also accepts the following command-line arguments for
connection parameters:

*-h */host/\\
*--host=*/host/

#+begin_quote
  Specifies the host name of the machine on which the server is running.
  If the value begins with a slash, it is used as the directory for the
  Unix domain socket.
#+end_quote

*-p */port/\\
*--port=*/port/

#+begin_quote
  Specifies the TCP port or local Unix domain socket file extension on
  which the server is listening for connections.
#+end_quote

*-U */username/\\
*--username=*/username/

#+begin_quote
  User name to connect as.
#+end_quote

*-w*\\
*--no-password*

#+begin_quote
  Never issue a password prompt. If the server requires password
  authentication and a password is not available by other means such as
  a .pgpass file, the connection attempt will fail. This option can be
  useful in batch jobs and scripts where no user is present to enter a
  password.
#+end_quote

*-W*\\
*--password*

#+begin_quote
  Force vacuumdb to prompt for a password before connecting to a
  database.

  This option is never essential, since vacuumdb will automatically
  prompt for a password if the server demands password authentication.
  However, vacuumdb will waste a connection attempt finding out that the
  server wants a password. In some cases it is worth typing *-W* to
  avoid the extra connection attempt.
#+end_quote

*--maintenance-db=*/dbname/

#+begin_quote
  Specifies the name of the database to connect to to discover which
  databases should be vacuumed, when *-a*/*--all* is used. If not
  specified, the postgres database will be used, or if that does not
  exist, template1 will be used. This can be a connection string. If so,
  connection string parameters will override any conflicting command
  line options. Also, connection string parameters other than the
  database name itself will be re-used when connecting to other
  databases.
#+end_quote

* ENVIRONMENT
*PGDATABASE*\\
*PGHOST*\\
*PGPORT*\\
*PGUSER*

#+begin_quote
  Default connection parameters
#+end_quote

*PG_COLOR*

#+begin_quote
  Specifies whether to use color in diagnostic messages. Possible values
  are always, auto and never.
#+end_quote

This utility, like most other PostgreSQL utilities, also uses the
environment variables supported by libpq (see Section 33.14).

* DIAGNOSTICS
In case of difficulty, see *VACUUM*(7) and *psql*(1) for discussions of
potential problems and error messages. The database server must be
running at the targeted host. Also, any default connection settings and
environment variables used by the libpq front-end library will apply.

* NOTES
vacuumdb might need to connect several times to the PostgreSQL server,
asking for a password each time. It is convenient to have a ~/.pgpass
file in such cases. See Section 33.15 for more information.

* EXAMPLES
To clean the database test:

#+begin_quote
  #+begin_example
    $ vacuumdb test
  #+end_example
#+end_quote

To clean and analyze for the optimizer a database named bigdb:

#+begin_quote
  #+begin_example
    $ vacuumdb --analyze bigdb
  #+end_example
#+end_quote

To clean a single table foo in a database named xyzzy, and analyze a
single column bar of the table for the optimizer:

#+begin_quote
  #+begin_example
    $ vacuumdb --analyze --verbose --table=foo(bar) xyzzy
  #+end_example
#+end_quote

* SEE ALSO
*VACUUM*(7)
