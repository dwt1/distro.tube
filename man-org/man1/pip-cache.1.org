#+TITLE: Man1 - pip-cache.1
#+DESCRIPTION: Linux manpage for pip-cache.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pip-cache - description of pip cache command

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know why you
    came to this page and what on it helped you and what did not. (/Read
    more about this research/)
  #+end_quote
#+end_quote

* DESCRIPTION
Inspect and manage pip's wheel cache.

Subcommands:

#+begin_quote

  - dir: Show the cache directory.

  - info: Show information about the cache.

  - list: List filenames of packages stored in the cache.

  - remove: Remove one or more package from the cache.

  - purge: Remove all items from the cache.
#+end_quote

*<pattern>* can be a glob expression or a package name.

* USAGE

#+begin_quote

  #+begin_quote
    #+begin_example
      python -m pip cache dir
      python -m pip cache info
      python -m pip cache list [<pattern>] [--format=[human, abspath]]
      python -m pip cache remove <pattern>
      python -m pip cache purge
    #+end_example
  #+end_quote
#+end_quote

* OPTIONS

#+begin_quote
  - *--format <list_format>* :: Select the output format among: human
    (default) or abspath
#+end_quote

*IMPORTANT:*

#+begin_quote

  #+begin_quote
    *Did this article help?*

    We are currently doing research to improve pip's documentation and
    would love your feedback. Please /email us/ and let us know:

    #+begin_quote

      1. What problem were you trying to solve when you came to this
         page?

      2. What content was useful?

      3. What content was not useful?
    #+end_quote
  #+end_quote
#+end_quote

* AUTHOR
pip developers

* COPYRIGHT
2008-2021, PyPA
