#+TITLE: Man1 - git-update-server-info.1
#+DESCRIPTION: Linux manpage for git-update-server-info.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
git-update-server-info - Update auxiliary info file to help dumb servers

* SYNOPSIS
#+begin_example
  git update-server-info
#+end_example

* DESCRIPTION
A dumb server that does not do on-the-fly pack generations must have
some auxiliary information files in $GIT_DIR/info and
$GIT_OBJECT_DIRECTORY/info directories to help clients discover what
references and packs the server has. This command generates such
auxiliary files.

* OUTPUT
Currently the command updates the following files. Please see
*gitrepository-layout*(5) for description of what they are for:

#+begin_quote
  ·

  objects/info/packs
#+end_quote

#+begin_quote
  ·

  info/refs
#+end_quote

* GIT
Part of the *git*(1) suite
