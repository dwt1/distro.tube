#+TITLE: Man1 - perllexwarn.1perl
#+DESCRIPTION: Linux manpage for perllexwarn.1perl
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
perllexwarn - Perl Lexical Warnings

* DESCRIPTION
Perl v5.6.0 introduced lexical control over the handling of warnings by
category. The =warnings= pragma generally replaces the command line flag
*-w*. Documentation on the use of lexical warnings, once partly found in
this document, is now found in the warnings documentation.
