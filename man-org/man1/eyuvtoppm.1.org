#+TITLE: Man1 - eyuvtoppm.1
#+DESCRIPTION: Linux manpage for eyuvtoppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
eyuvtoppm - convert a Berkeley YUV file to a PPM file

* SYNOPSIS
*eyuvtoppm* [*--width* /width/] [*--height* /height/] [/eyuvfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*eyuvtoppm* reads a Berkeley Encoder YUV (not the same as Abekas YUV)
file as input and produces a PPM image on Standard Output.

With no filename argument takes input from Standard Input. Otherwise,
/eyuvfile / is the file specification of the input file.

* SEE ALSO
*ppmtoeyuv*(1) , *yuvtoppm*(1) , *ppm*(5)
