#+TITLE: Man1 - asn1Decoding.1
#+DESCRIPTION: Linux manpage for asn1Decoding.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
asn1Decoding - ASN.1 DER decoder

* SYNOPSIS
*asn1Decoding* [/OPTION/] /DEFINITIONS ENCODED ASN1TYPE/

* DESCRIPTION
Decodes DER data in ENCODED file, for the ASN1TYPE element described in
ASN.1 DEFINITIONS file, and print decoded structures.

- *-b*, *--benchmark* :: perform a benchmark on decoding

- *-s*, *--strict* :: use strict DER decoding

- *-t*, *--no-time-strict* :: use strict DER decoding but not in time
  fields

- *-h*, *--help* :: display this help and exit

- *-v*, *--version* :: output version information and exit

* AUTHOR
Written by Fabio Fiorina.

* REPORTING BUGS
Report bugs to: help-libtasn1@gnu.org\\
GNU Libtasn1 home page: <https://www.gnu.org/software/libtasn1/>\\
General help using GNU software: <https://www.gnu.org/gethelp/>

* COPYRIGHT
Copyright © 2021 Free Software Foundation, Inc. License GPLv3+: GNU GPL
version 3 or later <https://gnu.org/licenses/gpl.html>.\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
The full documentation for *asn1Decoding* is maintained as a Texinfo
manual. If the *info* and *asn1Decoding* programs are properly installed
at your site, the command

#+begin_quote
  *info libtasn1*
#+end_quote

should give you access to the complete manual.
