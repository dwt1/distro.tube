#+TITLE: Man1 - Q
#+DESCRIPTION: Man1 - Q
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* Q
#+begin_src bash :exports results
readarray -t starts_with_q < <(find . -type f -iname "q*" | sort)

for x in "${starts_with_q[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./qalc.1.org][qalc.1]]                |
| [[file:./qalculate-gtk.1.org][qalculate-gtk.1]]       |
| [[file:./qemu.1.org][qemu.1]]                |
| [[file:./qemu-img.1.org][qemu-img.1]]            |
| [[file:./qemu-storage-daemon.1.org][qemu-storage-daemon.1]] |
| [[file:./qjackctl.1.org][qjackctl.1]]            |
| [[file:./qmicli.1.org][qmicli.1]]              |
| [[file:./qmi-firmware-update.1.org][qmi-firmware-update.1]] |
| [[file:./qmi-network.1.org][qmi-network.1]]         |
| [[file:./qpdf.1.org][qpdf.1]]                |
| [[file:./qrttoppm.1.org][qrttoppm.1]]            |
| [[file:./quest.1.org][quest.1]]               |
| [[file:./qutebrowser.1.org][qutebrowser.1]]         |
| [[file:./qv4l2.1.org][qv4l2.1]]               |
| [[file:./qvidcap.1.org][qvidcap.1]]             |
