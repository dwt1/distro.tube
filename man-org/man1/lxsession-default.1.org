#+TITLE: Man1 - lxsession-default.1
#+DESCRIPTION: Linux manpage for lxsession-default.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-default - Launching default applications set by LXSession

* SYNOPSIS
*lxsession-default* [COMMAND] [OPTIONS]

* DESCRIPTION
*lxsession-default* is a wrapper around LXSession D-Bus interface, which
launch default applications set by LXsession.

COMMAND is the application you want to launch, and the ones available
are define in desktop.conf.examples.

OPTIONS are available if the COMMAND accept it.

Example : To launch file_manager/command set in desktop.conf, use :
lxsession-default file_manager

* AUTHORS
Julien Lavergne <gilir@ubuntu.com>

Man page written to conform with Debian by Julien Lavergne.
