#+TITLE: Man1 - neqn.1
#+DESCRIPTION: Linux manpage for neqn.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
neqn - format equations for ASCII output

* SYNOPSIS
*neqn* [/eqn-options/]

* DESCRIPTION
/neqn/ invokes the /eqn/(1) command with the *ascii* output device.

Note that /eqn/ does not support low-resolution, typewriter-like devices
(although it may work adequately for very simple input).

* SEE ALSO
/eqn/(1)
