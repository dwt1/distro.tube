#+TITLE: Man1 - find-libprovides.1
#+DESCRIPTION: Linux manpage for find-libprovides.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
find-libprovides - Find sonames which are provided by a package

* SYNOPSIS
find-libprovides [options]

* DESCRIPTION
Finds soname's provided by a package and prints out a list in the
following format /<soname>=<soversion>-<soarch>/.

* OPTIONS
*--ignore-internal*

#+begin_quote
  Ignore internal libraries.
#+end_quote

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [devtools] or via
arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Aaron Griffin <aaronmgriffin@gmail.com>
#+end_quote

#+begin_quote
  ·

  Allan McRae <allan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dan McGee <dan@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Evangelos Foutras <evangelos@foutrelis.com>
#+end_quote

#+begin_quote
  ·

  Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
#+end_quote

#+begin_quote
  ·

  Jelle van der Waa <jelle@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Levente Polyak <anthraxx@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Pierre Schmitz <pierre@archlinux.de>
#+end_quote

#+begin_quote
  ·

  Sébastien Luttringer <seblu@seblu.net>
#+end_quote

#+begin_quote
  ·

  Sven-Hendrik Haase <svenstaro@gmail.com>
#+end_quote

#+begin_quote
  ·

  Thomas Bächler <thomas@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the devtools.git
repository.
