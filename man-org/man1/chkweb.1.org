#+TITLE: Man1 - chkweb.1
#+DESCRIPTION: Linux manpage for chkweb.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
chkweb - runs the LaTeX parts of a CWEB file through chktex

* SYNOPSIS
*chkweb* /[-hiqrW] [-l <rcfile>] [-[wemn] <[1-42]|all>]/ /[-d[0-...]]
[-o <outfile>] [-[btxgI][0|1]]/ *file1 file2 ...*

* DESCRIPTION
/chkweb/ runs the /LaTeX/ parts of a CWEB file through chktex.

* OPTIONS
Miscellaneous options:

- *-h --help* :: Print a help screen.

- *-i --license* :: Show distribution information.

- *-l --localrc* :: Read local .chktexrc formatted file.

- *-d --debug* :: Debug information. Give it a number.

- *-r --reset* :: Reset settings to default.

Muting warning messages:

- *-w --warnon* :: Makes msg # given a warning and turns it on.

- *-e --erroron* :: Makes msg # given an error and turns it on.

- *-m --msgon* :: Makes msg # given a message and turns it on.

- *-n --nowarn* :: Mutes msg # given.

Output control flags:

- *-V --pipeverb* :: How errors are displayed when stdout != tty.
  Defaults to the same as -v.

- *-s --splitchar* :: String used to split fields when doing -v0

- *-o --output* :: Redirect error report to a file.

- *-q --quiet* :: Shuts up about version information.

- *-f --format* :: Format to use for output

Boolean switches (1 -> enables / 0 -> disables):

- *-b --backup* :: Backup output file.

- *-x --wipeverb* :: Ignore contents of `\verb' commands.

- *-g --globalrc* :: Read global .chktexrc file.

- *-I --inputfiles* :: Execute \input statements.

- *-H --headererr* :: Show errors found in front of \begin{document}

- *-W --version* :: Version information

If no LaTeX files are specified on the command line, we will read from
stdin. For explanation of warning/error messages, please consult the
main document /usr/share/doc/chktex/ChkTeX.dvi.gz.

* DISTRIBUTION
Copyright (C) 1996 Jens T. Berger Thielemann

This program is free software; you can redistribute it and/or modify it
under the terms of the /GNU General Public License/ as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
*WITHOUT ANY WARRANTY;* without even the implied warranty of
*MERCHANTABILITY* or *FITNESS FOR A PARTICULAR PURPOSE.* See the /GNU
General Public License/ for more details.

You should have received a copy of the *GNU General Public License*
along with this program; if not, write to the /Free Software Foundation,
Inc.,/ 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

* ENVIRONMENT
No environment variables are used.

* FILES
None.

* AUTHOR
Jens T. Berger Thielemann, /<jensthi@ifi.uio.no>/

This manual page was cobbled together by Clint Adams
<schizo@debian.org>, based on the output of "chktex --help" and
deweb(1).

* SEE ALSO
*deweb(1), chktex(1)*
