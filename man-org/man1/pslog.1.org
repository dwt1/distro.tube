#+TITLE: Man1 - pslog.1
#+DESCRIPTION: Linux manpage for pslog.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pslog - report current logs path of a process

* SYNOPSIS
*pslog* /pid/ ...\\
*pslog -V*

* DESCRIPTION
The *pslog* command reports the current working logs of a process.

* OPTIONS
- *-V* :: Display version information.

* SEE ALSO
*pgrep*(1), *ps*(1), *pwdx*(1).

* AUTHOR
Vito Mule' [[mailto:mulevito@gmail.com][]] wrote *pslog* in 2015. Please
send bug reports to [[mailto:mulevito@gmail.com][]].
