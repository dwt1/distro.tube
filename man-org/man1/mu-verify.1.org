#+TITLE: Man1 - mu-verify.1
#+DESCRIPTION: Linux manpage for mu-verify.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
mu verify- verify message signatures and display information about them

* SYNOPSIS
*mu verify [options] <msgfile>*

* DESCRIPTION
*mu verify* is the *mu* command for verifying message signatures (such
as PGP/GPG signatures) and displaying information about them. The
sub-command works on message files, and does not require the message to
be indexed in the database.

*mu verify* depends on *gpg*, and uses the one it finds in your *PATH*.
If you want to use another one, you need to set *MU_GPG_PATH* to the
full path to the desired *gpg*.

* OPTIONS
- *-r*, *--auto-retrieve* :: attempt to find keys online (see the
  *auto-key-retrieve* option in the *gnupg(1)* documentation).

* EXAMPLES
To display aggregated (one-line) information about the signatures in a
message:

#+begin_example
     $ mu verify msgfile
#+end_example

To display information about all the signatures:

#+begin_example
     $ mu verify --verbose msgfile
#+end_example

If you only want to use the exit code, you can use:

#+begin_example
     $ mu verify --quiet msgfile
#+end_example

which does not give any output.

* RETURN VALUE
*mu verify* returns 0 when all signatures could be verified to be good,
and returns some non-zero error code when this is not the case.

#+begin_example
  | code | meaning                        |
  |------+--------------------------------|
  |    0 | ok                             |
  |    1 | some non-verified signature(s) |
#+end_example

* BUGS
Please report bugs if you find them: *https://github.com/djcb/mu/issues*

* AUTHOR
Dirk-Jan C. Binnema <djcb@djcbsoftware.nl>

* SEE ALSO
*mu*(1), *gpg*(1)
