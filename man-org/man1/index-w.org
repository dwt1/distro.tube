#+TITLE: Man1 - W
#+DESCRIPTION: Man1 - W
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* W
#+begin_src bash :exports results
readarray -t starts_with_w < <(find . -type f -iname "w*" | sort)

for x in "${starts_with_w[@]}"; do
   name=$(echo "$x" | awk -F / '{print $NF}' | sed 's/.org//g')
   echo "[[$x][$name]]"
done
#+end_src

#+RESULTS:
| [[file:./w.1.org][w.1]]                  |
| [[file:./w3m.1.org][w3m.1]]                |
| [[file:./w3mman.1.org][w3mman.1]]             |
| [[file:./wall.1.org][wall.1]]               |
| [[file:./watch.1.org][watch.1]]              |
| [[file:./watchgnupg.1.org][watchgnupg.1]]         |
| [[file:./wavpack.1.org][wavpack.1]]            |
| [[file:./wbmptopbm.1.org][wbmptopbm.1]]          |
| [[file:./wc.1.org][wc.1]]                 |
| [[file:./weave.1.org][weave.1]]              |
| [[file:./web2disk.1.org][web2disk.1]]           |
| [[file:./webpinfo.1.org][webpinfo.1]]           |
| [[file:./webpmux.1.org][webpmux.1]]            |
| [[file:./wget.1.org][wget.1]]               |
| [[file:./whatis.1.org][whatis.1]]             |
| [[file:./whereis.1.org][whereis.1]]            |
| [[file:./which.1.org][which.1]]              |
| [[file:./whiptail.1.org][whiptail.1]]           |
| [[file:./who.1.org][who.1]]                |
| [[file:./whoami.1.org][whoami.1]]             |
| [[file:./wifi-menu.1.org][wifi-menu.1]]          |
| [[file:./wildmidi.1.org][wildmidi.1]]           |
| [[file:./winicontoppm.1.org][winicontoppm.1]]       |
| [[file:./wirefilter.1.org][wirefilter.1]]         |
| [[file:./wmctrl.1.org][wmctrl.1]]             |
| [[file:./word-list-compress.1.org][word-list-compress.1]] |
| [[file:./write.1.org][write.1]]              |
| [[file:./wrjpgcom.1.org][wrjpgcom.1]]           |
| [[file:./wsgen-zulu-8.1.org][wsgen-zulu-8.1]]       |
| [[file:./wsimport-zulu-8.1.org][wsimport-zulu-8.1]]    |
| [[file:./wvdial.1.org][wvdial.1]]             |
| [[file:./wvdialconf.1.org][wvdialconf.1]]         |
| [[file:./wvgain.1.org][wvgain.1]]             |
| [[file:./wvtag.1.org][wvtag.1]]              |
| [[file:./wvunpack.1.org][wvunpack.1]]           |
