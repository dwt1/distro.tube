#+TITLE: Man1 - korgdump.1
#+DESCRIPTION: Linux manpage for korgdump.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
korgdump - List information about sound files in KORG synthesizer
format.

* SYNOPSIS
*korgdump* [ -v ] FILE

* DESCRIPTION
Prints out detailed information of sample based sound files (either with
.KMP or .KSF file type extension) used with KORG synthesizer keyboards
like Trinity, Triton, OASYS, M3 or Kronos.

There is no support for .PCG files in this version of korgdump yet.

* OPTIONS
- * FILE* :: filename of a sample based sound file as used with KORG
  synthesizer keyboards (.KMP or .KSF file)

- * -v* :: print version and exit

* SEE ALSO
*korg2gig(1),* *akaidump(1),* *rifftree(1),* *gigdump(1),* *dlsdump(1),*
*sf2dump(1)*

* BUGS
Check and report bugs at http://bugs.linuxsampler.org

* Author
Application and manual page written by Christian Schoenebeck
<cuse@users.sf.net>
