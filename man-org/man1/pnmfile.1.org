#+TITLE: Man1 - pnmfile.1
#+DESCRIPTION: Linux manpage for pnmfile.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
*pnmfile* - replaced by pamfile

* DESCRIPTION
This program is part of *Netpbm*(1)

*pnmfile* was replaced in Netpbm 10.9 (September 2002) by *pamfile*(1)

*pamfile* is backward compatible with *pnmfile*, but works on PAM images
too.
