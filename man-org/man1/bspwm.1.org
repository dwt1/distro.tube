#+TITLE: Man1 - bspwm.1
#+DESCRIPTION: Linux manpage for bspwm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bspwm - Binary space partitioning window manager

* SYNOPSIS
*bspwm* [*-h*|*-v*|*-c* /CONFIG_PATH/]

*bspc* /DOMAIN/ [/SELECTOR/] /COMMANDS/

*bspc* /COMMAND/ [/OPTIONS/] [/ARGUMENTS/]

* DESCRIPTION
*bspwm* is a tiling window manager that represents windows as the leaves
of a full binary tree.

It is controlled and configured via *bspc*.

* OPTIONS
*-h*

#+begin_quote
  Print the synopsis and exit.
#+end_quote

*-v*

#+begin_quote
  Print the version and exit.
#+end_quote

*-c* /CONFIG_PATH/

#+begin_quote
  Use the given configuration file.
#+end_quote

* COMMON DEFINITIONS

#+begin_quote
  #+begin_example
    DIR         := north | west | south | east
    CYCLE_DIR   := next | prev
  #+end_example
#+end_quote

* SELECTORS
Selectors are used to select a target node, desktop, or monitor. A
selector can either describe the target relatively or name it globally.

Selectors consist of an optional reference, a descriptor and any number
of non-conflicting modifiers as follows:

#+begin_quote
  #+begin_example
    [REFERENCE#]DESCRIPTOR(.MODIFIER)*
  #+end_example
#+end_quote

The relative targets are computed in relation to the given reference
(the default reference value is *focused*).

An exclamation mark can be prepended to any modifier in order to reverse
its meaning.

The following characters cannot be used in monitor or desktop names:
*#*, *:*, *.*.

The special selector *%<name>* can be used to select a monitor or a
desktop with an invalid name.

** Node
Select a node.

#+begin_quote
  #+begin_example
    NODE_SEL := [NODE_SEL#](DIR|CYCLE_DIR|PATH|any|first_ancestor|last|newest|
                            older|newer|focused|pointed|biggest|smallest|
                            <node_id>)[.[!]focused][.[!]active][.[!]automatic][.[!]local]
                                      [.[!]leaf][.[!]window][.[!]STATE][.[!]FLAG][.[!]LAYER][.[!]SPLIT_TYPE]
                                      [.[!]same_class][.[!]descendant_of][.[!]ancestor_of]

    STATE := tiled|pseudo_tiled|floating|fullscreen

    FLAG := hidden|sticky|private|locked|marked|urgent

    LAYER := below|normal|above

    SPLIT_TYPE := horizontal|vertical

    PATH := @[DESKTOP_SEL:][[/]JUMP](/JUMP)*

    JUMP := first|1|second|2|brother|parent|DIR
  #+end_example
#+end_quote

\\

*Descriptors*

#+begin_quote
  /DIR/

  #+begin_quote
    Selects the window in the given (spacial) direction relative to the
    reference node.
  #+end_quote

  /CYCLE_DIR/

  #+begin_quote
    Selects the node in the given (cyclic) direction relative to the
    reference node within a depth-first in-order traversal of the tree.
  #+end_quote

  /PATH/

  #+begin_quote
    Selects the node at the given path.
  #+end_quote

  any

  #+begin_quote
    Selects the first node that matches the given selectors.
  #+end_quote

  first_ancestor

  #+begin_quote
    Selects the first ancestor of the reference node that matches the
    given selectors.
  #+end_quote

  last

  #+begin_quote
    Selects the previously focused node relative to the reference node.
  #+end_quote

  newest

  #+begin_quote
    Selects the newest node in the history of the focused node.
  #+end_quote

  older

  #+begin_quote
    Selects the node older than the reference node in the history.
  #+end_quote

  newer

  #+begin_quote
    Selects the node newer than the reference node in the history.
  #+end_quote

  focused

  #+begin_quote
    Selects the currently focused node.
  #+end_quote

  pointed

  #+begin_quote
    Selects the leaf under the pointer.
  #+end_quote

  biggest

  #+begin_quote
    Selects the biggest leaf.
  #+end_quote

  smallest

  #+begin_quote
    Selects the smallest leaf.
  #+end_quote

  <node_id>

  #+begin_quote
    Selects the node with the given ID.
  #+end_quote
#+end_quote

\\

*Path Jumps*

#+begin_quote
  The initial node is the focused node (or the root if the path starts
  with ///) of the reference desktop (or the selected desktop if the
  path has a /DESKTOP_SEL/ prefix).

  1|first

  #+begin_quote
    Jumps to the first child.
  #+end_quote

  2|second

  #+begin_quote
    Jumps to the second child.
  #+end_quote

  brother

  #+begin_quote
    Jumps to the brother node.
  #+end_quote

  parent

  #+begin_quote
    Jumps to the parent node.
  #+end_quote

  /DIR/

  #+begin_quote
    Jumps to the node holding the edge in the given direction.
  #+end_quote
#+end_quote

\\

*Modifiers*

#+begin_quote
  [!]focused

  #+begin_quote
    Only consider the focused node.
  #+end_quote

  [!]active

  #+begin_quote
    Only consider nodes that are the focused node of their desktop.
  #+end_quote

  [!]automatic

  #+begin_quote
    Only consider nodes in automatic insertion mode. See also
    *--presel-dir* under *Node* in the *DOMAINS* section below.
  #+end_quote

  [!]local

  #+begin_quote
    Only consider nodes in the reference desktop.
  #+end_quote

  [!]leaf

  #+begin_quote
    Only consider leaf nodes.
  #+end_quote

  [!]window

  #+begin_quote
    Only consider nodes that hold a window.
  #+end_quote

  [!](tiled|pseudo_tiled|floating|fullscreen)

  #+begin_quote
    Only consider windows in the given state.
  #+end_quote

  [!]same_class

  #+begin_quote
    Only consider windows that have the same class as the reference
    window.
  #+end_quote

  [!]descendant_of

  #+begin_quote
    Only consider nodes that are descendants of the reference node.
  #+end_quote

  [!]ancestor_of

  #+begin_quote
    Only consider nodes that are ancestors of the reference node.
  #+end_quote

  [!](hidden|sticky|private|locked|marked|urgent)

  #+begin_quote
    Only consider windows that have the given flag set.
  #+end_quote

  [!](below|normal|above)

  #+begin_quote
    Only consider windows in the given layer.
  #+end_quote

  [!](horizontal|vertical)

  #+begin_quote
    Only consider nodes with the given split type.
  #+end_quote
#+end_quote

** Desktop
Select a desktop.

#+begin_quote
  #+begin_example
    DESKTOP_SEL := [DESKTOP_SEL#](CYCLE_DIR|any|last|newest|older|newer|
                                  [MONITOR_SEL:](focused|^<n>)|
                                  <desktop_id>|<desktop_name>)[.[!]focused][.[!]active]
                                                              [.[!]occupied][.[!]urgent][.[!]local]
  #+end_example
#+end_quote

\\

*Descriptors*

#+begin_quote
  /CYCLE_DIR/

  #+begin_quote
    Selects the desktop in the given direction relative to the reference
    desktop.
  #+end_quote

  any

  #+begin_quote
    Selects the first desktop that matches the given selectors.
  #+end_quote

  last

  #+begin_quote
    Selects the previously focused desktop relative to the reference
    desktop.
  #+end_quote

  newest

  #+begin_quote
    Selects the newest desktop in the history of the focused desktops.
  #+end_quote

  older

  #+begin_quote
    Selects the desktop older than the reference desktop in the history.
  #+end_quote

  newer

  #+begin_quote
    Selects the desktop newer than the reference desktop in the history.
  #+end_quote

  focused

  #+begin_quote
    Selects the currently focused desktop.
  #+end_quote

  ^<n>

  #+begin_quote
    Selects the nth desktop. If *MONITOR_SEL* is given, selects the nth
    desktop on the selected monitor.
  #+end_quote

  <desktop_id>

  #+begin_quote
    Selects the desktop with the given ID.
  #+end_quote

  <desktop_name>

  #+begin_quote
    Selects the desktop with the given name.
  #+end_quote
#+end_quote

\\

*Modifiers*

#+begin_quote
  [!]focused

  #+begin_quote
    Only consider the focused desktop.
  #+end_quote

  [!]active

  #+begin_quote
    Only consider desktops that are the focused desktop of their
    monitor.
  #+end_quote

  [!]occupied

  #+begin_quote
    Only consider occupied desktops.
  #+end_quote

  [!]urgent

  #+begin_quote
    Only consider urgent desktops.
  #+end_quote

  [!]local

  #+begin_quote
    Only consider desktops inside the reference monitor.
  #+end_quote
#+end_quote

** Monitor
Select a monitor.

#+begin_quote
  #+begin_example
    MONITOR_SEL := [MONITOR_SEL#](DIR|CYCLE_DIR|any|last|newest|older|newer|
                                  focused|pointed|primary|^<n>|
                                  <monitor_id>|<monitor_name>)[.[!]focused][.[!]occupied]
  #+end_example
#+end_quote

\\

*Descriptors*

#+begin_quote
  /DIR/

  #+begin_quote
    Selects the monitor in the given (spacial) direction relative to the
    reference monitor.
  #+end_quote

  /CYCLE_DIR/

  #+begin_quote
    Selects the monitor in the given (cyclic) direction relative to the
    reference monitor.
  #+end_quote

  any

  #+begin_quote
    Selects the first monitor that matches the given selectors.
  #+end_quote

  last

  #+begin_quote
    Selects the previously focused monitor relative to the reference
    monitor.
  #+end_quote

  newest

  #+begin_quote
    Selects the newest monitor in the history of the focused monitors.
  #+end_quote

  older

  #+begin_quote
    Selects the monitor older than the reference monitor in the history.
  #+end_quote

  newer

  #+begin_quote
    Selects the monitor newer than the reference monitor in the history.
  #+end_quote

  focused

  #+begin_quote
    Selects the currently focused monitor.
  #+end_quote

  pointed

  #+begin_quote
    Selects the monitor under the pointer.
  #+end_quote

  primary

  #+begin_quote
    Selects the primary monitor.
  #+end_quote

  ^<n>

  #+begin_quote
    Selects the nth monitor.
  #+end_quote

  <monitor_id>

  #+begin_quote
    Selects the monitor with the given ID.
  #+end_quote

  <monitor_name>

  #+begin_quote
    Selects the monitor with the given name.
  #+end_quote
#+end_quote

\\

*Modifiers*

#+begin_quote
  [!]focused

  #+begin_quote
    Only consider the focused monitor.
  #+end_quote

  [!]occupied

  #+begin_quote
    Only consider monitors where the focused desktop is occupied.
  #+end_quote
#+end_quote

* WINDOW STATES
tiled

#+begin_quote
  Its size and position are determined by the window tree.
#+end_quote

pseudo_tiled

#+begin_quote
  A tiled window that automatically shrinks but doesn't stretch beyond
  its floating size.
#+end_quote

floating

#+begin_quote
  Can be moved/resized freely. Although it doesn't use any tiling space,
  it is still part of the window tree.
#+end_quote

fullscreen

#+begin_quote
  Fills its monitor rectangle and has no borders.
#+end_quote

* NODE FLAGS
hidden

#+begin_quote
  Is hidden and doesn't occupy any tiling space.
#+end_quote

sticky

#+begin_quote
  Stays in the focused desktop of its monitor.
#+end_quote

private

#+begin_quote
  Tries to keep the same tiling position/size.
#+end_quote

locked

#+begin_quote
  Ignores the *node --close* message.
#+end_quote

marked

#+begin_quote
  Is marked (useful for deferred actions). A marked node becomes
  unmarked after being sent on a preselected node.
#+end_quote

urgent

#+begin_quote
  Has its urgency hint set. This flag is set externally.
#+end_quote

* STACKING LAYERS
There's three stacking layers: BELOW, NORMAL and ABOVE.

In each layer, the window are orderered as follow: tiled & pseudo-tiled
< floating < fullscreen.

* RECEPTACLES
A leaf node that doesn't hold any window is called a receptacle. When a
node is inserted on a receptacle in automatic mode, it will replace the
receptacle. A receptacle can be inserted on a node, preselected and
killed. Receptacles can therefore be used to build a tree whose leaves
are receptacles. Using the appropriate rules, one can then send windows
on the leaves of this tree. This feature is used in
/examples/receptacles/ to store and recreate layouts.

* DOMAINS
** Node
\\

*General Syntax*

#+begin_quote
  node [/NODE_SEL/] /COMMANDS/

  If /NODE_SEL/ is omitted, *focused* is assumed.
#+end_quote

\\

*Commands*

#+begin_quote
  *-f*, *--focus* [/NODE_SEL/]

  #+begin_quote
    Focus the selected or given node.
  #+end_quote

  *-a*, *--activate* [/NODE_SEL/]

  #+begin_quote
    Activate the selected or given node.
  #+end_quote

  *-d*, *--to-desktop* /DESKTOP_SEL/ [*--follow*]

  #+begin_quote
    Send the selected node to the given desktop. If *--follow* is
    passed, the focused node will stay focused.
  #+end_quote

  *-m*, *--to-monitor* /MONITOR_SEL/ [*--follow*]

  #+begin_quote
    Send the selected node to the given monitor. If *--follow* is
    passed, the focused node will stay focused.
  #+end_quote

  *-n*, *--to-node* /NODE_SEL/ [*--follow*]

  #+begin_quote
    Send the selected node on the given node. If *--follow* is passed,
    the focused node will stay focused.
  #+end_quote

  *-s*, *--swap* /NODE_SEL/ [*--follow*]

  #+begin_quote
    Swap the selected node with the given node. If *--follow* is passed,
    the focused node will stay focused.
  #+end_quote

  *-p*, *--presel-dir* [~]/DIR/|cancel

  #+begin_quote
    Preselect the splitting area of the selected node (or cancel the
    preselection). If *~* is prepended to /DIR/ and the current
    preselection direction matches /DIR/, then the argument is
    interpreted as *cancel*. A node with a preselected area is said to
    be in "manual insertion mode".
  #+end_quote

  *-o*, *--presel-ratio* /RATIO/

  #+begin_quote
    Set the splitting ratio of the preselection area.
  #+end_quote

  *-v*, *--move* /dx/ /dy/

  #+begin_quote
    Move the selected window by /dx/ pixels horizontally and /dy/ pixels
    vertically.
  #+end_quote

  *-z*, *--resize*
  top|left|bottom|right|top_left|top_right|bottom_right|bottom_left /dx/
  /dy/

  #+begin_quote
    Resize the selected window by moving the given handle by /dx/ pixels
    horizontally and /dy/ pixels vertically.
  #+end_quote

  *-r*, *--ratio* /RATIO/|(+|-)(/PIXELS/|/FRACTION/)

  #+begin_quote
    Set the splitting ratio of the selected node (0 < /RATIO/ < 1).
  #+end_quote

  *-R*, *--rotate* /90|270|180/

  #+begin_quote
    Rotate the tree rooted at the selected node.
  #+end_quote

  *-F*, *--flip* /horizontal|vertical/

  #+begin_quote
    Flip the the tree rooted at selected node.
  #+end_quote

  *-E*, *--equalize*

  #+begin_quote
    Reset the split ratios of the tree rooted at the selected node to
    their default value.
  #+end_quote

  *-B*, *--balance*

  #+begin_quote
    Adjust the split ratios of the tree rooted at the selected node so
    that all windows occupy the same area.
  #+end_quote

  *-C*, *--circulate* forward|backward

  #+begin_quote
    Circulate the windows of the tree rooted at the selected node.
  #+end_quote

  *-t*, *--state* [~](tiled|pseudo_tiled|floating|fullscreen)

  #+begin_quote
    Set the state of the selected window. If *~* is present and the
    current state matches the given state, then the argument is
    interpreted as the last state.
  #+end_quote

  *-g*, *--flag* hidden|sticky|private|locked|marked[=on|off]

  #+begin_quote
    Set or toggle the given flag for the selected node.
  #+end_quote

  *-l*, *--layer* below|normal|above

  #+begin_quote
    Set the stacking layer of the selected window.
  #+end_quote

  *-i*, *--insert-receptacle*

  #+begin_quote
    Insert a receptacle node at the selected node.
  #+end_quote

  *-c*, *--close*

  #+begin_quote
    Close the windows rooted at the selected node.
  #+end_quote

  *-k*, *--kill*

  #+begin_quote
    Kill the windows rooted at the selected node.
  #+end_quote
#+end_quote

** Desktop
\\

*General Syntax*

#+begin_quote
  desktop [/DESKTOP_SEL/] /COMMANDS/

  If /DESKTOP_SEL/ is omitted, *focused* is assumed.
#+end_quote

\\

*COMMANDS*

#+begin_quote
  *-f*, *--focus* [/DESKTOP_SEL/]

  #+begin_quote
    Focus the selected or given desktop.
  #+end_quote

  *-a*, *--activate* [/DESKTOP_SEL/]

  #+begin_quote
    Activate the selected or given desktop.
  #+end_quote

  *-m*, *--to-monitor* /MONITOR_SEL/ [*--follow*]

  #+begin_quote
    Send the selected desktop to the given monitor. If *--follow* is
    passed, the focused desktop will stay focused.
  #+end_quote

  *-s*, *--swap* /DESKTOP_SEL/ [*--follow*]

  #+begin_quote
    Swap the selected desktop with the given desktop. If *--follow* is
    passed, the focused desktop will stay focused.
  #+end_quote

  *-l*, *--layout* /CYCLE_DIR/|monocle|tiled

  #+begin_quote
    Set or cycle the layout of the selected desktop.
  #+end_quote

  *-n*, *--rename* <new_name>

  #+begin_quote
    Rename the selected desktop.
  #+end_quote

  *-b*, *--bubble* /CYCLE_DIR/

  #+begin_quote
    Bubble the selected desktop in the given direction.
  #+end_quote

  *-r*, *--remove*

  #+begin_quote
    Remove the selected desktop.
  #+end_quote
#+end_quote

** Monitor
\\

*General Syntax*

#+begin_quote
  monitor [/MONITOR_SEL/] /COMMANDS/

  If /MONITOR_SEL/ is omitted, *focused* is assumed.
#+end_quote

\\

*Commands*

#+begin_quote
  *-f*, *--focus* [/MONITOR_SEL/]

  #+begin_quote
    Focus the selected or given monitor.
  #+end_quote

  *-s*, *--swap* /MONITOR_SEL/

  #+begin_quote
    Swap the selected monitor with the given monitor.
  #+end_quote

  *-a*, *--add-desktops* <name>...

  #+begin_quote
    Create desktops with the given names in the selected monitor.
  #+end_quote

  *-o*, *--reorder-desktops* <name>...

  #+begin_quote
    Reorder the desktops of the selected monitor to match the given
    order.
  #+end_quote

  *-d*, *--reset-desktops* <name>...

  #+begin_quote
    Rename, add or remove desktops depending on whether the number of
    given names is equal, superior or inferior to the number of existing
    desktops.
  #+end_quote

  *-g*, *--rectangle* WxH+X+Y

  #+begin_quote
    Set the rectangle of the selected monitor.
  #+end_quote

  *-n*, *--rename* <new_name>

  #+begin_quote
    Rename the selected monitor.
  #+end_quote

  *-r*, *--remove*

  #+begin_quote
    Remove the selected monitor.
  #+end_quote
#+end_quote

** Query
\\

*General Syntax*

#+begin_quote
  query /COMMANDS/ [/OPTIONS/]
#+end_quote

\\

*Commands*

#+begin_quote
  The optional selectors are references.

  *-N*, *--nodes* [/NODE_SEL/]

  #+begin_quote
    List the IDs of the matching nodes.
  #+end_quote

  *-D*, *--desktops* [/DESKTOP_SEL/]

  #+begin_quote
    List the IDs (or names) of the matching desktops.
  #+end_quote

  *-M*, *--monitors* [/MONITOR_SEL/]

  #+begin_quote
    List the IDs (or names) of the matching monitors.
  #+end_quote

  *-T*, *--tree*

  #+begin_quote
    Print a JSON representation of the matching item.
  #+end_quote
#+end_quote

\\

*Options*

#+begin_quote
  *-m*,*--monitor* [/MONITOR_SEL/], *-d*,*--desktop* [/DESKTOP_SEL/],
  *-n*, *--node* [/NODE_SEL/]

  #+begin_quote
    Constrain matches to the selected monitor, desktop or node. The
    descriptor can be omitted for /-M/, /-D/ and /-N/.
  #+end_quote

  *--names*

  #+begin_quote
    Print names instead of IDs. Can only be used with /-M/ and /-D/.
  #+end_quote
#+end_quote

** Wm
\\

*General Syntax*

#+begin_quote
  wm /COMMANDS/
#+end_quote

\\

*Commands*

#+begin_quote
  *-d*, *--dump-state*

  #+begin_quote
    Dump the current world state on standard output.
  #+end_quote

  *-l*, *--load-state* <file_path>

  #+begin_quote
    Load a world state from the given file. The path must be absolute.
  #+end_quote

  *-a*, *--add-monitor* <name> WxH+X+Y

  #+begin_quote
    Add a monitor for the given name and rectangle.
  #+end_quote

  *-O*, *--reorder-monitors* <name>...

  #+begin_quote
    Reorder the list of monitors to match the given order.
  #+end_quote

  *-o*, *--adopt-orphans*

  #+begin_quote
    Manage all the unmanaged windows remaining from a previous session.
  #+end_quote

  *-h*, *--record-history* on|off

  #+begin_quote
    Enable or disable the recording of node focus history.
  #+end_quote

  *-g*, *--get-status*

  #+begin_quote
    Print the current status information.
  #+end_quote

  *-r*, *--restart*

  #+begin_quote
    Restart the window manager
  #+end_quote
#+end_quote

** Rule
\\

*General Syntax*

#+begin_quote
  rule /COMMANDS/
#+end_quote

\\

*Commands*

#+begin_quote
  *-a*, *--add* (<class_name>|*)[:(<instance_name>|*)[:(<name>|*)]]
  [*-o*|*--one-shot*]
  [monitor=MONITOR_SEL|desktop=DESKTOP_SEL|node=NODE_SEL] [state=STATE]
  [layer=LAYER] [split_dir=DIR] [split_ratio=RATIO]
  [(hidden|sticky|private|locked|marked|center|follow|manage|focus|border)=(on|off)]
  [rectangle=WxH+X+Y]

  #+begin_quote
    Create a new rule.
  #+end_quote

  *-r*, *--remove*
  ^<n>|head|tail|(<class_name>|*)[:(<instance_name>|*)[:(<name>|\*)]]...

  #+begin_quote
    Remove the given rules.
  #+end_quote

  *-l*, *--list*

  #+begin_quote
    List the rules.
  #+end_quote
#+end_quote

** Config
\\

*General Syntax*

#+begin_quote
  config [-m /MONITOR_SEL/|-d /DESKTOP_SEL/|-n /NODE_SEL/] <setting>
  [<value>]

  #+begin_quote
    Get or set the value of <setting>.
  #+end_quote
#+end_quote

** Subscribe
\\

*General Syntax*

#+begin_quote
  subscribe [/OPTIONS/] (all|report|monitor|desktop|node|...)*

  #+begin_quote
    Continuously print events. See the *EVENTS* section for the
    description of each event.
  #+end_quote
#+end_quote

\\

*Options*

#+begin_quote
  *-f*, *--fifo*

  #+begin_quote
    Print a path to a FIFO from which events can be read and return.
  #+end_quote

  *-c*, *--count* /COUNT/

  #+begin_quote
    Stop the corresponding *bspc* process after having received /COUNT/
    events.
  #+end_quote
#+end_quote

** Quit
\\

*General Syntax*

#+begin_quote
  quit [<status>]

  #+begin_quote
    Quit with an optional exit status.
  #+end_quote
#+end_quote

* EXIT CODES
If the server can't handle a message, *bspc* will return with a non-zero
exit code.

* SETTINGS
Colors are in the form /#RRGGBB/, booleans are /true/, /on/, /false/ or
/off/.

All the boolean settings are /false/ by default unless stated otherwise.

** Global Settings
/normal_border_color/

#+begin_quote
  Color of the border of an unfocused window.
#+end_quote

/active_border_color/

#+begin_quote
  Color of the border of a focused window of an unfocused monitor.
#+end_quote

/focused_border_color/

#+begin_quote
  Color of the border of a focused window of a focused monitor.
#+end_quote

/presel_feedback_color/

#+begin_quote
  Color of the *node --presel-{dir,ratio}* message feedback area.
#+end_quote

/split_ratio/

#+begin_quote
  Default split ratio.
#+end_quote

/status_prefix/

#+begin_quote
  Prefix prepended to each of the status lines.
#+end_quote

/external_rules_command/

#+begin_quote
  Absolute path to the command used to retrieve rule consequences. The
  command will receive the following arguments: window ID, class name,
  instance name, and intermediate consequences. The output of that
  command must have the following format: *key1=value1 key2=value2 ...*
  (the valid key/value pairs are given in the description of the /rule/
  command).
#+end_quote

/automatic_scheme/

#+begin_quote
  The insertion scheme used when the insertion point is in automatic
  mode. Accept the following values: *longest_side*, *alternate*,
  *spiral*.
#+end_quote

/initial_polarity/

#+begin_quote
  On which child should a new window be attached when adding a window on
  a single window tree in automatic mode. Accept the following values:
  *first_child*, *second_child*.
#+end_quote

/directional_focus_tightness/

#+begin_quote
  The tightness of the algorithm used to decide whether a window is on
  the /DIR/ side of another window. Accept the following values: *high*,
  *low*.
#+end_quote

/removal_adjustment/

#+begin_quote
  Adjust the brother when unlinking a node from the tree in accordance
  with the automatic insertion scheme.
#+end_quote

/presel_feedback/

#+begin_quote
  Draw the preselection feedback area. Defaults to /true/.
#+end_quote

/borderless_monocle/

#+begin_quote
  Remove borders of tiled windows for the *monocle* desktop layout.
#+end_quote

/gapless_monocle/

#+begin_quote
  Remove gaps of tiled windows for the *monocle* desktop layout.
#+end_quote

/top_monocle_padding/, /right_monocle_padding/,
/bottom_monocle_padding/, /left_monocle_padding/

#+begin_quote
  Padding space added at the sides of the screen for the *monocle*
  desktop layout.
#+end_quote

/single_monocle/

#+begin_quote
  Set the desktop layout to *monocle* if there's only one tiled window
  in the tree.
#+end_quote

/pointer_motion_interval/

#+begin_quote
  The minimum interval, in milliseconds, between two motion notify
  events.
#+end_quote

/pointer_modifier/

#+begin_quote
  Keyboard modifier used for moving or resizing windows. Accept the
  following values: *shift*, *control*, *lock*, *mod1*, *mod2*, *mod3*,
  *mod4*, *mod5*.
#+end_quote

/pointer_action1/, /pointer_action2/, /pointer_action3/

#+begin_quote
  Action performed when pressing /pointer_modifier/ + /button<n>/.
  Accept the following values: *move*, *resize_side*, *resize_corner*,
  *focus*, *none*.
#+end_quote

/click_to_focus/

#+begin_quote
  Button used for focusing a window (or a monitor). The possible values
  are: *button1*, *button2*, *button3*, *any*, *none*. Defaults to
  *button1*.
#+end_quote

/swallow_first_click/

#+begin_quote
  Don't replay the click that makes a window focused if /click_to_focus/
  isn't *none*.
#+end_quote

/focus_follows_pointer/

#+begin_quote
  Focus the window under the pointer.
#+end_quote

/pointer_follows_focus/

#+begin_quote
  When focusing a window, put the pointer at its center.
#+end_quote

/pointer_follows_monitor/

#+begin_quote
  When focusing a monitor, put the pointer at its center.
#+end_quote

/mapping_events_count/

#+begin_quote
  Handle the next *mapping_events_count* mapping notify events. A
  negative value implies that every event needs to be handled.
#+end_quote

/ignore_ewmh_focus/

#+begin_quote
  Ignore EWMH focus requests coming from applications.
#+end_quote

/ignore_ewmh_fullscreen/

#+begin_quote
  Block the fullscreen state transitions that originate from an EWMH
  request. The possible values are: *none*, *all*, or a comma separated
  list of the following values: *enter*, *exit*.
#+end_quote

/ignore_ewmh_struts/

#+begin_quote
  Ignore strut hinting from clients requesting to reserve space (i.e.
  task bars).
#+end_quote

/center_pseudo_tiled/

#+begin_quote
  Center pseudo tiled windows into their tiling rectangles. Defaults to
  /true/.
#+end_quote

/honor_size_hints/

#+begin_quote
  Apply ICCCM window size hints.
#+end_quote

/remove_disabled_monitors/

#+begin_quote
  Consider disabled monitors as disconnected.
#+end_quote

/remove_unplugged_monitors/

#+begin_quote
  Remove unplugged monitors.
#+end_quote

/merge_overlapping_monitors/

#+begin_quote
  Merge overlapping monitors (the bigger remains).
#+end_quote

** Monitor and Desktop Settings
/top_padding/, /right_padding/, /bottom_padding/, /left_padding/

#+begin_quote
  Padding space added at the sides of the monitor or desktop.
#+end_quote

** Desktop Settings
/window_gap/

#+begin_quote
  Size of the gap that separates windows.
#+end_quote

** Node Settings
/border_width/

#+begin_quote
  Window border width.
#+end_quote

* POINTER BINDINGS
/click_to_focus/

#+begin_quote
  Focus the window (or the monitor) under the pointer if the value isn't
  *none*.
#+end_quote

/pointer_modifier/ + /button1/

#+begin_quote
  Move the window under the pointer.
#+end_quote

/pointer_modifier/ + /button2/

#+begin_quote
  Resize the window under the pointer by dragging the nearest side.
#+end_quote

/pointer_modifier/ + /button3/

#+begin_quote
  Resize the window under the pointer by dragging the nearest corner.
#+end_quote

The behavior of /pointer_modifier/ + /button<n>/ can be modified through
the /pointer_action<n>/ setting.

* EVENTS
/report/

#+begin_quote
  See the next section for the description of the format.
#+end_quote

/monitor_add <monitor_id> <monitor_name> <monitor_geometry>/

#+begin_quote
  A monitor is added.
#+end_quote

/monitor_rename <monitor_id> <old_name> <new_name>/

#+begin_quote
  A monitor is renamed.
#+end_quote

/monitor_remove <monitor_id>/

#+begin_quote
  A monitor is removed.
#+end_quote

/monitor_swap <src_monitor_id> <dst_monitor_id>/

#+begin_quote
  A monitor is swapped.
#+end_quote

/monitor_focus <monitor_id>/

#+begin_quote
  A monitor is focused.
#+end_quote

/monitor_geometry <monitor_id> <monitor_geometry>/

#+begin_quote
  The geometry of a monitor changed.
#+end_quote

/desktop_add <monitor_id> <desktop_id> <desktop_name>/

#+begin_quote
  A desktop is added.
#+end_quote

/desktop_rename <monitor_id> <desktop_id> <old_name> <new_name>/

#+begin_quote
  A desktop is renamed.
#+end_quote

/desktop_remove <monitor_id> <desktop_id>/

#+begin_quote
  A desktop is removed.
#+end_quote

/desktop_swap <src_monitor_id> <src_desktop_id> <dst_monitor_id>
<dst_desktop_id>/

#+begin_quote
  A desktop is swapped.
#+end_quote

/desktop_transfer <src_monitor_id> <src_desktop_id> <dst_monitor_id>/

#+begin_quote
  A desktop is transferred.
#+end_quote

/desktop_focus <monitor_id> <desktop_id>/

#+begin_quote
  A desktop is focused.
#+end_quote

/desktop_activate <monitor_id> <desktop_id>/

#+begin_quote
  A desktop is activated.
#+end_quote

/desktop_layout <monitor_id> <desktop_id> tiled|monocle/

#+begin_quote
  The layout of a desktop changed.
#+end_quote

/node_add <monitor_id> <desktop_id> <ip_id> <node_id>/

#+begin_quote
  A node is added.
#+end_quote

/node_remove <monitor_id> <desktop_id> <node_id>/

#+begin_quote
  A node is removed.
#+end_quote

/node_swap <src_monitor_id> <src_desktop_id> <src_node_id>
<dst_monitor_id> <dst_desktop_id> <dst_node_id>/

#+begin_quote
  A node is swapped.
#+end_quote

/node_transfer <src_monitor_id> <src_desktop_id> <src_node_id>
<dst_monitor_id> <dst_desktop_id> <dst_node_id>/

#+begin_quote
  A node is transferred.
#+end_quote

/node_focus <monitor_id> <desktop_id> <node_id>/

#+begin_quote
  A node is focused.
#+end_quote

/node_activate <monitor_id> <desktop_id> <node_id>/

#+begin_quote
  A node is activated.
#+end_quote

/node_presel <monitor_id> <desktop_id> <node_id> (dir DIR|ratio
RATIO|cancel)/

#+begin_quote
  A node is preselected.
#+end_quote

/node_stack <node_id_1> below|above <node_id_2>/

#+begin_quote
  A node is stacked below or above another node.
#+end_quote

/node_geometry <monitor_id> <desktop_id> <node_id> <node_geometry>/

#+begin_quote
  The geometry of a window changed.
#+end_quote

/node_state <monitor_id> <desktop_id> <node_id>
tiled|pseudo_tiled|floating|fullscreen on|off/

#+begin_quote
  The state of a window changed.
#+end_quote

/node_flag <monitor_id> <desktop_id> <node_id>
hidden|sticky|private|locked|marked|urgent on|off/

#+begin_quote
  One of the flags of a node changed.
#+end_quote

/node_layer <monitor_id> <desktop_id> <node_id> below|normal|above/

#+begin_quote
  The layer of a window changed.
#+end_quote

/pointer_action <monitor_id> <desktop_id> <node_id>
move|resize_corner|resize_side begin|end/

#+begin_quote
  A pointer action occurred.
#+end_quote

Please note that *bspwm* initializes monitors before it reads messages
on its socket, therefore the initial monitor events can't be received.

* REPORT FORMAT
Each report event message is composed of items separated by colons.

Each item has the form /<type><value>/ where /<type>/ is the first
character of the item.

/M<monitor_name>/

#+begin_quote
  Focused monitor.
#+end_quote

/m<monitor_name>/

#+begin_quote
  Unfocused monitor.
#+end_quote

/O<desktop_name>/

#+begin_quote
  Occupied focused desktop.
#+end_quote

/o<desktop_name>/

#+begin_quote
  Occupied unfocused desktop.
#+end_quote

/F<desktop_name>/

#+begin_quote
  Free focused desktop.
#+end_quote

/f<desktop_name>/

#+begin_quote
  Free unfocused desktop.
#+end_quote

/U<desktop_name>/

#+begin_quote
  Urgent focused desktop.
#+end_quote

/u<desktop_name>/

#+begin_quote
  Urgent unfocused desktop.
#+end_quote

/L(T|M)/

#+begin_quote
  Layout of the focused desktop of a monitor.
#+end_quote

/T(T|P|F|=|@)/

#+begin_quote
  State of the focused node of a focused desktop.
#+end_quote

/G(S?P?L?M?)/

#+begin_quote
  Active flags of the focused node of a focused desktop.
#+end_quote

* ENVIRONMENT VARIABLES
/BSPWM_SOCKET/

#+begin_quote
  The path of the socket used for the communication between *bspc* and
  *bspwm*. If it isn't defined, then the following path is used:
  //tmp/bspwm<host_name>_<display_number>_<screen_number>-socket/.
#+end_quote

* CONTRIBUTORS

#+begin_quote
  ·

  Steven Allen <steven at stebalien.com>
#+end_quote

#+begin_quote
  ·

  Thomas Adam <thomas at xteddy.org>
#+end_quote

#+begin_quote
  ·

  Ivan Kanakarakis <ivan.kanak at gmail.com>
#+end_quote

* AUTHOR
Bastien Dejean <nihilhill at gmail.com>
