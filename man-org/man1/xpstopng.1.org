#+TITLE: Man1 - xpstopng.1
#+DESCRIPTION: Linux manpage for xpstopng.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
xpstopng - XPS to PNG converter

* SYNOPSIS
*xpstopng* [/OPTION/...] FILE [/OUTPUT FILE/]

* DESCRIPTION
*xpstopng* converts XPS documents to PNG format. *xpstopng* reads the
XPS file, /FILE/, and writes a PNG file per page with the page number
and file type appended to /OUTPUT FILE/. If /OUTPUT FILE/ is not
specified "page" will be used.

* OPTIONS
*-?*, *--help*

#+begin_quote
  Show help options.
#+end_quote

*-d* /DOCUMENT/, *--document*=/DOCUMENT/

#+begin_quote
  The document inside the XPS file to convert. By default, the first
  document of the XPS file is used.
#+end_quote

*-f* /PAGE/, *--first*=/PAGE/

#+begin_quote
  The first page to convert.
#+end_quote

*-l* /PAGE/, *--last*=/PAGE/

#+begin_quote
  The last page to convert.
#+end_quote

*-o*, *--odd*

#+begin_quote
  Convert only odd pages.
#+end_quote

*-e*, *--even*

#+begin_quote
  Convert only even pages.
#+end_quote

*-r* /RESOLUTION/, *--resolution*=/RESOLUTION/

#+begin_quote
  Horizontal and vertical resolution in PPI (Pixels Per Inch). The
  default is 150 PPI.
#+end_quote

*--rx*=/RESOLUTION/

#+begin_quote
  Horizontal resolution in PPI (Pixels Per Inch). The default is 150
  PPI.
#+end_quote

*--ry*=/RESOLUTION/

#+begin_quote
  Vertical resolution in PPI (Pixels Per Inch). The default is 150 PPI.
#+end_quote

*-x* /X/, *--crop-x*=/X/

#+begin_quote
  The x-coordinate of the crop area top left corner.
#+end_quote

*-y* /Y/, *--crop-y*=/Y/

#+begin_quote
  The y-coordinate of the crop area top left corner.
#+end_quote

*-w* /WIDTH/, *--crop-width*=/WIDTH/

#+begin_quote
  The width of crop area.
#+end_quote

*-h* /HEIGHT/, *--crop-height*=/HEIGHT/

#+begin_quote
  The height of crop area.
#+end_quote

*-t*, *--transparent-bg*

#+begin_quote
  Use a transparent background for pages instead of white.
#+end_quote

* BUGS
Please send bug reports to
*https://bugzilla.gnome.org/enter_bug.cgi?product=libgxps*.

* SEE ALSO
*xpstojpeg*(1) *xpstopdf*(1) *xpstops*(1) *xpstosvg*(1)
