#+TITLE: Man1 - notionflux.1
#+DESCRIPTION: Linux manpage for notionflux.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
is a tool to send scripts to the Lua scripting engine inside the

window manager. The script can thus access notion's internal Lua API.

There are two modes of operation, interactive and batch. Interactive
mode is enabled by running

with its standard input connected to a terminal and without passing any
commandline flags.

Otherwise, batch mode is enabled:

Force batch-mode even if stdin is connected to a terminal. Lua code is
read from stdin until the end-of-file is reached, is sent to notion for
processing, and the result is read back and displayed.

Almost the same as above, except instead of reading from stdin, the
command line parameter

is submitted for processing.

The content of the file is evaluated.

This mode is similar to running an interactive

shell. Tab completion for notion's Lua API is available, as well as
line-editing.

Code that has been entered is evaluated by notion as soon as it is a
complete Lua statement. The result is displayed after which the next
statement can be entered.

The

environment variable must be set to an X server running notion in order
for

to obtain the socket path.

connects to

through a Unix domain socket. This socket is created with read/write
permissions only for the user who started notion. The socket path can be
queried with the following command:

$ xprop -root _NOTION_MOD_NOTIONFLUX_SOCKET
_NOTION_MOD_NOTIONFLUX_SOCKET(STRING) = "/tmp/fileuj6onu"

The command

$ notionflux -e "return notioncore.current():name()" "emacs:
notionflux.1"

will display the title of the currently focused window, while

$ notionflux -e "print(notioncore.current():name())" "emacs:
notionflux.1" nil

will collect the messages and print them before returning. In order to
write to notion's standard output (usually

like print used to in previous versions of

please use

$ notionflux -e 'io.stdout:write("hello .xsession-errors!\n")' nil

Using input redirection:

$ echo "return notioncore.current()" | notionflux WClientWin: 0x9bb6b8

Using interactive mode:

$ notionflux lua> notioncore.current():rootwin_of() WRootWin: 0x8c9688
lua> do ...> local x = 42 -- Lua defaults to global scope, don't litter
...> return x ...> end 42 lua> ^D

In any response from mod_notionflux, strings are quoted in a way that
can be directly pasted back into a Lua interpreter to yield the same
string, while other types are turned into their string representation
using

This is why there are quotation marks around the response in the first
example where nil (which is the return value of a block which has no
return statement) does not have them.

An ionflux command appeared in ion3 around 2004. It was adapted to
notion by the notion development team when notion was forked from ion3
in 2010.

was rewritten in 2019 by

under the GNU GPL in order to add the interactive mode.

Currently, the script must be 4095 bytes or shorter. If you need more,
consider passing a filename, which is passed to notion for evaluation
and therefore circumvents this limit.
