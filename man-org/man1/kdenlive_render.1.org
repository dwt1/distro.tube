#+TITLE: Man1 - kdenlive_render.1
#+DESCRIPTION: Linux manpage for kdenlive_render.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kdenlive_render - Render program for Kdenlive.

* SYNOPSIS
kdenlive_render [-erase] [in=pos] [out=pos] [render] [profile]
[rendermodule] [player] [src] [dest] [[arg1] [arg2] ...]

* DESCRIPTION
Render program for Kdenlive. Renders kdenlive project detached from
Kdenlive, while providing feedback through kuiserver. This program is
not meant to be called directly by users, but is called from Kdenlive,
when a rendering process is started.

* OPTIONS
** 
** Options:
- *-erase* :: If present, src file will be erased after rendering

- *in=pos* :: Start rendering at frame pos

- *out=pos* :: End rendering at frame pos

- *render* :: Path to MLT melt render program

- *profile* :: The MLT video profile

- *rendermodule* :: The MLT consumer used for rendering, usually it is
  avformat

- *player* :: Path to video player to play when rendering is over, use
  '-' to disable playing

- *src* :: Source file - usually MLT playlist

- *dest* :: Destination file

- *args* :: Space separated libavformat arguments

** 
* ENVIRONMENT
- *KDENLIVE_RENDER_LOG* :: If $*KDENLIVE_RENDER_LOG* is set, a log will
  be written to /tmp/kdenlive_render.log.XXXXXXXX, that contains
  information about the result of calling melt, ffmpeg, etc. This can be
  used for debugging.

* SEE ALSO
Please see the homepage at *http://www.kdenlive.org/*\\

* AUTHORS
#+begin_example
  Jean-Baptiste Mardelle <jb@kdenlive.org>


  Marco Gittler <g.marco@freenet.de>


#+end_example
