#+TITLE: Man1 - pspell-config.1
#+DESCRIPTION: Linux manpage for pspell-config.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pspell-config - prints information about a libpspell installation

* SYNOPSIS
*pspell-config* /[options]/\\

* DESCRIPTION
*pspell-config* displays information about libpspell installation,
mostly for use in build scripts. Note that this script is provided for
backward compatibility with programs that use pspell. Do not use as it
will eventually go away.

* OPTIONS
- *--version* :: outputs version information about the installed pspell

- *--datadir* :: displays the data directory

- *--pkgdatadir* :: displays the aspell package directory

* AUTHOR
This manual page was written by Brian Nelson <pyro@debian.org>.
