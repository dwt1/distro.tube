#+TITLE: Man1 - llvm-link.1
#+DESCRIPTION: Linux manpage for llvm-link.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
llvm-link - LLVM bitcode linker

* SYNOPSIS
*llvm-link* [/options/] /filename .../

* DESCRIPTION
*llvm-link* takes several LLVM bitcode files and links them together
into a single LLVM bitcode file. It writes the output file to standard
output, unless the /-o/ option is used to specify a filename.

* OPTIONS

#+begin_quote
  - *-f* :: Enable binary output on terminals. Normally, *llvm-link*
    will refuse to write raw bitcode output if the output stream is a
    terminal. With this option, *llvm-link* will write raw bitcode
    regardless of the output device.
#+end_quote

#+begin_quote
  - *-o filename* :: Specify the output file name. If *filename* is
    "*-*", then *llvm-link* will write its output to standard output.
#+end_quote

#+begin_quote
  - *-S* :: Write output in LLVM intermediate language (instead of
    bitcode).
#+end_quote

#+begin_quote
  - *-d* :: If specified, *llvm-link* prints a human-readable version of
    the output bitcode file to standard error.
#+end_quote

#+begin_quote
  - *-help* :: Print a summary of command line options.
#+end_quote

#+begin_quote
  - *-v* :: Verbose mode. Print information about what *llvm-link* is
    doing. This typically includes a message for each bitcode file
    linked in and for each library found.
#+end_quote

* EXIT STATUS
If *llvm-link* succeeds, it will exit with 0. Otherwise, if an error
occurs, it will exit with a non-zero value.

* AUTHOR
Maintained by the LLVM Team (https://llvm.org/).

* COPYRIGHT
2003-2021, LLVM Project
