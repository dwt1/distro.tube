#+TITLE: Man1 - gig2mono.1
#+DESCRIPTION: Linux manpage for gig2mono.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gig2mono - Converts Gigasampler (.gig) files from stereo to mono.

* SYNOPSIS
*gig2mono* [ -v ] [ -r ] [ --left | --right | --mix ] FILE_OR_DIR1 [
FILE_OR_DIR2 ... ]

* DESCRIPTION
Takes a list of Gigasampler (.gig) files and / or directories as
argument(s) and converts the individual Gigasampler files from stereo to
mono audio format. Given directories are scanned for .gig files. The
Gigasampler files are directly modified, not copied. Gigasampler files
already being in mono format are ignored. Since at this point the
Gigasampler format only defines mono and stereo samples, this program
currently also assumes all samples in the .gig files provided to be
either mono or stereo.

* OPTIONS
- * FILE_OR_DIR1* :: Gigasampler (.gig) filename or directory

- * FILE_OR_DIR2* :: Gigasampler (.gig) filename or directory

- * -v* :: Print version and exit.

- * -r* :: Recurse through subdirectories.

- * --mix* :: Convert by mixing left & right audio channels together
  (default).

- * --left* :: Convert by using left audio channel data.

- * --right* :: Convert by using right audio channel data.

* SEE ALSO
*gig2stereo(1),* *gigextract(1),* *gigdump(1),* *gigmerge(1)*

* BUGS
Check and report bugs at http://bugs.linuxsampler.org

* Author
Application and manual page written by Christian Schoenebeck
<cuse@users.sf.net>
