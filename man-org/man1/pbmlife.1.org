#+TITLE: Man1 - pbmlife.1
#+DESCRIPTION: Linux manpage for pbmlife.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmlife - apply Conway's rules of Life to a PBM image

* SYNOPSIS
*pbmlife* [/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmlife* reads a PBM image as input, applies the rules of Life to it
for one generation, and produces a PBM image as output.

A white pixel in the image is interpreted as a live beastie, and a black
pixel as an empty space.

* SEE ALSO
*pbm*(5)

* AUTHOR
Copyright (C) 1988, 1991 by Jef Poskanzer.
