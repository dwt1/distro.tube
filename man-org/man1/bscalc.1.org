#+TITLE: Man1 - bscalc.1
#+DESCRIPTION: Linux manpage for bscalc.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
bscalc - manual page for bscalc 2.6

* DESCRIPTION
usage: bscalc [-h] [--version]

#+begin_quote
  [-u {B,KiB,MiB,GiB,TiB,PiB,EiB,KB,MB,GB,TB,PB,EB}] [-b] [-k] [-m] [-g]
  [-t] [-p] [-e] [--KB] [--MB] [--GB] [--TB] [--PB] [--EB] [-H]
  EXPRESSION_PART [EXPRESSION_PART ...]
#+end_quote

** positional arguments:

#+begin_quote
  EXPRESSION_PART
#+end_quote

** optional arguments:
- *-h*, *--help* :: show this help message and exit

- *--version* :: show program's version number and exit

- *-u* {B,KiB,MiB,GiB,TiB,PiB,EiB,KB,MB,GB,TB,PB,EB}, *--unit*
  {B,KiB,MiB,GiB,TiB,PiB,EiB,KB,MB,GB,TB,PB,EB} :: Unit to show the
  result in

- *-b*, *-B* :: Show result in bytes

- *-k*, *-K*, *--KiB* :: Show result in KiB

- *-m*, *-M*, *--MiB* :: Show result in MiB

- *-g*, *-G*, *--GiB* :: Show result in GiB

- *-t*, *-T*, *--TiB* :: Show result in TiB

- *-p*, *-P*, *--PiB* :: Show result in PiB

- *-e*, *-E*, *--EiB* :: Show result in EiB

- *--KB* :: Show result in KB

- *--MB* :: Show result in MB

- *--GB* :: Show result in GB

- *--TB* :: Show result in TB

- *--PB* :: Show result in PB

- *--EB* :: Show result in EB

- *-H*, *--human-readable* :: Show only single 'best' result

Report issues at https://github.com/storaged-project/libbytesize/issues
