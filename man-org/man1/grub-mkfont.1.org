#+TITLE: Man1 - grub-mkfont.1
#+DESCRIPTION: Linux manpage for grub-mkfont.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
grub-mkfont - make GRUB font files

* SYNOPSIS
*grub-mkfont* [/OPTION/...] [/OPTIONS/] /FONT_FILES/

* DESCRIPTION
Convert common font file formats into PF2

- *-a*, *--force-autohint* :: force autohint

- *-b*, *--bold* :: convert to bold font

- *-c*, *--asce*=/NUM/ :: set font ascent

- *-d*, *--desc*=/NUM/ :: set font descent

- *-i*, *--index*=/NUM/ :: select face index

- *-n*, *--name*=/NAME/ :: set font family name

- *--no-bitmap* :: ignore bitmap strikes when loading

- *--no-hinting* :: disable hinting

- *-o*, *--output*=/FILE/ :: save output in FILE [required]

- *-r*, *--range*=/FROM-TO[/,FROM-TO] :: set font range

- *-s*, *--size*=/SIZE/ :: set font size

- *-v*, *--verbose* :: print verbose messages.

- -?, *--help* :: give this help list

- *--usage* :: give a short usage message

- *-V*, *--version* :: print program version

Mandatory or optional arguments to long options are also mandatory or
optional for any corresponding short options.

* REPORTING BUGS
Report bugs to <bug-grub@gnu.org>.

* SEE ALSO
*grub-mkconfig*(8)

The full documentation for *grub-mkfont* is maintained as a Texinfo
manual. If the *info* and *grub-mkfont* programs are properly installed
at your site, the command

#+begin_quote
  *info grub-mkfont*
#+end_quote

should give you access to the complete manual.
