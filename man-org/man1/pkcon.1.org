#+TITLE: Man1 - pkcon.1
#+DESCRIPTION: Linux manpage for pkcon.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pkcon - PackageKit console client

* SYNOPSIS
*pkcon* [*OPTION*...] [*COMMAND*]

* DESCRIPTION
This manual page documents briefly the *pkcon* command.

*pkcon* is the command line client for PackageKit.

* COMMANDS
pkcon knows about the following commands. Depending on the backend in
use, not all of them may be available.

backend-details

#+begin_quote
  Print information about the PackageKit backend in use.
#+end_quote

get-roles

#+begin_quote
  List the roles that a transaction can have.
#+end_quote

get-groups

#+begin_quote
  List the available package groups.
#+end_quote

get-filters

#+begin_quote
  List the available filters.
#+end_quote

get-transactions

#+begin_quote
  List known transactions.
#+end_quote

get-time /ROLE/

#+begin_quote
  Print the time that has passed since the last transaction with the
  given role.
#+end_quote

search [name|details|group|file] /DATA/

#+begin_quote
  Search for a package matching the given data. The search is performed
  in the package information that is indicated by the first argument:
  name, details, group or files.
#+end_quote

install /PACKAGES/

#+begin_quote
  Install the given packages from repositories.
#+end_quote

install-local /FILES/

#+begin_quote
  Install the given packages from the local filesystem.
#+end_quote

download /DIRECTORY/ /PACKAGES/

#+begin_quote
  Download the given packages from repositories, and store them in the
  given directory.
#+end_quote

install-sig /TYPE/ /KEY_ID/ /PACKAGE/

#+begin_quote
  Install a package signature. Only GPG signatures are supported at this
  time.
#+end_quote

remove /PACKAGE/

#+begin_quote
  Remove the given package.
#+end_quote

update [/PACKAGES/]

#+begin_quote
  Update the system by installing available updates. If a list of
  packages is specified, only install updates for these packages.
#+end_quote

refresh [force]

#+begin_quote
  Refresh the cached information about available updates.
#+end_quote

resolve /PACKAGE/

#+begin_quote
  Resolve the given package name and print information about installed
  or available packages and updates.
#+end_quote

upgrade-system /DISTRIBUTION/ [minimal|default|complete]

#+begin_quote
  Upgrade the system to the given distribution.
#+end_quote

get-updates

#+begin_quote
  List available updates.
#+end_quote

get-distro-upgrades

#+begin_quote
  List available distribution upgrades.
#+end_quote

depends-on /PACKAGE/

#+begin_quote
  List dependencies fo the given package.
#+end_quote

required-by /PACKAGE/

#+begin_quote
  List packages that require the given package.
#+end_quote

get-details /PACKAGE/

#+begin_quote
  Print details about the available or installed package with the given
  name.
#+end_quote

get-details-local /FILE/

#+begin_quote
  Print details about the local package.
#+end_quote

get-files /PACKAGE/

#+begin_quote
  List the files contained in the given package.
#+end_quote

get-files-local /FILE/

#+begin_quote
  List the files contained in the local package.
#+end_quote

get-update-detail /PACKAGE/

#+begin_quote
  Print the package changelog for the given package.
#+end_quote

get-packages

#+begin_quote
  List all available and installed packages.
#+end_quote

repo-list

#+begin_quote
  List all configured package repositories.
#+end_quote

repo-enable /REPOSITORY/

#+begin_quote
  Enable the given repository.
#+end_quote

repo-disable /REPOSITORY/

#+begin_quote
  Disable the given repository.
#+end_quote

repo-set-data /REPOSITORY/ /PARAMETER/ /DATA/

#+begin_quote
  Set the given parameter to the given value for the repository.
#+end_quote

repo-remove /REPOSITORY/ /PARAMETER/

#+begin_quote
  Removes the repository and optionally any packages installed from it.
#+end_quote

what-provides /STRING/

#+begin_quote
  List packages that provide the given string.
#+end_quote

accept-eula /EULA_ID/

#+begin_quote
  Accept the EULA with the given id.
#+end_quote

get-categories

#+begin_quote
  List available categories.
#+end_quote

repair

#+begin_quote
  Attempt to repair the system package database.
#+end_quote

offline-get-prepared

#+begin_quote
  Print information about the prepared offline update. If no offline
  update is prepared, exit with an exit code of 1.
#+end_quote

offline-trigger

#+begin_quote
  Trigger an offline update.
#+end_quote

offline-status

#+begin_quote
  Print information about the result of the last offline update.
#+end_quote

* OPTIONS
The following options can be used to influence the behavior of *pkcon*.

--version

#+begin_quote
  Print the program version and exit.
#+end_quote

-h, --help

#+begin_quote
  Show help options.
#+end_quote

--help-all

#+begin_quote
  Show all help options.
#+end_quote

--filter /FILTER/

#+begin_quote
  Set the filter to use.
#+end_quote

-y, --noninteractive

#+begin_quote
  Install packages without asking for confirmation.
#+end_quote

--only-download

#+begin_quote
  Prepare the transaction by downloading packages only.
#+end_quote

-n, --background

#+begin_quote
  Run the command using idle network bandwidth and also using less
  power.
#+end_quote

-p, --plain

#+begin_quote
  Print to screen a machine-readable output, rather than using animated
  widgets.
#+end_quote

-v, --verbose

#+begin_quote
  Show debugging information.
#+end_quote

-c, --cache-age /AGE/

#+begin_quote
  Set the maximum acceptable age for cached metadata, in seconds. Use -1
  for never.
#+end_quote

--allow-untrusted

#+begin_quote
  Allow untrusted packages to be installed.
#+end_quote

--allow-downgrade

#+begin_quote
  Allow packages to be downgraded during transaction.
#+end_quote

--allow-reinstall

#+begin_quote
  Allow packages to be reinstalled during transaction.
#+end_quote

* RETURN VALUES
*0*

#+begin_quote
  Success
#+end_quote

*1*

#+begin_quote
  Failed with miscellaneous internal error.
#+end_quote

*3*

#+begin_quote
  Failed with syntax error, or failed to parse command.
#+end_quote

*4*

#+begin_quote
  Failed as a file or directory was not found.
#+end_quote

*5*

#+begin_quote
  Nothing useful was done.
#+end_quote

*6*

#+begin_quote
  The initial setup failed, e.g. setting the network proxy.
#+end_quote

*7*

#+begin_quote
  The transaction failed, see the detailed error for more information.
#+end_quote

* SEE ALSO
pkmon (1).

The programs are documented fully on http://www.packagekit.org.

* AUTHOR
This manual page was written by Richard Hughes <richard@hughsie.com>.

* AUTHOR
*Richard Hughes*

#+begin_quote
  Author.
#+end_quote

* COPYRIGHT
\\
Copyright © 2007 - 2013 Richard Hughes\\
