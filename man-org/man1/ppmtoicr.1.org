#+TITLE: Man1 - ppmtoicr.1
#+DESCRIPTION: Linux manpage for ppmtoicr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
ppmtoicr - convert a PPM image into NCSA ICR format

* SYNOPSIS
*ppmtoicr*

[*-windowname* /name/]

[*-expand* /expand/]

[*-display* /display/]

[*-rle*]

[/ppmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*ppmtoicr* reads a PPM file as input. Produces an NCSA Telnet
Interactive Color Raster graphic file as output.

If /ppmfile/ is not supplied, *ppmtoicr* reads from Standard Input.

Interactive Color Raster (ICR) is a protocol for displaying raster
graphics on workstation screens. The protocol is implemented in NCSA
Telnet for the Macintosh version 2.3. The ICR protocol shares
characteristics of the Tektronix graphics terminal emulation protocol.
For example, escape sequences are used to control the display.

*ppmtoicr* will output the appropriate sequences to create a window of
the dimensions of the input image, create a colormap of up to 256 colors
on the display, then load the picture data into the window.

Note that there is no icrtoppm tool - this transformation is one way.

* OPTIONS
- *-windowname* /name/ :: Output will be displayed in /name/ (Default is
  to use /ppmfile/ or 'untitled' if the input is from Standard Input).

- *-expand* /expand/ :: Output will be expanded on display by factor
  /expand/ (For example, a value of 2 will cause four pixels to be
  displayed for every input pixel.)

- *-display* /display/ :: Output will be displayed on screen numbered
  /display/

- *-rle* :: Use run-length encoded format for display. (This will nearly
  always result in a quicker display, but may skew the colormap).

* EXAMPLES
To display a PPM file named *ppmfile* using the protocol:

#+begin_example
      ppmtoicr ppmfile
#+end_example

This will create a window named /ppmfile/ on the display with the
correct dimensions for /ppmfile/, create and download a colormap of up
to 256 colors, and download the picture into the window. You may achieve
the same effect with the following sequence:

#+begin_example
      ppmtoicr ppmfile > filename
      cat filename
#+end_example

To display a GIF file using the protocol in a window titled after the
input file, zoom the displayed image by a factor of 2, and run-length
encode the data:

#+begin_example
      giftopnm giffile | ppmtoicr -w giffile -r -e 2
#+end_example

* LIMITATIONS
The protocol uses frequent fflush() calls to speed up display. If you
save the output to a file for later display via *cat*, *ppmtoicr* will
draw much more slowly. In either case, increasing the blocksize limit on
the display will speed up transmission substantially.

* SEE ALSO
*ppm*(5)

NCSA Telnet for the Macintosh, University of Illinois at
Urbana-Champaign (1989)

* AUTHOR
Copyright (C) 1990 by Kanthan Pillay (/svpillay@Princeton.EDU/),
Princeton University Computing and Information Technology.
