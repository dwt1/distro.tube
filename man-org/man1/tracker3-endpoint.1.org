#+TITLE: Man1 - tracker3-endpoint.1
#+DESCRIPTION: Linux manpage for tracker3-endpoint.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
tracker3-endpoint - Create a SPARQL endpoint

* SYNOPSIS
#+begin_example
  tracker3 endpoint [--dbus-service | -b] <service_name>
                    [--database-path | -d] <database_path>
                    [[--ontology | -o] <ontology_name> |
                     [--ontology-path | -p] <ontology_path>]
                    [--http-port] <port>
                    [--loopback]
                    [[--system | --session]]
#+end_example

* DESCRIPTION
This command allows creating SPARQL endpoints. The endpoint will be able
to handle SPARQL select and update queries, and notify about changes in
it.

The endpoint is exported via DBus, accessible through the given
/service_name/, either using it in a *SERVICE* clause, or by creating a
dedicated bus-based SPARQL connection.

When creating a database, the /ontology_name/ (or alternatively, a
/ontology_path/) must be provided in order to generate the database. If
/ontology_name/ is used, the ontology must exist in
/$datadir/tracker/ontologies/

The database itself will be stored according to /database_path/.

* OPTIONS
*-b, --dbus-service=<*/service_name/*>*

#+begin_quote
  Service name to use on the endpoint.
#+end_quote

*-d, --database-path=<*/database_path/*>*

#+begin_quote
  The path where the database will be stored.
#+end_quote

*-o, --ontology*

#+begin_quote
  The name of an ontology in /$datadir/tracker/ontologies/ to use on the
  constructed database.
#+end_quote

*-p, --ontology-path*

#+begin_quote
  Full path to an ontology to use on the constructed database.
#+end_quote

*--session*

#+begin_quote
  Use the session bus. This is the default.
#+end_quote

*--system*

#+begin_quote
  Use the system bus.
#+end_quote

*-l, --list*

#+begin_quote
  List all SPARQL endpoints available in DBus
#+end_quote

*--http-port*

#+begin_quote
  Creates a HTTP endpoint that listens in the specified port
#+end_quote

*--loopback*

#+begin_quote
  Allows only HTTP connections in the loopback device. Only effective
  with HTTP endpoints.
#+end_quote

* EXAMPLES
Export a Nepomuk endpoint with the /org.example.Example1/ bus name.

#+begin_quote
  #+begin_example
    $ tracker3 endpoint -b org.example.Example1 -o nepomuk -d /tmp/example1
  #+end_example
#+end_quote

Access this endpoint with the *tracker3-sparql(1)* subcommand.

#+begin_quote
  #+begin_example
    $ tracker3 sparql --dbus-service org.example.Example1 -q "
      SELECT ?s ?o
      WHERE {
        ?u a ?o
      }"
  #+end_example
#+end_quote

Export a Nepomuk endpoint via HTTP.

#+begin_quote
  #+begin_example
    $ tracker3 endpoint --http-port 8080 -o nepomuk --loopback
  #+end_example
#+end_quote

Access this endpoint via HTTP.

#+begin_quote
  #+begin_example
    $ tracker3 sparql --remote-service http://127.0.0.1:8080/sparql -q "
      SELECT ?u {
        ?u a rdfs:Resource
      }"
  #+end_example
#+end_quote

* SEE ALSO
*tracker3-sparql*(1),

https://www.w3.org/TR/sparql11-query/
