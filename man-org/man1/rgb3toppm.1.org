#+TITLE: Man1 - rgb3toppm.1
#+DESCRIPTION: Linux manpage for rgb3toppm.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
rgb3toppm - combine three PGM images (R, G, B) into one PPM image

* SYNOPSIS
*rgb3toppm* /redpgmfile/ /greenpgmfile/ /bluepgmfile/

* DESCRIPTION
This program is part of *Netpbm*(1)

*rgb3toppm* reads three PGM images as input, taking each to represent
one component (red, green, and blue) of a color image. It produces a PPM
image as output.

* SEE ALSO
*ppmtorgb3*(1) , *pamstack*(1) , *pgmtoppm*(1) , *ppmtopgm*(1) ,
*ppm*(5) , *pgm*(5)

* AUTHOR
Copyright (C) 1991 by Jef Poskanzer.
