#+TITLE: Man1 - gtk4-broadwayd.1
#+DESCRIPTION: Linux manpage for gtk4-broadwayd.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gtk4-broadwayd - Broadway display server

* SYNOPSIS
*gtk4-broadwayd* [--port /PORT/] [--address /ADDRESS/] [--unixsocket
/ADDRESS/] [/:DISPLAY/]

* DESCRIPTION
*gtk4-broadwayd* is a display server for the Broadway GDK backend. It
allows multiple GTK applications to display their windows in the same
web browser, by connecting to gtk4-broadwayd.

When using gtk4-broadwayd, specify the display number to use, prefixed
with a colon, similar to X. The default display number is 0.

#+begin_quote
  #+begin_example
    gtk4-broadwayd :5
  #+end_example
#+end_quote

Then point your web browser at http://127.0.0.1:8085. Start your
applications like this:

#+begin_quote
  #+begin_example
    GDK_BACKEND=broadway BROADWAY_DISPLAY=:5 gtk4-demo
  #+end_example
#+end_quote

* OPTIONS
--port

#+begin_quote
  Use /PORT/ as the HTTP port, instead of the default 8080 +
  (/DISPLAY/ - 1).
#+end_quote

--address

#+begin_quote
  Use /ADDRESS/ as the HTTP address, instead of the default
  http://127.0.0.1:/PORT/.
#+end_quote

--unixsocket

#+begin_quote
  Use /ADDRESS/ as the unix domain socket address. This option overrides
  --address and --port. It is available only on Unix-like systems.
#+end_quote
