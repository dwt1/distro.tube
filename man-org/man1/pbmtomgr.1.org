#+TITLE: Man1 - pbmtomgr.1
#+DESCRIPTION: Linux manpage for pbmtomgr.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
.

* NAME
pbmtomgr - convert a PBM image into a MGR bitmap

* SYNOPSIS
*pbmtomgr*

[/pbmfile/]

* DESCRIPTION
This program is part of *Netpbm*(1)

*pbmtomgr* reads a PBM image as input and produces a MGR bitmap as
output.

*MGR*(1) is a window manager that is a smaller alternative to the X
Windows System.

* SEE ALSO
*mgrtopbm*(1) , *pbm*(5)

* AUTHOR
Copyright (C) 1989 by Jef Poskanzer.
