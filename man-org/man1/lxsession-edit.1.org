#+TITLE: Man1 - lxsession-edit.1
#+DESCRIPTION: Linux manpage for lxsession-edit.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
lxsession-edit - a simple session editor GUI for LXDE which follows
autostart specs from FreeDesktop.Org

* SYNOPSIS
*lxsession-edit* [ DE ]

* DESCRIPTION
*lxsession-edit* is an application to change list of applications which
will be ran on session start. Session name may be retrieved from command
line, from environment variables /XDG_CURRENT_DESKTOP/ or
/DESKTOP_SESSION/, otherwise "*LXDE*" is assumed.

* OPTIONS
- *DE* :: provide desktop session name to edit autostart list

* ENVIRONMENT
- /XDG_CURRENT_DESKTOP/ :: default variable to get session name

- /DESKTOP_SESSION/ :: fallback variable to get session name

* AUTHORS
This manual page is written by Andriy Grytsenko <andrej@rep.kiev.ua>.
