#+TITLE: Man1 - gettextize.1
#+DESCRIPTION: Linux manpage for gettextize.1
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gettextize - install or upgrade gettext infrastructure

* SYNOPSIS
*gettextize* [/OPTION/]... [/package-dir/]

* DESCRIPTION
Prepares a source package to use gettext.

* OPTIONS
- *--help* :: print this help and exit

- *--version* :: print version information and exit

- *-f*, *--force* :: force writing of new files even if old exist

- *--po-dir*=/DIR/ :: specify directory with PO files

- *--no-changelog* :: don't update or create ChangeLog files

- *--symlink* :: make symbolic links instead of copying files

- *-n*, *--dry-run* :: print modifications but don't perform them

* AUTHOR
Written by Ulrich Drepper

* REPORTING BUGS
Report bugs in the bug tracker at
<https://savannah.gnu.org/projects/gettext> or by email to
<bug-gettext@gnu.org>.

* COPYRIGHT
Copyright © 1995-2020 Free Software Foundation, Inc. License GPLv3+: GNU
GPL version 3 or later <https://gnu.org/licenses/gpl.html>\\
This is free software: you are free to change and redistribute it. There
is NO WARRANTY, to the extent permitted by law.

* SEE ALSO
The full documentation for *gettextize* is maintained as a Texinfo
manual. If the *info* and *gettextize* programs are properly installed
at your site, the command

#+begin_quote
  *info gettextize*
#+end_quote

should give you access to the complete manual.
