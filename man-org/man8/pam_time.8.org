#+TITLE: Manpages - pam_time.8
#+DESCRIPTION: Linux manpage for pam_time.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_time - PAM module for time control access

* SYNOPSIS
*pam_time.so* [conffile=conf-file] [debug] [noaudit]

* DESCRIPTION
The pam_time PAM module does not authenticate the user, but instead it
restricts access to a system and or specific applications at various
times of the day and on specific days or over various terminal lines.
This module can be configured to deny access to (individual) users based
on their name, the time of day, the day of week, the service they are
applying for and their terminal from which they are making their
request.

By default rules for time/port access are taken from config file
/etc/security/time.conf. An alternative file can be specified with the
/conffile/ option.

If Linux PAM is compiled with audit support the module will report when
it denies access.

* OPTIONS
*conffile=/path/to/time.conf*

#+begin_quote
  Indicate an alternative time.conf style configuration file to override
  the default.
#+end_quote

*debug*

#+begin_quote
  Some debug information is printed with *syslog*(3).
#+end_quote

*noaudit*

#+begin_quote
  Do not report logins at disallowed time to the audit subsystem.
#+end_quote

* MODULE TYPES PROVIDED
Only the *account* type is provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  Access was granted.
#+end_quote

PAM_ABORT

#+begin_quote
  Not all relevant data could be gotten.
#+end_quote

PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_PERM_DENIED

#+begin_quote
  Access was not granted.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  The user is not known to the system.
#+end_quote

* FILES
/etc/security/time.conf

#+begin_quote
  Default configuration file
#+end_quote

* EXAMPLES

#+begin_quote
  #+begin_example
    #%PAM-1.0
    #
    # apply pam_time accounting to login requests
    #
    login  account  required  pam_time.so
          
  #+end_example
#+end_quote

* SEE ALSO
*time.conf*(5), *pam.d*(5), *pam*(8).

* AUTHOR
pam_time was written by Andrew G. Morgan <morgan@kernel.org>.
