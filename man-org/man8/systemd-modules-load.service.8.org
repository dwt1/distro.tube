#+TITLE: Manpages - systemd-modules-load.service.8
#+DESCRIPTION: Linux manpage for systemd-modules-load.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-modules-load.service, systemd-modules-load - Load kernel modules
at boot

* SYNOPSIS
systemd-modules-load.service

/usr/lib/systemd/systemd-modules-load

* DESCRIPTION
systemd-modules-load.service is an early boot service that loads kernel
modules. It reads static configuration from files in /usr/ and /etc/,
but also runtime configuration from /run/ and the kernel command line
(see below).

See *modules-load.d*(5) for information about the configuration format
of this service and paths where configuration files can be created.

* KERNEL COMMAND LINE
systemd-modules-load.service understands the following kernel command
line parameters:

/modules_load=/, /rd.modules_load=/

#+begin_quote
  Takes a comma-separated list of kernel modules to statically load
  during early boot. The option prefixed with "rd." is read by the
  initial RAM disk only.
#+end_quote

* SEE ALSO
*systemd*(1), *modules-load.d*(5),
