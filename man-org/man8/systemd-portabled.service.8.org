#+TITLE: Manpages - systemd-portabled.service.8
#+DESCRIPTION: Linux manpage for systemd-portabled.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-portabled.service, systemd-portabled - Portable service manager

* SYNOPSIS
systemd-portabled.service

/usr/lib/systemd/systemd-portabled

* DESCRIPTION
*systemd-portabled* is a system service that may be used to attach,
detach and inspect portable service images.

Most of *systemd-portabled*s functionality is accessible through the
*portablectl*(1) command.

See *Portable Services*[1] for details about the concepts this service
implements.

* SEE ALSO
*systemd*(1), *portablectl*(1), *org.freedesktop.portable1*(5)

* NOTES
-  1. :: Portable Services

  https://systemd.io/PORTABLE_SERVICES
