#+TITLE: Manpages - idmapwb.8
#+DESCRIPTION: Linux manpage for idmapwb.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
idmapwb.so - winbind ID mapping plugin for cifs-utils

* DESCRIPTION
This plugin allows the utilities in cifs-utils to work in conjuction
with the winbind facility of Samba suite. It handles several functions
including mapping UID and GID to SIDs and vice versa.

Utilities are usually configured to use the correct plugin by creating a
symlink at //etc/cifs-utils/idmap-plugin/ that points to the correct
plugin that you wish to use.

This plugin requires that winbindd(8) be properly configured and
running.

* SEE ALSO
getcifsacl(1), setcifsacl(1), cifs.idmap(8), samba(7), smb.conf(5),
winbindd(8)

* AUTHOR
idmapwb.so was written by Jeff Layton </jlayton@samba.org/>

Information about idmapwb.8 is found in manpage for: [[../\- winbind ID mapping plugin for cifs-utils
was written by Jeff Layton <\fI\%jlayton@samba.org\fP>][\- winbind ID mapping plugin for cifs-utils
was written by Jeff Layton <\fI\%jlayton@samba.org\fP>]]