#+TITLE: Manpages - sg_sat_phy_event.8
#+DESCRIPTION: Linux manpage for sg_sat_phy_event.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sg_sat_phy_event - use ATA READ LOG EXT via a SAT pass-through to fetch
SATA phy event counters

* SYNOPSIS
*sg_sat_phy_event* [/--ck_cond/] [/--extend/] [/--help/] [/--hex/]
[/--ignore/] [/--len=/{16|12}] [/--raw/] [/--reset/] [/--verbose/]
[/--version/] /DEVICE/

* DESCRIPTION
This utility sends an ATA READ LOG EXT with the log page ("address") set
to 11h to /DEVICE/ and outputs the response. Log page 11h is defined in
the SATA 2.5 standard and contains phy event counters. Rather than send
this command directly to the /DEVICE/, are sent via a SCSI transport
which is assumed to contain a SCSI to ATA Translation (SAT) Layer
(SATL). The SATL may be in an operating system driver, in host bus
adapter firmware or in some external enclosure.

The SAT standard (SAT ANSI INCITS 431-2007, prior draft: sat-r09.pdf at
www.t10.org) defines two SCSI "ATA PASS-THROUGH" commands: one using a
16 byte "cdb" and the other with a 12 byte cdb. This utility defaults to
using the 16 byte cdb variant. SAT-2 is also a standard: SAT-2 ANSI
INCITS 465-2010 and the draft prior to that is sat2r09.pdf . The SAT-3
project has started and the most recent draft is sat3r01.pdf .

* OPTIONS
Arguments to long options are mandatory for short options as well.

- *-c*, *--ck_cond* :: sets the CK_COND bit in the ATA PASS-THROUGH SCSI
  cdb. The default setting is clear (i.e. 0). When set the SATL should
  yield a sense buffer containing a ATA Result descriptor irrespective
  of whether the command succeeded or failed. When clear the SATL should
  only yield a sense buffer containing a ATA Result descriptor if the
  command failed.

- *-e*, *--extend* :: sets the EXTEND bit in the ATA PASS-THROUGH SCSI
  cdb. The default setting is clear (i.e. 0). When set a 48 bit LBA
  command is sent to the device. This option has no effect when
  /--len=12/.

- *-h*, *--help* :: outputs the usage message summarizing command line
  options then exits. Ignores /DEVICE/ if given.

- *-H*, *--hex* :: outputs the ATA READ LOG EXT response in hex. The
  default action (i.e. without any '-H' options) is to output the
  response in hex, grouped in 16 bit words (i.e. the ATA standard's
  preference). When given once, the response is output in ASCII hex
  bytes (i.e. the SCSI standard's preference). When given twice (i.e.
  '-HH') the output is in hex, grouped in 16 bit words, the same as the
  default but without a header.

- *-i*, *--ignore* :: usually the phy counter identifier names are
  decoded. When this option is given, the numeric value of the
  identifier is output, the vendor flag, the data length (in bytes) and
  the corresponding value.

- *-l*, *--len*={16|12} :: this is the length of the SCSI cdb used for
  the ATA PASS-THROUGH commands. The argument can either be 16 or 12.
  The default is 16. The larger cdb size is needed for 48 bit LBA
  addressing of ATA devices. On the other hand some SCSI transports
  cannot convey SCSI commands longer than 12 bytes.

- *-r*, *--raw* :: output the ATA READ LOG EXT response in binary. The
  output should be piped to a file or another utility when this option
  is used. The binary is sent to stdout, and errors are sent to stderr.

- *-R*, *--reset* :: reset the counters after the current values are
  returned, decoded and displayed.

- *-v*, *--verbose* :: increases the level or verbosity.

- *-V*, *--version* :: print out version string

* NOTES
The SCSI ATA PASS-THROUGH (12) command's opcode is 0xa1 and it clashes
with the MMC set's BLANK command used by cd/dvd writers. So a SATL in
front of an ATAPI device that uses MMC (i.e. has peripheral device
type 5) probably should treat opcode 0xa1 as a BLANK command and send it
through to the cd/dvd drive. The ATA PASS-THROUGH (16) command's opcode
(0x85) does not clash with anything so it is a better choice.

In the 2.4 series of Linux kernels the /DEVICE/ must be a SCSI generic
(sg) device. In the 2.6 series block devices (e.g. disks and ATAPI DVDs)
can also be specified. For example "sg_inq /dev/sda" will work in the
2.6 series kernels. From lk 2.6.6 other SCSI "char" device names may be
used as well (e.g. "/dev/st0m"). Prior to lk 2.6.29 USB mass storage
limited sense data to 18 bytes which made the *--ck_cond* option yield
strange (truncated) results.

* EXIT STATUS
The exit status of sg_sat_identify is 0 when it is successful. Otherwise
see the sg3_utils(8) man page.

* AUTHOR
Written by Douglas Gilbert

* REPORTING BUGS
Report bugs to <dgilbert at interlog dot com>.

* COPYRIGHT
Copyright © 2006-2020 Douglas Gilbert\\
This software is distributed under a FreeBSD license. There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.

* SEE ALSO
*sg_sat_identify,sg_sat_read_gplog(sg3_utils),*
*smp_rep_phy_err_log(smp_utils),sdparm(sdparm),hdparm(hdparm)*
