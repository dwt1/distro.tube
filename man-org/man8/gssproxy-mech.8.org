#+TITLE: Manpages - gssproxy-mech.8
#+DESCRIPTION: Linux manpage for gssproxy-mech.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
gssproxy-mech - GssProxy GSSAPI mechanism plugin

* SYNOPSIS
*proxymech_v1 2.16.840.1.113730.3.8.15.1
/usr/lib64/gssproxy/proxymech.so * [/options/]

* DESCRIPTION
The gssproxy proxymech module is a interposer plugin that is loaded by
GSSAPI. It is enabled by /etc/gss/mech configuration file.

The interposer plugin allows to intercept the entire GSSAPI
communication and detour to the *gssproxy* daemon. When the interposer
plugin is installed two other conditions need to be met in order to
activate it:

a) interposer configuration file

#+begin_quote
  The plugin needs to be manually enabled in the /etc/gss/mech file.
#+end_quote

b) gssproxy environment variable

#+begin_quote
  The interposer plugin will not forward to the gssproxy daemon unless
  the environment variable named /GSS_USE_PROXY=yes/ is set.
#+end_quote

Furthermore, the interposer plugin can be configured to behave in
different ways when called from the GSSAPI. This behavior is controlled
via the /GSSPROXY_BEHAVIOR/ environment variable. It accepts four
different values:

LOCAL_ONLY

#+begin_quote
  All commands received with this setting will cause to immediately
  reenter the GSSAPI w/o any interaction with the gssproxy daemon. When
  the request cannot be processed it will just fail.
#+end_quote

LOCAL_FIRST

#+begin_quote
  All commands received with this setting will cause to immediately
  reenter the GSSAPI. When the local GSSAPI cannot process the request,
  it will resend the request to the gssproxy daemon.
#+end_quote

REMOTE_FIRST

#+begin_quote
  All commands received with this setting will be forwarded to the
  gssproxy daemon first. If the request cannot be handled there, the
  request will reenter the local GSSAPI.
#+end_quote

REMOTE_ONLY

#+begin_quote
  This setting is currently not fully implemented and therefor not
  supported.
#+end_quote

The default setting for /GSSPROXY_BEHAVIOR/ is REMOTE_FIRST.

Finally the interposer may need to use a special per-service socket in
order to communicate with gssproxy. The path to this socket is set via
the /GSSPROXY_SOCKET/ environment variable.

* SEE ALSO
*gssproxy.conf*(5) and *gssproxy*(8).

* AUTHORS
*GSS-Proxy - http://fedorahosted.org/gss-proxy*

Information about gssproxy-mech.8 is found in manpage for: [[../2\&.16\&.840\&.1\&.113730\&.3\&.8\&.15\&.1 /usr/lib64/gssproxy/proxymech\&.so \fR [\fIoptions\fR]][2\&.16\&.840\&.1\&.113730\&.3\&.8\&.15\&.1 /usr/lib64/gssproxy/proxymech\&.so \fR [\fIoptions\fR]]]