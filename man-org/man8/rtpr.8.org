#+TITLE: Manpages - rtpr.8
#+DESCRIPTION: Linux manpage for rtpr.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
rtpr - replace backslashes with newlines.

* DESCRIPTION
*rtpr* is a trivial shell script which converts backslashes in standard
input to newlines. It's sole purpose is to be fed with input from *ip*
when executed with it's *--oneline* flag.

* EXAMPLES
- ip --oneline address show | rtpr :: Undo oneline converted
  *ip-address* output.

* SEE ALSO
*ip*(8)

* AUTHORS
Stephen Hemminger <shemming@brocade.com>
