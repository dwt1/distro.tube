#+TITLE: Manpages - pam_rootok.8
#+DESCRIPTION: Linux manpage for pam_rootok.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pam_rootok - Gain only root access

* SYNOPSIS
*pam_rootok.so* [debug]

* DESCRIPTION
pam_rootok is a PAM module that authenticates the user if their /UID/ is
/0/. Applications that are created setuid-root generally retain the
/UID/ of the user but run with the authority of an enhanced
effective-UID. It is the real /UID/ that is checked.

* OPTIONS
*debug*

#+begin_quote
  Print debug information.
#+end_quote

* MODULE TYPES PROVIDED
The *auth*, *account* and *password* module types are provided.

* RETURN VALUES
PAM_SUCCESS

#+begin_quote
  The /UID/ is /0/.
#+end_quote

PAM_AUTH_ERR

#+begin_quote
  The /UID/ is *not* /0/.
#+end_quote

* EXAMPLES
In the case of the *su*(1) application the historical usage is to permit
the superuser to adopt the identity of a lesser user without the use of
a password. To obtain this behavior with PAM the following pair of lines
are needed for the corresponding entry in the /etc/pam.d/su
configuration file:

#+begin_quote
  #+begin_example
    # su authentication. Root is granted access by default.
    auth  sufficient   pam_rootok.so
    auth  required     pam_unix.so
          
  #+end_example
#+end_quote

* SEE ALSO
*su*(1), *pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_rootok was written by Andrew G. Morgan, <morgan@kernel.org>.
