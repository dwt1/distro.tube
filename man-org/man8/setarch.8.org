#+TITLE: Manpages - setarch.8
#+DESCRIPTION: Linux manpage for setarch.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
setarch - change reported architecture in new program environment and/or
set personality flags

* SYNOPSIS
*setarch* [/arch/] [options] [/program/ [/argument/...]]

*setarch* *--list*|*-h*|*-V*

*arch* [options] [/program/ [/argument/...]]

* DESCRIPTION
*setarch* modifies execution domains and process personality flags.

The execution domains currently only affects the output of *uname -m*.
For example, on an AMD64 system, running *setarch i386* /program/ will
cause /program/ to see i686 instead of /x86_64/ as the machine type. It
can also be used to set various personality options. The default
/program/ is */bin/sh*.

Since version 2.33 the /arch/ command line argument is optional and
*setarch* may be used to change personality flags (ADDR_LIMIT_*,
SHORT_INODE, etc) without modification of the execution domain.

* OPTIONS
*--list*

#+begin_quote
  List the architectures that *setarch* knows about. Whether *setarch*
  can actually set each of these architectures depends on the running
  kernel.
#+end_quote

*--uname-2.6*

#+begin_quote
  Causes the /program/ to see a kernel version number beginning with
  2.6. Turns on *UNAME26*.
#+end_quote

*-v*, *--verbose*

#+begin_quote
  Be verbose.
#+end_quote

*-3*, *--3gb*

#+begin_quote
  Specifies /program/ should use a maximum of 3GB of address space.
  Supported on x86. Turns on *ADDR_LIMIT_3GB*.
#+end_quote

*--4gb*

#+begin_quote
  This option has no effect. It is retained for backward compatibility
  only, and may be removed in future releases.
#+end_quote

*-B*, *--32bit*

#+begin_quote
  Limit the address space to 32 bits to emulate hardware. Supported on
  ARM and Alpha. Turns on *ADDR_LIMIT_32BIT*.
#+end_quote

*-F*, *--fdpic-funcptrs*

#+begin_quote
  Treat user-space function pointers to signal handlers as pointers to
  address descriptors. This option has no effect on architectures that
  do not support *FDPIC* ELF binaries. In kernel v4.14 support is
  limited to ARM, Blackfin, Fujitsu FR-V, and SuperH CPU architectures.
#+end_quote

*-I*, *--short-inode*

#+begin_quote
  Obsolete bug emulation flag. Turns on *SHORT_INODE*.
#+end_quote

*-L*, *--addr-compat-layout*

#+begin_quote
  Provide legacy virtual address space layout. Use when the /program/
  binary does not have *PT_GNU_STACK* ELF header. Turns on
  *ADDR_COMPAT_LAYOUT*.
#+end_quote

*-R*, *--addr-no-randomize*

#+begin_quote
  Disables randomization of the virtual address space. Turns on
  *ADDR_NO_RANDOMIZE*.
#+end_quote

*-S*, *--whole-seconds*

#+begin_quote
  Obsolete bug emulation flag. Turns on *WHOLE_SECONDS*.
#+end_quote

*-T*, *--sticky-timeouts*

#+begin_quote
  This makes *select*(2), *pselect*(2), and *ppoll*(2) system calls
  preserve the timeout value instead of modifying it to reflect the
  amount of time not slept when interrupted by a signal handler. Use
  when /program/ depends on this behavior. For more details see the
  timeout description in *select*(2) manual page. Turns on
  *STICKY_TIMEOUTS*.
#+end_quote

*-X*, *--read-implies-exec*

#+begin_quote
  If this is set then *mmap*(3p) *PROT_READ* will also add the
  *PROT_EXEC* bit - as expected by legacy x86 binaries. Notice that the
  ELF loader will automatically set this bit when it encounters a legacy
  binary. Turns on *READ_IMPLIES_EXEC*.
#+end_quote

*-Z*, *--mmap-page-zero*

#+begin_quote
  SVr4 bug emulation that will set *mmap*(3p) page zero as read-only.
  Use when /program/ depends on this behavior, and the source code is
  not available to be fixed. Turns on *MMAP_PAGE_ZERO*.
#+end_quote

*-V*, *--version*

#+begin_quote
  Display version information and exit.
#+end_quote

*-h*, *--help*

#+begin_quote
  Display help text and exit.
#+end_quote

* EXAMPLE

#+begin_quote
  #+begin_example
    setarch --addr-no-randomize mytestprog
    setarch ppc32 rpmbuild --target=ppc --rebuild foo.src.rpm
    setarch ppc32 -v -vL3 rpmbuild --target=ppc --rebuild bar.src.rpm
    setarch ppc32 --32bit rpmbuild --target=ppc --rebuild foo.src.rpm
  #+end_example
#+end_quote

* AUTHORS
* SEE ALSO
*personality*(2), *select*(2)

* REPORTING BUGS
For bug reports, use the issue tracker at
<https://github.com/karelzak/util-linux/issues>.

* AVAILABILITY
The *setarch* command is part of the util-linux package which can be
downloaded from /Linux Kernel Archive/
<https://www.kernel.org/pub/linux/utils/util-linux/>.
