#+TITLE: Manpages - kcookiejar5.8
#+DESCRIPTION: Linux manpage for kcookiejar5.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
kcookiejar5 - Command line interface to the KDE HTTP cookie daemon

* SYNOPSIS
*kcookiejar5*

[-h, --help] [-v, --version] [--shutdown] [--remove/ domain/]
[--remove-all] [--reload-config]

* DESCRIPTION
*kcookiejar5* is a command line interface to the HTTP cookie store used
by KDE, a D-Bus service to store/retrieve/clean cookies.

* OPTIONS
*-h--help*

#+begin_quote
  Show help about options.
#+end_quote

*-v--version*

#+begin_quote
  Show version information.
#+end_quote

*--shutdown*

#+begin_quote
  Shut down cookie jar and the D-Bus service.
#+end_quote

*--remove* /domain/

#+begin_quote
  Removes cookies for /domain/ from the cookie jar.
#+end_quote

*--remove-all*

#+begin_quote
  Removes all the cookies from the cookie jar.
#+end_quote

*--reaload-config*

#+begin_quote
  Reloads the configuration file.
#+end_quote

* USAGE
*kcookiejar5* is a command line tool to access the kded module which
manages cookies in Konqueror and other KDE applications.

When started without parameters it loads the kded module to provide the
D-Bus interface to store cookies.

When *kcookiejar5* is started with some parameters, it does additional
tasks to the cookies jar it provides, like removing the cookies from one
/domain/.

* SEE ALSO
/kf5options/(7), /qt5options/(7)

* BUGS
Please use *KDEs bugtracker*[1] to report bugs.

* AUTHORS
*Waldo Bastian* <bastian@kde.org>\\

#+begin_quote
  Author.
#+end_quote

*Dawit Alemayehu* <adawit@kde.org>\\

#+begin_quote
  Author.
#+end_quote

* NOTES
-  1. :: KDE's bugtracker

  https://bugs.kde.org
