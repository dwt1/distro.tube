#+TITLE: Manpages - kadmin.local.8
#+DESCRIPTION: Linux manpage for kadmin.local.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about kadmin.local.8 is found in manpage for: [[../man1/kadmin.1][man1/kadmin.1]]