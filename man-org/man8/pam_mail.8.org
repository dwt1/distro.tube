#+TITLE: Manpages - pam_mail.8
#+DESCRIPTION: Linux manpage for pam_mail.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"
* NAME
pam_mail - Inform about available mail

* SYNOPSIS
*pam_mail.so* [close] [debug] [dir=/maildir/] [empty] [hash=/count/]
[noenv] [nopen] [quiet] [standard]

* DESCRIPTION
The pam_mail PAM module provides the "you have new mail" service to the
user. It can be plugged into any application that has credential or
session hooks. It gives a single message indicating the /newness/ of any
mail it finds in the users mail folder. This module also sets the PAM
environment variable, *MAIL*, to the users mail directory.

If the mail spool file (be it /var/mail/$USER or a pathname given with
the *dir=* parameter) is a directory then pam_mail assumes it is in the
/Maildir/ format.

* OPTIONS
*close*

#+begin_quote
  Indicate if the user has any mail also on logout.
#+end_quote

*debug*

#+begin_quote
  Print debug information.
#+end_quote

*dir=*/maildir/

#+begin_quote
  Look for the users mail in an alternative location defined by
  maildir/<login>. The default location for mail is /var/mail/<login>.
  Note, if the supplied maildir is prefixed by a ~, the directory is
  interpreted as indicating a file in the users home directory.
#+end_quote

*empty*

#+begin_quote
  Also print message if user has no mail.
#+end_quote

*hash=*/count/

#+begin_quote
  Mail directory hash depth. For example, a /hashcount/ of 2 would make
  the mail file be /var/spool/mail/u/s/user.
#+end_quote

*noenv*

#+begin_quote
  Do not set the *MAIL* environment variable.
#+end_quote

*nopen*

#+begin_quote
  Dont print any mail information on login. This flag is useful to get
  the *MAIL* environment variable set, but to not display any
  information about it.
#+end_quote

*quiet*

#+begin_quote
  Only report when there is new mail.
#+end_quote

*standard*

#+begin_quote
  Old style "You have..." format which doesnt show the mail spool being
  used. This also implies "empty".
#+end_quote

* MODULE TYPES PROVIDED
The *session* and *auth* (on establishment and deletion of credentials)
module types are provided.

* RETURN VALUES
PAM_BUF_ERR

#+begin_quote
  Memory buffer error.
#+end_quote

PAM_SERVICE_ERR

#+begin_quote
  Badly formed arguments.
#+end_quote

PAM_SUCCESS

#+begin_quote
  Success.
#+end_quote

PAM_USER_UNKNOWN

#+begin_quote
  User not known.
#+end_quote

* EXAMPLES
Add the following line to /etc/pam.d/login to indicate that the user has
new mail when they login to the system.

#+begin_quote
  #+begin_example
    session  optional  pam_mail.so standard
          
  #+end_example
#+end_quote

* SEE ALSO
*pam.conf*(5), *pam.d*(5), *pam*(8)

* AUTHOR
pam_mail was written by Andrew G. Morgan <morgan@kernel.org>.

Information about pam_mail.8 is found in manpage for: [[../ optional  pam_mail\&.so standard][ optional  pam_mail\&.so standard]]