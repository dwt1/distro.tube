#+TITLE: Manpages - genfstab.8
#+DESCRIPTION: Linux manpage for genfstab.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
genfstab - generate output suitable for addition to an fstab file

* SYNOPSIS
genfstab [options] root

* DESCRIPTION
genfstab helps fill in an fstab file by autodetecting all the current
mounts below a given mountpoint and printing them in fstab-compatible
format to standard output. It can be used to persist a manually mounted
filesystem hierarchy and is often used during the initial install and
configuration of an OS.

* OPTIONS
*-f* <filter>

#+begin_quote
  Restrict output to mountpoints matching the prefix /filter/.
#+end_quote

*-L*

#+begin_quote
  Use labels for source identifiers (shortcut for /-t LABEL/).
#+end_quote

*-p*

#+begin_quote
  Exclude pseudofs mounts (default behavior).
#+end_quote

*-P*

#+begin_quote
  Include pseudofs mounts.
#+end_quote

*-t* <tag>

#+begin_quote
  Use /tag/ for source identifiers (should be one of: /LABEL/, /UUID/,
  /PARTLABEL/, /PARTUUID/).
#+end_quote

*-U*

#+begin_quote
  Use UUIDs for source identifiers (shortcut for /-t UUID/).
#+end_quote

*-h*

#+begin_quote
  Output syntax and command line options.
#+end_quote

* SEE ALSO
*pacman*(8)

* BUGS
Bugs can be reported on the bug tracker /https://bugs.archlinux.org/ in
the Arch Linux category and title prefixed with [arch-install-scripts]
or via arch-projects@archlinux.org.

* AUTHORS
Maintainers:

#+begin_quote
  ·

  Dave Reisner <dreisner@archlinux.org>
#+end_quote

#+begin_quote
  ·

  Eli Schwartz <eschwartz@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
arch-install-scripts.git repository.
