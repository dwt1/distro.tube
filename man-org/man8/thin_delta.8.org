#+TITLE: Manpages - thin_delta.8
#+DESCRIPTION: Linux manpage for thin_delta.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*thin_delta *- Print the differences in the mappings between two thin
devices.

* SYNOPSIS
#+begin_example
  thin_delta [options] {device|file}
#+end_example

* DESCRIPTION
*thin_delta allows you to compare the mappings in two thin volumes
(snapshots* allow common blocks between thin volumes).

This tool cannot be run on live metadata unless the *--metadata-snap
option is* used.

* OPTIONS
- **--thin1, --snap1 {natural}** :: The numeric identifier for the first
  thin volume to diff.

- **--thin2, --snap2 {natural}** :: The numeric identifier for the
  second thin volume to diff.

- **--metadata-snap [block nr]** :: Use a metadata snapshot.

#+begin_example
      If you want to get information out of a live pool then you will need to
      take a metadata snapshot and use this switch.  In order for the information
      to be meaningful, you need to ensure the thin volumes you're examining are
      not changing (ie, do not activate those thins).
#+end_example

- **--verbose** :: Provide extra information on the mappings.

- **-h, --help** :: Print help and exit.

- **-V, --version** :: Output version information and exit.

* SEE ALSO
*thin_dump(8), thin_repair(8), thin_restore(8), thin_rmap(8),
thin_metadata_size(8)*

* AUTHOR
Joe Thornber <ejt@redhat.com>, Heinz Mauelshagen <heinzm@redhat.com>
