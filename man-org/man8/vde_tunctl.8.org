#+TITLE: Manpages - vde_tunctl.8
#+DESCRIPTION: Linux manpage for vde_tunctl.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
vde_tunctl --- create and manage persistent TUN/TAP interfaces

* SYNOPSIS
*vde_tunctl* [*-f* /tun-clone-device/] [*-u* /owner/] [*-t*
/device-name/]

*vde_tunctl* [*-f* /tun-clone-device/] *-d* /device-name/

* DESCRIPTION
*vde_tunctl* allows the host sysadmin to preconfigure a TUN/TAP device
for use by a particular user. That user may open and use the device, but
may not change any aspects of the host side of the interface.

vde_tunctl is a simple copy of *tunctl* done for practical purposes.

* USAGE
To create an interface for use by a particular user, invoke tunctl
without the -d option:

#+begin_example
  # vde_tunctl -u someuser  
  Set 'tap0' persistent and owned by uid 500 
   
#+end_example

Then, configure the interface as normal:

#+begin_example
  # ifconfig tap0 192.168.0.254 up  
  # route add -host 192.168.0.253 dev tap0  
  # bash -c 'echo 1 > /proc/sys/net/ipv4/conf/tap0/proxy_arp'  
  # arp -Ds 192.168.0.253 eth0 pub        
#+end_example

To delete the interface, use the -d option:

#+begin_example
  # vde_tunctl -d tap0  
  Set 'tap0' nonpersistent 
   
#+end_example

* SEE ALSO
*vde_switch*(1) *vde_plug2tap*(1)

* AUTHOR
tunctl was written by Jeff Dike jdike@karaya.com

This manual page is based on tunctl manual page written by Matt
Zimmerman mdz@debian.org for the *Debian GNU/Linux* system.
