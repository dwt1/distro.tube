#+TITLE: Manpages - systemd-logind.service.8
#+DESCRIPTION: Linux manpage for systemd-logind.service.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd-logind.service, systemd-logind - Login manager

* SYNOPSIS
systemd-logind.service

/usr/lib/systemd/systemd-logind

* DESCRIPTION
*systemd-logind* is a system service that manages user logins. It is
responsible for:

#+begin_quote
  ·

  Keeping track of users and sessions, their processes and their idle
  state. This is implemented by allocating a systemd slice unit for each
  user below user.slice, and a scope unit below it for each concurrent
  session of a user. Also, a per-user service manager is started as
  system service instance of user@.service for each logged in user.
#+end_quote

#+begin_quote
  ·

  Generating and managing session IDs. If auditing is available and an
  audit session ID is already set for a session, then this ID is reused
  as the session ID. Otherwise, an independent session counter is used.
#+end_quote

#+begin_quote
  ·

  Providing *polkit*[1]-based access for users for operations such as
  system shutdown or sleep
#+end_quote

#+begin_quote
  ·

  Implementing a shutdown/sleep inhibition logic for applications
#+end_quote

#+begin_quote
  ·

  Handling of power/sleep hardware keys
#+end_quote

#+begin_quote
  ·

  Multi-seat management
#+end_quote

#+begin_quote
  ·

  Session switch management
#+end_quote

#+begin_quote
  ·

  Device access management for users
#+end_quote

#+begin_quote
  ·

  Automatic spawning of text logins (gettys) on virtual console
  activation and user runtime directory management
#+end_quote

User sessions are registered with logind via the *pam_systemd*(8) PAM
module.

See *logind.conf*(5) for information about the configuration of this
service.

See *sd-login*(3) for information about the basic concepts of logind
such as users, sessions and seats.

See *org.freedesktop.login1*(5) and *org.freedesktop.LogControl1*(5) for
information about the D-Bus APIs systemd-logind provides.

For more information on the inhibition logic see the *Inhibitor Lock
Developer Documentation*[2].

If you are interested in writing a display manager that makes use of
logind, please have look at *Writing Display Managers*[3]. If you are
interested in writing a desktop environment that makes use of logind,
please have look at *Writing Desktop Environments*[4].

* SEE ALSO
*systemd*(1), *systemd-user-sessions.service*(8), *loginctl*(1),
*logind.conf*(5), *pam_systemd*(8), *sd-login*(3)

* NOTES
-  1. :: polkit

  http://www.freedesktop.org/wiki/Software/polkit

-  2. :: Inhibitor Lock Developer Documentation

  https://www.freedesktop.org/wiki/Software/systemd/inhibit

-  3. :: Writing Display Managers

  https://www.freedesktop.org/wiki/Software/systemd/writing-display-managers

-  4. :: Writing Desktop Environments

  http://www.freedesktop.org/wiki/Software/systemd/writing-desktop-environments
