#+TITLE: Manpages - zeppelin.8
#+DESCRIPTION: Linux manpage for zeppelin.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
zeppelin - ATM LAN Emulation client demon (LED) Zeppelin

* SYNOPSIS
*zeppelin* [*-b ]* [*-c /LECS_address/ | -s /LES_address/*] [*-e /esi/*]
[*-n /VLAN_name/*] [*-m /mesg_mask/*] [*-l /listen_address |/*
selector*]* [*-i /interface_number/*] [*-I /physical_interface_number/*]
[*-t /1516|1580|4544|9234|18190/*] [*-1 ]* [*-2 ]* [*-p ]* [*-F
/logfile/*] [*-f /Fore_specific_name/*]

* DESCRIPTION
A LAN Emulation Client is an entity in an ATM endstation that performs
data forwarding, address resolution and other control functions. It uses
the LUNI interface when communicating with other components in emulated
LANs. It provides upper protocol layers a MAC like interface similar to
IEEE 802.3/Ethernet or IEEE 802.5/Token Ring LAN.

LAN Emulation client code is divided into two parts: user space
application LAN Emulation Demon called (LED) *zeppelin(8)*, and the
kernel component. *Zeppelin* is responsible for control operations
needed in LAN Emulation clienthood. It forms the necessary VCCs and
receives all the LE control frames and acts accordingly. It also
controls the operation of the LEC kernel component.

Linux LEC supports only Ethernet type of emulated LAN.

*SIGHUP* causes restart of the LEC. All resources are released and
*zeppelin* is started.

* OPTIONS
- -b :: Run in background (i.e. in a forked child process) after
  initializing kernel interface.

- -c LECS_address :: ATM address of *lecs(8) (Lan Emulation
  Configuration Server), if not* set, Well-Known server address is used.

- -s LES_address :: ATM address of *les(8) (Lan Emulation Server), can
  be used in* bypassing configuration phase in joining emulated Lan i.e
  *lecs address* is not used.

- -e esi :: Mac address to use when communicating in Emulated LAN. E.g.
  00:20:22:23:04:05 .

- -n VLAN_name :: Name of the virtual LAN to which joining is requested.
  This is used in LE_CONFIGURE_REQUEST to LECS or LE_JOIN_RESPONSE to
  LES, if configuration phase is bypassed.

- -m mesg_mask :: Sometimes one wants to know more what is happening in
  LE daemon e.g. when nothing works. This is a hexadecimal long value
  setting global message mask. 0 = No messages, ffff = All messages.

- -l listen_address | selector :: Local ATM address that zeppelin uses
  as local binding point in signalling. Use this if you are running more
  than one client or a set of LE servers. The local ATM address can be
  specified by either giving the full ATM address or the desired
  selector byte.

- -i interface_number :: Linux LEC supports up to 40+8 network
  interfaces. The interface_number tells zeppelin to which of these to
  attach. Ethernet type network interfaces are numbered from "lec0" to
  "lec39" and Token Ring interfaces are from "lec40" to "lec47". These
  parameters are tweakable during the kernel compile, see
  <linux/atmlec.h>.

- -I physical_interface_number :: The physical interface this LANE
  client should bind to. If your host has multiple ATM network cards,
  you can use this option to choose which card this zeppelin will use.

- -t MTU :: The MTU of ELAN to join. You need to also use *ifconfig(8)
  to* set the MTU of the LANE interface.

- -1 :: Run as LANEv1 client. This is the default.

- -2 :: Run as LANEv2 client. This is required by MPOA.

- -p :: Enable proxy. When started with this option, it is possible to
  bridge packets between ATM and Ethernet. That is, you can use LANE
  interfaces with normal bridging. See the Bridging mini-Howto for more
  info.

- -F logfile :: Instead of writing debug messages to *stderr, write the
  messages* to the file *logfile. Use syslog as the file name to use*
  the *syslog(3) facility.*

- -f Fore specific name :: The LANE servers on Fore ATM switches can
  display a special name if a client can supply one. This name shows
  with command 'conf lane les show advanced'.

* TOKEN RING CONSIDERATIONS
A number of lec interfaces is reserved for Token Ring ELANs when the
kernel is compiled with Token Ring (CONFIG_TR) support. See the
discussion about interface_number command line option above. The Linux
Token Ring LANE client does not support bridging between legacy Token
Ring and ATM parts of the ELAN, so using the -p option is not
recommended. Token Ring support has received less testing than its
Ethernet counterpart.

* FILES
//var/run/lec[interface number].pid/ The file containing the process id
of zeppelin.

* BUGS
John Bonham died 1980 and Led Zeppelin broke.

Please report any other bugs to Heikki Vatiainen <hessu@cs.tut.fi>

* AUTHORS
Marko Kiiskila, TUT <carnil@cs.tut.fi> and Heikki Vatiainen, TUT
<hessu@cs.tut.fi>

* SEE ALSO
lecs(8), mpcd(8), atmsigd(8), les(8), qos(7)
