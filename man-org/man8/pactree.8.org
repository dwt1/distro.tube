#+TITLE: Manpages - pactree.8
#+DESCRIPTION: Linux manpage for pactree.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
pactree - package dependency tree viewer

* SYNOPSIS
/pactree/ [options] package

* DESCRIPTION
Pactree produces a dependency tree for a package.

By default, a tree-like output is generated, but with the /--graph/
option, a Graphviz description is generated.

* OPTIONS
*-a, --ascii*

#+begin_quote
  Use ASCII characters for tree formatting. By default, pactree will use
  Unicode line drawing characters if it is able to detect that the
  locale supports them.
#+end_quote

*-b, --dbpath*

#+begin_quote
  Specify an alternative database location.
#+end_quote

*-c, --color*

#+begin_quote
  Colorize output.
#+end_quote

*-d, --depth <num>*

#+begin_quote
  Limits the number of levels of dependency to show. A zero means show
  the named package only, one shows the packages that are directly
  required.
#+end_quote

*-g, --graph*

#+begin_quote
  Generate a Graphviz description. If this option is given, the
  /--color/ and /--linear/ options are ignored.
#+end_quote

*-h, --help*

#+begin_quote
  Output syntax and command-line options.
#+end_quote

*-l, --linear*

#+begin_quote
  Prints package names at the start of each line, one per line.
#+end_quote

*-r, --reverse*

#+begin_quote
  Show packages that depend on the named package.
#+end_quote

*-s, --sync*

#+begin_quote
  Read package data from sync databases instead of local database.
#+end_quote

*-u, --unique*

#+begin_quote
  List dependent packages once. Implies /--linear/.
#+end_quote

*-o, --optional[=DEPTH]*

#+begin_quote
  Additionally prints optional dependencies up to a certain depth,
  default 1 for immediate optional dependencies. When used in
  conjunction with /-r/ it shows which packages it is optional for. In
  Graphviz mode, produce dotted lines. Negative values mean infinite
  depth.
#+end_quote

*--config <file>*

#+begin_quote
  Specify an alternate pacman configuration file.
#+end_quote

*--debug*

#+begin_quote
  Print log messages produced by libalpm.
#+end_quote

*--gpgdir <dir>*

#+begin_quote
  Specify an alternate GnuPG directory for verifying database signatures
  (default is /etc/pacman.d/gnupg).
#+end_quote

* SEE ALSO
*pacman*(8), *pacman.conf*(5), *makepkg*(8)

* BUGS
Bugs? You must be kidding; there are no bugs in this software. But if we
happen to be wrong, send us an email with as much detail as possible to
pacman-contrib@lists.archlinux.org.

* AUTHORS
Current maintainers:

#+begin_quote
  ·

  Johannes Löthberg <johannes@kyriasis.com>
#+end_quote

#+begin_quote
  ·

  Daniel M. Capella <polyzen@archlinux.org>
#+end_quote

For additional contributors, use git shortlog -s on the
pacman-contrib.git repository.
