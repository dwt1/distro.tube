#+TITLE: Manpages - aa-mergeprof.8
#+DESCRIPTION: Linux manpage for aa-mergeprof.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
#+begin_example
#+end_example

\\

* NAME
aa-mergeprof - merge AppArmor security profiles.

* SYNOPSIS
*aa-mergeprof /file/ [/file/ ...] [/-d /path/to/profiles/]*

* OPTIONS
*file*

One or more files containing profiles to merge into the profile
directory (see -d).

*-d --dir /path/to/profiles*

Specifies the target directory for the merged AppArmor security profile
set. Defaults to /etc/apparmor.d.

* DESCRIPTION
*aa-mergeprof*

* BUGS
If you find any bugs, please report them at
<https://gitlab.com/apparmor/apparmor/-/issues>.

* SEE ALSO
/apparmor/ (7), /apparmor.d/ (5), /aa_change_hat/ (2), /aa-genprof/ (1),
/aa-logprof/ (1), /aa-enforce/ (1), /aa-audit/ (1), /aa-complain/ (1),
/aa-disable/ (1), and <https://wiki.apparmor.net>.
