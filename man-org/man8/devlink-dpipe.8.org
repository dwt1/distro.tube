#+TITLE: Manpages - devlink-dpipe.8
#+DESCRIPTION: Linux manpage for devlink-dpipe.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
devlink-dpipe - devlink dataplane pipeline visualization

* SYNOPSIS
*devlink* [ /OPTIONS/ ] *dpipe* { *table* | *header* } { /COMMAND/ |
*help* }

/OPTIONS/ := { *-V*[/ersion/] }

*devlink dpipe table show */DEV/ [ *name* /TABLE_NAME/ ]

*devlink dpipe table set */DEV/ *name*/ TABLE_NAME /

*devlink dpipe table dump */DEV/ *name*/ TABLE_NAME /

*devlink dpipe header show */DEV/

*devlink dpipe help*

* DESCRIPTION
** devlink dpipe table show - display devlink dpipe table attributes
- *name*/ TABLE_NAME/ :: Specifies the table to operate on.

** devlink dpipe table set - set devlink dpipe table attributes
- *name*/ TABLE_NAME/ :: Specifies the table to operate on.

** devlink dpipe table dump - dump devlink dpipe table entries
- *name*/ TABLE_NAME/ :: Specifies the table to operate on.

** devlink dpipe header show - display devlink dpipe header attributes
- *name*/ TABLE_NAME/ :: Specifies the table to operate on.

* EXAMPLES
devlink dpipe table show pci/0000:01:00.0

#+begin_quote
  Shows all dpipe tables on specified devlink device.
#+end_quote

devlink dpipe table show pci/0000:01:00.0 name mlxsw_erif

#+begin_quote
  Shows mlxsw_erif dpipe table on specified devlink device.
#+end_quote

devlink dpipe table set pci/0000:01:00.0 name mlxsw_erif
counters_enabled true

#+begin_quote
  Turns on the counters on mlxsw_erif table.
#+end_quote

devlink dpipe table dump pci/0000:01:00.0 name mlxsw_erif

#+begin_quote
  Dumps content of mlxsw_erif table.
#+end_quote

devlink dpipe header show pci/0000:01:00.0

#+begin_quote
  Shows all dpipe headers on specified devlink device.
#+end_quote

* SEE ALSO
*devlink*(8), *devlink-dev*(8), *devlink-monitor*(8),\\

* AUTHOR
Jiri Pirko <jiri@mellanox.com>
