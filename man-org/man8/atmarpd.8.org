#+TITLE: Manpages - atmarpd.8
#+DESCRIPTION: Linux manpage for atmarpd.8
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
atmarpd - ATMARP demon

* SYNOPSIS
*atmarpd* [*-b*] [*-d*] [*-D /directory/*] [*-l /logfile/*] [*-m*]
[*-n*]\\
*atmarpd* *-V*

* DESCRIPTION
*atmarpd* implements the ATMARP protocol as specified in RFC1577 and
RFC1755. Address resolution requests are sent from the kernel (using a
comparably simple protocol) to the ATMARP demon, which then performs the
dialog with the network.

*atmarpd* can operate as an ATMARP client and as an ATMARP server. If
also supports the concurrent use of several IP over ATM interfaces.

*atmarpd* is configured from the command line using the *atmarp*
program. Unless debugging is enabled, the ATMARP table is written after
every change to the file */var/run/atmarpd.table* (or to a file with the
same name in a different directory, if the *-D* option is used).

Note that *atmarpd* disables support for SVCs if signaling is not
available at start time, i.e. if *atmsigd* is not running.

* OPTIONS
- -b :: Run in background (i.e. in a forked child process) after
  initializing.

- -d :: Enables (lots of) debugging output. By default, *atmarpd* is
  comparably quiet.

- -D dump_dir :: Changes the directory where *atmarpd writes its table*
  (*atmarpd.table). By default, /var/run is used.*

- -l logfile :: Write diagnostic messages to the specified file instead
  of to standard error. The special name *syslog is used to send
  diagnostics to the* system logger.

- -m :: Enables merging of incoming calls if the address is known. An
  incoming connection on which no InARP reply has been received yet, but
  which originates from an ATM address for which an ATMARP entry already
  exists, is automatically added to that entry. This assumes that there
  is a 1:1 mapping between IP addresses and ATMARP addresses. By
  default, this assumption is not made, which frequently results in the
  setup of duplicate connections. Note that RFC1577 requires that an
  ATMARP server sends an InARP request on an incoming connection.
  Merging may therefore violate RFC1577 in this case.

- -n :: Prints addresses in numeric format only, i.e. no address to name
  translation is attempted.

- -V :: Prints the version number of *atmarpd on standard output and
  exits.*

* FILES
- */var/run/atmarpd.table* :: ATMARP table

- */proc/atm/arp* :: table of currently active IP over ATM VCs

* BUGS
*atmarpd removes ATMARP entries from the kernel table while refreshing*
them.

* AUTHOR
Werner Almesberger, EPFL ICA <werner.almesberger@epfl.ch>

* SEE ALSO
atmarp(8), atmsigd(8)
