#+TITLE: Manpages - utimes.2
#+DESCRIPTION: Linux manpage for utimes.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about utimes.2 is found in manpage for: [[../man2/utime.2][man2/utime.2]]