#+TITLE: Manpages - flistxattr.2
#+DESCRIPTION: Linux manpage for flistxattr.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about flistxattr.2 is found in manpage for: [[../man2/listxattr.2][man2/listxattr.2]]