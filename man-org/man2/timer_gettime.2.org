#+TITLE: Manpages - timer_gettime.2
#+DESCRIPTION: Linux manpage for timer_gettime.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about timer_gettime.2 is found in manpage for: [[../man2/timer_settime.2][man2/timer_settime.2]]