#+TITLE: Manpages - getresuid32.2
#+DESCRIPTION: Linux manpage for getresuid32.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about getresuid32.2 is found in manpage for: [[../man2/getresuid.2][man2/getresuid.2]]