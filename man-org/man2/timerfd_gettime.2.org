#+TITLE: Manpages - timerfd_gettime.2
#+DESCRIPTION: Linux manpage for timerfd_gettime.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about timerfd_gettime.2 is found in manpage for: [[../man2/timerfd_create.2][man2/timerfd_create.2]]