#+TITLE: Manpages - sysinfo.2
#+DESCRIPTION: Linux manpage for sysinfo.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sysinfo - return system information

* SYNOPSIS
#+begin_example
  #include <sys/sysinfo.h>

  int sysinfo(struct sysinfo *info);
#+end_example

* DESCRIPTION
*sysinfo*() returns certain statistics on memory and swap usage, as well
as the load average.

Until Linux 2.3.16, *sysinfo*() returned information in the following
structure:

#+begin_example
  struct sysinfo {
      long uptime;             /* Seconds since boot */
      unsigned long loads[3];  /* 1, 5, and 15 minute load averages */
      unsigned long totalram;  /* Total usable main memory size */
      unsigned long freeram;   /* Available memory size */
      unsigned long sharedram; /* Amount of shared memory */
      unsigned long bufferram; /* Memory used by buffers */
      unsigned long totalswap; /* Total swap space size */
      unsigned long freeswap;  /* Swap space still available */
      unsigned short procs;    /* Number of current processes */
      char _f[22];             /* Pads structure to 64 bytes */
  };
#+end_example

In the above structure, the sizes of the memory and swap fields are
given in bytes.

Since Linux 2.3.23 (i386) and Linux 2.3.48 (all architectures) the
structure is:

#+begin_example
  struct sysinfo {
      long uptime;             /* Seconds since boot */
      unsigned long loads[3];  /* 1, 5, and 15 minute load averages */
      unsigned long totalram;  /* Total usable main memory size */
      unsigned long freeram;   /* Available memory size */
      unsigned long sharedram; /* Amount of shared memory */
      unsigned long bufferram; /* Memory used by buffers */
      unsigned long totalswap; /* Total swap space size */
      unsigned long freeswap;  /* Swap space still available */
      unsigned short procs;    /* Number of current processes */
      unsigned long totalhigh; /* Total high memory size */
      unsigned long freehigh;  /* Available high memory size */
      unsigned int mem_unit;   /* Memory unit size in bytes */
      char _f[20-2*sizeof(long)-sizeof(int)];
                               /* Padding to 64 bytes */
  };
#+end_example

In the above structure, sizes of the memory and swap fields are given as
multiples of /mem_unit/ bytes.

* RETURN VALUE
On success, *sysinfo*() returns zero. On error, -1 is returned, and
/errno/ is set to indicate the error.

* ERRORS
- *EFAULT* :: /info/ is not a valid address.

* VERSIONS
*sysinfo*() first appeared in Linux 0.98.pl6.

* CONFORMING TO
This function is Linux-specific, and should not be used in programs
intended to be portable.

* NOTES
All of the information provided by this system call is also available
via //proc/meminfo/ and //proc/loadavg/.

* SEE ALSO
*proc*(5)

* COLOPHON
This page is part of release 5.13 of the Linux /man-pages/ project. A
description of the project, information about reporting bugs, and the
latest version of this page, can be found at
https://www.kernel.org/doc/man-pages/.
