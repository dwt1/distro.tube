#+TITLE: Manpages - geteuid.2
#+DESCRIPTION: Linux manpage for geteuid.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about geteuid.2 is found in manpage for: [[../man2/getuid.2][man2/getuid.2]]