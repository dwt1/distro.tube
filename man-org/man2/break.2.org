#+TITLE: Manpages - break.2
#+DESCRIPTION: Linux manpage for break.2
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about break.2 is found in manpage for: [[../man2/unimplemented.2][man2/unimplemented.2]]