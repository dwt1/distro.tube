#+TITLE: Manpages - openttd.6
#+DESCRIPTION: Linux manpage for openttd.6
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
Select the blitter

see

for a full list.

Use

instead of

Set debug verbosity for all categories to

or 1 if omitted.

Set debug verbosity to

for a specific category

Start a dedicated server.

Network debug level will be set to 6. If you want to change this, set

after setting

Start in world editor mode.

Fork into background (dedicated server only, see

Load

at start or start a new game if omitted.

must be either an absolute path or one relative to the current path or
one of the search paths.

Seed the pseudo random number generator with

Display a summary of all options and list all the available AIs,
blitters, sound, music and video drivers, graphics sets and sound sets.

Select the graphics set

see

for a full list.

Redirect

output; see

Select the music driver

see

for a full list.

Select the music set

see

for a full list.

Join a network game, optionally specifying a port to connect to and
player to play as.

Password used to join server. Only useful with

Password used to join company. Only useful with

Write some information about the specified savegame and exit.

Set the resolution to

×

pixels.

Select the sound driver

see

for a full list.

Select the sound set

see

for a full list.

Set the starting year to

Select the video driver

see

for a full list.

Do not automatically save to config file on exit.

(includes community maintained manual),

Transport Tycoon Deluxe was written by Chris Sawyer and published by
Microprose.

is a free reimplementation.
