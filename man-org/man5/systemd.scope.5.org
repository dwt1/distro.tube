#+TITLE: Manpages - systemd.scope.5
#+DESCRIPTION: Linux manpage for systemd.scope.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
systemd.scope - Scope unit configuration

* SYNOPSIS
/scope/.scope

* DESCRIPTION
Scope units are not configured via unit configuration files, but are
only created programmatically using the bus interfaces of systemd. They
are named similar to filenames. A unit whose name ends in ".scope"
refers to a scope unit. Scopes units manage a set of system processes.
Unlike service units, scope units manage externally created processes,
and do not fork off processes on its own.

The main purpose of scope units is grouping worker processes of a system
service for organization and for managing resources.

*systemd-run --scope* may be used to easily launch a command in a new
scope unit from the command line.

See the *New Control Group Interfaces*[1] for an introduction on how to
make use of scope units from programs.

Note that, unlike service units, scope units have no "main" process: all
processes in the scope are equivalent. The lifecycle of the scope unit
is thus not bound to the lifetime of one specific process, but to the
existence of at least one process in the scope. This also means that the
exit statuses of these processes are not relevant for the scope unit
failure state. Scope units may still enter a failure state, for example
due to resource exhaustion or stop timeouts being reached, but not due
to programs inside of them terminating uncleanly. Since processes
managed as scope units generally remain children of the original process
that forked them off, it is also the job of that process to collect
their exit statuses and act on them as needed.

* AUTOMATIC DEPENDENCIES
** Implicit Dependencies
Implicit dependencies may be added as result of resource control
parameters as documented in *systemd.resource-control*(5).

** Default Dependencies
The following dependencies are added unless /DefaultDependencies=no/ is
set:

#+begin_quote
  ·

  Scope units will automatically have dependencies of type /Conflicts=/
  and /Before=/ on shutdown.target. These ensure that scope units are
  removed prior to system shutdown. Only scope units involved with early
  boot or late system shutdown should disable /DefaultDependencies=/
  option.
#+end_quote

* OPTIONS
Scope files may include a [Scope] section, which carries information
about the scope and the units it contains. A number of options that may
be used in this section are shared with other unit types. These options
are documented in *systemd.kill*(5) and *systemd.resource-control*(5).
The options specific to the [Scope] section of scope units are the
following:

/RuntimeMaxSec=/

#+begin_quote
  Configures a maximum time for the scope to run. If this is used and
  the scope has been active for longer than the specified time it is
  terminated and put into a failure state. Pass "infinity" (the default)
  to configure no runtime limit.
#+end_quote

* SEE ALSO
*systemd*(1), *systemd-run*(1), *systemd.unit*(5),
*systemd.resource-control*(5), *systemd.service*(5),
*systemd.directives*(7).

* NOTES
-  1. :: New Control Group Interfaces

  https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface/
