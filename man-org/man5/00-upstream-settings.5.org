#+TITLE: Manpages - 00-upstream-settings.5
#+DESCRIPTION: Linux manpage for 00-upstream-settings.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*00-upstream-settings* - dconf configuration file

* SYNOPSIS
*/etc/dconf/db/ibus.d/00-upstream-settings*

* DESCRIPTION
IBus is an Intelligent Input Bus. It is a new input framework for Linux
OS. It provides full featured and user friendly input method user
interface. It also may help developers to develop input method easily.

*00-upstream-settings* is a text configuration file of dconf and can be
converted to /etc/dconf/db/ibus by *dconf update* command with a write
privilege in /etc/dconf/db. /etc/dconf/db/ibus is a database file of
dconf and saves the IBus default settings. The saved keys and values can
be readed by dconf command.

#+begin_quote
  #+begin_example
    env DCONF_PROFILE=ibus dconf list /desktop/ibus/
  #+end_example
#+end_quote

* BUGS
If you find a bug, please report it at
https://github.com/ibus/ibus/issues

* SEE ALSO
*dconf*(1) *ibus*(5)
