#+TITLE: Manpages - printers.conf.5
#+DESCRIPTION: Linux manpage for printers.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
printers.conf - printer configuration file for cups

* DESCRIPTION
The *printers.conf* file defines the local printers that are available.
It is normally located in the //etc/cups/ directory and is maintained by
the *cupsd*(8) program. This file is not intended to be edited or
managed manually.

* NOTES
The name, location, and format of this file are an implementation detail
that will change in future releases of CUPS.

* SEE ALSO
*classes.conf*(5), *cups-files.conf*(5), *cupsd*(8), *cupsd.conf*(5),
*mime.convs*(5), *mime.types*(5), *subscriptions.conf*(5), CUPS Online
Help (http://localhost:631/help)

* COPYRIGHT
Copyright © 2021 by OpenPrinting.
