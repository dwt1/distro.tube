#+TITLE: Manpages - sane-sp15c.5
#+DESCRIPTION: Linux manpage for sane-sp15c.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-sp15c - SANE backend for Fujitsu ScanPartner 15C flatbed scanner

* DESCRIPTION
The *sane-sp15c* library implements a SANE (Scanner Access Now Easy)
backend which provides access to the Fujitsu flatbed scanners. At
present, the following scanner is known to work with these backend:

#+begin_quote
  #+begin_example
    Vendor:  Model:           Rev:
    -------- ---------------- -----
    FCPA     ScanPartner 15C  1.01
  #+end_example
#+end_quote

The ScanPartner 15C driver supports lineart (1-bit), halftone (1-bit),
grayscale (4-bit and 8-bit), and color (3 x 8-bit) scanning.

Other scanners in these families may work. The ScanPartner 15C seems to
be a repackaging of the ScanPartner 600C. People are encouraged to try
these driver with the other scanners and to contact the author with test
results.

* CONFIGURATION
A modest effort has been made to expose the standard options to the API.
This allows frontends such as *xscanimage*(1) to set scanning region,
resolution, bit-depth (and color), and enable the automatic document
feeder.

* SEE ALSO
*sane*(7)*,* *sane-scsi*(5)*,* *sane-fujitsu*(5), *xscanimage*(1)\\
Fujitsu ScanPartner 15C OEM Manual, Doc. No. 250-0081-0\\
Fujitsu M3096G OEM Manual, part number 50FH5028E-05\\
Fujitsu M3096GX/M3093GX/M3093DG OEM Manual, part number C150-E015...03

* AUTHOR
Randolph Bentson </bentson@holmsjoen.com/>, with credit to the unnamed
author of the coolscan driver

* LIMITATIONS
Testing limited to a Linux 2.2.5 kernel\\
Can't quite get the scan page/minute performance in ADF modes. This may
be due to limited system buffer size.

* BUGS
I'm sure there are plenty, and not too well hidden, but I haven't seen
them yet.\\
Both scanners claim to have separate control of resolution in X and Y
directions. I confess I haven't tested this yet. I have found that
*xsane*(1) doesn't even display this capability.\\
Threshold settings on the SP15C don't seem to affect the results of
lineart mode scans.\\
It might be possible to merge these two drivers without much effort
since the SP15C driver was derived from the M3096G driver. They were
split so as to keep the second driver development from breaking the
working first driver. Watch this space for changes.
