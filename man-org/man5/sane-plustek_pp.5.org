#+TITLE: Manpages - sane-plustek_pp.5
#+DESCRIPTION: Linux manpage for sane-plustek_pp.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
sane-plustek_pp - SANE backend for Plustek parallel port flatbed
scanners

* DESCRIPTION
The *sane-plustek_pp* library implements a SANE (Scanner Access Now
Easy) backend that provides access to Plustek ASIC 9600[1/3] and
P9800[1/3] based parallel port flatbed scanners. The access of the
scanner is either done directly by the backend or via kernel module,
called pt_drv which can be created out of the *sane-plustek_pp* backend
code - see also section *BUILDING THE KERNEL MODULE* for further
information.

* SUPPORTED DEVICES
At present, the following scanners should work with this backend and/or
the kernel module:

*PLUSTEK SCANNERS*

#+begin_example
  Parallelport Model:    ASIC: Properties:
  ---------------------- ----- ------------------------
  OpticPro PT12          98003 600x1200 dpi 36bit 512Kb
  OpticPro P12           98003 600x1200 dpi 36bit 512Kb
  OpticPro 9636T/12000T  98001 600x1200 dpi 36bit 512Kb
  OpticPro 12000P Turbo  98001 600x1200 dpi 36bit 512Kb
  OpticPro 9636P+/Turbo  98001 600x1200 dpi 36bit 512Kb
  OpticPro 9636P         96003 600x1200 dpi 36bit 128Kb
  OpticPro 12000P/96000P 96003 600x1200 dpi 36bit 128Kb
  OpticPro 1236P         96003 600x1200 dpi 30bit 128Kb
  OpticPro 9600P         96003 600x1200 dpi 30bit 128Kb
  OpticPro 9630P/FBIV    96003 600x1200 dpi 30bit 128Kb
  OpticPro 9630PL (14")  96003 600x1200 dpi 30bit 128Kb
  OpticPro A3I           96003  400x800 dpi 36bit 128Kb
  OpticPro 600P/6000P    96003  300x600 dpi 30bit  32Kb
  OpticPro 4831P         96003  300x600 dpi 30bit  32Kb
  OpticPro 4830P/FBIII   96003  300x600 dpi 30bit  32Kb
  OpticPro 4800P/FBII    96001  300x600 dpi 24bit  32Kb
#+end_example

*PRIMAX SCANNERS*

There are some scanners sold by Primax, but they are in fact Plustek
devices. These scanners are also supported. The following table will
show the relationship:

#+begin_example
  Model:                      Plustek Model:  Remarks:
  --------------------------- --------------  ------------
  Colorado 4800               OpticPro 4800   not tested
  Compact 4800 Direct         OpticPro 600    mov=2
  Compact 4800 Direct 30bit   OpticPro 4830   mov=7
  Compact 9600 Direct 30bit   OpticPro 9630   works
#+end_example

*GENIUS SCANNERS*

The following devices are sold as Genius Scanners, but are in fact
Plustek devices. The table will show the relationship:

#+begin_example
  Model:                      Remarks:
  --------------------------- ----------------------------
  Colorpage Vivid III V2      Like P12 but has two buttons
                              and Wolfson DAC
#+end_example

*ARIES SCANNERS*

There's one scanner sold as Aries Scanner, but is in fact a Plustek
device. The following table will show the relationship:

#+begin_example
  Model:                      Plustek Model:  Remarks:
  --------------------------- --------------  ------------
  Scan-It 4800                OpticPro 600    mov=2
#+end_example

*BrightScan SCANNERS*

There's one scanner sold as BrightScan OpticPro Scanner, this is also a
rebadged Plustek device. The following table will show the relationship:

#+begin_example
  Model:                      Remarks:
  --------------------------- ----------------------------
  BrightScan OpticPro         OpticPro P12
#+end_example

* DEVICE NAMES
This backend works in two modes, the so called "direct-mode" and the
"kernel-mode". In direct-mode, the user-space backend is used, in
kernel-mode, you should have a kernel-module named pt_drv loaded. This
backends default device is:

#+begin_quote
  /0x378/
#+end_quote

This "default device" will be used, if no configuration file can be
found. It is rather the base address of the parallel port on i386
machines.

As the backend supports up to four devices, it is possible to specify
them in the configuration file

#+begin_quote
  //etc/sane.d/plustek_pp.conf/
#+end_quote

See this file for examples.

* CONFIGURATION
This section describes the backend's configuration file entries. The
file is located at: //etc/sane.d/plustek_pp.conf/

For a proper setup, you will need at least two entries:

#+begin_quote
  /[direct]/\\
  /device 0x378/
#+end_quote

or

#+begin_quote
  /[kernel]/\\
  /device /dev/pt_drv/
#+end_quote

/direct/ tells the backend, that the following devicename (here /0x378/)
has to be interpreted as parallel port scanner device. In fact it is the
address to use, alternatively you can use //dev/parport0/ if the backend
has been compiled with libieee1284 support. /kernel/ should only be
used, when a kernel-module has been built out of the backend sources.
See below for more instructions about this.

Further options:

option warmup t

#+begin_quote
  /t/ specifies the warmup period in seconds
#+end_quote

option lampOff t

#+begin_quote
  /t/ is the time in seconds for switching off the lamps in standby mode
#+end_quote

option lOffonEnd b

#+begin_quote
  /b/ specifies the behaviour when closing the backend, 1 --> switch
  lamps off, 0 --> do not change lamp status
#+end_quote

option mov m

#+begin_quote
  /m/ is the model override switch, which only works in direct mode.

  - /m/ = 0 :: default: no override

  - /m/ = 1 :: OpticPro 9630PL override (works if OP9630 has been
    detected) forces legal size (14")

  - /m/ = 2 :: Primax 4800Direct override (works if OP600 has been
    detected) swaps red/green color

  - /m/ = 3 :: OpticPro 9636 override (works if OP9636 has been
    detected) disables backends transparency/negative capabilities

  - /m/ = 4 :: OpticPro 9636P override (works if OP9636 has been
    detected) disables backends transparency/negative capabilities

  - /m/ = 5 :: OpticPro A3I override (works if OP12000 has been
    detected) enables A3 scanning

  - /m/ = 6 :: OpticPro 4800P override (works if OP600 has been
    detected) swaps red/green color

  - /m/ = 7 :: Primax 4800Direct 30bit override (works if OP4830 has
    been detected)
#+end_quote

See the /plustek_pp.conf/ file for examples.

* BUILDING THE KERNEL MODULE
As mentioned before, the *sane-plustek_pp* backend code can also be
compiled and installed as linux kernel module. To do so, you will need
the source-files of this sane-backend installation. Unpack this tar-ball
and go to the directory: /sane-backends/doc/plustek/. Within this
directory, you should find a script called: /MakeModule.sh/. Now if your
Linux kernelsources are installed correctly, it should be possible to
build, install and load the module *pt_drv*. *Please note,* that the
kernelsources need to be configured correctly. Refer to your
distributions manual on how this is done. As root user, try

/./MakeModule.sh/

the script will try and get all necessary information about your running
kernel and will lead you through the whole installation process.\\
*Note: Installing and loading the can only be done as* superuser.

* KERNEL MODULE SETUP
The configuration of the kernel module is done by providing some or more
options found below to the kernel module at load time. This can be done
by invoking *insmod*(8) with the appropriate parameters or appending the
options to the file //etc/modules.conf (kernel < 2.6.x)/ or
//etc/modprobe.conf (kernel >= 2.6.x)/

*The Options:*\\
lampoff=lll

#+begin_quote
  The value /lll/ tells the driver, after how many seconds to switch-off
  the lamp(s). The default value is 180. 0 will disable this feature.\\
  *HINT:* Do not use a value that is too small, because often switching
  on/off the lamps will reduce their lifetime.
#+end_quote

port=ppp

#+begin_quote
  /ppp/ specifies the port base address, where the scanner is connected
  to. The default value is 0x378, which is normally a standard.
#+end_quote

warmup=www

#+begin_quote
  /www/ specifies the time in seconds, how long a lamp has to be on,
  until the driver will start to scan. The default value is 30.
#+end_quote

lOffonEnd=e

#+begin_quote
  /e/ specifies the behaviour when unloading the driver, 1 --> switch
  lamps off, 0 --> do not change lamp status
#+end_quote

slowIO=s

#+begin_quote
  /s/ specifies which I/O functions the driver should use, 1 --> use
  delayed functions, 0 --> use the non-delayed ones
#+end_quote

forceMode=fm

#+begin_quote
  /fm/ specifies port mode which should be used, 0 --> autodetection, 1
  --> use SPP mode and 2 --> use EPP mode
#+end_quote

mov=m

#+begin_quote
  - /m/ = 0 :: default: no override

  - /m/ = 1 :: OpticPro 9630PL override (works if OP9630 has been
    detected) forces legal size (14")

  - /m/ = 2 :: Primax 4800Direct override (works if OP600 has been
    detected) swaps red/green color

  - /m/ = 3 :: OpticPro 9636 override (works if OP9636 has been
    detected) disables backends transparency/negative capabilities

  - /m/ = 4 :: OpticPro 9636P override (works if OP9636 has been
    detected) disables backends transparency/negative capabilities

  - /m/ = 5 :: OpticPro A3I override (works if OP12000 has been
    detected) enables A3 scanning

  - /m/ = 6 :: OpticPro 4800P override (works if OP600 has been
    detected) swaps red/green color

  - /m/ = 7 :: Primax 4800Direct 30bit override (works if OP4830 has
    been detected)
#+end_quote

Sample entry for file //etc/modules.conf/:

#+begin_quote
  alias char-major-40 pt_drv\\
  pre-install pt_drv modprobe -k parport\\
  options pt_drv lampoff=180 warmup=15 port=0x378 lOffonEnd=0 mov=0
  slowIO=0 forceMode=0
#+end_quote

For multidevice support, simply add values separated by commas to the
different options

#+begin_quote
  options pt_drv port=0x378,0x278 mov=0,4 slowIO=0,1 forceMode=0,1
#+end_quote

Remember to call *depmod*(8) after changing //etc/conf.modules/.

* PARALLEL PORT MODES
The current driver works best, when the parallel port has been set to
EPP-mode. When detecting any other mode such as ECP or PS/2 the driver
tries to set to a faster, supported mode. If this fails, it will use the
SPP mode, as this mode should work with all Linux supported parallel
ports. If in doubt, enter your BIOS and set it to any mode except ECP.

Former Plustek scanner models (4830, 9630) supplied a ISA parallel port
adapter card. This card is *not* supported by the driver.

The ASIC 96001/3 based models have sometimes trouble with high
resolution modes. If you encounter sporadic corrupted images (parts
duplicated or shifted horizontally) kill all other applications before
scanning and (if sufficient memory available) disable swapping.

See the /plustek_pp.conf/ file for examples.

* FILES
- //etc/sane.d/plustek_pp.conf/ :: The backend configuration file

- //usr/lib/sane/libsane-plustek_pp.a/ :: The static library
  implementing this backend.

- //usr/lib/sane/libsane-plustek_pp.so/ :: The shared library
  implementing this backend (present on systems that support dynamic
  loading).

- //lib/modules/<Kernel-Version>/kernel/drivers/parport/pt_drv.o/ :: The
  Linux kernelmodule for kernels < 2.6.x.

- //lib/modules/<Kernel-Version>/kernel/drivers/parport/pt_drv.ko/ :: The
  Linux kernelmodule for kernels >= 2.6.x.

* ENVIRONMENT
- *SANE_CONFIG_DIR* :: This environment variable specifies the list of
  directories that may contain the configuration file. Under UNIX, the
  directories are separated by a colon (`:'), under OS/2, they are
  separated by a semi-colon (`;'). If this variable is not set, the
  configuration file is searched in two default directories: first, the
  current working directory (".") and then in //etc/sane.d/. If the
  value of the environment variable ends with the directory separator
  character, then the default directories are searched after the
  explicitly specified directories. For example, setting
  *SANE_CONFIG_DIR* to "/tmp/config:" would result in directories
  /tmp/config/, /./, and //etc/sane.d/ being searched (in this order).

- *SANE_DEBUG_PLUSTEK_PP* :: If the library was compiled with debug
  support enabled, this environment variable controls the debug level
  for this backend. Higher debug levels increase the verbosity of the
  output.

Example: export SANE_DEBUG_PLUSTEK_PP=10

* SEE ALSO
*sane*(7), *xscanimage*(1),\\
//usr/share/doc/sane/plustek/Plustek-PARPORT.changes/

* CONTACT AND BUG-REPORTS
Please send any information and bug-reports to:\\
*SANE Mailing List*

Additional info and hints can be obtained from our\\
Mailing-List archive at:\\
/http://www.sane-project.org/mailing-lists.html/

To obtain debug messages from the backend, please set the
environment-variable *SANE_DEBUG_PLUSTEK_PP* before calling your
favorite scan-frontend (i.e. *xscanimage*(1)),*i.e.:*\\
/export SANE_DEBUG_PLUSTEK_PP=20 ; xscanimage/

The value controls the verbosity of the backend.

* KNOWN BUGS & RESTRICTIONS
* The Halftoning works, but the quality is poor

* Printers (especially HP models) will start to print during scanning.
This in fact is a problem to other printers too, using bidirectional
protocol (see www.plustek.com (TAIWAN) page for further details)

* The driver does not support these manic scalings up to 16 times the
physical resolution. The only scaling is done on resolutions between the
physical resolution of the CCD-sensor and the stepper motor i.e. you
have a 600x1200 dpi scanner and you are scanning using 800dpi, so
scaling is necessary, because the sensor only delivers 600dpi but the
motor is capable to perform 800dpi steps.

* On some devices, the pictures seems bluish

/ASIC 98001 based models:/

* The 300dpi transparency and negative mode does not work correctly.

* There is currently no way to distinguish a model with and without
transparency unit.

* The scanned images seem to be too dark (P9636T)

/ASIC 96003/1 based models:/

* 30bit mode is currently not supported.

* On low end systems under heavy system load the driver may lose data,
which can result in picture corruption or cause the sensor to hit the
scan bed.

* The scanning speed on 600x1200 dpi models is slow.

* The scanning quality of the A3I is poor.
