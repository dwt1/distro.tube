#+TITLE: Manpages - acl.5
#+DESCRIPTION: Linux manpage for acl.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
This manual page describes POSIX Access Control Lists, which are used to
define more fine-grained discretionary access rights for files and
directories.

Every object can be thought of as having associated with it an ACL that
governs the discretionary access to that object; this ACL is referred to
as an access ACL. In addition, a directory may have an associated ACL
that governs the initial access ACL for objects created within that
directory; this ACL is referred to as a default ACL.

An ACL consists of a set of ACL entries. An ACL entry specifies the
access permissions on the associated object for an individual user or a
group of users as a combination of read, write and search/execute
permissions.

An ACL entry contains an entry tag type, an optional entry tag
qualifier, and a set of permissions. We use the term qualifier to denote
the entry tag qualifier of an ACL entry.

The qualifier denotes the identifier of a user or a group, for entries
with tag types of ACL_USER or ACL_GROUP, respectively. Entries with tag
types other than ACL_USER or ACL_GROUP have no defined qualifiers.

The following entry tag types are defined:

The ACL_USER_OBJ entry denotes access rights for the file owner.

ACL_USER entries denote access rights for users identified by the
entry's qualifier.

The ACL_GROUP_OBJ entry denotes access rights for the file group.

ACL_GROUP entries denote access rights for groups identified by the
entry's qualifier.

The ACL_MASK entry denotes the maximum access rights that can be granted
by entries of type ACL_USER, ACL_GROUP_OBJ, or ACL_GROUP.

The ACL_OTHER entry denotes access rights for processes that do not
match any other entry in the ACL.

When an access check is performed, the ACL_USER_OBJ and ACL_USER entries
are tested against the effective user ID. The effective group ID, as
well as all supplementary group IDs are tested against the ACL_GROUP_OBJ
and ACL_GROUP entries.

A valid ACL contains exactly one entry with each of the ACL_USER_OBJ,
ACL_GROUP_OBJ, and ACL_OTHER tag types. Entries with ACL_USER and
ACL_GROUP tag types may appear zero or more times in an ACL. An ACL that
contains entries of ACL_USER or ACL_GROUP tag types must contain exactly
one entry of the ACL_MASK tag type. If an ACL contains no entries of
ACL_USER or ACL_GROUP tag types, the ACL_MASK entry is optional.

All user ID qualifiers must be unique among all entries of ACL_USER tag
type, and all group IDs must be unique among all entries of ACL_GROUP
tag type.

The

function returns an ACL with zero ACL entries as the default ACL of a
directory, if the directory is not associated with a default ACL. The

function also accepts an ACL with zero ACL entries as a valid default
ACL for directories, denoting that the directory shall not be associated
with a default ACL. This is equivalent to using the

function.

The permissions defined by ACLs are a superset of the permissions
specified by the file permission bits.

There is a correspondence between the file owner, group, and other
permissions and specific ACL entries: the owner permissions correspond
to the permissions of the ACL_USER_OBJ entry. If the ACL has an ACL_MASK
entry, the group permissions correspond to the permissions of the
ACL_MASK entry. Otherwise, if the ACL has no ACL_MASK entry, the group
permissions correspond to the permissions of the ACL_GROUP_OBJ entry.
The other permissions correspond to the permissions of the ACL_OTHER_OBJ
entry.

The file owner, group, and other permissions always match the
permissions of the corresponding ACL entry. Modification of the file
permission bits results in the modification of the associated ACL
entries, and modification of these ACL entries results in the
modification of the file permission bits.

The access ACL of a file object is initialized when the object is
created with any of the

or

functions. If a default ACL is associated with a directory, the

parameter to the functions creating file objects and the default ACL of
the directory are used to determine the ACL of the new object:

The new object inherits the default ACL of the containing directory as
its access ACL.

The access ACL entries corresponding to the file permission bits are
modified so that they contain no permissions that are not contained in
the permissions specified by the

parameter.

If no default ACL is associated with a directory, the

parameter to the functions creating file objects and the file creation
mask (see

are used to determine the ACL of the new object:

The new object is assigned an access ACL containing entries of tag types
ACL_USER_OBJ, ACL_GROUP_OBJ, and ACL_OTHER. The permissions of these
entries are set to the permissions specified by the file creation mask.

The access ACL entries corresponding to the file permission bits are
modified so that they contain no permissions that are not contained in
the permissions specified by the

parameter.

A process may request read, write, or execute/search access to a file
object protected by an ACL. The access check algorithm determines
whether access to the object will be granted.

the effective user ID of the process matches the user ID of the file
object owner,

the ACL_USER_OBJ entry contains the requested permissions, access is
granted,

access is denied.

the effective user ID of the process matches the qualifier of any entry
of type ACL_USER,

the matching ACL_USER entry and the ACL_MASK entry contain the requested
permissions, access is granted,

access is denied.

the effective group ID or any of the supplementary group IDs of the
process match the file group or the qualifier of any entry of type
ACL_GROUP,

the ACL contains an ACL_MASK entry,

the ACL_MASK entry and any of the matching ACL_GROUP_OBJ or ACL_GROUP
entries contain the requested permissions, access is granted,

access is denied.

(note that there can be no ACL_GROUP entries without an ACL_MASK entry)

the ACL_GROUP_OBJ entry contains the requested permissions, access is
granted,

access is denied.

the ACL_OTHER entry contains the requested permissions, access is
granted.

access is denied.

A long and a short text form for representing ACLs is defined. In both
forms, ACL entries are represented as three colon separated fields: an
ACL entry tag type, an ACL entry qualifier, and the discretionary access
permissions. The first field contains one of the following entry tag
type keywords:

A

ACL entry specifies the access granted to either the file owner (entry
tag type ACL_USER_OBJ) or a specified user (entry tag type ACL_USER).

A

ACL entry specifies the access granted to either the file group (entry
tag type ACL_GROUP_OBJ) or a specified group (entry tag type ACL_GROUP).

A

ACL entry specifies the maximum access which can be granted by any ACL
entry except the

entry for the file owner and the

entry (entry tag type ACL_MASK).

An other ACL entry specifies the access granted to any process that does
not match any

or

ACL entries (entry tag type ACL_OTHER).

The second field contains the user or group identifier of the user or
group associated with the ACL entry for entries of entry tag type
ACL_USER or ACL_GROUP, and is empty for all other entries. A user
identifier can be a user name or a user ID number in decimal form. A
group identifier can be a group name or a group ID number in decimal
form.

The third field contains the discretionary access permissions. The read,
write and search/execute permissions are represented by the

and

characters, in this order. Each of these characters is replaced by the

character to denote that a permission is absent in the ACL entry. When
converting from the text form to the internal representation,
permissions that are absent need not be specified.

White space is permitted at the beginning and end of each ACL entry, and
immediately before and after a field separator (the colon character).

The long text form contains one ACL entry per line. In addition, a
number sign

may start a comment that extends until the end of the line. If an
ACL_USER, ACL_GROUP_OBJ or ACL_GROUP ACL entry contains permissions that
are not also contained in the ACL_MASK entry, the entry is followed by a
number sign, the string “effective:”, and the effective access
permissions defined by that entry. This is an example of the long text
form:

user::rw- user:lisa:rw- #effective:r-- group::r-- group:toolies:rw-
#effective:r-- mask::r-- other::r--

The short text form is a sequence of ACL entries separated by commas,
and is used for input. Comments are not supported. Entry tag type
keywords may either appear in their full unabbreviated form, or in their
single letter abbreviated form. The abbreviation for

is

the abbreviation for

is

the abbreviation for

is

and the abbreviation for

is

The permissions may contain at most one each of the following characters
in any order:

These are examples of the short text form:

u::rw-,u:lisa:rw-,g::r--,g:toolies:rw-,m::r--,o::r--
g:toolies:rw,u:lisa:rw,u::wr,g::r,o::r,m::r

IEEE 1003.1e draft 17 defines Access Control Lists that include entries
of tag type ACL_MASK, and defines a mapping between file permission bits
that is not constant. The standard working group defined this relatively
complex interface in order to ensure that applications that are
compliant with IEEE 1003.1 (“POSIX.1”) will still function as expected
on systems with ACLs. The IEEE 1003.1e draft 17 contains the rationale
for choosing this interface in section B.23.

On a system that supports ACLs, the file utilities

and

change their behavior in the following way:

For files that have a default ACL or an access ACL that contains more
than the three required ACL entries, the

utility in the long form produced by

displays a plus sign

after the permission string.

If the

flag is specified, the

utility also preserves ACLs. If this is not possible, a warning is
produced.

The

utility always preserves ACLs. If this is not possible, a warning is
produced.

The effect of the

utility, and of the

system call, on the access ACL is described in

The IEEE 1003.1e draft 17 (“POSIX.1e”) document describes several
security extensions to the IEEE 1003.1 standard. While the work on
1003.1e has been abandoned, many UNIX style systems implement parts of
POSIX.1e draft 17, or of earlier drafts.

Linux Access Control Lists implement the full set of functions and
utilities defined for Access Control Lists in POSIX.1e, and several
extensions. The implementation is fully compliant with POSIX.1e draft
17; extensions are marked as such. The Access Control List manipulation
functions are defined in the ACL library (libacl, -lacl). The POSIX
compliant interfaces are declared in the

header. Linux-specific extensions to these functions are declared in the

header.

The first group of functions is supported on most systems with
POSIX-like access control lists, while the second group is supported on
fewer systems. For applications that will be ported the second group is
best avoided.

These non-portable extensions are available on Linux systems.

Andreas Gruenbacher, <andreas.gruenbacher@gmail.com>
