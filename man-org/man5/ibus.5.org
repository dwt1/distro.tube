#+TITLE: Manpages - ibus.5
#+DESCRIPTION: Linux manpage for ibus.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
*IBUS* - dconf database file for IBus

* SYNOPSIS
*/etc/dconf/db/ibus*

* DESCRIPTION
IBus is an Intelligent Input Bus. It is a new input framework for Linux
OS. It provides full featured and user friendly input method user
interface. It also may help developers to develop input method easily.

*/etc/dconf/db/ibus* is a database file dconf and saves the IBus default
settings. It can be generated from
/etc/dconf/db/ibus.d/00-upstream-settings by *dconf update* command with
a write privilege in /etc/dconf/db. The saved keys and values can be
readed by dconf command.

#+begin_quote
  #+begin_example
    env DCONF_PROFILE=ibus dconf list /desktop/ibus/
  #+end_example
#+end_quote

* BUGS
If you find a bug, please report it at
https://github.com/ibus/ibus/issues

* SEE ALSO
*dconf*(1) *00-upstream-settings*(5)
