#+TITLE: Manpages - logrotate.conf.5
#+DESCRIPTION: Linux manpage for logrotate.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"


Information about logrotate.conf.5 is found in manpage for: [[../man8/logrotate.8][man8/logrotate.8]]