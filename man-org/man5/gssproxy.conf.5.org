#+TITLE: Manpages - gssproxy.conf.5
#+DESCRIPTION: Linux manpage for gssproxy.conf.5
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* NAME
gssproxy.conf - GssProxy Daemon Configuration file

* DESCRIPTION
Optional configuration directives for the gssproxy daemon.

GSS-Proxy conf files are classic ini-style configuration files. Each
option consist of a key = value pair. Any characters behind # will be
treated as comments and will be ignored. Boolean parameters accept "1",
"true", "yes" and "on" as positive values. All other values will be
considered as negative values.

GSS-Proxy conf files must either be named "gssproxy.conf", or be of the
form "##-foo.conf" (that is, start with two numbers followed by a dash,
and end in ".conf"). Files not conforming to this will be ignored unless
specifically requested through command line parameters. Within a single
file, any duplicate values or sections will be merged. Across multiple
files, duplicates will generate a warning, and the first value
encountered will take precedence (i.e., there is no merging).

* SECTIONS
A section in a GSS-Proxy conf file is identified by the sectionname in
square brackets ([sectionname]).

There is one special section for global gssproxy settings, called
[gssproxy].

Services such as nfs, apache, ssh, etc. are represented by sections like
[service/nfs], [service/apache], etc. and are identified by the "euid"
setting (see below).

* VARIABLE SUBSTITUTIONS
String parameters may contain substitution patterns. This allows
gssproxy to deal with patterns for the storage location of keytabs or
credential caches easier.

The supported patterns are:

%U

#+begin_quote
  substitutes to the users numeric uid (e.g. 123)
#+end_quote

%u

#+begin_quote
  substitutes to the users username (e.g. john).
#+end_quote

* OPTIONS
gssproxy supports the following options:

allow_any_uid (boolean)

#+begin_quote
  Allow any process of any user to use this service.

  Note that absent a custom socket option this option may cause a
  service definition to mask access to following services. To avoid
  issues change the order of services in your configuation file so that
  services with allow_any_uid enabled are listed last, or define a
  custom socket for other services.

  Default: false
#+end_quote

allow_protocol_transition (boolean)

#+begin_quote
  Allow clients to request a ticket to self for an arbitrary user.

  This option controls whether s4u2self requests are allowed for the
  requesting client. The configured keytab is used as the service
  identity for which a ticket is requested. The KDC still needs to allow
  the operation for it to succeed.

  Default: false
#+end_quote

allow_constrained_delegation (boolean)

#+begin_quote
  Allow clients to request a ticket to another service using an evidence
  ticket.

  This option controls whether s4u2proxy requests are allowed for the
  requesting client. The KDC still needs to allow the operation for it
  to succeed.

  Default: false
#+end_quote

allow_client_ccache_sync (boolean)

#+begin_quote
  Allow clients to request credentials to be sent back for better
  caching.

  This option allows the proxy, in certain circumstances, to send back
  an additional option in the response structure of certain calls when
  it determines that a new ticket may have been added to the internal
  ccache. Clients can then replace their (encrypted) copy with the
  updated ccache.

  Default: false
#+end_quote

cred_store (string)

#+begin_quote
  This parameter allows to control in which way gssproxy should use the
  cred_store interface provided by GSSAPI. The parameter can be defined
  multiple times per service.

  The syntax of the cred_store parameter is as follows: cred_store =
  <cred_store_option>:<cred_store_value>

  Currently this interface supports the following options:

  keytab

  #+begin_quote
    Defines the keytab the service should use. Example: cred_store =
    keytab:/path/to/keytab
  #+end_quote

  client_keytab

  #+begin_quote
    Defines a client keytab the service should use. Example: cred_store
    = client_keytab:/path/to/client_keytab.
  #+end_quote

  ccache

  #+begin_quote
    Defines a credential cache the service should use. Example:
    cred_store = ccache:/path/to/ccache.
  #+end_quote

  Notably the client_keytab and the ccache setting typically are used
  with variable substitution placeholders (see above). For example:

  #+begin_quote
    #+begin_example
          cred_store = keytab:/etc/krb5.keytab
          cred_store = ccache:FILE:/var/lib/gssproxy/krb5cc_%U
          cred_store = client_keytab:/var/lib/gssproxy/%U.keytab
    #+end_example
  #+end_quote

  Default: cred_store =
#+end_quote

cred_usage (string)

#+begin_quote
  Allow to restrict the kind of operations permitted for this service.

  The allowed options are: initiate, accept, both

  Default: cred_usage = both
#+end_quote

debug (boolean)

#+begin_quote
  Enable debugging to syslog. Setting to true is identical to setting
  debug_level to 1.

  Default: debug = false
#+end_quote

debug_level (integer)

#+begin_quote
  Detail level at which to log debugging messages. 0 corresponds to no
  logging, while 1 turns on basic debug logging. Level 2 increases
  verbosity, including more detailed credential verification.

  At level 3 and above, KRB5_TRACE output is logged. If KRB5_TRACE was
  already set in the execution environment, trace output is sent to its
  value instead.

  Default: 1 if debug is true, otherwise 0
#+end_quote

enforce_flags (string)

#+begin_quote
  A list of GSS Request Flags that are added unconditionally to every
  context initialization call. Flags can only be added to the list or
  removed from the list by prepending a +/- sign to the flag name or
  value.

  Recognized flag names: DELEGATE, MUTUAL_AUTH, REPLAY_DETECT, SEQUENCE,
  CONFIDENTIALITY, INTEGRITY, ANONYMOUS

  Examples:

  #+begin_quote
    #+begin_example
          enforce_flags = +REPLAY_DETECT
          enforce_flags = -0x0001
    #+end_example
  #+end_quote

  Default: enforce_flags =
#+end_quote

euid (integer or string)

#+begin_quote
  Either the numeric (e.g., 48) or symbolic (e.g., apache) effective uid
  of a running process, required to identify a service.

  The "euid" parameter is imperative, any section without it will be
  discarded.

  Default: euid =
#+end_quote

filter_flags (string)

#+begin_quote
  A list of GSS Request Flags that are filtered unconditionally from
  every context initialization call. Flags can only be added to the list
  or removed from the list by prepending a +/- sign to the flag name or
  value.

  NOTE: Because often gssproxy is used to withold access to credentials
  the Delegate Flag is filtered by default. To allow a service to
  delegate credentials use the first example below.

  Recognized flag names: DELEGATE, MUTUAL_AUTH, REPLAY_DETECT, SEQUENCE,
  CONFIDENTIALITY, INTEGRITY, ANONYMOUS

  Examples:

  #+begin_quote
    #+begin_example
          filter_flags = -DELEGATE
          filter_flags = -0x0001 +ANONYMOUS
    #+end_example
  #+end_quote

  Default: filter_flags = +DELEGATE
#+end_quote

impersonate (boolean)

#+begin_quote
  Use impersonation (s4u2self + s4u2proxy) to obtain credentials

  Default: impersonate = false
#+end_quote

kernel_nfsd (boolean)

#+begin_quote
  Boolean flag that allows the Linux kernel to check if gssproxy is
  running (via /proc/net/rpc/use-gss-proxy).

  Default: kernel_nfsd = false
#+end_quote

krb5_principal (string)

#+begin_quote
  The krb5 principal to be used preferred for this service, if one isnt
  requested by the application. Note that this does not enforce use of
  this specific name; it only sets a default.

  Default: krb5_principal =
#+end_quote

mechs (string)

#+begin_quote
  Currently only /krb5/ is supported.

  The "mechs" parameter is imperative, any section without it will be
  discarded.

  Default: mechs =
#+end_quote

program (string)

#+begin_quote
  If specified, this service will only match when the program being run
  is the specified string.

  Programs are assumed to be specified as canonical paths (i.e., no
  relative paths, no symlinks). Additionally, the | character is
  reserved for future use and therefore forbidden.
#+end_quote

run_as_user (string)

#+begin_quote
  The name of the user gssproxy will drop privileges to.

  This option is only available in the global section.

  Default: run_as_user =
#+end_quote

selinux_context (string)

#+begin_quote
  This option is deprecated. Use a custom socket or euid instead.
#+end_quote

socket (string)

#+begin_quote
  This parameter allows to create a per-service socket file over which
  gssproxy client and server components communicate.

  When this parameter is not set, gssproxy will use a compiled-in
  default.
#+end_quote

syslog_status (boolean)

#+begin_quote
  Enable per-call debugging output to the syslog. This may be useful for
  investigating problems in applications using gssproxy.

  Default: syslog_status = false
#+end_quote

trusted (boolean)

#+begin_quote
  Defines whether this service is considered trusted. Use with caution,
  this enables impersonation.

  Default: trusted = false
#+end_quote

worker threads (integer)

#+begin_quote
  Defines the amount of worker threads gssproxy will create at startup.

  Default: worker threads =
#+end_quote

* SEE ALSO
*gssproxy*(8) and *gssproxy-mech*(8).

* AUTHORS
*GSS-Proxy - http://fedorahosted.org/gss-proxy*
