#+TITLE: Manpages - clock_getcpuclockid.3p
#+DESCRIPTION: Linux manpage for clock_getcpuclockid.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
clock_getcpuclockid --- access a process CPU-time clock (*ADVANCED
REALTIME*)

* SYNOPSIS
#+begin_example
  #include <time.h>
  int clock_getcpuclockid(pid_t pid, clockid_t *clock_id);
#+end_example

* DESCRIPTION
The /clock_getcpuclockid/() function shall return the clock ID of the
CPU-time clock of the process specified by /pid/. If the process
described by /pid/ exists and the calling process has permission, the
clock ID of this clock shall be returned in /clock_id/.

If /pid/ is zero, the /clock_getcpuclockid/() function shall return the
clock ID of the CPU-time clock of the process making the call, in
/clock_id/.

The conditions under which one process has permission to obtain the
CPU-time clock ID of other processes are implementation-defined.

* RETURN VALUE
Upon successful completion, /clock_getcpuclockid/() shall return zero;
otherwise, an error number shall be returned to indicate the error.

* ERRORS
The /clock_getcpuclockid/() function shall fail if:

- *EPERM* :: The requesting process does not have permission to access
  the CPU-time clock for the process.

The /clock_getcpuclockid/() function may fail if:

- *ESRCH* :: No process can be found corresponding to the process
  specified by /pid/.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The /clock_getcpuclockid/() function is part of the Process CPU-Time
Clocks option and need not be provided on all implementations.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//clock_getres/ ( )/, //timer_create/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<time.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
