#+TITLE: Manpages - sched_yield.3p
#+DESCRIPTION: Linux manpage for sched_yield.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
sched_yield --- yield the processor

* SYNOPSIS
#+begin_example
  #include <sched.h>
  int sched_yield(void);
#+end_example

* DESCRIPTION
The /sched_yield/() function shall force the running thread to
relinquish the processor until it again becomes the head of its thread
list. It takes no arguments.

* RETURN VALUE
The /sched_yield/() function shall return 0 if it completes
successfully; otherwise, it shall return a value of -1 and set /errno/
to indicate the error.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
The conceptual model for scheduling semantics in POSIX.1‐2008 defines a
set of thread lists. This set of thread lists is always present
regardless of the scheduling options supported by the system. On a
system where the Process Scheduling option is not supported, portable
applications should not make any assumptions regarding whether threads
from other processes will be on the same thread list.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<sched.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
