#+TITLE: Manpages - unsetenv.3p
#+DESCRIPTION: Linux manpage for unsetenv.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
unsetenv --- remove an environment variable

* SYNOPSIS
#+begin_example
  #include <stdlib.h>
  int unsetenv(const char *name);
#+end_example

* DESCRIPTION
The /unsetenv/() function shall remove an environment variable from the
environment of the calling process. The /name/ argument points to a
string, which is the name of the variable to be removed. The named
argument shall not contain an *'='* character. If the named variable
does not exist in the current environment, the environment shall be
unchanged and the function is considered to have completed successfully.

The /unsetenv/() function shall update the list of pointers to which
/environ/ points.

The /unsetenv/() function need not be thread-safe.

* RETURN VALUE
Upon successful completion, zero shall be returned. Otherwise, -1 shall
be returned, /errno/ set to indicate the error, and the environment
shall be unchanged.

* ERRORS
The /unsetenv/() function shall fail if:

- *EINVAL* :: The /name/ argument points to an empty string, or points
  to a string containing an *'='* character.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
Refer to the RATIONALE section in //setenv/ ( )/.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//getenv/ ( )/, //setenv/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<stdlib.h>*/,
/*<sys_types.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
