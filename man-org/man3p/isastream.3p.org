#+TITLE: Manpages - isastream.3p
#+DESCRIPTION: Linux manpage for isastream.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
isastream --- test a file descriptor (*STREAMS*)

* SYNOPSIS
#+begin_example
  #include <stropts.h>
  int isastream(int fildes);
#+end_example

* DESCRIPTION
The /isastream/() function shall test whether /fildes/, an open file
descriptor, is associated with a STREAMS-based file.

* RETURN VALUE
Upon successful completion, /isastream/() shall return 1 if /fildes/
refers to a STREAMS-based file and 0 if not. Otherwise, /isastream/()
shall return -1 and set /errno/ to indicate the error.

* ERRORS
The /isastream/() function shall fail if:

- *EBADF* :: The /fildes/ argument is not a valid open file descriptor.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
The /isastream/() function may be removed in a future version.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<stropts.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
