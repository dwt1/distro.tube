#+TITLE: Manpages - getpgid.3p
#+DESCRIPTION: Linux manpage for getpgid.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
getpgid --- get the process group ID for a process

* SYNOPSIS
#+begin_example
  #include <unistd.h>
  pid_t getpgid(pid_t pid);
#+end_example

* DESCRIPTION
The /getpgid/() function shall return the process group ID of the
process whose process ID is equal to /pid/. If /pid/ is equal to 0,
/getpgid/() shall return the process group ID of the calling process.

* RETURN VALUE
Upon successful completion, /getpgid/() shall return a process group ID.
Otherwise, it shall return (*pid_t*)-1 and set /errno/ to indicate the
error.

* ERRORS
The /getpgid/() function shall fail if:

- *EPERM* :: The process whose process ID is equal to /pid/ is not in
  the same session as the calling process, and the implementation does
  not allow access to the process group ID of that process from the
  calling process.

- *ESRCH* :: There is no process with a process ID equal to /pid/.

The /getpgid/() function may fail if:

- *EINVAL* :: The value of the /pid/ argument is invalid.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//exec/ /, //fork/ ( )/, //getpgrp/ ( )/, //getpid/ ( )/,
//getsid/ ( )/, //setpgid/ ( )/, //setsid/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<unistd.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
