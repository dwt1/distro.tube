#+TITLE: Manpages - difftime.3p
#+DESCRIPTION: Linux manpage for difftime.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
difftime --- compute the difference between two calendar time values

* SYNOPSIS
#+begin_example
  #include <time.h>
  double difftime(time_t time1, time_t time0);
#+end_example

* DESCRIPTION
The functionality described on this reference page is aligned with the
ISO C standard. Any conflict between the requirements described here and
the ISO C standard is unintentional. This volume of POSIX.1‐2017 defers
to the ISO C standard.

The /difftime/() function shall compute the difference between two
calendar times (as returned by /time/()): /time/1- /time/0.

* RETURN VALUE
The /difftime/() function shall return the difference expressed in
seconds as a type *double*.

* ERRORS
No errors are defined.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
//asctime/ ( )/, //clock/ ( )/, //ctime/ ( )/, //gmtime/ ( )/,
//localtime/ ( )/, //mktime/ ( )/, //strftime/ ( )/, //strptime/ ( )/,
//time/ ( )/, //utime/ ( )/

The Base Definitions volume of POSIX.1‐2017, /*<time.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
