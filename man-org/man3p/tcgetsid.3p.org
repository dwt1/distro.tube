#+TITLE: Manpages - tcgetsid.3p
#+DESCRIPTION: Linux manpage for tcgetsid.3p
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/man-org/header.org"
* PROLOG
This manual page is part of the POSIX Programmer's Manual. The Linux
implementation of this interface may differ (consult the corresponding
Linux manual page for details of Linux behavior), or the interface may
not be implemented on Linux.

* NAME
tcgetsid --- get the process group ID for the session leader for the
controlling terminal

* SYNOPSIS
#+begin_example
  #include <termios.h>
  pid_t tcgetsid(int fildes);
#+end_example

* DESCRIPTION
The /tcgetsid/() function shall obtain the process group ID of the
session for which the terminal specified by /fildes/ is the controlling
terminal.

* RETURN VALUE
Upon successful completion, /tcgetsid/() shall return the process group
ID of the session associated with the terminal. Otherwise, a value of -1
shall be returned and /errno/ set to indicate the error.

* ERRORS
The /tcgetsid/() function shall fail if:

- *EBADF* :: The /fildes/ argument is not a valid file descriptor.

- *ENOTTY* :: The calling process does not have a controlling terminal,
  or the file is not the controlling terminal.

/The following sections are informative./

* EXAMPLES
None.

* APPLICATION USAGE
None.

* RATIONALE
None.

* FUTURE DIRECTIONS
None.

* SEE ALSO
The Base Definitions volume of POSIX.1‐2017, /*<termios.h>*/

* COPYRIGHT
Portions of this text are reprinted and reproduced in electronic form
from IEEE Std 1003.1-2017, Standard for Information Technology --
Portable Operating System Interface (POSIX), The Open Group Base
Specifications Issue 7, 2018 Edition, Copyright (C) 2018 by the
Institute of Electrical and Electronics Engineers, Inc and The Open
Group. In the event of any discrepancy between this version and the
original IEEE and The Open Group Standard, the original IEEE and The
Open Group Standard is the referee document. The original Standard can
be obtained online at http://www.opengroup.org/unix/online.html .

Any typographical or formatting errors that appear in this page are most
likely to have been introduced during the conversion of the source files
to man page format. To report such errors, see
https://www.kernel.org/doc/man-pages/reporting_bugs.html .
