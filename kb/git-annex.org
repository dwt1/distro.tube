#+TITLE: Git Annex

* About Git-Annex
Git is not really appropriate for syncing very large files, and hosts like GitHub and GitLab have file size restrictions.  This is where git-annex comes in, since it approaches this problem through a unique use of symlinks.

* Installation
On Arch Linux, you can install git-annex with pacman:

#+begin_example
sudo pacman -S git-annex
#+end_example

Dependencies will include git, of course, as well as a few dozen Haskell libraries.  Don't worry though, the Haskell libraries are all small.  It shouldn't take more than a few seconds for them to install.

* Syncing files between two local folders

** The first folder
To get started, let's create a new git repo and initialize it:

#+begin_example
mkdir repo_name
cd repo_name
git init
#+end_example

Now, we also need to initialize the repo with git-annex, which will require us to add a name to this git-annex repo.

#+begin_example
git annex init "FileSync"
#+end_example

Be aware that until a repository (or one of its remotes) has been initialized, git-annex will refuse to operate on it, to avoid accidentally using it in a repository that was not intended to have an annex.

Now add a big file for syncing:
#+begin_example
git annex add bigfile
git commit -m "Added a big file."
#+end_example

** The second folder
Now, let's go to the second folder location which in this example will be my Nextcloud folder at ~/nc.  Let's make a new directory just for git annex, and initialize that directory with both git and git-annex.

#+begin_example
cd ~/nc
mkdir annex
cd annex
git init
git annex init "Nextcloud"
#+end_example

** Make each folder aware of the other
Go to the first folder and enter:

#+begin_example
git remote add Nextcloud ~/nc/annex
#+end_example

Go to the second folder and enter:

#+begin_example
git remote add FileSync ~/test
#+end_example

** Sync the two folders
Change into ~/test and enter:
#+begin_example
git annex sync
#+end_example

Then change into ~/nc/annex and enter the same:
#+begin_example
git annex sync
#+end_example

At this point, we have created symlinks between the two locations. To sync the actual content, enter the following command in each location:

#+begin_example
git annex sync --content
#+end_example

* Storing large files in GitHub/GitLab
Create a new GitLab repo and then clone it locally.  Then create some example files.

#+begin_example
cd repo
vim bigfile    ## add some text
vim largefile  ## add some text
#+end_example

Then, initialize this repo with git-annex:
#+begin_example
git annex init
#+end_example

Then add the files:
#+begin_example
git annex add .
git commit -m "Add a message"
#+end_example

If we run a 'git log' we can see that we've made our commit.

#+begin_example
git annex sync --content
#+end_example

Now, if we were to open a browser and view the repo on GitLab, we will see our new files were pushed, but if you open them on GitLab, you will see that the contents are not the text we added.  Instead the contents are a reference to a git-annex object, which is information needed for the symlinking.

On the local repo, the actual content of the file is still there:
#+begin_example
cat bigfile
cat largefile
#+end_example

** Syncing to a second folder
 Clone the GitLab repo in a new folder someone on your machine, or even a second machine:

#+begin_example
git clone git@gitlab.com:dwt1/some_repo
cd some_repo
ls -la
 #+end_example

 You will see the names of the files are all there, but the files are actually just symlinks.  There is no actual content to the files.

 #+begin_example
cat bigfile
  => No such file or directory
 #+end_example

 This error occurs because the symlink contains the information to get the actual files, but it cannot do this until we 'get' the files or 'sync' the content.

 For getting a single file:

 #+begin_example
git annex get bigfile
 #+end_example

For getting all of the files:

 #+begin_example
git annex sync --content
 #+end_example
