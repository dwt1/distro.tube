#+TITLE: Sed
#+DESCRIPTION: Knowledge Base - Sed
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* What is sed?
Sed is a "stream editor" for filtering and transforming text.  In short, it's an editor for modifying files automatically.  So if you want to write a script that makes changes in a file, sed is the tool that you probably want to use for that.
* s for substitution
While sed has a ton of commands, most people only learn the 's' command.  And it's understandable why that's the case.  The 's' command (substitution) is so useful.  Let me show an example:
** Replace the first occurrence of a regular expression in each line of a file:
+ sed s/find/replace/ <oldfile >newfile
+ sed s/red/green/ <.Xresources >sed-test
+ sed s/red/green/ .Xresources >sed-test (also works)
+ echo "Derek" | sed 's/Derek/DT/'
+ echo "The Emacs file manager is dired" | sed 's/red/green/'
** Replace all occurrences of a regular expression in a file:
+ sed 's/regularexpression/replacement/g' filename
+ man sed | sed 's/sed/SED/g' | less
+ man sed | sed 's/ sed /SED/g' | less
** Replace all occurrences of a string in a file, overwriting the file (i.e. in-place):
+ sed -i 's/find/replace/g' filename
+ sed -i 's/Taylor/Tyler/g' .bashrc
+ sed -i 's/Tyler/Taylor/g' .bashrc
** Replace only on lines matching the line pattern:
+ tldr sed | sed '/Replace/s/the/THE /'
** Delete lines matching the line pattern:
+ tldr sed | sed '/line_pattern/d'
** Apply multiple find-replace expressions to a file:
+ sed -e 's/find/replace/' -e 's/find/replace/' filename
+ cat /etc/shells | sed -e 's/usr/u/g' -e 's/bin/b/g'
** Replace separator `/` by any other character not used in the find or replace patterns, e.g., `#`:
+ sed 's#find#replace#' filename
+ cat /etc/shells | sed -e 's|usr|u|g' -e 's#bin#b#g'
** Print only the lines containing the search string
+ cat /etc/shells | sed -n '/usr/p'
** PRO TIPS: Delete all spaces tabs at end of every line
*** Spaces
#+begin_example
sed -i 's/ *$//' test.sh
#+end_example
*** Tabs
#+begin_example
sed -i 's/[[:space:]]*$//' test.sh
#+end_example
** Delete empty lines
+ cat test.sh | sed '/^$/d'
** Change lowercase to uppercase and vice versa:
+ sed 's/[a-z]/\U&/g' test.sh
+ sed 's/[A-Z]/\L&/g' test.sh
** Print the first 11 lines of a file:
+ sed 11q filename (head replacement)
+ awk 'NR < 12'  (NR is # of lines seen so far)
+ head -11

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
