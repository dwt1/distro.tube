#+TITLE: Those Saying "Linux Is Hard" Make Me Laugh
#+DESCRIPTION: DT Articles - Those Saying "Linux Is Hard" Make Me Laugh
#+SETUPFILE: https://distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Those Saying "Linux Is Hard" Make Me Laugh
/By Derek Taylor at June 18, 2022/

You what I find  funny.  I go to these online forums and chat rooms, and I see these people telling potential new-to-Linux users, "Don't install Linux because Linux is hard!"  And it makes me laugh because I don't know what the hell these people are talking about, because in many cases Linux is easier than Windows or Mac or any of the proprietary alternatives.

* Installation
For one thing, most Linux distributions these days can be installed in 15 minutes or less right--much quicker than Windows can be installed.  Of course, most people don't have to install Windows themselves.  They buy a Machine and Windows is already there.  Of course, you could do that with Linux.  Linux does not have as big a desktop market share as Windows, so there's not a lot of companies that sell Machines that have Linux pre-installed, but there are a few custom boutiques out there.   So even that criticism in my opinion is not a valid criticism.

* Linux will blow up!
Also, you get these scaremongers out there you know they're saying if you are going to install Linux--DON'T!  Because your system one day is just going to blow up on you.  They tell potential new Linux users that Linux breaks all the time right.  Again, it makes me laugh because...how many millions of Windows users out there have actually destroyed their Machines?  Millions.  Many millions of Windows users have "borked" their Machines by doing something dumb, or in some cases not doing anything at all.  They just updated Windows and the updates sometimes actually permanently break people's Windows systems.  So again, I don't understand where all this
craziness comes from as far as the "Linux is hard" crowd.

* The problem is most people aren't beginners.
I think a lot of it has to do with a lot of people come to Linux with bad expectations, and what I mean about bad expectations is that these people are saying Linux is hard for =beginners=!  The problem is there's very, very few actual beginners out there.  Think about it.  There's very few people that are trying Linux for the first time that have never used a computer before.  Most people have used a computer their entire lives.  They've used Windows or Mac or Chrome OS.  These days, Android and iOS are very popular operating systems.

So these =beginners= have used all of these proprietary operating systems, in some cases, for their entire lives.   They're not a computer beginner!  So they go into this with expectations that Linux is going to somehow work like those other proprietary operating systems they've used, and when Linux doesn't meet those expectations, they cry foul and want to blame Linux.  You see this with a lot of new users when they first switch to Linux, and they start using their machine they just installed Linux Mint or Ubuntu or whatever distribution.

And then they go to these forums and ask "Hey, I can't find my C: drive!"  And then the Linux forums have to tell these noobs that the file system hierarchy is completely different than your file system on Windows.  Then the beginner starts thinking "What?! Linux is different?! Linux sucks!"

Then the new-to-Linux user gets frustrated when they don't have the same applications on Linux that they had on Windows.  They might say something like:

"Where's Windows media player?  You know, I went to the Arch forums and those guys said Windows media player doesn't exist on Linux.  Can you believe that?!  Linux sucks!"

They blame all of this on Linux when really they should blame themselves for having these crazy expectations that Linux is going to be the same as Windows.  If Linux was the same as Windows, why would anybody use Linux?  Think about it.  If all operating systems were the same, then we'd all use the same one operating system, and there would be no need to to try anything else.  The reason we have many different operating systems is because they're all different.  They don't all do the same things.

* Linux has an odd learning curve.
I think part of the problem with this "Linux is hard" argument is that Linux has a really odd learning curve, because for the most basic computer tasks, Linux is super easy.  For most basic task, Linux is far easier to use than Windows in most cases.  So for many people Linux is not hard at all.  Linux is drop dead easy.  Somebody can just take a Linux computer and immediately know how to do everything, as far moving around their desktop and opening programs and getting stuff done.

The problem is when you need more specialized tools for more specialized tasks.  That's where Linux can fall apart a little bit, because most of those specialized tools that people need are proprietary software, and if the proprietor of that software only makes a Windows version, then the new-to-Linux user will blame Linux for that.  But that's not Linux's fault!

If you want to blame somebody for the fact that your one piece of proprietary software you need is not available on Linux, then blame the proprietor of that software. Linux can't make that software work on Linux.  The proprietor of that software that decided to write a Windows version only--THEY could write a Linux version if they wanted to.  Go complain to that company because when you say that "Linux sucks" because of that reason, you're really giving Linux a lot more shade than it deserves, because that's not a Linux problem.  That's a "proprietary software versus free and open source software" problem.

Linux has a weird learning curve because once you get past the basics and into more advanced stuff.  Once you become an advanced Linux user and you learn a lot of command line tools and Bash scripting, then you can kind of make your own tools.  You can put a lot of these pieces of knowledge that you've gained together, and with the use of scripting and a lot of these command line utilities you can actually make something work for your specialized needs.  You can build your own tools at that point!  Then, Linux is far better than Windows because now you've unlocked so much stuff that you can do on Linux that you can't do on Windows.  At the point, you start to appreciate Linux.  Now, you would find it frustrating and limiting to go back to Windows or Mac.

* Linux market share
Until Linux has a substantial market share until we get a much bigger market share of desktop computer users, we are always going to face this problem of these people have already learned how to use a computer on Windows or Mac.  When Linux adoption finally reaches a point where most people start learning on Linux rather than Windows, then we will see a change in attitude.  People that are learning computers for the very first time will learn on Linux, and then when they try to use those proprietary operating systems like Windows or Mac, they are going to get frustrated at Windows and Mac for not being like Linux!

I know a lot of people are going to disagree with me and say "Linux is never going to gain that kind of market share."  Hey, a guy can dream!  It's not entirely out of the realm of possibility that Microsoft won't one day transition Windows away from their NT kernel over to using a Linux kernel.  Then, Windows will actually be just another Linux distribution; and, of course, at that point we're going to be living in this strange Bizarro world where everything is turned on its head--everything's opposite!  Everybody will make fun of the "Arch noobs" and constantly tell people: "By the way, I use Windows-Linux!"
